# Copyright 2015-2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np


def diff(f, x, h=1e-6, order=2, form='central'):
    """
    Computes the numerical derivative of a scalar function f at point x using finite differences

    Parameters:
    -----------
    f : method (f(x))
        scalar function

    x : float
        point at which the derivative is computed

    h : float
        step used for finite differences formulas

    order : integer
            order of the formula (only order 2 implemented yet)

    form : string
           'central' for centered differences formula
           'forward' for forward differences formula
           'backward' for backward differences formula
    """

    if order == 2:
        if form == 'central':
            return (f(x + h) - f(x - h)) / 2. / h
        elif form == 'forward':
            return (-3.*f(x) + 4.*f(x + h) - f(x + 2.*h)) / 2. / h
        elif form == 'backward':
            return (3.*f(x) - 4.*f(x - h) + f(x - 2.*h)) / 2. / h
        else:
            raise ValueError("type must be one of the following: 'central', "
                             "'forward', or 'backward")
    else:
        raise ValueError("Order {} not implemented".format(order))


def main():
    x = np.linspace(0, 2*np.pi, 1001)
    y = -np.sin(x)
    y_center = []
    y_back = []
    y_for = []
    for xi in x:
        y_center.append(diff(np.cos, xi, form='central'))
        y_back.append(diff(np.cos, xi, form='backward'))
        y_for.append(diff(np.cos, xi, form='forward'))

    print(max(np.abs((np.array(y_center) - y))))
    print(max(np.abs((np.array(y_back) - y))))
    print(max(np.abs((np.array(y_for) - y))))

if __name__ == "__main__":
    main()
