# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.surrogate_modelling.surrogate_object import SurrogateObject
import numpy as np
import pandas as pd
from itertools import product


class LinearRegression(SurrogateObject):

    def __init__(self, sampledPoints, sampleValues, basis_kind, paramValues,
                 rescaling="standarizing", path=None):

        super(LinearRegression, self).__init__()

        # If given a path, try to load from there, otherwise reconstruct the
        # object
        if path:
            self._load(path)
            return

        initBasis = {"Polynomials": self._initializePolynomials,
                     "Cubic spline": self._initializeRBF,
                     "Gaussian": self._initializeRBF,
                     "tanh": self._initializeTanH}
        rbfs = {"Cubic spline": self._cubic_spline,
                "Gaussian": self._gaussian}

        # Try to convert the samplePoints entity into an array,
        # also, make sure it's composed by doubles
        sampledPoints = np.asarray(sampledPoints, dtype=np.float64)

        dim = len(sampledPoints.shape)
        if dim == 1:
            sampledPoints = sampledPoints.reshape((sampledPoints.shape[0],
                                                   1))
        elif dim != 2:
            raise ValueError('Sampled points must be given in a 2D ' +
                             'array')

        self.nSampledPoints, self.nParameters = sampledPoints.shape

        # Working with ndarrays is faster than directly operating
        # the DataFrame, so we convert data into that
        self.columns = None
        if isinstance(sampleValues, pd.Series):
            self.columns = sampleValues.index
            sampleValues = sampleValues.values
        elif isinstance(sampleValues, pd.DataFrame):
            self.columns = sampleValues.columns
            sampleValues = sampleValues.values
        else:
            sampleValues = np.asarray(sampleValues, dtype=np.float64)

        dim = len(sampleValues.shape)
        if dim == 1:
            sampleValues = sampleValues.reshape((sampleValues.shape[0], 1))
        elif dim != 2:
            raise ValueError('Sampled values must be given in a 2D ' +
                             'array')

        if sampleValues.shape[0] != self.nSampledPoints:
            raise ValueError("The number of sampled points does not " +
                             "match the number of sample values")

        self.nFunctions = sampleValues.shape[1]

        if rescaling == "standarizing":
            self.pointsMean, self.pointsStdv = self.mean_n_std(sampledPoints)
            self.valuesMean, self.valuesStdv = self.mean_n_std(sampleValues)
        elif rescaling == "normalizing":
            self.pointsMean = (np.max(sampledPoints, axis=0) -
                               np.min(sampledPoints, axis=0)) / 2.
            self.pointsStdv = self.pointsMean
            self.valuesMean = (np.max(sampleValues, axis=0) -
                               np.min(sampleValues, axis=0)) / 2.
            self.valuesStdv = self.valuesMean
        else:
            raise ValueError("Value of <rescaling> unrecognized.")

        # Normalize the sampled points array
        self.normalSampledPoints = self.normalize(sampledPoints,
                                                  self.pointsMean,
                                                  self.pointsStdv)
        # Normalize the values array
        self.normalValues = self.normalize(sampleValues,
                                           self.valuesMean,
                                           self.valuesStdv)

        # Initialize basis
        if basis_kind in ["Polynomials"]:
            parameters = paramValues
        elif basis_kind in ["Cubic spline", "Gaussian"]:
            self.rbf = rbfs[basis_kind]
            self._setCenters(paramValues[0])
            parameters = self.rbfCenters
            self.param = paramValues[1]
        elif basis_kind in ["tanh"]:
            self._setCenters(paramValues[0])
            parameters = self.rbfCenters
            self.param = paramValues[1]

        initBasis[basis_kind](parameters)
        Phi = []
        for base in self.basis:
            Phi.append(base(self.normalSampledPoints))
        self.Phi = np.array(Phi).T
        self.A = self.Phi.T.dot(self.Phi)
        self.B = self.Phi.T.dot(self.normalValues)
        self.C = np.linalg.solve(self.A, self.B)

    def get_prediction(self, x, provideRMSE=False):

        xi = self.normalize(x, self.pointsMean, self.pointsStdv)
        phi = []
        for base in self.basis:
            phi.append(base(xi))
        phi = np.array(phi).T
        return self.denormalize(phi.dot(self.C), self.valuesMean,
                                self.valuesStdv)

    def _initializePolynomials(self, n):

        listOfIndices = [range(n + 1) for _ in range(self.nParameters)]

        listOfExponents = []
        for x in product(*listOfIndices):
            if np.sum(x) <= n:
                listOfExponents.append(x)

        self.basis = []
        for X in listOfExponents:
            self.basis.append(lambda Y, X=X: [np.prod([y ** x for x, y in
                                                       zip(X, yrow)])
                                              for yrow in Y])

    def _initializeRBF(self, loc):

        self.basis = []
        for l in loc:
            self.basis.append(lambda X, l=l: [self.rbf(x[0], l[0])
                                              for x in X])

    def _initializeTanH(self, loc):

        if self.nParameters != 1:
            raise ValueError("This basis_kind cannot be used.")

        self.basis = []
        for l in loc:
            self.basis.append(lambda X, l=l: [np.tanh(self.param * (x[0] -
                                                                    l[0]))
                                              for x in X])

    def _setCenters(self, loc):
        """
        Places the centers of the RBFs

        Parameters
        ----------
        loc : None, integer, or iterable
              locations of the centers of the RBFs. If it is None a RBF is
              placed at every training point. If it is an integer a
              full-factorial design is used to place them.

        Raises
        ------
        TypeError
            * If `loc` is not None, integer, or a NumPy array.

        ValueError
            * If `loc` is an integer larger than the number of samples.
            * If `loc` is a NumPy array of points whose dimension is lower than
              the dimension of the sampled points.
        """
        if loc is not None and not isinstance(loc, (int, np.ndarray)):
            raise TypeError("Parameter <loc> has an incorrect type.")
        elif isinstance(loc, int) and loc > self.nSampledPoints:
            raise ValueError("Parameter <loc> cannot be larger than the " +
                             "number of training samples.")
        elif isinstance(loc, np.ndarray) and loc.shape[1] != self.nParameters:
            raise ValueError("The dimension of the points provided in the " +
                             "parameter <loc> must be equal to the dimension" +
                             " of the sampled points.")

        if loc is None:
            # Place a RBF at each sample
            self.rbfCenters = self.normalSampledPoints
        elif isinstance(loc, int):
            # Create a regular mesh
            positions = np.zeros((loc, self.nParameters))
            for d in range(self.nParameters):
                x0 = min(self.normalSampledPoints[:, d])
                x1 = max(self.normalSampledPoints[:, d])
                positions[:, d] = np.linspace(x0, x1, loc)
            l = np.zeros((loc ** self.nParameters, self.nParameters))
            ii = 0
            for t in product(range(loc), repeat=self.nParameters):
                for d in range(self.nParameters):
                    l[ii, d] = positions[t[d], d]
                ii += 1
            self.rbfCenters = l
        else:
            self.rbfCenters = self.normalize(loc, self.pointsMean,
                                             self.pointsStdv)
        self.nCenters = self.rbfCenters.shape[0]

    def _evaluate_distance(self, x, y):
        """
        L-2, Euclidean distance

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        This method is private.
        """
        return np.linalg.norm(x - y)

    def _cubic_spline(self, x, y):
        """
        Cubic spline function. f(x, y) = |x - y| ** 3

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return self.param * d ** 3

    def _gaussian(self, x, y):
        """
        Gaussian function.
        f(x, y) = \exp{-\lambda * |x - y| ** 2}

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return np.exp(- self.param * d ** 2)
