# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from itertools import product
from mathlas.object import MathlasObject


class SurrogateObject(MathlasObject):
    """A base-class object for surrogate modelling.
    Derives from MathlasObject and implements array normalization methods."""
    def __init__(self):
        super(SurrogateObject, self).__init__()

    @staticmethod
    def mean_n_std(array):
        """
        Mean and standard deviation of a matrix.

        Return the mean and standard deviation of the elements in the given
        array along thew first axis.

        Parameters
        ----------
        array : NumPy array-like
                Array whose mean and standard deviation are to be computed.

        Returns
        -------
        out : tuple of NumPy arrays
              A tuple with the mean and the standard deviation over the rows of
              array.

        Examples
        --------
        >>> A = np.array([[1, 2], [2, 3], [3, 4]])
        >>> mean_n_std(A)
        (array([ 2.,  3.]), array([ 1.,  1.]))

        """
        mean = array.mean(axis=0)
        std = array.std(axis=0, ddof=1)

        return mean, std

    @staticmethod
    def normalize(array, mean, std):
        """
        Normalize the given array.

        Normalize a NumPy array-like based on the given mean and standard
        deviations. The formula for the normalization is
        ``normalizedArray = (array - mean) / std``.

        Parameters
        ----------
        array : NumPy array-like
                Array with the points to normalize.
        mean : NumPy array-like
               Array with the mean to use for normalization.
        std : NumPy array-like
              Array with the standard deviations to use for normalization.

        Returns
        -------
        out : NumPy array-like
              Normalized array

        Examples
        --------
        >>> A = np.array([[1, 2], [2, 3], [3, 4]])
        >>> mean, std = mean_n_std(A)
        >>> normalize(A, mean, std)
        array([[-1., -1.],
               [ 0.,  0.],
               [ 1.,  1.]])
        """
        return (array - mean) / std

    @staticmethod
    def denormalize(normal_array, mean, std):
        """
        Denormalize a NumPy array-like

        Denormalize a NumPy array-like based on the given mean and standard
        deviations. The formula for the denormalization is
        ``array = normal_array * std + mean``.

        Parameters
        ----------
        normal_array : NumPy array-like
                       Array with the normalized values to denormalize.
        mean : NumPy array-like
               Array with the mean to use for denormalization.
        std : NumPy array-like
              Array with the standard deviations to use for denormalization.

        Returns
        -------
        out : NumPy array-like
              Denormalized array

        Examples
        --------
        >>> A = np.array([[1, 2], [2, 3], [3, 4]])
        >>> mean, std = mean_n_std(A)
        >>> B = normalize(A, mean, std)
        >>> A-Kriging.denormalize(B, mean, std)
        array([[ 0.,  0.],
               [ 0.,  0.],
               [ 0.,  0.]])
        """
        return np.multiply(normal_array, std) + mean

    def get_prediction(self, x, provide_std):
        raise NotImplementedError("Method <get_prediction> must be " +
                                  "reimplemented by derived classes.")

    def get_pdf(self):
        raise NotImplementedError("Method <get_pdf> must be " +
                                  "reimplemented by derived classes.")

    def get_cdf_quantile(self):
        raise NotImplementedError("Method <get_cdf_quantile> must be " +
                                  "reimplemented by derived classes.")

    def get_cdf_point(self):
        raise NotImplementedError("Method <get_cdf_point> must be " +
                                  "reimplemented by derived classes.")

    def extract_line(self, p0, p1, s_range=(0.05, 0.05),
                     n_points=101, provide_std=False):

        xi = np.linspace(-s_range[0], 1. + s_range[1], n_points)
        m = (p1 - p0)
        n = p0
        x = np.array([m * s + n for s in xi])
        if provide_std:
            y, std = self.get_prediction(x, provide_std)
        else:
            y = self.get_prediction(x, provide_std)
            std = None
        return xi, np.array([[0.], [1.]]), x, y, std

    def extract_grid(self, p0, p1, p2, p_range=(0.05, 0.05), q_range=(0.05, 0.05),
                     n_points=101, provide_std=False):

        p_array = np.linspace(-p_range[0], 1. + p_range[1], n_points)
        q_array = np.linspace(-q_range[0], 1. + q_range[1], n_points)

        mp = (p1 - p0)
        pval = mp.dot(p2 - p0) / np.linalg.norm(mp)
        mq = (p2 - p0) - pval * mp
        n = p0
        x = np.array([n + mp * p + mq * q for p, q in product(p_array, q_array)])
        if provide_std:
            y, std = self.get_prediction(x, provide_std)
            std = np.reshape(std, (n_points, n_points, self.n_functions))
        else:
            y = self.get_prediction(x, provide_std)
            std = None

        s = np.array([(p, q) for p, q in product(p_array, q_array)])
        s = np.reshape(s, (n_points, n_points, 2))
        x = np.reshape(x, (n_points, n_points, self.n_parameters))
        y = np.reshape(y, (n_points, n_points, self.n_functions))
        return s, np.array([[0., 0.], [1., 0.], [pval, 1.]]), x, y, std
