# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.surrogate_modelling.surrogate_object import SurrogateObject
import numpy as np
from scipy.stats import norm
import pandas as pd
from itertools import combinations, product


class RBF(SurrogateObject):

    def __init__(self, sampledPoints, sampleValues, rbf_kind, param=None,
                 centers=None, noiseFactor=None, path=None):
        """
        Initialise a RBF estimator object.

        Parameters
        ----------
        sampledPoints : NumPy array or Pandas DataFrame, optional
                        Co-ordinates of the sampled points. The array must
                        contain one row with co-ordinates per sampled point.
                        The number of rows in `sampledPoints` must match the
                        number of rows in `sampleValues`.

                        If `sampledPoints` is None (default), `sample` must be
                        provided.

                        The data must be provided as a 1D or 2D ndarray.

        sampleValues : NumPy array or Pandas DataFrame, optional
                       Value of the sampled function at the sampled points.
                       The number of rows in `sampleValues` must match the
                       number of rows in `sampledPoints`.

                       If None (default), `sample` must be provided.

                       The data must be provided as a 1D or 2D ndarray.

        rbf_kind : string
                   Type of radial basis function used.

                    The allowed values are "Linear spline", "Cubic spline",
                    "Thin plate spline", "Quadric spline", "Gaussian",
                    "Multiquadric", "Inverse multiquadric", and
                    "Inverse quadric"

        param : float, optional
                value of the hyper-parameter of the chosen radial basis
                function.

        centers : None, integer, or iterable
                  locations of the centers of the RBFs. If it is None a RBF is
                  placed at every training point. If it is an integer a
                  full-factorial design is used to place them.

        noiseFactor : None, int, or NumPy array
                      Non-dimensional value of the noise. It is used to modify
                      the calculation of the prediction coefficients and the
                      estimated uncertainty. Default value is None; this value
                      means that no noise will be accounted. It can also be an
                      integer (the same level of noise for all functions) or a
                      NumPy array (whose length must be equal to the number of
                      functions).

        path : path to a pickled cache object, optional
              Generate the Kriging object from a pickled cache file
              instead of from the given params. The pickled file
              must have been generated with the ``cache()`` method
              of the object in a previous run.

              If given, the rest of the parameters will be ignored.

        Raises
        ------
        TypeError
            * If `param` is not a number, integer, or float.
            * If `noiseFactor` is not None, float, or a NumPy array.

        ValueError
            * If not enough info about sampled points and their values is
              given.
            * If the dimensions of the various parameters are not coherent.
            * If given an unsupported value for `regressionType` or
              `covarianceTypes`.
            * If `noiseFactor` is a NumPy array but does not have as many
              elements as functions are being modelled.
        """

        super(RBF, self).__init__()

        # If given a path, try to load from there, otherwise reconstruct the
        # object
        if path:
            self._load(path)
        else:
            rbfs = {"Linear spline": self._linear_spline,
                    "Cubic spline": self._cubic_spline,
                    "Thin plate spline": self._thin_plate_spline,
                    "Quadric spline": self._quadric_poliharmonic_spline,
                    "Gaussian": self._gaussian,
                    "Multiquadric": self._multiquadric,
                    "Inverse multiquadric": self._multiquadric,
                    "Inverse quadric": self._inverse_quadric}

            # Try to convert the samplePoints entity into an array,
            # also, make sure it's composed by doubles
            sampledPoints = np.asarray(sampledPoints, dtype=np.float64)

            dim = len(sampledPoints.shape)
            if dim == 1:
                sampledPoints = sampledPoints.reshape((sampledPoints.shape[0],
                                                       1))
            elif dim != 2:
                raise ValueError('Sampled points must be given in a 2D ' +
                                 'array')

            self.nSampledPoints, self.nParameters = sampledPoints.shape

            # Working with ndarrays is faster than directly operating
            # the DataFrame, so we convert data into that
            self.columns = None
            if isinstance(sampleValues, pd.Series):
                self.columns = sampleValues.index
                sampleValues = sampleValues.values
            elif isinstance(sampleValues, pd.DataFrame):
                self.columns = sampleValues.columns
                sampleValues = sampleValues.values
            else:
                sampleValues = np.asarray(sampleValues, dtype=np.float64)

            dim = len(sampleValues.shape)
            if dim == 1:
                sampleValues = sampleValues.reshape((sampleValues.shape[0], 1))
            elif dim != 2:
                raise ValueError('Sampled values must be given in a 2D ' +
                                 'array')

            if sampleValues.shape[0] != self.nSampledPoints:
                raise ValueError("The number of sampled points does not " +
                                 "match the number of sample values")

            self.nFunctions = sampleValues.shape[1]

            # Normalize the sampled points array
            self.pointsMean, self.pointsStdv = self.mean_n_std(sampledPoints)
            self.normalSampledPoints = self.normalize(sampledPoints,
                                                      self.pointsMean,
                                                      self.pointsStdv)

            # Normalize the values array
            self.valuesMean, self.valuesStdv = self.mean_n_std(sampleValues)
            self.normalValues = self.normalize(sampleValues,
                                               self.valuesMean,
                                               self.valuesStdv)

            # Create an array with the centers of the RBFs
            self.centers = centers
            self._setCenters(centers)

            # Verify that the noise factor is correct
            if not (noiseFactor is None or
                    isinstance(noiseFactor, (float, np.ndarray))):
                raise TypeError("Parameter <noiseFactor> is not None nor " +
                                "float nor a NumPy array.")
            elif (isinstance(noiseFactor, np.ndarray) and
                  len(noiseFactor) != self.nFunctions):
                raise ValueError("Parameter <noiseFactor> must have as many " +
                                 "elements as functions are being treated.")
            if centers is not None and noiseFactor is not None:
                print("<NoiseFactor> has been provided but its value will " +
                      "not be used.")
            else:
                self.noiseFactor = noiseFactor

            # Store the RBF kind, as asked by the user
            # Also, if a parameter was given, store it for later use
            self.rbf_kind = rbf_kind
            self.rbf = rbfs[rbf_kind]
            if rbf_kind in ["Linear spline", "Cubic spline",
                            "Thin plate spline", "Quadric spline"]:
                self.param = None
            else:
                if param is None:
                    # Se puede meter algo para que optimice el parámetro
                    raise ValueError("param cannot be None for given RBF type")
                elif not isinstance(param, (int, float)):
                    raise TypeError("param must be numeric for given RBF type")
                else:
                    self.param = param

            # Calculate the covariance matrix and, from it, the
            # linear coefficients
            self._buildCovarianceMatrix()
            self.coeffs = np.zeros((self.nCenters, self.nFunctions))
            if self.centers is None:
                for i in range(self.normalValues.shape[1]):
                    y = self.normalValues[:, i]
                    self.coeffs[:, i] = np.linalg.solve(self.C, y)
            else:
                for i in range(self.normalValues.shape[1]):
                    y = self.normalValues[:, i]
                    A = self.C.dot(self.C.T)
                    B = self.C.dot(y)
                    self.coeffs[:, i] = np.linalg.solve(A, B)

            predictedValues = self.C.T.dot(self.coeffs)
            V = predictedValues - self.normalValues
            self.sg2 = V.T.dot(V) / self.nSampledPoints

    def _setCenters(self, loc):
        """
        Places the centers of the RBFs

        Parameters
        ----------
        loc : None, integer, or iterable
              locations of the centers of the RBFs. If it is None a RBF is
              placed at every training point. If it is an integer a
              full-factorial design is used to place them.

        Raises
        ------
        TypeError
            * If `loc` is not None, integer, or a NumPy array.

        ValueError
            * If `loc` is an integer larger than the number of samples.
            * If `loc` is a NumPy array of points whose dimension is lower than
              the dimension of the sampled points.
        """
        if loc is not None and not isinstance(loc, (int, np.ndarray)):
            raise TypeError("Parameter <loc> has an incorrect type.")
        elif isinstance(loc, int) and loc > self.nSampledPoints:
            raise ValueError("Parameter <loc> cannot be larger than the " +
                             "number of training smples.")
        elif isinstance(loc, np.ndarray) and loc.shape[1] != self.nParameters:
            raise ValueError("The dimension of the points provided in the " +
                             "parameter <loc> must be equal to the dimension" +
                             " of the sampled points.")

        if loc is None:
            # Place a RBF at each sample
            self.rbfCenters = self.normalSampledPoints
        elif isinstance(loc, int):
            # Create a regular mesh
            positions = np.zeros((loc, self.nParameters))
            for d in range(self.nParameters):
                x0 = min(self.normalSampledPoints[:, d])
                x1 = max(self.normalSampledPoints[:, d])
                positions[:, d] = np.linspace(x0, x1, loc)
            l = np.zeros((loc ** self.nParameters, self.nParameters))
            ii = 0
            for t in product(range(loc), repeat=self.nParameters):
                for d in range(self.nParameters):
                    l[ii, d] = positions[t[d], d]
                ii += 1
            self.rbfCenters = l
        else:
            self.rbfCenters = self.normalize(loc, self.pointsMean,
                                             self.pointsStdv)
        self.nCenters = self.rbfCenters.shape[0]

    def _buildCovarianceMatrix(self):
        """
        Compute and store the covariance matrix based on the stored parameters.

        This is a private method.
        """
        if self.rbf_kind in ["Linear spline", "Cubic spline",
                             "Thin plate spline", "Quadric spline"]:
            self.C = np.zeros((self.nCenters, self.nSampledPoints))
        elif self.rbf_kind in ["Gaussian", "Multiquadric", "Inverse quadric",
                               "Inverse multiquadric"]:
            self.C = np.ones((self.nCenters, self.nSampledPoints))
        if self.centers is None:
            for indx1, indx2 in combinations(range(self.nSampledPoints), 2):
                x1 = self.normalSampledPoints[indx1, :]
                x2 = self.normalSampledPoints[indx2, :]
                self.C[indx1, indx2] = self.C[indx2, indx1] = self.rbf(x1, x2)
                if self.noiseFactor is not None:
                    self.C += self.noiseFactor * np.eye(self.nSampledPoints)
        else:
            for r, xt in enumerate(self.rbfCenters):
                for p, x in enumerate(self.normalSampledPoints):
                    self.C[r, p] = self.rbf(x, xt)

    def get_prediction(self, predictionPoints, provideRMSE=False):
        """
        Get the constructed estimator's prediction at the given points.

        Calculates the best linear unbiased prediction (BLUP) for the given
        point. If `provideRMSE` is `True` the method also provides the
        root-mean-square error at those points.

        Parameters
        ----------
        predictionPoints : NumPy array-like or Pandas DataFrame
                           Positions of the points where the estimation is
                           requested.
                           If predictionPoints is a DataFrame or the
                           `sampleValues` parameter used to construct the model
                           was one, the returned BLUP will also be a DataFrame
                           whose indices will match those of predictionPoints.

                           predictionPoints must either be a 1D or 2D array.
                           If it is a 1D array, the parameter is assumed to
                           contain the coordinates of a single point.

        provideRMSE : boolean, optional
                      Must the method return the uncertainty? Default is False.

        Returns
        -------
        out : tuple or NumPy array-like
        If `provideRMSE` is `False`:
            A NumPy array or Pandas DataFrame with the BLUP.
        If `provideRMSE` is `True`:
            A tuple with the BLUP and a NumPy array with the RMSE.

        Raises
        ------
        ValueError
            If the number of columns in `points` does not match the number of
            parameters used to create the model.
        """
        indices = None
        if isinstance(predictionPoints, pd.Series):
            predictionPoints = predictionPoints.values
        elif isinstance(predictionPoints, pd.DataFrame):
            indices = predictionPoints.index
            predictionPoints = predictionPoints.values
        else:
            predictionPoints = np.asarray(predictionPoints, dtype=np.float64)

        dim = len(predictionPoints.shape)
        if dim == 1:
            predictionPoints = \
                       predictionPoints.reshape((1, predictionPoints.shape[0]))
        elif dim != 2:
            raise ValueError('Prediction points must be given in a 2D ' +
                             'array')

        if predictionPoints.shape[1] != self.normalSampledPoints.shape[1]:
            raise ValueError("Given points don't have the same dimension as " +
                             "the sampled points given to construct the model")

        normalPredictionPoints = self.normalize(predictionPoints,
                                                self.pointsMean,
                                                self.pointsStdv)
        N = normalPredictionPoints.shape[0]
#         Y = np.zeros((N, self.nFunctions))

        if self.rbf_kind in ["Linear spline", "Cubic spline",
                             "Thin plate spline", "Quadric spline"]:
            phi = np.zeros((self.nCenters, N))
        elif self.rbf_kind in ["Gaussian", "Multiquadric", "Inverse quadric",
                               "Inverse multiquadric"]:
            phi = np.ones((self.nCenters, N))
        for r, xt in enumerate(self.rbfCenters):
            for p, x in enumerate(normalPredictionPoints):
                phi[r, p] = self.rbf(x, xt)

        Y = phi.T.dot(self.coeffs)

        prediction = self.denormalize(Y, self.valuesMean, self.valuesStdv)
        if self.columns is not None or indices is not None:
            prediction = pd.DataFrame(prediction,
                                      index=indices,
                                      columns=self.columns)

        if provideRMSE:
            if self.centers is None:
                # This is a constant term that evaluates self.rbf(x, x)
                # in each of the points.
                # Since the rbf's depend on the distance between the two given
                # points (which is 0), it is just evaluated once here.
                RMSE = (self.rbf(0., 0.) -
                        np.diag(phi.T.dot(np.linalg.solve(self.C, phi))) +
                        1.e-16)
                RMSE = np.sqrt(RMSE)

                RMSE = np.repeat(RMSE.reshape(N, 1), self.nFunctions, axis=1)

                for fId in range(self.nFunctions):
                    RMSE[:, fId] *= self.valuesStdv[fId]

                if self.columns is not None or indices is not None:
                    RMSE = pd.DataFrame(RMSE, index=indices,
                                        columns=self.columns)
            else:
                RMSE = self.valuesStdv * self.sg2 * np.ones((N,
                                                             self.nFunctions))
            return prediction, RMSE

        return prediction

    def _evaluate_distance(self, x, y):
        """
        L-2, Euclidean distance

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        This method is private.
        """
        return np.linalg.norm(x - y)

    def _linear_spline(self, x, y):
        """
        Linear spline function. f(x, y) = |x - y|

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return d

    def _cubic_spline(self, x, y):
        """
        Cubic spline function. f(x, y) = |x - y| ** 3

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return d ** 3

    def _thin_plate_spline(self, x, y):
        """
        Thin plate spline function. f(x, y) = \log(|x - y|) * |x - y| ** 2

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y) + 1.e-16
        return np.log(d) * (d ** 2)

    def _quadric_poliharmonic_spline(self, x, y):
        """
        Quadric spline function. f(x, y) = \log(|x - y|) * |x - y| ** 4

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y) + 1.e-16
        return np.log(d) * (d ** 4)

    def _poliharmonic_spline(self, x, y):
        """
        General poliharmonic function.
        f(x, y) = \log(|x - y|) ** {(n + 1) (mod 2)} * |x - y| ** n

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y) + 1.e-16
        if self.param % 2 == 2:
            return np.log(d) * (d ** self.param)
        else:
            return d ** self.param

    def _gaussian(self, x, y):
        """
        Gaussian function.
        f(x, y) = \exp{-\lambda * |x - y| ** 2}

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return np.exp(- self.param * d ** 2)

    def _multiquadric(self, x, y):
        """
        Multiquadric function.
        f(x, y) = \sqrt{1 + (\lambda * |x - y|) ** 2}

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return np.sqrt(1. + (self.param * d) ** 2)

    def _inverse_multiquadric(self, x, y):
        """
        Inverse multiquadric function.
        f(x, y) = \frac{1}{\sqrt{1 + (\lambda * |x - y|) ** 2}}

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return 1. / np.sqrt(1. + (self.param * d) ** 2)

    def _inverse_quadric(self, x, y):
        """
        Inverse quadric function.
        f(x, y) = \frac{1}{1 + (\lambda * |x - y|) ** 2}

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return 1. / (1. + (self.param * d) ** 2)

    def get_pdf(self, X, Y, productOfVariables=False):
        """
        Calculates the probability of getting a response Y if X has happened,
        i.e., p(Y|X).

        Parameters
        ----------
        X : NumPy array.
            Non-normalized values of the feats. `M1xK` size.

        Y : NumPy array.
            Non-normalized values of the responses whose probability must be
            calculated. `M2x1`

        productOfVariables : Boolean.
                             If True, M1 must be equal to M2 and the
                             probability, p(Y[j, d]|X[j,:]) is calculated for
                             each row; thus, the result is an array of size
                             `Mx1` where `M=M1=M2`. If False For each value of
                             the response and each point in the features space
                             a probability is calculated. In this case an array
                             of size `M1xM2` is obtained; it has the values
                             p(Y[j,d]|[X[k, :]).

        Returns
        -------
        out : Numpy array of floats.
              2D array pdf[j,d] = p(Y[j,d]|[X[j, :]) or
              3D array pdf[j,k,d] = p(Y[j,d]|[X[k, :])
              It may be a 1D or 2D array (see `productOfVariables` above).
        """

        BLUP, RMSE = self.get_prediction(X, provideRMSE=True)

        if productOfVariables:
            f = (2. * np.pi * RMSE)
            pdf = []
            for y in Y:
                e = -0.5 * ((BLUP - y) / RMSE) ** 2
                pdf.append(np.exp(e) / f)
        else:
            f = np.sqrt(2. * np.pi) * (RMSE + 1e-300)
            e = -0.5 * ((BLUP - Y) / RMSE) ** 2
            pdf = np.exp(e) / f

        return np.asarray(pdf)

    def get_cdf_quantile(self, X, Y):
        """
        Evaluates the quantile for a given response.

        Parameters
        ----------
        X : float or NumPy array.
            Non-normalized values of the feats.

        Y : float or NumPy array.
            Non-normalized values of the responses.

        Returns
        -------
        Q : float or Numpy array.
            Quantiles.
        """
        # Evaluate predictions
        BLUP, RMSE = self.get_prediction(X, provideRMSE=True)
        # Convert Y if required
        if ((isinstance(Y, (int, float)) and BLUP.shape[1] == 1) or
                (Y.shape[0] == 0 and BLUP.shape[1] == Y.shape[1])):
            Y = [Y] * len(X)
            Y = np.array(Y)
        # Calculate quantiles
        Q = []
        for y, mu, stdev in zip(Y, BLUP, RMSE):
            Q.append(norm.cdf(y, loc=mu, scale=stdev))
        return np.asarray(Q)

    def get_cdf_point(self, X, Q):
        """
        Evaluates the value of the response for a given quantile.

        Parameters
        ----------
        X : float or NumPy array.
            Non-normalized values of the feats.

        Q : float or Numpy array.
            Quantiles.

        Returns
        -------
        Y : float or NumPy array.
            Non-normalized values of the responses.
        """
        # Evaluate predictions
        BLUP, RMSE = self.get_prediction(X, provideRMSE=True)
        # Convert Y if required
        if ((isinstance(Q, (int, float)) and BLUP.shape[1] == 1) or
                (Q.shape[0] == 0 and BLUP.shape[1] == Q.shape[1])):
            Q = [Q] * len(X)
            Q = np.array(Q)
        # Calculate points where quantiles are reached
        Y = []
        for q, mu, stdev in zip(Q, BLUP, RMSE):
            Y.append(norm.ppf(q, loc=mu, scale=stdev))
        return np.asarray(Y)
