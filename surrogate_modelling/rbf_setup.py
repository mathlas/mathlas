# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import pandas as pd
from itertools import product, combinations
from mathlas.surrogate_modelling.surrogate_object import SurrogateObject


class RBFSetup(SurrogateObject):

    def __init__(self, training_data, result_column, must_normalize_values,
                 rescaling="standarizing", path=None):

        super(RBFSetup, self).__init__()

        # If given a path, try to load from there,
        # otherwise reconstruct the object
        if path:
            self._load(path)
            return

        # Store inputs for further use
        self.training_data = training_data.copy()
        self.column_labels = training_data.columns
        self.result_label = result_column
        self.data_columns = [col for col in training_data.columns if col != result_column]
        self.rbfs = {"Linear spline": self._linear_spline,
                     "Cubic spline": self._cubic_spline,
                     "Thin plate spline": self._thin_plate_spline,
                     "Quadric spline": self._quadric_poliharmonic_spline,
                     "Gaussian": self._gaussian,
                     "Multiquadric": self._multiquadric,
                     "Inverse multiquadric": self._multiquadric,
                     "Inverse quadric": self._inverse_quadric}

        # Initialize some variables
        self.w = None
        self.rbf_label = None
        self.rbf_function = None
        self.training_method = None
        self.scale_parameter = None

        # Working with ndarrays is faster than operating on DataFrame, so we convert data into that
        self.sampled_points = self.training_data[self.data_columns].values
        self.n_samples, self.n_feats = self.sampled_points.shape
        self.sampled_values = self.training_data[self.result_label].values

        if rescaling == "standarizing":
            self.points_mean, self.points_std = self.mean_n_std(self.sampled_points)
            if must_normalize_values:
                self.values_mean, self.values_std = self.mean_n_std(self.sampled_values)
        elif rescaling == "normalizing":
            self.points_mean = (np.max(self.sampled_points, axis=0) -
                                np.min(self.sampled_points, axis=0)) / 2.
            self.points_std = self.points_mean
            if must_normalize_values:
                self.values_mean = (np.max(self.sampled_values, axis=0) -
                                    np.min(self.sampled_values, axis=0)) / 2.
                self.values_std = self.values_mean
        else:
            raise ValueError("Value of <rescaling> unrecognized.")

        # Normalize the sampled points array
        self.normal_points = self.normalize(self.sampled_points, self.points_mean, self.points_std)
        # Normalize the values array
        if must_normalize_values:
            self.normal_values = self.normalize(self.sampled_values, self.values_mean,
                                                self.values_std)

    def _set_centres(self, loc, are_normalized):
        """
        Places the centers of the RBFs

        Parameters
        ----------
        loc : None, integer, or iterable
              locations of the centers of the RBFs. If it is None a RBF is
              placed at every training point. If it is an integer a
              full-factorial design is used to place them.
        are_normalized : Boolean
                         Are locations of the centres already normalized?

        Raises
        ------
        TypeError
            * If `loc` is not None, integer, NumPy array or Pandas DataFrame.

        ValueError
            * If `loc` is an integer larger than the number of samples.
            * If `loc` is a NumPy array of points whose dimension is lower than
              the dimension of the sampled points.
        """
        if loc is not None and not isinstance(loc, (int, np.ndarray, pd.DataFrame)):
            raise TypeError("Parameter <loc> has an unsupported type ({}).".format(type(loc)))
        elif isinstance(loc, int) and loc > self.n_samples and self.training_method != "bayesian":
            raise ValueError("Parameter <loc> cannot be larger than the number of training samples")
        elif isinstance(loc, np.ndarray) and loc.shape[1] != self.n_feats:
            raise ValueError("The dimension of the points provided in the parameter <loc> must be "
                             "equal to the dimension of the sampled points.")

        if loc is None:
            # Place a RBF at each sample
            self.rbf_centres = self.normal_points
        elif isinstance(loc, int):
            # Create a regular mesh
            positions = np.zeros((loc, self.n_feats))
            for d in range(self.n_feats):
                x0 = min(self.normal_points[:, d])
                x1 = max(self.normal_points[:, d])
                positions[:, d] = np.linspace(x0, x1, loc)
            l = np.zeros((loc ** self.n_feats, self.n_feats))
            ii = 0
            for t in product(range(loc), repeat=self.n_feats):
                for d in range(self.n_feats):
                    l[ii, d] = positions[t[d], d]
                ii += 1
            self.rbf_centres = l
        elif isinstance(loc, np.ndarray):
            if not are_normalized:
                self.rbf_centres = self.normalize(loc, self.points_mean, self.points_std)
            else:
                self.rbf_centres = loc
        elif isinstance(loc, pd.DataFrame):
            if not are_normalized:
                self.rbf_centres = self.normalize(loc.values, self.points_mean, self.points_std)
            else:
                self.rbf_centres = loc.values
        else:
            raise TypeError("Parameter <loc> has an unsupported type ({}).".format(type(loc)))
        self.n_centres = self.rbf_centres.shape[0]

    def _initialize_rbfs(self, locations):
        self.basis = []
        for l in locations:
            self.basis.append(lambda xi, eta=l: [self.rbf(x, l) for x in xi])

    def _build_covariance_matrix(self):
        """
        Compute and store the covariance matrix based on the stored parameters.

        This is a private method.
        """
        if self.rbf_label in ["Linear spline", "Cubic spline", "Thin plate spline",
                              "Quadric spline"]:
            self.C = np.zeros((self.n_centres, self.n_samples))
        elif self.rbf_label in ["Gaussian", "Multiquadric", "Inverse quadric",
                                "Inverse multiquadric"]:
            self.C = np.ones((self.n_centres, self.n_samples))
        if self.rbf_centres is None:
            for index1, index2 in combinations(range(self.n_samples), 2):
                x1 = self.normal_points[index1, :]
                x2 = self.normal_points[index2, :]
                self.C[index1, index2] = self.C[index2, index1] = self.rbf_function(x1, x2)
        else:
            for r, xt in enumerate(self.rbf_centres):
                for p, x in enumerate(self.normal_points):
                    self.C[r, p] = self.rbf_function(x, xt)

    @staticmethod
    def _evaluate_distance(x, y):
        """
        L-2, Euclidean distance

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        This method is private.
        """
        return np.linalg.norm(x - y)

    def _linear_spline(self, x, y):
        """
        Linear spline function. f(x, y) = |x - y|

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return d

    def _cubic_spline(self, x, y):
        """
        Cubic spline function. f(x, y) = |x - y| ** 3

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return d ** 3

    def _thin_plate_spline(self, x, y):
        """
        Thin plate spline function. f(x, y) = \log(|x - y|) * |x - y| ** 2

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y) + 1.e-16
        return np.log(d) * (d ** 2)

    def _quadric_poliharmonic_spline(self, x, y):
        """
        Quadric spline function. f(x, y) = \log(|x - y|) * |x - y| ** 4

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y) + 1.e-16
        return np.log(d) * (d ** 4)

    def _poliharmonic_spline(self, x, y):
        """
        General poliharmonic function.
        f(x, y) = \log(|x - y|) ** {(n + 1) (mod 2)} * |x - y| ** n

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y) + 1.e-16
        if self.scale_parameter % 2 == 2:
            return np.log(d) * (d ** self.scale_parameter)
        else:
            return d ** self.scale_parameter

    def _gaussian(self, x, y):
        """
        Gaussian function.
        f(x, y) = \exp{-\lambda * |x - y| ** 2}

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return np.exp(- self.scale_parameter * d ** 2)

    def _multiquadric(self, x, y):
        """
        Multiquadric function.
        f(x, y) = \sqrt{1 + (\lambda * |x - y|) ** 2}

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return np.sqrt(1. + (self.scale_parameter * d) ** 2)

    def _inverse_multiquadric(self, x, y):
        """
        Inverse multiquadric function.
        f(x, y) = \frac{1}{\sqrt{1 + (\lambda * |x - y|) ** 2}}

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return 1. / np.sqrt(1. + (self.scale_parameter * d) ** 2)

    def _inverse_quadric(self, x, y):
        """
        Inverse quadric function.
        f(x, y) = \frac{1}{1 + (\lambda * |x - y|) ** 2}

        Parameters
        ----------
        x : iterable
            first point

        y : iterable
            second point

        Returns
        -------
        out : float
              value of the function

        Private method
        """
        d = self._evaluate_distance(x, y)
        return 1. / (1. + (self.scale_parameter * d) ** 2)