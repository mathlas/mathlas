# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Kriging estimator
-----------------
"""

import numpy as np
from numpy import linalg as la
from scipy.special import iv
from scipy.stats import norm
from copy import copy
from itertools import combinations_with_replacement as cwr
from itertools import product
from mathlas.surrogate_modelling.surrogate_object import SurrogateObject
from mathlas.doe.sample import Sample
import pandas as pd


class Kriging(SurrogateObject):
    """
    Initialise a Kriging estimator object.

    Parameters
    ----------
    sampledPoints : NumPy array or Pandas DataFrame, optional
                    Co-ordinates of the sampled points. The array must
                    contain one row with co-ordinates per sampled point.
                    The number of rows in `sampledPoints` must match the
                    number of rows in `sampleValues`.

                    If `sampledPoints` is None (default), `sample` must be
                    provided.

                    The data must be provided as a 1D or 2D ndarray.

    sampleValues : NumPy array or Pandas DataFrame, optional
                   Value of the sampled function at the sampled points.
                   The number of rows in `sampleValues` must match the
                   number of rows in `sampledPoints`.

                   If None (default), `sample` must be provided.

                   The data must be provided as a 1D or 2D ndarray.

    sample : mathlas.DoE.Sample, optional
             Sample object with the position of the sampled points
             and the values of the sampling function.

             If None (default), both `sampledPoints` and
             `sampleValues` must be provided

    regressionType : string, optional
                     Type of regression to apply to the sampled values.

                     Allowed values are:
                       - ``constant``: the weighted-average value (Default)
                       - ``linear``: linear approximation
                       - ``quadratic``: quadratic approximation, including
                         the cross products
                       - ``userDefined`` `userDefinedValues` must be
                         provided

    userDefinedValues : list of tuples, optional
                   userDefined functions used for obtaining the regression
                   term.
    covarianceTypes : string or list of strings, optional
                   The type of covariance model to use for each of the
                   parameter.
                   If given a string, the covariance model will be used
                   for all the parameters.

                   Allowed values are:
                     - ``Gauss``: Gaussian regression (Default)
                     - ``vonMises``: vonMises regression

                   The length of covarianteTypes must match the number of
                   params (columns) in sampledPoints.
    parameterSpan : list of parameter spans, optional
                   Only used if any of the elements in covarianceTypes is
                   ``Gauss``.-
                   List of values where the vonMises parameters span to.
    optimize : boolean, optional
               Whether to automatically optimize the hyperparameters with
               the default values or not. The default is True.

               Should you wish to use different optimization parameters,
               you can choose to not run the optimization process automatically
               and call :py:meth:`~Kriring.optimizeHyperParameters` yourself.

    path : path to a pickled cache object, optional
           Generate the Kriging object from a pickled cache file
           instead of from the given params. The pickled file
           must have been generated with the ``cache()`` method
           of the object in a previous run.

           If given, the rest of the parameters will be ignored.

    Raises
    ------
    TypeError
        * If `sampledPoints` is given and is either a NumPy array nor a
          pandas DataFrame.
        * If `sampleValues` is given and is neither a NumPy array nor a
          pandas DataFrame.
        * If `sample` is given and is not a mathlas.DoE.sample.

    ValueError
        * If not enough info about sampled points and their values is
          given.
        * If the dimensions of the various parameters are not coherent.
        * If given an unsupported value for `regressionType` or
          `covarianceTypes`.
    """
    def __init__(self, sampledPoints=None, sampleValues=None, sample=None,
                 regressionType='constant', userDefinedValues=None,
                 covarianceTypes='Gauss', parameterSpan=None, optimize=True,
                 path=None):
        super(Kriging, self).__init__()

        # If given a path, try to load from there, otherwise reconstruct the
        # object
        if path:
            self._load(path)
        else:
            self.columns = None
            # Some data-cheking for sanity
            if sampledPoints is not None:
                if not isinstance(sampledPoints,
                                  (np.ndarray, pd.DataFrame)):
                    raise TypeError('sampledPoints must be a NumPy array ' +
                                    'or pandas DataFrame')

                # Working with ndarrays is faster than directly operating
                # the DataFrame, so we change into that
                if isinstance(sampledPoints, pd.DataFrame):
                    sampledPoints = sampledPoints.values

            if sampleValues is not None:
                if not isinstance(sampleValues,
                                  (np.ndarray, pd.DataFrame)):
                    raise TypeError('sampleValues must be a NumPy array or ' +
                                    'pandas DataFrame')

                if isinstance(sampleValues, pd.DataFrame):
                    self.columns = sampleValues.columns
                    sampleValues = sampleValues.values
                elif isinstance(sampleValues, pd.Series):
                    self.columns = [sampleValues.name]
                    sampleValues = sampleValues.values

            if sample is not None:
                if not isinstance(sample, Sample):
                    raise TypeError('sample must be a Sample object')

                sampledPoints = sample.X.values
                sampleValues = sample.Y.values
                self.columns = sample.Y.columns

            # If given a 1D array, convert it to a column
            # 2D-ndarray since internally we need that
            dim = len(sampleValues.shape)
            if dim == 1:
                sampleValues = sampleValues.reshape((sampleValues.shape[0], 1))
            elif dim != 2:
                raise ValueError('Sampled values must be given in a 2D ' +
                                 'array')
            dim = len(sampledPoints.shape)
            if dim == 1:
                sampledPoints = sampledPoints.reshape((sampledPoints.shape[0],
                                                       1))
            elif dim != 2:
                raise ValueError('Sampled points must be given in a 2D ' +
                                 'array')

            # Check that we've been given some form of input
            if sampledPoints is None or sampleValues is None:
                raise ValueError('List of sampled point locations or ' +
                                 'sampled values not given')

            # Store the dimensions of the problem
            self.nSampledPoints = sampledPoints.shape[0]
            self.nParameters = sampledPoints.shape[1]
            self.nFunctions = sampleValues.shape[1]

            # Check
            if self.nSampledPoints != sampleValues.shape[0]:
                raise ValueError("Dimensions of locations and values don't " +
                                 "match ({} != {})".
                                 format(self.nSampledPoints,
                                        sampleValues.shape[0]))

            if covarianceTypes:
                # If given a string, convert it to a list
                if isinstance(covarianceTypes, str):
                    covarianceTypes = [covarianceTypes] * self.nParameters

                # Check length
                if len(covarianceTypes) != self.nParameters:
                    raise ValueError("covarianceTypes doesn't match the size" +
                                     "of sampledPoints")

                # Check data
                supportedCovariances = ('Gauss', 'vonMises')
                for cov in covarianceTypes:
                    if cov not in supportedCovariances:
                        raise ValueError('Covariance type {} not supported'.
                                         format(cov))

                # Store it
                self.covarianceTypes = covarianceTypes

            if parameterSpan:
                if 'vonMises' in covarianceTypes:
                    if len(parameterSpan) != self.nParameters:
                        raise ValueError("parameterSpan doesn't match the " +
                                         "size of sampledPoints")

                    self.parameterSpan = parameterSpan

            # Type of regresion used (default: ordinary Kriging)
            supportedRegressions = ('constant', 'linear',
                                    'quadratic', 'userDefined')
            if regressionType is not None:
                if regressionType not in supportedRegressions:
                    msg = 'Regression {} not supported'.format(regressionType)
                    raise ValueError(msg)

            self.regressionType = regressionType

            # Store the userDefined values, but only if regression type
            # is userDefined
            self.userDefinedValues = None
            if self.regressionType == 'userDefined':
                self.userDefinedValues = userDefinedValues

            # Normalize the given inputs, and store them
            self.sampledPoints = sampledPoints
            self.sampleValues = sampleValues

            # Normalize the points array
            self.pointsMean, self.pointsStdv = self.mean_n_std(sampledPoints)
            self.normalPoints = self.normalize(sampledPoints,
                                               self.pointsMean,
                                               self.pointsStdv)

            # Normalize the values array
            self.valuesMean, self.valuesStdv = self.mean_n_std(sampleValues)
            self.normalValues = self.normalize(sampleValues,
                                               self.valuesMean,
                                               self.valuesStdv)

            # Create some initial data
            self.theta = np.ones((self.nParameters, self.nFunctions))
            self.regPoints = self._evalRegressands(
                                 userDefinedValues=userDefinedValues)

            # Finally, optimize the hyperparameters if told to do so
            if optimize:
                self.optimizeHyperParameters()

    def _buildCovarianceMatrix(self, fId=None):
        """
        Compute and store the covariance matrix based on the stored parameters.

        This is a private method.

        Parameters
        ----------
        fId : integer
              A numerical index of the function to act over, with the same
              criterion as in getCLF, or None to act over all the functions.
        """
        # Determine what to iterate over and initialise the covariance
        # matrix, either completely (because it does not exist or we're
        # iterating over all the function ids) or just the part pertaining
        # to the given function id
        if fId is not None:
            funcIds = (fId,)
            if not hasattr(self, 'covMatrix'):
                self.covMatrix = np.ones((self.nSampledPoints,
                                          self.nSampledPoints,
                                          self.nFunctions))
            else:
                self.covMatrix[..., fId] = np.ones((self.nSampledPoints,
                                                    self.nSampledPoints))
        else:
            funcIds = range(self.nFunctions)
            self.covMatrix = np.ones((self.nSampledPoints,
                                      self.nSampledPoints,
                                      self.nFunctions))

        # Start loop over parameters
        for paramId in range(self.nParameters):
            v = self.normalPoints[:, paramId]
            c = self.covarianceTypes[paramId]

            # Calculate the squared rescaled distances
            distance = np.zeros((self.nSampledPoints, self.nSampledPoints))
            for id1, id2 in cwr(range(self.nSampledPoints), 2):
                dist = np.abs(v[id1] - v[id2])
                distance[id1, id2] = distance[id2, id1] = dist

            # Calculates a piece of the CovarianceMatrix
            for i in funcIds:
                t = self.theta[paramId, i]
                if c == 'Gauss':
                    squaredDistance = distance ** 2.
                    partialMatrix = np.exp(-1. * t * squaredDistance)
                else:
                    # vonMises
                    span = self.parameterSpan[paramId]
                    normalDistance = distance * 2. * np.pi / span
                    divisor = 2 * np.pi * iv(0, t)
                    partialMatrix = (np.exp(t * np.cos(normalDistance)) /
                                     divisor)

                self.covMatrix[..., i] = np.multiply(self.covMatrix[..., i],
                                                     partialMatrix)

        # A regularization term is added to improve numerical stability
        addend = (np.eye(self.nSampledPoints) * (10. + self.nSampledPoints) *
                  2.22e-16)

        # Compute and store the determinant of the covariance matrix (and its
        # base-10 logarithm)
        self.covDet = np.ones(self.nFunctions)
        self.covLogDet = np.zeros(self.nFunctions)
        for funcId in funcIds:
            self.covMatrix[..., funcId] += addend
            covEig, _ = la.eigh(self.covMatrix[..., funcId])
            for elem in covEig:
                self.covDet[funcId] *= elem
                self.covLogDet[funcId] += np.log10(np.abs(elem))

    def _evalRegressands(self, sampledPoints=None, userDefinedValues=None):
        """
        Evaluate the regressands.

        Calculate the values of the regressands at the regressors.

        This is a private method

        Parameters
        ----------
        sampledPoints : NumPy array-like, optional
                        Array with the regressors. If not provided the
                        locations of the points used to generate the model are
                        used.
        userDefinedValues : NumPy array-like, optional
                            If the regressiontype chosen was 'userDefined' the
                            value of the regressands must be provided in this
                            variable.

        Returns
        -------
        out : NumPy array
              Array with the values of the regressands. Each row contains the
              regressands for a sampled point and each column the value for a
              different regression term.

        Raises
        ------
        ValueError
            * If sampledPoints is given and the number of columns does not
              match the number of parameters used to build the model.
            * If `regressionType` is `userDefined` but `regValues` is not
              provided.
            * If `regressionType` is `userDefined` but the number of points in
              `regValues` does not match the number of points in
              `sampledPoints`
        """
        # Parameter check
        # We don't actually check for the type of the input parameters; as
        # long as NumPy accepts them, we do too.
        if sampledPoints is None:
            sampledPoints = self.normalPoints
        else:
            if sampledPoints.shape[1] != self.nParameters:
                raise ValueError('sampledPoints does not have the sames ' +
                                 'dimension as the stored parameters ' +
                                 '({}-{})'.format(sampledPoints.shape[1],
                                                  self.nParameters))

        # Cannot read from object values, as sampledPoints might've changed
        nPoints = sampledPoints.shape[0]
        nParameters = sampledPoints.shape[1]

        # Compute the regression values differently depending on the given
        # regression type
        if self.regressionType == 'constant':
            regValues = np.ones([nPoints, 1])
        elif self.regressionType == 'linear':
            regValues = np.ones([nPoints, nParameters + 1])
            for id0 in range(nParameters):
                regValues[:, id0 + 1] = sampledPoints[:, id0].squeeze()
        elif self.regressionType == 'quadratic':
            nFunctions = int((nParameters + 1) * (nParameters + 2) / 2)
            regValues = np.ones([nPoints, nFunctions])
            id0 = nParameters + 1
            for id1 in range(nParameters):
                v1 = sampledPoints[:, id1]
                regValues[:, id1 + 1] = v1.squeeze()
                for id2 in range(id1, nParameters):
                    v2 = sampledPoints[:, id2]
                    regValues[:, id0] = np.multiply(v1, v2).squeeze()
                    id0 += 1
        elif self.regressionType == 'userDefined':
            if userDefinedValues is None:
                raise ValueError('userDefinedValues      must be given for user-defined ' +
                                 'regressions')

            if userDefinedValues.shape[0] != nPoints:
                raise ValueError('The dimension of userDefinedValues does ' +
                                 'not match the dimension of sampledPoints')

            regValues = userDefinedValues

        return regValues

    def _performRegression(self, fId):
        """
        Calculate the regression model.

        Solve the minimum squares problem in order to compute the regression
        model coefficients.

        This is a private method

        Parameters
        ----------
        fId : integer
              A numerical index of the function to act over, with the same
              criterion as in getCLF, or None to act over all the functions.
        """

        # Determine what to iterate over
        if fId is not None:
            funcIds = (fId,)
        else:
            funcIds = range(self.nFunctions)

        # We only want to generate this once
        if not hasattr(self, 'regCoefficients'):
            self.regCoefficients = np.zeros((self.regPoints.shape[1],
                                             self.nFunctions))

        for funcId in funcIds:
            A = self.regPoints.T.dot(la.solve(self.covMatrix[..., funcId],
                                              self.regPoints))
            B = self.regPoints.T.dot(la.solve(self.covMatrix[..., funcId],
                                              self.normalValues[:, funcId]))
            self.regCoefficients[:, funcId] = la.solve(A, B)

    def getCLF(self, fId=None):
        """
        Compute and store the logarithm to base 10 of the Condensed
        Likelihood Function.

        Parameters
        ----------
        fId : integer or string
              An identifier for the function whose CLF is to be updated.
              If given an integer, fId represents the function whose
              sampled values were provided in the i-th column of the
              constructor for `sampleValues`. `fId` may be negative, as long
              as its value makes sense.
              If given a string and a Pandas DataFrame was given to the
              constructor for `sampleValues`, the string represents the
              column name in that DataFrame.

              If `fId` is `None`, all the function CLFs will be computed.

        Returns
        -------
        out : 1D NumPY array of floats or float
              The value of the condensed likelihood function for all the
              functions of the problem if `fId` is None or the CLF for the
              funcion identified by `fId` if given.

        Raises
        ------
        TypeError : if `fId` is provided and is not an integer nor a string
        ValueError : if `fId` does not refer to a known function
        """

        # Various checks
        if fId and not isinstance(fId, (int, str)):
            raise TypeError('fId must be an integer or string')

        if isinstance(fId, int):
            if fId >= self.nFunctions:
                raise ValueError('Given fId is higher than the number of ' +
                                 'sampled functions')
        elif isinstance(fId, str):
            if self.columns is None:
                raise ValueError('sampleValues not given as a DataFrame')

            try:
                fId = self.columns.index(fId)
            except ValueError:
                raise ValueError('Given fId not in sampled function names')

        # Initializing some important values
        self._buildCovarianceMatrix(fId)
        self._performRegression(fId)

        # Initialise parameters, if needed
        if not hasattr(self, 'V'):
            self.V = np.zeros((self.nSampledPoints, self.nFunctions))
        if not hasattr(self, 'squaredSigma'):
            self.squaredSigma = np.zeros(self.nFunctions)
        if not hasattr(self, 'CLF'):
            self.CLF = np.zeros(self.nFunctions)

        # What to iterate over (one function vs all of them)
        if fId is not None:
            funcIds = (fId,)
        else:
            funcIds = range(self.nFunctions)

        for i in funcIds:
            self.V[:, i] = self.normalValues[:, i]
            for j in range(self.regPoints.shape[0]):
                self.V[j, i] -= self.regPoints[j].dot(self.regCoefficients[:,
                                                                           i])

            # Averaged error
            self.squaredSigma[i] = (self.V[:, i].dot(
                                           la.solve(self.covMatrix[..., i],
                                                    self.V[:, i])) /
                                    self.nSampledPoints)
            # Condensed Likelihood Function
            self.CLF[i] = 0.5 * (np.log10(self.squaredSigma[i]) +
                                 self.covLogDet[i] / self.nSampledPoints)

        # Return either a NumPy array or a float
        if fId is not None:
            return self.CLF[fId]
        else:
            return self.CLF

    def optimizeHyperParameters(self, span=0.1,
                                lowerBound=1e-2, upperBound=10.,
                                debug=False):
        """
        Optimize the estimator's scaling factors.

        Scaling factor optimization via Pattern-Search (Hooke and Jeeves)
        algorithm.

        Parameters
        ----------
        span : float, optional
               Initial span of the (hyper-cross)
        lowerBound : float, optional
               Lower bound to the minimum value of the scaling factor
        upperBound : float, optional
               Upper bound to the maximum value of the scaling factor
        """
        # Initialize parameters that are function independant
        nParameters = self.normalPoints.shape[1]

        HyperCross = np.zeros((2 * nParameters, nParameters))
        for index1 in range(nParameters):
            index2 = index1 + nParameters
            HyperCross[index1, index1] = 1.
            HyperCross[index2, index1] = -1.

        # Value initialization
        self.getCLF()
        given_span = span

        # Iterate over all the given functions
        for i in range(self.nFunctions):
            if self.columns is not None and debug:
                print('Optimizing {}...'.format(self.columns[i]))
            span = given_span
            theta = self.theta[:, i].copy()
            CLF = self.CLF[i]
            # Iterate until cross span is very small
            # TODO: Function evaluations could be avoided by keeping a
            #       list of previously evaluated positions
            j = 0
            while span > 1e-10:
                # Neighbourhood values
                neighVals = []
                # print(self.theta[:, i])
                for row in range(len(HyperCross)):
                    self.theta[:, i] = theta + span * HyperCross[row, :]
                    if ((self.theta[:, i].min() > lowerBound) and
                            (self.theta[:, i].max() < upperBound)):
                        self.getCLF(i)
                        neighVals.append(self.CLF[i])
                    else:
                        neighVals.append(1e30)

                # Is there a value smaller than the smallest found?
                # No?  => Reduce span
                # Yes? => Continue from that point
                if min(neighVals) >= CLF:
                    self.theta[:, i] = theta.copy()
                    span /= 2.
                else:
                    CLF = min(neighVals)
                    self.theta[:, i] = (theta + span *
                                        HyperCross[neighVals.index(CLF), :])
                    theta = self.theta[:, i].copy()

                j += 1

        # Recalculate covariance matrix and regression coefficients for
        # all the solution values
        self.getCLF()

    def optimizeHyperParametersV2(self, span=0.1,
                                  LowerBound=1e-2, UpperBound=10.):
        """
        Optimize the estimator's scaling factors without storing hyper-cross.

        Scaling factor optimization via Pattern-Search (Hooke and Jeeves)
        algorithm without storing hyper-cross values.

        Parameters
        ----------
        span : float, optional
               Initial span of the (hyper-cross)
        lowerBound : float, optional
               Lower bound to the minimum value of the scaling factor
        upperBound : float, optional
               Upper bound to the maximum value of the scaling factor
        """

        # TODO: This variant will not work since it has not been updated
        #       to handle multiple functions

        # Initializing
        nParameters = self.normalPoints.shape[1]
        self.getCLF()
        bestCLF = self.CLF
        theta = self.theta
        its = 0

        # Iterate until cross span is very small
        while span > 1e-10:
            MustDecrease = True
            # Explore neighborhood
            for param in range(nParameters):
                for side in range(2):
                    theta0 = copy(theta)
                    theta0[0, param] += span * (-1.) ** side
                    if LowerBound < theta0[0, param] < UpperBound:
                        self.theta = theta0
                        its += 1
                        self.getCLF()
                        if self.CLF < bestCLF:
                            bestCLF = copy(self.CLF)
                            bestTheta = copy(theta0)
                            MustDecrease = False
            if MustDecrease:
                span /= 2
            else:
                theta = copy(bestTheta)

        # Recalculate covariance matrix and regression coefficients for the
        # solution values
        self.theta = copy(bestTheta)
        self.getCLF()

    def get_prediction(self, points, provideRMSE=False,
                       userDefinedValues=None):
        """
        Get the constructed estimator's prediction at the given points.

        Calculates the best linear unbiased prediction (BLUP) for the given
        point. If `provideRMSE` is `True` the method also provides the
        root-mean-square error at those points.

        Parameters
        ----------
        points : NumPy array-like or Pandas DataFrame
                 Positions of the points where the estimation is requested.
                 If points is a DataFrame, the returned BLUP will also be a
                 DataFrame whose indices will match those of points.
        userDefinedValues : NumPy array-like or Pandas DataFrame
                 The value of the regressands if `regressionType` is set to
                 `userDefined`.

        Returns
        -------
        out : tuple or NumPy array-like
        If `provideRMSE` is `False`:
            A NumPy array or Pandas DataFrame with the BLUP.
        If `provideRMSE` is `True`:
            A tuple with the BLUP and a NumPy array with the RMSE.

        Raises
        ------
        ValueError
            If the number of columns in `points` does not match the number of
            parameters used to create the model.
        """
        indices = None
        if isinstance(points, pd.DataFrame):
            indices = points.index
            points = points.values

        # Make sure we accept 1D NumPy arrays, too
        dim = len(points.shape)
        if dim == 1:
            points = points.reshape((points.shape[0], 1))
        elif dim != 2:
            raise ValueError('Points must be given in a 2D array')

        # Sampled points must be normalized
        normalPoints = self.normalize(points,
                                      self.pointsMean, self.pointsStdv)

        # Initializing variables
        nInputs = self.normalPoints.shape[0]
        nParameters = self.normalPoints.shape[1]
        nPoints = normalPoints.shape[0]

        # Check that the given points have the same dimensions as the ones
        # used to create the model
        if nParameters != normalPoints.shape[1]:
            raise ValueError("The number of parameters in points does not " +
                             "match the number of parameters used to create" +
                             " the model {} - {}".format(nParameters,
                                                         self.nParameters))

        if userDefinedValues is not None:
            # If it's a Pandas DataFrame -> Use only the NumPy array
            if isinstance(userDefinedValues, pd.DataFrame):
                userDefinedValues = userDefinedValues.values

        # Calculate the correlation of the samples with the original locations
        covSamples = np.ones((nInputs, nPoints, self.nFunctions))
        # self.covCoefficients has the same shape as self.V
        self.covCoefficients = np.zeros((self.nSampledPoints, self.nFunctions))
        dummy = np.zeros((nPoints, self.nFunctions))
        # Evaluate regressands at the sampled points
        regSamples = self._evalRegressands(sampledPoints=normalPoints,
                                           userDefinedValues=userDefinedValues)
        for fId in range(self.nFunctions):
            for param in range(nParameters):
                c = self.covarianceTypes[param]
                t = self.theta[param, fId]
                # Compute the distance between the normalised points and the
                # normalised samples
                distance = np.zeros([self.nSampledPoints, nPoints])
                for i, j in product(range(self.nSampledPoints),
                                    range(nPoints)):
                    if isinstance(normalPoints, pd.DataFrame):
                        distance[i, j] = np.abs(self.normalPoints[i, param] -
                                                normalPoints.iloc[j, param])
                    else:
                        distance[i, j] = np.abs(self.normalPoints[i, param] -
                                                normalPoints[j, param])

                # The expression of the functions depend on the type of
                # correlation used for this parameter
                if c == 'Gauss':
                    squaredDistance = distance ** 2.
                    partialMatrix = np.exp(-1. * t * squaredDistance)
                else:
                    # vonMises
                    span = self.parameterSpan[param]
                    normalDistance = distance * 2. * np.pi / span
                    divisor = 2 * np.pi * iv(0, t)
                    partialMatrix = (np.exp(t * np.cos(normalDistance)) /
                                     divisor)

                covSamples[..., fId] = np.multiply(covSamples[..., fId],
                                                   partialMatrix)

            # Obtaining the covarience coefficients
            self.covCoefficients[:, fId] = la.solve(self.covMatrix[..., fId],
                                                    self.V[:, fId])

            dummy[:, fId] = (covSamples[..., fId].T.
                             dot(self.covCoefficients[:, fId]) +
                             regSamples.dot(self.regCoefficients[:, fId]))

        # Best Linear Unbiased Predictor
        # Re-scale to convert back to original scale
        # If we were given a Pandas DataFrame, return the result as a
        # DataFrame, too
        BLUP = self.denormalize(dummy, self.valuesMean, self.valuesStdv)
        if (indices is not None) or (self.columns is not None):
            BLUP = pd.DataFrame(BLUP, index=indices, columns=self.columns)

        # We only compute these if the user asked for the error meassure, since
        # the following are somewhat costly operations
        if provideRMSE:
            RMSE = np.zeros((nPoints, self.nFunctions))
            for fId in range(self.nFunctions):
                aVector = (self.regPoints.T.dot(la.solve(self.covMatrix[...,
                                                                        fId],
                                                         covSamples[..., fId]))
                           - regSamples.T)
                aMatrix = (self.regPoints.T.dot(la.solve(self.covMatrix[...,
                                                                        fId],
                                                         self.regPoints)))
                for pointId in range(nPoints):
                    u = aVector[:, pointId]
                    r = covSamples[:, pointId, fId]
                    addend1 = u.T.dot(la.solve(aMatrix, u))
                    addend2 = r.T.dot(la.solve(self.covMatrix[..., fId], r))
                    RMSE[pointId, fId] = 1 + addend1 - addend2 + 1e-16
                # RMSE must be re-scaled
                RMSE[:, fId] = (np.sqrt(RMSE[:, fId] *
                                        self.squaredSigma[fId]) *
                                self.valuesStdv[fId])

            # Convert to DataFrame, if needed
            if (indices is not None) or (self.columns is not None):
                RMSE = pd.DataFrame(RMSE,
                                    index=indices,
                                    columns=self.columns)

            return BLUP, RMSE
        else:
            return BLUP

    def get_pdf(self, X, Y, productOfVariables=False):
        """
        Calculates the probability of getting a response Y if X has happened,
        i.e., p(Y|X).

        Parameters
        ----------
        X : NumPy array.
            Non-normalized values of the feats. `M1xK` size.

        Y : NumPy array.
            Non-normalized values of the responses whose probability must be
            calculated. `M2x1`

        productOfVariables : Boolean.
                             If True, M1 must be equal to M2 and the
                             probability, p(Y[j, d]|X[j,:]) is calculated for
                             each row; thus, the result is an array of size
                             `Mx1` where `M=M1=M2`. If False For each value of
                             the response and each point in the features space
                             a probability is calculated. In this case an array
                             of size `M1xM2` is obtained; it has the values
                             p(Y[j,d]|[X[k, :]).

        Returns
        -------
        out : Numpy array of floats.
              2D array pdf[j,d] = p(Y[j,d]|[X[j, :]) or
              3D array pdf[j,k,d] = p(Y[j,d]|[X[k, :])
              It may be a 1D or 2D array (see `productOfVariables` above).
        """

        BLUP, RMSE = self.get_prediction(X, provideRMSE=True)

        if productOfVariables:
            f = (2. * np.pi * RMSE)
            pdf = []
            for y in Y:
                e = -0.5 * ((BLUP - y) / RMSE) ** 2
                pdf.append(np.exp(e) / f)
        else:
            f = np.sqrt(2. * np.pi) * (RMSE + 1e-300)
            e = -0.5 * ((BLUP - Y) / RMSE) ** 2
            pdf = np.exp(e) / f

        return np.asarray(pdf)

    def get_cdf_quantile(self, X, Y):
        """
        Evaluates the quantile for a given response.

        Parameters
        ----------
        X : float or NumPy array.
            Non-normalized values of the feats.

        Y : float or NumPy array.
            Non-normalized values of the responses.

        Returns
        -------
        Q : float or Numpy array.
            Quantiles.
        """
        # Evaluate predictions
        BLUP, RMSE = self.get_prediction(X, provideRMSE=True)
        # Convert Y if required
        if ((isinstance(Y, (int, float)) and BLUP.shape[1] == 1) or
                (Y.shape[0] == 0 and BLUP.shape[1] == Y.shape[1])):
            Y = [Y] * len(X)
            Y = np.array(Y)
        # Calculate quantiles
        Q = []
        for y, mu, stdev in zip(Y, BLUP, RMSE):
            Q.append(norm.cdf(y, loc=mu, scale=stdev))
        return np.asarray(Q)

    def get_cdf_point(self, X, Q):
        """
        Evaluates the value of the response for a given quantile.

        Parameters
        ----------
        X : float or NumPy array.
            Non-normalized values of the feats.

        Q : float or Numpy array.
            Quantiles.

        Returns
        -------
        Y : float or NumPy array.
            Non-normalized values of the responses.
        """
        # Evaluate predictions
        BLUP, RMSE = self.get_prediction(X, provideRMSE=True)
        # Convert Y if required
        if ((isinstance(Q, (int, float)) and BLUP.shape[1] == 1) or
                (Q.shape[0] == 0 and BLUP.shape[1] == Q.shape[1])):
            Q = [Q] * len(X)
            Q = np.array(Q)
        # Calculate points where quantiles are reached
        Y = []
        for q, mu, stdev in zip(Q, BLUP, RMSE):
            Y.append(norm.ppf(q, loc=mu, scale=stdev))
        return np.asarray(Y)
 