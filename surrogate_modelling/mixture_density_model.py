# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.object import MathlasObject
import numpy as np
from itertools import combinations, product


def mdm_method(*args, **kwargs):
    """
    Return the return value of the given method in class
     Mixture_Density_Model when applied to object obj with the given args
    """
    # We don't check if the method is callable, since we don't want to hide
    # exceptions. We do check that obj and method were given
    if 'method' not in kwargs.keys():
        raise ValueError('Required argument "method" not passed')
    if 'obj' not in kwargs.keys():
        raise ValueError('Required argument "obj" not passed')

    f = getattr(MixtureDensityModel, kwargs['method'])

    return f(kwargs['obj'], *args)


class MixtureDensityModel(MathlasObject):
    """
    Mixture of density functions.
    This class can calculate the model and plot the results
    """

    def __init__(self, samples=None, n_density_functions=None, init_mode='K-means',
                 list_of_density_functions=None, n_neighbors=1, span_of_feats=None,
                 must_calculate_model=True, params=None, seed=None, path=None):
        """
        Constructor of the Mixture Density Model, a probabilistic generative
        model.

        Parameters
        ----------
        samples : NumPy array, optional
                  Training dataset of size `Mx1`

        n_density_functions : integer or list, optional
                              Number of density funtions (number of components). Alternatively,
                               it can be a list of integers; if that is the case, the code selects
                               the value of K that provides the better model.

        init_mode : string, optional.
                    how the initial values of the parameters are chosen.
                    The accepted values are: "user-defined","random", or
                    "K-means".

        list_of_density_functions : string, optional
                                    a list of the names of the density functions
                                     to be used for each variable. Its lenght must
                                     be equal to the number of feats. It can take
                                     the values "Gaussian", "Log-normal" or
                                     "Periodic".

        n_neighbors : integer, optional
                      Number of neighboring cells to be simulated in case a
                       periodic variable exists.

                      Default value is 1.

        span_of_feats : dictionary, optional
                        Dictionary whose keys are the integer indices of those
                         feats which are periodic. Their assigned values are the
                         span of each period.

        must_calculate_model: boolean, optional
                              Must the constructor call the method to calculate
                               the model?

        params : tuple, optional
                 values of the mean vectors and variance matrices for the
                 initial solution if the `init_mode` variable takes the value
                 "user-defined".

        seed : integer or None
               Seed used to initialize random number generator. If None, a default value is set

        path : String, optional.
               Path where the model is going to be stored or will be read.

               If None (default), `samples` and `K` (at least must be provided.
               If it is provided and no other variable must be provided, the
               model will be read. If all this variables are provided and also
               `path` is provided the object will be stored once trained.

           If given, the rest of the parameters will be ignored.

        Raises
        ------
        ValueError
            * If `path` is None and either `samples` or `K`are None.
            * If `spanOfFeats`does not contain information about the periodic
              variables.

        TypeError
            * If the `K` is not a lis or an integer.
        """
        # If a path is provided and no other input is provided the object is
        #  loaded from file instead of training the model
        if path is not None and (samples is None and
                                 n_density_functions is None):
            self._load(path)
            return
        else:
            # If no path is provided all we check that all the required inputs
            #  are correctly initialized.
            if (samples is None) or (n_density_functions is None):
                raise ValueError("The values of <samples> and <K> must be " +
                                 "provided as inputs")

        # Some input parameters are stored for further use
        self.samples = samples
        self.n, self.d = self.samples.shape

        # If no value of <seed> has been provided a default value is set.
        self.seed = 211104 if seed is None else seed

        # If no value of <list_of_density_functions> has been provided a
        #  default value is set.
        if list_of_density_functions is None:
            list_of_density_functions = ["Gaussian"] * self.d
        else:
            assert self.d == len(list_of_density_functions)
        self.list_of_density_functions = list_of_density_functions

        # Create list of neighbors
        self.neighborhood = [range(-n_neighbors, n_neighbors + 1)
                             if x == "Periodic" else [0]
                             for x in list_of_density_functions]

        self.span_of_feats = span_of_feats
        if span_of_feats is not None:
            v = [index for index, x in enumerate(self.list_of_density_functions)
                 if x == "Periodic"]
            if sorted(span_of_feats.keys()) != v:
                raise ValueError("spanOfFeats isn't correctly initialized")
            for ii, v in span_of_feats.items():
                self.neighborhood[ii] = [x * 2 * np.pi
                                         for x in self.neighborhood[ii]]

        self.are_there_periodic_feats = any([x == "Periodic" for x
                                             in self.list_of_density_functions])
        # Transform variables, if required
        self.mod_samples = np.zeros(self.samples.shape)
        self.sample_reference = {}
        for ii, pdfName in enumerate(self.list_of_density_functions):
            if pdfName == "Gaussian":
                # No transformation is requiered
                self.mod_samples[:, ii] = self.samples[:, ii]
            elif pdfName == "Log-normal":
                # logarithmic/exponential transformation performed
                self.mod_samples[:, ii] = np.log(self.samples[:, ii] + 1e-16)
            elif pdfName == "Periodic":
                # the periodic case requires a linear tranformation. Slope is
                # related to the range and it is not required to keep it but
                # the offset must be stored.
                self.sample_reference[ii] = np.min(self.samples[:, ii])
                factor = 2. * np.pi / self.span_of_feats[ii]
                self.mod_samples[:, ii] = factor * (self.samples[:, ii] -
                                                    self.sample_reference[ii])

        # =====================================================================
        # The mixture density model is fitted
        # =====================================================================
        if not must_calculate_model:
            self._initialize_hyperparameters(init_mode, params)
            return

        if isinstance(n_density_functions, int):
            self.n_density_functions = n_density_functions
            # Initialize means, covariance matrices, and priors
            self._initialize_hyperparameters(init_mode, params)
            self._fit_mixture_density_model()
        elif isinstance(n_density_functions, list):
            # Iterate over all the elements of the list
            solutions = []
            bic = []
            for k in n_density_functions:
                self.n_density_functions = k
                # Initialize means, covariance matrices, and priors
                self._initialize_hyperparameters(init_mode, params)
                self._fit_mixture_density_model()
                # Evaluate BIC value
                bic.append(self.likelihood - 0.5 * k * np.log(self.n))
                solutions.append((k, self.pi, self.mu, self.covariance_matrix))
                # print k, BIC[-1]
            # Choose the largest BIC value and keep that solution
            index = bic.index(max(bic))
            self.n_density_functions, self.pi, self.mu, self.covariance_matrix = solutions[index]
            self._expectation()
        else:
            raise TypeError("Value of <K> is not recognized")

        # If a path is provided results are stored
        if path is not None:
            self.cache(path)

    def _initialize_hyperparameters(self, init_mode, params):
        """
        Initialize the values of the parameters of the density functions.

        Parameters
        ----------
        init_mode : string
                    how the initial values of the parameters are chosen.
                    Options: "user-defined", "random", or "K-means".

        params : tuple
                 values of the mean vectors and variance matrices for the
                 initial solution if the `init_mode` variable takes the value
                 "user-defined".

        Raises
        ------
        ValueError
            * if `init_mode` is "user-defined" but the value of `params` is
              None.
            * If `init_mode` takes a value different from "user-defined",
              "random", or "K-means".
        """
        # Initialize a seed to asure reproducibility of results
        np.random.rand(self.seed)

        # They can be provided by the user
        if init_mode == 'user-defined':
            if params is None:
                raise ValueError('The value of param has not been provided')
            mu, covariance_matrix = params
            # Initialize means
            if not (isinstance(mu, list) and len(mu) == self.n_density_functions):
                raise ValueError("User-defined means are not correctly formatted")
            for m in mu:
                if not (isinstance(m, np.ndarray) and m.shape[0] == self.d):
                    raise ValueError("User-defined means are not correctly formatted")
            self.mu = mu
            # Initialize covariance matrices
            if not (isinstance(covariance_matrix, list) and
                    len(covariance_matrix) == self.n_density_functions):
                raise ValueError("User-defined covariance matrices are not correctly formatted")
            for s in covariance_matrix:
                if not (isinstance(s, np.ndarray) and s.shape[0] == self.d and
                        s.shape[1] == self.d):
                    raise ValueError("User-defined covariance matrices are not correctly formatted")
            self.covariance_matrix = covariance_matrix
            # Initialize priors
            self.pi = np.zeros(self.n_density_functions)
            for v in self.mod_samples:
                d, c = 1e30, None
                for k, m in enumerate(self.mu):
                    if np.linalg.norm(v - m) < d:
                        c = k
                        d = np.linalg.norm(v - m)
                self.pi[c] += 1.
            self.pi /= self.n

        # Also, they can be generated at random
        elif init_mode == 'random':

            # Initialize means
            self.mu = []
            for k in range(self.n_density_functions):
                ri = np.random.randint(self.n)
                self.mu.append(self.mod_samples[ri, :])
            # Initialize covariance matrices and priors
            self.pi = np.zeros(self.n_density_functions)
            self.covariance_matrix = [np.zeros((self.d, self.d))
                                      for _ in range(self.n_density_functions)]
            for v in self.mod_samples:
                d, c = 1e30, None
                for k, m in enumerate(self.mu):
                    if np.linalg.norm(v - m) < d:
                        c = k
                        d = np.linalg.norm(v - m)
                self.pi[c] += 1.
                xi = (v - self.mu[c]).reshape((self.d, 1))
                self.covariance_matrix[c] += xi.T * xi
            self.covariance_matrix = [s / n for s, n in zip(self.covariance_matrix, self.pi)]
            self.pi /= self.n

        # they can be generated using k-means
        elif init_mode == 'K-means':

            cluster = self._evaluate_kmeans()
            # Initialize covariance matrices and priors
            self.pi = np.zeros(self.n_density_functions)
            self.covariance_matrix = [np.zeros((self.d, self.d))
                                      for _ in range(self.n_density_functions)]
            for c, v in zip(cluster, self.mod_samples):
                self.pi[c] += 1.
                xi = (v - self.mu[c]).reshape((self.d, 1))
                self.covariance_matrix[c] += xi.T * xi
            self.covariance_matrix = [s / n for s, n in zip(self.covariance_matrix, self.pi)]
            self.pi /= self.n
        else:
            raise ValueError("Value of <init_mode> is not recognized")

    def _fit_mixture_density_model(self):
        """
        Fits the data to a mixture of density models.
        """

        # Initialize the iteration
        self._expectation()
        l1 = self.likelihood
        err = 1e30

        # Best values are stored
        l_max = l1
        mu_l_max = self.mu[:]
        cov_mat_l_max = self.covariance_matrix[:]
        pi_l_max = self.pi[:]
        c = 0
        while err > 1e-6 and c < 50:
            # Maximization step of the EM algorithm
            self._maximization()
            l0 = l1
            # Expectation step of the EM algorithm
            self._expectation()
            # Very narrow pdfs can lead to over- and under-flow errors. This is
            # avoided by keeping a record of the best solution found along the
            # optimization process
            l1 = self.likelihood
            if l1 > l_max:
                l_max = l1
                mu_l_max = self.mu[:]
                cov_mat_l_max = self.covariance_matrix[:]
                pi_l_max = self.pi[:]
            c += 1
            # Evaluate estimation of the error
            err = np.abs((l1 - l0) / l0)
        # The optimum case is selected
        if l1 < l_max:
            self.mu = mu_l_max
            self.covariance_matrix = cov_mat_l_max
            self.pi = pi_l_max

    def _scaling_factor(self, samples, not_marginalized_variables=None,
                        original_scale=True):
        """
        Calculates the factor by which the transformed probability density
        functions must be divided.

        Parameters
        ----------
        samples : list or 1D NumPy array
                  Co-ordinates of the point for which the scaling factor is going to
                   be calculated.

        original_scale : Boolean, optional.
                        Is the varible `X` provided in its original
                        co-ordinates? If False, transformed co-ordinates must
                        be provided. Default value is True.

        Returns
        -------
        f : float
            Value of the scaling factor.
        """
        if not_marginalized_variables is None:
            not_marginalized_variables = range(self.d)
        f = np.ones((len(samples),))
        for index, x in enumerate(samples):
            for jj, ii in enumerate(not_marginalized_variables):
                xi, pdf_name = x[jj], self.list_of_density_functions[ii]
                if pdf_name == "Log-normal":
                    if original_scale:
                        f[index] *= xi
                    else:
                        f[index] *= np.exp(xi)
                elif pdf_name == "Periodic":
                        f[index] *= self.span_of_feats[ii] / (2. * np.pi)
        return f

    def _expectation(self):
        """
        Expectation step of the EM algorithm. This method calculates the value
        of the probability density function for the whole training dataset.
        """

        # Avoid singularities
        for k in range(self.n_density_functions):
            if np.linalg.det(self.covariance_matrix[k]) < 1e-10:
                ri = np.random.randint(self.n)
                self.mu[k] = self.mod_samples[ri, :]
                self.covariance_matrix[k] = np.eye(self.d)

        # Evaluate Gaussian distributions
        self.gauss = np.zeros((self.n, self.n_density_functions))
        for k, m, s in zip(range(self.n_density_functions), self.mu, self.covariance_matrix):
            si = np.linalg.inv(s)
            f = np.sqrt(np.linalg.det(s)) * ((2. * np.pi) ** (0.5 * self.d))
            for indx, x in enumerate(self.mod_samples):
                self.gauss[indx, k] = 0.
                for values in product(*self.neighborhood):
                    xi = x - m + np.array(values)
                    eta = -0.5 * xi.dot(si.dot(xi.T))
                    self.gauss[indx, k] += np.exp(eta)
                lf = self._scaling_factor(np.array([x]), original_scale=False)
                self.gauss[indx, k] /= f * lf

        # Evaluate posterior probability
        self.gamma = self.pi * self.gauss + 1e-30
        self.likelihood = np.log(np.prod(np.sum(self.gamma, 0)) + 1e-16)
        for index in range(self.n):
            self.gamma[index, :] /= np.sum(self.gamma[index, :])

    def _maximization(self):
        """
        Maximization step of the EM algorithm. It updates the parameters.
        """
        g = self.gamma
        # Maximize hyperparameters
        nk = np.sum(g, 0)
        self.pi = nk / self.n
        # If there are not any periodic variables the process is simpler.
        if not self.are_there_periodic_feats:
            for k in range(self.n_density_functions):
                self.mu[k] = np.sum(self.mod_samples * g[:, k:k + 1], 0) / nk[k]
                xi = np.sqrt(g[:, k:k + 1]) * (self.mod_samples - self.mu[k])
                self.covariance_matrix[k] = xi.T.dot(xi) / nk[k]
        else:
            self.mu = self._calculate_mean(self.mod_samples, g)
            for k in range(self.n_density_functions):
                xi = []
                for x in self.mod_samples:
                    distances = self._how_far_are(x, self.mu[k], False)
                    xi.append(distances)
                xi = np.sqrt(g[:, k:k + 1]) * np.array(xi)
                self.covariance_matrix[k] = xi.T.dot(xi) / nk[k]

    def get_pdf(self, samples):
        """
        Calculate the value of the probability density function for a single
        point in the original co-ordinates

        Parameters
        ----------
        samples : NumPY array-like
                  coordinates of the point in the original coordinates
        """
        # Perform some checks
        if not isinstance(samples, np.ndarray):
            raise TypeError("X must be a NumPy array")
        elif len(samples.shape) > 2:
            raise ValueError("X must be a NumPy array of 1 or 2 dimensions")
        elif len(samples.shape) == 1:
            samples = np.array([samples]).T
        if samples.shape[1] != self.d:
            raise ValueError("The dimension of the points must be equal to " +
                             "the number of feats.")
        # Dictionary of transformations
        fun = {"Gaussian": lambda x, i: x,
               "Log-normal": lambda x, i: np.log(x + 1e-16),
               "Periodic": lambda x, i: ((x - self.sample_reference[i]) *
                                         2. * np.pi / self.span_of_feats[i])}

        # Transform variables
        mod_samples = np.zeros(samples.shape)
        for ii in range(mod_samples.shape[1]):
            mod_samples[:, ii] = fun[self.list_of_density_functions[ii]](samples[:, ii], ii)
        # Evaluate some parameters dependent only on the components, not the
        # point co-ordinates
        f0 = (2. * np.pi) ** (0.5 * self.d)
        f = []
        si = []
        for s in self.covariance_matrix:
            f.append(np.sqrt(np.linalg.det(s)) * f0)
            si.append(np.linalg.inv(s))

        # Evaluate Gaussian distributions
        n_samples = mod_samples.shape[0]
        gauss = np.zeros((n_samples, self.n_density_functions))
        for ii, y in enumerate(mod_samples):
            for k, m in zip(range(self.n_density_functions), self.mu):
                gauss[ii, k] = 0.
                for values in product(*self.neighborhood):
                    xi = y - m + np.array(values)
                    eta = -0.5 * xi.dot(si[k].dot(xi.T))
                    gauss[ii, k] += np.exp(eta)
                gauss[ii, k] /= f[k]

        # Evaluate posterior probability
        gamma = self.pi * gauss
        lf = self._scaling_factor(samples)
        pdf = np.sum(gamma, axis=1) / lf
        pdf[lf < 1e-100] = 0.

        return pdf

    def get_marginalized_pdf(self, samples, not_marginalized_feats):
        """
        Calculate the value of the marginalized probability density function
        for a single point in the original, non-transformed co-ordinates.

        Parameters
        ----------
        samples : NumPY array-like
                  coordinates of the point in the original coordinates

        not_marginalized_feats : List of integers
                                 list of variables that are not marginalized.
        """
        # Perform some checks
        if not isinstance(samples, np.ndarray):
            raise TypeError("X must be a NumPy array")
        elif len(samples.shape) > 2:
            raise ValueError("X must be a NumPy array of 1 or 2 dimensions")
        elif len(samples.shape) == 1:
            samples = np.array([samples]).T

        # set alias
        v = not_marginalized_feats
        if len(v) != samples.shape[1]:
            raise ValueError("The dimension of the points must be equal to " +
                             "the number of feats.")
        # Dictionary of transformations
        fun = {"Gaussian": lambda x, i: x,
               "Log-normal": lambda x, i: np.log(x + 1e-16),
               "Periodic": lambda x, i: ((x - self.sample_reference[i]) *
                                         2. * np.pi / self.span_of_feats[i])}

        # Transform variables
        mod_samples = np.zeros(samples.shape)
        for index, ii in enumerate(v):
            mod_samples[:, index] = fun[self.list_of_density_functions[ii]](samples[:, index], ii)
        # Evaluate some parameters dependent only on the components, not the
        # point co-ordinates
        f0 = (2. * np.pi) ** (0.5 * self.d)
        f = []
        si = []
        for s in self.covariance_matrix:
            f.append(np.sqrt(np.linalg.det(s[v, :][:, v])) * f0)
            si.append(np.linalg.inv(s[v, :][:, v]))

        # Evaluate Gaussian distributions
        n_samples = mod_samples.shape[0]
        gauss = np.zeros((n_samples, self.n_density_functions))
        for ii, y in enumerate(mod_samples):
            for k, m in zip(range(self.n_density_functions), self.mu):
                gauss[ii, k] = 0.
                for values in product(*self.neighborhood):
                    xi = y - m[v] + np.array(values)[v]
                    eta = -0.5 * xi.dot(si[k].dot(xi.T))
                    gauss[ii, k] += np.exp(eta)
                gauss[ii, k] /= f[k]

        # Evaluate posterior probability
        gamma = self.pi * gauss
        lf = self._scaling_factor(samples, v)
        pdf = np.sum(gamma, axis=1) / lf
        pdf[lf < 1e-100] = 0.

        return pdf

    def get_conditional_pdf(self, known_event, random_event, not_conditional_feats):
        """
        Calculate the value of the conditional probability density function
        for a list of points in the original, non-transformed co-ordinates.

        Parameters
        ----------
        known_event : NumPY array-like
                      coordinates of the condition point in the original coordinates

        random_event : NumPY array-like
                       coordinates of the point in the original coordinates

        not_conditional_feats : List of integers
                                list of variables that are not marginalized.
        """
        # Perform some checks for non-conditional points
        if not isinstance(random_event, np.ndarray):
            raise TypeError("X must be a NumPy array")
        elif len(random_event.shape) > 2:
            raise ValueError("X must be a NumPy array of 1 or 2 dimensions")
        elif len(random_event.shape) == 1:
            random_event = np.array([random_event]).T

        # Perform some checks for conditional points
        if not isinstance(known_event, np.ndarray):
            raise TypeError("Xcond must be a NumPy array")
        elif len(known_event.shape) > 2:
            raise ValueError("Xcond must be a NumPy array of 1-2 dimensions")
        elif len(known_event.shape) == 1:
            known_event = np.array([known_event]).T

        # set alias
        conditional_feats = [elem for elem in range(self.d)
                             if elem not in not_conditional_feats]
        if len(not_conditional_feats) != random_event.shape[1]:
            raise ValueError("The dimension of the non-conditional points " +
                             "must be equal to the number of feats.")
        if (self.d - len(not_conditional_feats)) != known_event.shape[1]:
            raise ValueError("The dimension of the conditional points " +
                             "must be equal to the number of feats.")
        # Dictionary of transformations
        fun = {"Gaussian": lambda x, i: x,
               "Log-normal": lambda x, i: np.log(x + 1e-16),
               "Periodic": lambda x, i: ((x - self.sample_reference[i]) *
                                         2. * np.pi / self.span_of_feats[i])}

        # Transform variables
        mod_random_event = np.zeros(random_event.shape)
        for index, ii in enumerate(not_conditional_feats):
            f = fun[self.list_of_density_functions[ii]]
            mod_random_event[:, index] = f(random_event[:, index], ii)
        mod_known_event = np.zeros(known_event.shape)
        for index, ii in enumerate(conditional_feats):
            f = fun[self.list_of_density_functions[ii]]
            mod_known_event[:, index] = f(known_event[:, index], ii)

        # Evaluate some parameters dependent only on the components, not the
        # point co-ordinates
        f0a = (2. * np.pi) ** (0.5 * len(not_conditional_feats))
        f0b = (2. * np.pi) ** (0.5 * len(conditional_feats))
        fa, fb = [], []
        si = []
        ma, mb, mc = [], [], []
        sibb = []
        for m, s in zip(self.mu, self.covariance_matrix):
            # letter a is used to point the non-conditional variables, while b
            # is reserved to conditional inputs.
            # Store average values
            ma.append(m[not_conditional_feats])
            mb.append(m[conditional_feats])
            # Split the covariance matrix
            saa = s[not_conditional_feats, :][:, not_conditional_feats]
            sab = s[not_conditional_feats, :][:, conditional_feats]
            sba = s[conditional_feats, :][:, not_conditional_feats]
            sbb = s[conditional_feats, :][:, conditional_feats]
            # evaluate multiplicative factor in the mean value
            lbb = np.linalg.inv(sbb)
            sibb.append(lbb)
            mc.append(sab.dot(lbb))
            # evaluate covariance matrix
            s = saa - sab.dot(lbb.dot(sba))
            # calculate other useful values
            fa.append(np.sqrt(np.linalg.det(s) * f0a))
            fb.append(np.sqrt(np.linalg.det(sbb) * f0b))
            si.append(np.linalg.inv(s))

        # Initialize some variables
        n_random = mod_random_event.shape[0]
        n_known = mod_known_event.shape[0]
        gauss = np.zeros((n_known, n_random, self.n_density_functions))
        new_pi = np.zeros((n_known, self.n_density_functions))

        # Evaluate new weights
        for ii_cond, y_cond in enumerate(mod_known_event):
            for k in range(self.n_density_functions):
                for values in product(*self.neighborhood):
                    xi = y_cond - mb[k] + np.array(values)[conditional_feats]
                    eta = -0.5 * xi.dot(sibb[k].dot(xi.T))
                    new_pi[ii_cond, k] += np.exp(eta)
                new_pi[ii_cond, k] /= fb[k]
                new_pi[ii_cond, k] *= self.pi[k]
        new_pi /= np.sum(new_pi, axis=1)

        # Evaluate Gaussian distributions
        for ii, ii_cond in product(range(n_random), range(n_known)):
            y = mod_random_event[ii, :]
            y_cond = mod_known_event[ii_cond, :]
            for k in range(self.n_density_functions):
                gauss[ii_cond, ii, k] = 0.
                m = ma[k] + mc[k].dot(y_cond - mb[k])
                for values in product(*self.neighborhood):
                    xi = y - m + np.array(values)[not_conditional_feats]
                    eta = -0.5 * xi.dot(si[k].dot(xi.T))
                    gauss[ii_cond, ii, k] += np.exp(eta)
                gauss[ii_cond, ii, k] /= fa[k]

        # Evaluate posterior probability
        gamma = gauss.copy()
        for ii in range(n_random):
            gamma[:, ii, :] *= new_pi
        lf = self._scaling_factor(random_event, not_conditional_feats)
        pdf = np.sum(gamma, axis=2) / lf
        pdf[:, lf < 1e-100] = 0.

        return pdf

    def _evaluate_kmeans(self):
        """
        Calculate the K-means clustering.

        Returns
        -------
        cluster : list
                  list that matches, for all elements of the training dataset,
                  the index of the cluster where they belong.
        """

        # Calculate initial means
        n = min(1000, self.n)
        l = range(n)
        np.random.shuffle(l)
        samples = self.mod_samples[l[:n], :]
        distances = np.zeros((n, n))
        for i1, i2 in combinations(range(n), 2):
            distances[i1, i2] = self._how_far_are(samples[i1, :], samples[i2, :], True)
            distances[i2, i1] = distances[i1, i2]

        indices = list(np.unravel_index(distances.argmax(), distances.shape))
        while len(indices) < self.n_density_functions:
            d = 0.
            for ii in indices:
                d += distances[ii, :]

            # The new clustroid cannot have been already selected
            index = d.argmax()
            while index in indices:
                d[index] = -1
                index = d.argmax()
            indices.append(index)
        self.mu = [samples[index] for index in indices]

        err = 1e30
        while err > 1e-3:
            # Expectation
            cluster = [0 for _ in range(self.n)]
            n = [0 for _ in range(self.n_density_functions)]
            for index, v in enumerate(self.mod_samples):
                d = 1e30
                for k, m in enumerate(self.mu):
                    if self._how_far_are(v, m, True) < d:
                        c = k
                        d = self._how_far_are(v, m, True)
                cluster[index] = c
                n[c] += 1

            # Maximization
            weights = np.zeros((self.n, self.n_density_functions))
            for ii, c in enumerate(cluster):
                weights[ii, c] = 1.
            mu = self._calculate_mean(self.mod_samples, weights)

            # Evaluate error
            err = 0.
            for m0, m1 in zip(self.mu, mu):
                err = max(err, np.linalg.norm(m0 - m1, np.inf))
            self.mu = mu

        return cluster

    def _how_far_are(self, samples1, samples2, provide_global_distance=False):
        """
        Evaluate the distance between two vectors.

        Parameters
        ----------
        samples1 : Numpy Array
                   first point

        samples2 : Numpy Array
                   first point
        provide_global_distance : Boolean.
                                  Shall the method provide the global,
                                  Euclidean distance or a list of distances in
                                  each direction?

        Returns
        -------
        out : float or list
              global distance or a list of distances for aech feat. Global
              distance is the root square of the sum of squared distances
              for each feat.
        """
        if provide_global_distance and not self.are_there_periodic_feats:
            return np.linalg.norm(samples1 - samples2)
        else:
            list_of_d = []
            for k in range(self.d):
                x1 = samples1[k]
                x2 = samples2[k]
                xu = max(x1, x2)
                xl = min(x1, x2)
                s = 1 if x1 == xu else -1.
                if self.list_of_density_functions[k] == "Periodic":
                    d = s * min(xu - xl, xl + 2. * np.pi - xu)
                else:
                    d = s * (xu - xl)
                list_of_d.append(d)
            if provide_global_distance:
                return np.sqrt(np.sum([x ** 2 for x in list_of_d]))
            else:
                return list_of_d

    def _calculate_mean(self, samples, weights):
        """
        Evaluates the weighted mean of some data, <samples>. <samples> is an array of size N by
         D, where N is the number of points of dimension D which are grouped in
         K clusters.
        The weights, W, are provided by the user and for each row of W, the sum
         should be one. sum_k=1^K W_ik = 1.

        Return the list of weighted means.

        Parameters
        ----------
        samples : NumPy array-like
                  Data set, an array of NxD.
        weights : NumPy array-like
                  Weights, an array of NxK

        Returns
        -------
        mu : List
             K lists of D-dimensional arrays

        """
        # Initialize values
        mu = [np.zeros(self.d) for _ in range(self.n_density_functions)]
        nk = np.sum(weights, 0)
        for k in range(self.n_density_functions):
            mu[k] = np.sum(samples * weights[:, k:k + 1], 0) / nk[k]

        # For each dimension evaluate mean
        for d in range(self.d):
            # Periodic variables require an special treatment
            if self.list_of_density_functions[d] == "Periodic":
                z0 = np.exp(1j * samples[:, [d]])
                c0 = np.sum([z.real * w for z, w in zip(z0, weights)], 0)
                s0 = np.sum([z.imag * w for z, w in zip(z0, weights)], 0)
                r20 = [(c / n) ** 2 + (s / n) ** 2 for n, s, c in zip(nk, s0, c0)]
                s0 = [np.sqrt((n / (n - 1.)) * (r2 - 1. / n))
                      for n, r2 in zip(nk, r20)]
                zs = np.sum(z0 * weights, 0)
                zm = [z / n for n, z in zip(nk, zs)]
                for k in range(self.n_density_functions):
                    mu[k][d] = (np.log(zm[k] / s0[k])).imag % (2. * np.pi)

        return mu
