# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import pandas as pd
from time import time
from itertools import product
from mathlas.surrogate_modelling.rbf_setup import RBFSetup


class BayesianRegression(RBFSetup):

    def __init__(self, training_data, result_column, rescaling="standarizing", path=None):

        super(BayesianRegression, self).__init__(training_data, result_column, True, rescaling,
                                                 path)
        self.variance_reference = None
        self.evidences = None
        self.params = {}
        self.best_model = None
        self.alpha, self.beta = None, None
        self.A, self.B, self.C = None, None, None
        self.mean = self.training_data[self.result_label].mean()
        self.std = self.training_data[self.result_label].std()

    def train(self, rbf_label, scale_parameter, centres=None, training_method="conventional"):
        """
        Trains a liner regression model

        Parameters
        ----------
        rbf_label : string
                    name of the family of basis functions
        scale_parameter : float
                          scaling value used by the basis functions
        centres : integer, NumPy array, tuple or None
                  number or location of the RBFs
        training_method : string
                          "conventional" or "bayesian" for a model based on maximum-a-priori or
                           bayesian approaches, respectively.

        Returns
        -------
        None
        """

        self.training_method = training_method
        if training_method == "conventional":
            return self.train_conventional(rbf_label, scale_parameter, centres)
        elif training_method == "bayesian":
            return self.train_bayesian(rbf_label, scale_parameter, centres)
        else:
            raise ValueError("Inputted method to train regression is not recognized")

    def train_conventional(self, rbf_label, scale_parameter, centres=None, are_centres_normalized=True):
        """
        Trains a liner regression model using a maximum-a-priori approach

        Parameters
        ----------
        rbf_label : string
                    name of the family of basis functions
        scale_parameter : float
                          scaling value used by the basis functions
        centres : integer, NumPy array, tuple or None
                  number or location of the RBFs

        Returns
        -------
        None
        """
        # Initialize basis
        self.rbf_label = rbf_label
        self.rbf_function = self.rbfs[rbf_label]
        self._set_centres(centres, are_centres_normalized)
        self.scale_parameter = scale_parameter
        # Calculate the covariance matrix and, from it, the
        # linear coefficients
        self._build_covariance_matrix()

        if self.rbf_centres is None:
            self.w = np.linalg.solve(self.C, self.normal_values)
        else:
            a = self.C.dot(self.C.T)
            b = self.C.dot(self.normal_values)
            self.w = np.linalg.solve(a, b)

        predicted_values = self.C.T.dot(self.w)
        e = predicted_values - self.normal_values
        self.variance_reference = e.T.dot(e) / self.n_samples

    def train_bayesian(self, rbf_label, scale_parameter,
                       centres=None, are_centres_normalized=False,
                       alpha=None, beta=None):
        """
        Trains a liner regression model using the Bayesian Linear Regression algorithm

        Parameters
        ----------
        rbf_label : string
                    name of the family of basis functions
        scale_parameter : float
                          scaling value used by the basis functions
        centres : integer, NumPy array, pandas DataFrame, tuple or None
                  number or location of the RBFs
        are_centres_normalized : Boolean
                                 are the provided centres normalized?
        alpha, beta : floats
                      parameters of the prior distribution

        Returns
        -------
        None
        """

        # Initialize basis
        self.rbf_label = rbf_label
        self.rbf_function = self.rbfs[rbf_label]
        self._set_centres(centres, are_centres_normalized)
        self.scale_parameter = scale_parameter
        # Calculate the covariance matrix and, from it, the linear coefficients
        self._build_covariance_matrix()

        alpha = 1. if alpha is None else alpha
        beta = 1. if beta is None else beta
        y = self.C.dot(self.normal_values)

        id_matrix = np.eye(self.C.shape[0])
        while True:
            a = beta * self.C.dot(self.C.T)
            b = a + alpha * id_matrix
            self.w = beta * np.linalg.solve(b, y)
            predicted_values = self.C.T.dot(self.w)
            e = self.normal_values - predicted_values

            eigenvalues, _ = np.linalg.eigh(a)
            gamma = np.sum([l / (l + alpha) for l in eigenvalues])
            new_alpha = gamma / (np.linalg.norm(self.w) ** 2 + 1e-16)
            new_beta = (self.n_samples - gamma) / np.linalg.norm(e) ** 2

            if ((np.abs((new_alpha - alpha) / alpha) < 1e-4 or np.abs(new_alpha - alpha) < 1e-4) and
                    (np.abs((new_beta - beta) / beta) < 1e-4) or np.abs(new_beta - beta) < 1e-4):
                break
            else:
                alpha = new_alpha
                beta = new_beta

        self.alpha, self.beta = alpha, beta
        self.A = alpha * id_matrix + beta * self.C.dot(self.C.T)
        self.variance_reference = 1. / beta

        evidence = 0.5 * (self.n_centres * np.log(self.alpha) + self.n_samples * np.log(self.beta) -
                          np.log(np.linalg.det(self.A)) - self.n_samples * np.log(2. * np.pi) -
                          self.beta * np.linalg.norm(e) ** 2 -
                          self.alpha * np.linalg.norm(self.w) ** 2)

        return evidence

    def train_optimal_evidence(self, rbf_labels, scale_parameters,
                               list_of_centres, are_centers_normalized):
        """
        Chooses the best model from a list maximizing the evidence function

        Parameters
        ----------
        rbf_labels : iterable of strings
                     list of names of the family of basis functions
        scale_parameters : iterable of floats
                           iterable of scaling values used by the basis functions
        list_of_centres : iterable
                          list of number/locations of the RBFs
        are_centers_normalized : boolean
                                 Are locations of the centres already normalized?
        """

        self.training_method = "bayesian"
        best_evidence = -np.inf
        best_model = None
        cases = []
        for rbf_label, centres in product(rbf_labels, list_of_centres):
            if rbf_label in ["Linear spline", "Cubic spline", "Thin plate spline"]:
                cases.append([rbf_label, None, centres])
            else:
                for scale_parameter in scale_parameters:
                    cases.append([rbf_label, scale_parameter, centres])

        self.evidences = pd.DataFrame(np.zeros((len(cases), 4)), index=range(len(cases)),
                                      columns=["evidence", "alpha", "beta", "Wall time"])
        for i, case in enumerate(cases):
            rbf_label, scale_parameter, centres = case
            t0 = time()
            evidence = self.train_bayesian(rbf_label, scale_parameter, centres, are_centers_normalized)
            t1 = time()
            self.evidences.loc[i, "evidence"] = evidence
            self.evidences.loc[i, "alpha"] = self.alpha
            self.evidences.loc[i, "beta"] = self.beta
            self.evidences.loc[i, "Wall time"] = t1 - t0
            self.params[i] = case

            if evidence > best_evidence:
                best_evidence = evidence
                best_model = (rbf_label, scale_parameter, centres,
                              are_centers_normalized, self.alpha,
                              self.beta)

        self.best_model = best_model if best_model is not None else "NOT FOUND"
        if best_model is not None:
            self.train_bayesian(*self.best_model)

    def predict(self, prediction, provide_variance=False):
        """
        Get the constructed estimator's prediction at the given points.

        Calculates the best linear unbiased prediction (BLUP) for the given
        point. If `provideRMSE` is `True` the method also provides the
        root-mean-square error at those points.

        Parameters
        ----------
        prediction : Pandas DataFrame
                     Positions of the points where the estimation is
                           requested.
                           If predictionPoints is a DataFrame or the
                           `sampleValues` parameter used to construct the model
                           was one, the returned BLUP will also be a DataFrame
                           whose indices will match those of predictionPoints.

                           predictionPoints must either be a 1D or 2D array.
                           If it is a 1D array, the parameter is assumed to
                           contain the coordinates of a single point.

        provide_variance : boolean, optional
                           Must the method return the uncertainty? Default is False.

        Returns
        -------
        out : tuple or NumPy array-like
        If `provideRMSE` is `False`:
            A NumPy array or Pandas DataFrame with the BLUP.
        If `provideRMSE` is `True`:
            A tuple with the BLUP and a NumPy array with the RMSE.

        Raises
        ------
        ValueError
            If the number of columns in `points` does not match the number of
            parameters used to create the model.
        """

        # If the optimization of the evidence function did not found a good solution mean and
        #  variance of the data are returned
        if self.best_model == "NOT FOUND":
            prediction[self.result_label] = self.mean
            prediction["variance"] = self.std ** 2
            if provide_variance:
                return prediction[[self.result_label, "variance"]].values
            else:
                return prediction[self.result_label].values

        normal_prediction = self.normalize(prediction[self.data_columns].values,
                                           self.points_mean, self.points_std)
        n_predictions = normal_prediction.shape[0]
        if self.rbf_label in ["Linear spline", "Cubic spline",
                              "Thin plate spline", "Quadric spline"]:
            phi = np.zeros((self.n_centres, n_predictions))
        elif self.rbf_label in ["Gaussian", "Multiquadric", "Inverse quadric",
                                "Inverse multiquadric"]:
            phi = np.ones((self.n_centres, n_predictions))
        else:
            raise ValueError("Value of <rbf_label> unrecognized.")
        for r, xt in enumerate(self.rbf_centres):
            for p, x in enumerate(normal_prediction):
                phi[r, p] = self.rbf_function(x, xt)

        y = phi.T.dot(self.w)
        prediction[self.result_label] = self.denormalize(y, self.values_mean, self.values_std)

        if provide_variance:
            if self.training_method == "conventional":
                if self.rbf_centres is None:
                    # This is a constant term that evaluates self.rbf(x, x) in each of the points.
                    # Since the rbfs depend on the distance between the two given points (which is
                    #  0), it is just evaluated once here.
                    bias = self.rbf_function(0., 0.)
                    var = np.sqrt(bias - np.diag(phi.T.dot(np.linalg.solve(self.C, phi))) + 1.e-16)
                else:
                    var = self.variance_reference * np.ones((n_predictions, 1))
            elif self.training_method == "bayesian":
                var = 1. / self.beta + np.diag(phi.T.dot(np.linalg.solve(self.A, phi)))
            else:
                raise ValueError("Inputted method to train regression is not recognized")

            prediction["variance"] = (self.values_std ** 2) * var
            return prediction[[self.result_label, "variance"]].values
        else:
            return prediction[self.result_label].values



    # def get_pdf(self, X, Y, productOfVariables=False):
    #     """
    #     Calculates the probability of getting a response Y if X has happened,
    #     i.e., p(Y|X).
    #
    #     Parameters
    #     ----------
    #     X : NumPy array.
    #         Non-normalized values of the feats. `M1xK` size.
    #
    #     Y : NumPy array.
    #         Non-normalized values of the responses whose probability must be
    #         calculated. `M2x1`
    #
    #     productOfVariables : Boolean.
    #                          If True, M1 must be equal to M2 and the
    #                          probability, p(Y[j, d]|X[j,:]) is calculated for
    #                          each row; thus, the result is an array of size
    #                          `Mx1` where `M=M1=M2`. If False For each value of
    #                          the response and each point in the features space
    #                          a probability is calculated. In this case an array
    #                          of size `M1xM2` is obtained; it has the values
    #                          p(Y[j,d]|[X[k, :]).
    #
    #     Returns
    #     -------
    #     out : Numpy array of floats.
    #           2D array pdf[j,d] = p(Y[j,d]|[X[j, :]) or
    #           3D array pdf[j,k,d] = p(Y[j,d]|[X[k, :])
    #           It may be a 1D or 2D array (see `productOfVariables` above).
    #     """
    #
    #     BLUP, RMSE = self.get_prediction(X, provideRMSE=True)
    #
    #     if productOfVariables:
    #         f = (2. * np.pi * RMSE)
    #         pdf = []
    #         for y in Y:
    #             e = -0.5 * ((BLUP - y) / RMSE) ** 2
    #             pdf.append(np.exp(e) / f)
    #     else:
    #         f = np.sqrt(2. * np.pi) * (RMSE + 1e-300)
    #         e = -0.5 * ((BLUP - Y) / RMSE) ** 2
    #         pdf = np.exp(e) / f
    #
    #     return np.asarray(pdf)
    #
    # def get_cdf_quantile(self, X, Y):
    #     """
    #     Evaluates the quantile for a given response.
    #
    #     Parameters
    #     ----------
    #     X : float or NumPy array.
    #         Non-normalized values of the feats.
    #
    #     Y : float or NumPy array.
    #         Non-normalized values of the responses.
    #
    #     Returns
    #     -------
    #     Q : float or Numpy array.
    #         Quantiles.
    #     """
    #     # Evaluate predictions
    #     BLUP, RMSE = self.get_prediction(X, provideRMSE=True)
    #     # Convert Y if required
    #     if ((isinstance(Y, (int, float)) and BLUP.shape[1] == 1) or
    #             (Y.shape[0] == 0 and BLUP.shape[1] == Y.shape[1])):
    #         Y = [Y] * len(X)
    #         Y = np.array(Y)
    #     # Calculate quantiles
    #     Q = []
    #     for y, mu, stdev in zip(Y, BLUP, RMSE):
    #         Q.append(norm.cdf(y, loc=mu, scale=stdev))
    #     return np.asarray(Q)
    #
    # def get_cdf_point(self, X, Q):
    #     """
    #     Evaluates the value of the response for a given quantile.
    #
    #     Parameters
    #     ----------
    #     X : float or NumPy array.
    #         Non-normalized values of the feats.
    #
    #     Q : float or Numpy array.
    #         Quantiles.
    #
    #     Returns
    #     -------
    #     Y : float or NumPy array.
    #         Non-normalized values of the responses.
    #     """
    #     # Evaluate predictions
    #     BLUP, RMSE = self.get_prediction(X, provideRMSE=True)
    #     # Convert Y if required
    #     if ((isinstance(Q, (int, float)) and BLUP.shape[1] == 1) or
    #             (Q.shape[0] == 0 and BLUP.shape[1] == Q.shape[1])):
    #         Q = [Q] * len(X)
    #         Q = np.array(Q)
    #     # Calculate points where quantiles are reached
    #     Y = []
    #     for q, mu, stdev in zip(Q, BLUP, RMSE):
    #         Y.append(norm.ppf(q, loc=mu, scale=stdev))
    #     return np.asarray(Y)
