# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from mathlas.plotting.mathlas_plots import MathlasPlot
from scipy.special import iv


def mixture_of_vonmises(D, K):
    """TODO: mixture_of_vonmises lacks docstring"""

    N = len(D)
    mu = np.array([2. * np.pi * ii / K for ii in range(K)])
    sg = np.array([10. for _ in range(K)])
    pi = [1. / K] * K

    # Estimate loglikelihood value
    f = [1. / (2. * np.pi * iv(0, sg[ii])) for ii in range(K)]
    loglikelihood = 0.
    for d in D:
        for ii in range(K):
            loglikelihood += (np.log(pi[ii]) + np.log(f[ii]) +
                              sg[ii] * np.cos(d - mu[ii]))
    loglikelihood /= N

    err = 1e30
    while err > 1e-3:
        # Expectation step
        pk = []
        for d in D:
            l = []
            for ii in range(K):
                p = f[ii] * np.exp(sg[ii] * np.cos(d - mu[ii]))
                l.append(pi[ii] * p)
            pk.append(l)
        pk = np.array(pk)
        for ii in range(N):
            pk[ii, :] /= np.sum(pk[ii, :])

        # Maximization step
        pi = [np.sum(pk[:, ii]) / N for ii in range(K)]
        E = [np.sum(pk[:, ii] * np.exp(1j * D)) for ii in range(K)]
        M = [np.abs(c) for c in E]
        rho = np.array([m / np.sum(pk[:, ii]) for ii, m in enumerate(M)])
        mu = np.array([np.log(c / m).imag % (2 * np.pi) for c, m in zip(E, M)])

        sg = []
        for Re in rho:
            k = min(Re * (2. - Re ** 2) / (1 - Re ** 2), 700)
            f = iv(1, k) / iv(0, k) - Re
            should_loop = np.abs(f) > 1e-6
            while should_loop:
                km = k - 1e-4
                kp = k + 1e-4
                fm = iv(1, km) / iv(0, km)
                fp = iv(1, kp) / iv(0, kp)
                df = (fp - fm) / 2e-4
                dk = f / df
                while k - dk < 0:
                    dk /= 2
                k -= dk
                if k > 700:
                    k = 700.
                    break
                f = iv(1, k) / iv(0, k) - Re
                should_loop = np.abs(dk) > 1e-3 and np.abs(f) > 1e-6
            sg.append(k)

        # Estimate loglikelihood value
        f = [1. / (2. * np.pi * iv(0, sg[ii])) for ii in range(K)]
        old_loglikelihood = loglikelihood
        loglikelihood = 0.
        for d in D:
            for ii in range(K):
                loglikelihood += (np.log(pi[ii]) + np.log(f[ii]) +
                                  sg[ii] * np.cos(d - mu[ii]))
        loglikelihood /= N
        err = np.abs((old_loglikelihood - loglikelihood) / old_loglikelihood)

    return pi, mu, sg

if __name__ == "__main__":

    # ========================================================================
    N = 5000
    # ========================================================================
    pi1 = 0.4
    mu1 = 3. * np.pi / 2.
    sg1 = 0.5 * np.pi
    # ========================================================================
    pi2 = 0.6
    mu2 = np.pi / 2.
    sg2 = 0.1 * np.pi
    # ========================================================================

    # Generate data
    V1 = (mu1 + sg1 * np.random.randn(int(N * pi1))) % (2. * np.pi)
    V2 = (mu2 + sg2 * np.random.randn(int(N * pi2))) % (2. * np.pi)
    V = np.concatenate((V1, V2))

    # Create model
    K = 2
    pi, mu, sg = mixture_of_vonmises(V, K)

    # Predict
    X = np.arange(0., 2. * np.pi, 0.01)
    Y = []
    f = [1. / (2. * np.pi * iv(0, sg[ii])) for ii in range(K)]
    for indx in range(len(X)):
        x = X[indx]
        y = 0.
        for ii in range(K):
            y += pi[ii] * f[ii] * np.exp(sg[ii] * np.cos(x - mu[ii]))
        Y.append(y)

    # Plot
    plt = MathlasPlot()
    plt.hist(V, nBins=72, color="o")
    plt.plot(X, Y)
    plt.show()
