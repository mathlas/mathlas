# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.surrogate_modelling.surrogate_object import SurrogateObject
import numpy as np
import pandas as pd
from itertools import product


class QfoldCV(SurrogateObject):

    def __init__(self, obj, sampledPoints, sampleValues, Q, params,
                 shuffle="no", path=None):
        """
        Performs a q-fold cross-validation process.

        Parameters
        ----------
        sampledPoints : NumPy array or Pandas DataFrame, optional
                        Co-ordinates of the sampled points. The array must
                        contain one row with co-ordinates per sampled point.
                        The number of rows in `sampledPoints` must match the
                        number of rows in `sampleValues`.

                        If `sampledPoints` is None (default), `sample` must be
                        provided.

                        The data must be provided as a 1D or 2D ndarray.

        sampleValues : NumPy array or Pandas DataFrame, optional
                       Value of the sampled function at the sampled points.
                       The number of rows in `sampleValues` must match the
                       number of rows in `sampledPoints`.

                       If None (default), `sample` must be provided.

                       The data must be provided as a 1D or 2D ndarray.

        Q : integer
            Number of partitions used.

        params : dictionary
                 lists of the parameters to optimized. User must provide which
                 values are tested.

        shuffle : boolean, optional
                  Should data be shuffled before perform partitions?
                  Default is False.

        path : path to a pickled cache object, optional
              Generate the Kriging object from a pickled cache file
              instead of from the given params. The pickled file
              must have been generated with the ``cache()`` method
              of the object in a previous run.

              If given, the rest of the parameters will be ignored.

        Raises
        ------
        TypeError
            * If `param` is not a number, integer, or float.

        ValueError
            * If not enough info about sampled points and their values is
              given.
            * If the dimensions of the various parameters are not coherent.
        """

        super(QfoldCV, self).__init__()

        # If given a path, try to load from there, otherwise reconstruct the
        # object
        if path:
            self._load(path)
            return

        # Try to convert the samplePoints entity into an array,
        # also, make sure it's composed by doubles
        sampledPoints = np.asarray(sampledPoints, dtype=np.float64)

        dim = len(sampledPoints.shape)
        if dim == 1:
            sampledPoints = sampledPoints.reshape((sampledPoints.shape[0],
                                                   1))
        elif dim != 2:
            raise ValueError('Sampled points must be given in a 2D ' +
                             'array')

        self.nSampledPoints, self.nParameters = sampledPoints.shape

        # Working with ndarrays is faster than directly operating
        # the DataFrame, so we convert data into that
        self.columns = None
        if isinstance(sampleValues, pd.Series):
            self.columns = sampleValues.index
            sampleValues = sampleValues.values
        elif isinstance(sampleValues, pd.DataFrame):
            self.columns = sampleValues.columns
            sampleValues = sampleValues.values
        else:
            sampleValues = np.asarray(sampleValues, dtype=np.float64)

        dim = len(sampleValues.shape)
        if dim == 1:
            sampleValues = sampleValues.reshape((sampleValues.shape[0], 1))
        elif dim != 2:
            raise ValueError('Sampled values must be given in a 2D ' +
                             'array')

        if sampleValues.shape[0] != self.nSampledPoints:
            raise ValueError("The number of sampled points does not " +
                             "match the number of sample values")

        self.nFunctions = sampleValues.shape[1]

        # Store the samples and shuffle if required
        self.sampledPoints = sampledPoints
        self.sampledValues = sampleValues
        if shuffle.lower() == "yes":
            indices = range(self.nSampledPoints)
            np.random.shuffle(indices)
            self.sampledPoints = self.sampledPoints[indices]
            self.sampledValues = self.sampledValues[indices]

        # Create partitions
        if shuffle.lower() in ["yes", "no"]:
            partitions = self.partition(range(self.nSampledPoints), Q)
        elif shuffle.lower() == "one-out-of":
            partitions = [range(q, self.nSampledPoints, Q) for q in range(Q)]
        else:
            raise ValueError("<shuffle> has an unrecognized value.")
        # Unfold values of the parameters to iterate over
        paramLabels = params.keys()
        paramValues = [params[label] for label in paramLabels]
        # Loop over all the values of the parameters
        inputs, Etr, Ecv = [], [], []
        minError = 1e300
        for values in product(*paramValues):
            # Initialize inputs and temporal variables
            kwargs = {label: value for label, value
                      in zip(paramLabels, values)}
            E = np.empty(self.nSampledPoints)
            foo, bar = 0., 0.
            thisBagOfModels = []
            for q in range(Q):
                # Create lists of indices for training and cross-validation
                indx_cv = partitions[q]
                indx_tr = [ii for ii in range(self.nSampledPoints)
                           if ii not in indx_cv]
                # Train model and store if required
                model = obj(self.sampledPoints[indx_tr],
                            self.sampledValues[indx_tr], **kwargs)
                thisBagOfModels.append(model)
                # Calculate cross-validation error
                pred = model.get_prediction(self.sampledPoints[indx_cv])
                E[indx_cv] = np.abs(self.sampledValues[indx_cv] - pred)
                # Calculate training error
                pred = model.get_prediction(self.sampledPoints[indx_tr])
                foo += np.sum((self.sampledValues[indx_tr] - pred) ** 2)
                bar += len(indx_tr)
            # Store values for further reference
            inputs.append(kwargs)
            Etr.append(np.sqrt(foo / bar))
            Ecv.append(np.sqrt(np.sum(E ** 2)) / self.nSampledPoints)
            # Update optimal solution
            if Ecv[-1] < minError:
                minError = Ecv[-1]
                optParams = inputs[-1]
                bagOfModels = list(thisBagOfModels)
        # Store solution(s) for further use
        self.cvError = minError
        self.optParams = optParams
        self.bagOfModels = bagOfModels
        # Train a global model
        self.globalModel = obj(self.sampledPoints, self.sampledValues,
                               **optParams)

    def get_prediction(self, predictionPoints, provideRMSE=False, bagging=False,
                       **kwargs):
        """
        Performs a prediction

        Parameters
        ----------
        predictionPoints : NumPy array-like or Pandas DataFrame
                           Positions of the points where the estimation is
                           requested.
                           If predictionPoints is a DataFrame or the
                           `sampleValues` parameter used to construct the model
                           was one, the returned BLUP will also be a DataFrame
                           whose indices will match those of predictionPoints.

                           predictionPoints must either be a 1D or 2D array.
                           If it is a 1D array, the parameter is assumed to
                           contain the coordinates of a single point.

        provideRMSE : boolean, optional
                      Must the method return the uncertainty? Default is False.

        bagging : boolean, optional
                  Must the bagging technique be used? Default is False.

        Returns
        -------
        out : tuple or NumPy array-like
        If `provideRMSE` is `False`:
            A NumPy array or Pandas DataFrame with the BLUP.
        If `provideRMSE` is `True`:
            A tuple with the BLUP and a NumPy array with the RMSE.
        """
        if bagging:
            if not provideRMSE:
                prediction = np.zeros((predictionPoints.shape[0],
                                       self.nFunctions))
                for model in self.bagOfModels:
                    prediction += model.getPrediction(predictionPoints,
                                                      provideRMSE, **kwargs)
                prediction /= len(self.bagOfModels)
                return prediction
            else:
                prediction = np.zeros((predictionPoints.shape[0],
                                       self.nFunctions))
                rmse = np.zeros((predictionPoints.shape[0], self.nFunctions))
                for model in self.bagOfModels:
                    foo, bar = model.getPrediction(predictionPoints,
                                                   provideRMSE, **kwargs)
                    prediction += foo
                    rmse += bar ** 2
                prediction /= len(self.bagOfModels)
                rmse /= len(self.bagOfModels)
                rmse = np.sqrt(rmse)
                return prediction, rmse
        else:
            return self.globalModel.get_prediction(predictionPoints,
                                                   provideRMSE, **kwargs)

    @staticmethod
    def partition(lst, n):
        """
        Creates a partition of a list in N chunks

        Parameters
        ----------
        lst : iterable
              List of values to divide

        n : integer
            Number of chunks to be created
        """
        d = len(lst) / float(n)
        return [lst[int(round(d * i)): int(round(d * (i + 1)))]
                for i in range(n)]

if __name__ == "__main__":

    pass
