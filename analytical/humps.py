# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


def HumpsFunction1D(x):
    """
    Evaluates humps function

    The Humps function is a one dimensional function that, in :math:`[0,1]`, has
    one maximum in x=0.3 and a lower minimum in x=0.9.

    Parameters
    ----------
    x : float
        x value

    Returns
    -------
    result : float
             Humps function value at x
    """
    result = 1 / ((x - 0.3) ** 2 + 0.01) + 1 / ((x - 0.9) ** 2 + 0.04) - 6
    return result

if __name__ == '__main__':

    import plotly
    import numpy as np
    from mathlas.plotting.mathlas_plots import MathlasPlot
    from plotly.graph_objs import *

    # Get humps values x ∈ [0, 2]
    xmin = 0.
    xmax = 2.
    x = np.linspace(xmin, xmax, 101)

    y = HumpsFunction1D(x)

    plt = MathlasPlot()
    plt.plot(x, y, title='Humps function', axis_labels=('$x$', '$f(x)$'))
    plt.savefig('humps.png')

    trace1 = Scatter(x=x, y=y)
    data = Data([trace1])

    # Make a layout object
    layout = Layout(
        title='Humps function',
        autosize=True,
        width=1000,
        height=700,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        )
    )

    fig = Figure(data=data, layout=layout)
    plotly.offline.plot(fig, filename='humps.html')
