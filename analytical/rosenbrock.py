# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np


def rosenbrock_2d(x, y, a=1, b=100):
    """
    Evaluates the Rosenbrock function

    Given the values of the points co-ordinates (x,y) provides
    the value of the Rosenbrock function. The conventional size of the domain
    is -5<=x<=10, -5<=y<=10. The Rosenbrock function has one global minimum at (x,y) = (a, a^2).

    Parameters
    ----------
    x : float
        x value

    y : float
        y value

    a : float, optional
        Rosenbrock function first constant

        Default value a = 1.0

    b : float, optional
        Rosembrock function second constant

        Default value b = 100.0

    Returns
    -------
    value
    Rosenbrock function value at (x,y)
    """
    return (a - x) ** 2 + b * (y - x**2)**2


if __name__ == '__main__':

    import plotly
    import matplotlib.pyplot as plt
    from plotly.graph_objs import *

    # Plot contours of the Rosembrock function in [-2, 2] X [-2, 3]
    xmin = -2.
    xmax = 2.
    ymin = -2.
    ymax = 3.
    x = np.linspace(xmin, xmax, 51)
    y = np.linspace(ymin, ymax, 51)

    X, Y = np.meshgrid(x, y)
    Z = rosenbrock_2d(X, Y)

    # The Rosembrock function has 1 minimum
    xlocmin = (1,)
    ylocmin = (1,)

    # Plot original figure of merit
    levels = np.logspace(np.log10(np.min(Z)), np.log10(np.max(Z)), 11)
    CS = plt.contour(X, Y, Z, levels=levels, axis_labels=[r'x_1', r'x_2'])
    plt.clabel(CS, inline=1, fontsize=8)
    plt.scatter(xlocmin, ylocmin, s=40)
    ax = plt.gca()
    ax.set_xlabel(r"x_1")
    ax.set_ylabel(r"x_2")
    plt.title("Rosenbrock function")
    plt.savefig("rosenbrock_contours.png")

    # Plot 3D plot of the Rosenbrock function in [-2, 2] X [-2, 3]
    trace1 = Surface(x=x, y=y, z=Z)
    data = Data([trace1])

    # Make a layout object
    layout = Layout(
        title='Rosenbrock function',
        autosize=True,
        width=1000,
        height=700,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        )
    )

    fig = Figure(data=data, layout=layout)
    plotly.offline.plot(fig, filename='rosenbrock.html')
