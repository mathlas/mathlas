# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np


def BraninFunction2D(x, y):
    r"""
    Evaluates the Branin function

    Given the values of the points co-ordinates (x,y) provides the value of the Branin
    function. The conventional domain is -5<=x<=10, 0<=y<=15. The Branin function
    has three global minima in the just mentioned domain, namely:

    * (x,y) = (-pi,12.275)
    * (x,y) = ( pi, 2.275)
    * (x,y) = (9.42478,2.475)

    where it takes the value f(x,y) = 0.397887

    Parameters
    ----------
    x : float
        x value

    y : float
        y value

    Returns
    -------
    float
    Branin function value
    """
    return ((y - x * x * 5.1 / 4 / np.pi / np.pi + x * 5. / np.pi - 6) ** 2. +
            10. * (1. - 1. / 8. / np.pi) * np.cos(x) + 10.)


if __name__ == '__main__':

    import plotly
    import matplotlib.pyplot as plt
    from mathlas.plotting.mathlas_plots import MathlasPlot
    from plotly.graph_objs import *

    # Plot contours of the Branin function in [-5, 10] X [0, 15]
    xmin = -5.
    xmax = 10.
    ymin = 0.
    ymax = 15.
    x = np.linspace(xmin, xmax, 51)
    y = np.linspace(ymin, ymax, 51)

    X, Y = np.meshgrid(x, y)
    Z = BraninFunction2D(X, Y)

    # The Branin function has 3 minima
    xlocmin = (-np.pi, np.pi, 9.42478)
    ylocmin = (12.275, 2.275, 2.475)

    mplt = MathlasPlot(figsize=(11, 10))
    # Plot original figure of merit
    levels = np.logspace(np.log10(np.min(Z)), np.log10(np.max(Z)), 11)
    CS = plt.contour(X, Y, Z, levels=levels, axis_labels=[r'x_1', r'x_2'])
    plt.clabel(CS, inline=1, fontsize=8)
    plt.scatter(xlocmin, ylocmin, s=40)
    ax = plt.gca()
    ax.set_xlabel(r"x_1")
    ax.set_ylabel(r"x_2")
    plt.title("Branin function")
    plt.savefig("branin_contours.png")

    # Plot 3D plot of the Branin function in [-5, 10] X [0, 15]

    # z = [[branin((xelem, yelem)) for yelem in y] for xelem in x]
    # z = [[BraninFunction2D(xelem, yelem) for yelem in y] for xelem in x]

    trace1 = Surface(x=x, y=y, z=Z)
    data = Data([trace1])

    # Make a layout object
    layout = Layout(
        title='Branin function',
        autosize=True,
        width=1000,
        height=700,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        )
    )

    fig = Figure(data=data, layout=layout)
    plotly.offline.plot(fig, filename='branin.html')
    # Uncomment to show
    # plotly.offline.plot(fig)

