# Introduction

![Mathlas logo](doc/_static/mathlas_logo.svg)

While working at Mathlas Consulting we developed a set of methods that we used 
while working on various projects for our clients. The *mathlas* repo 
—as we call it— is now publicly available here under an 
[Apache 2.0 license](COPYING).

This set of methods reflects the work of the almost 4 years 
[Luis S. Lorente Manzanares, PhD](https://www.linkedin.com/in/luis-s-lorente-68178a23), 
& [Diego Alonso Fernández, PhD](https://www.linkedin.com/pub/diego-alonso-fern%C3%A1ndez/46/b85/72a) 
spent at Mathlas, [Joseba Echevarria García](https://josebagar.com)'s almost 
3 years there and our combined previous experience in the various fields 
required to assemble the collection.

The repo contains the project-independent parts of the projects we worked on and 
is composed of different packages, described here:

* [`analytical`](analytical): Analytical function definitions, mainly used for 
  testing optimization methods. Includes Branin, humps and Rosenbrock functions.
* [`dimensionality_reduction`](dimensionality_reduction): Methods related to 
  data dimensionality reduction, including Incremental Association Markov Blanket 
  routines and categorizers.
* [`doe`](doe): Design of experiments methods, including LHC and others.
* [`geo`](geo): Geographical routines, including a fuzzy street names matcher 
  (for use with spanish land registry data) and a geographical distance computer 
  amongst others.
* [`machine_learning`](machine_learning): Various machine learning algorithms, 
  including decision tree based methods (Random Forest, AdaBoost), 
  neural networks & Hidden Markov Models.
* [`misc`](misc): Some stuff we found useful like colour codes for printing 
  formatted text to an ANSI terminal, some graphical notifications code, a 
  progress bar and a simple profiler.
* [`not_for_clients`](not_for_clients): A dragon exception I like to include to 
  indicate conditions which should not happen.
* [`optimization`](optimization): Quite a few numerical optimization code using 
  various algorithms.
* [`plotting`](plotting): Various plotting routines on top of Matplotlib 
  (to get corporate colours and use presentation-friendly font sizes by 
  default), interactive map creation routines based on OpenLayers and some 
  colourmaps.
* [`statistics`](statistics): Various probability distributions, as well as 
  statistical tests and some helper code.
* [`surrogate_modelling`](surrogate_modelling): Various surrogate modelling 
  routines, including various types of probability density mixture models, 
  Kriging models & others

# References

While we developed new methods, we routinely relied on 
[Christopher M. Bishop's](https://research.microsoft.com/~cmbishop) excellent 
[Pattern Recognition and Machine Learning](https://www.microsoft.com/en-us/research/people/cmbishop/#prml-book) 
book. I personally also found Jeremy Kun's 
[Math ∩ Programming](https://jeremykun.com/) blog to be very instructive.

# Disclaimer

Please note that although our idea at Mathlas was always to release these
set of methods as open source, we didn't get to the point of having the repo
ready before we closed the company. Unit tests & documentation are lacking
and comments in code would have had to be reviewed for accuracy
and completeness. We  therefore release the methods hoping they will be useful
to others, but provide them in an AS IS basis.
