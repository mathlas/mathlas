.. mathlas optimization optimizers module documentation

Optimizers
==========

Description
-----------
This module includes the Hooke and Jeeves method for mathematical function
optimization.

Documentation
-------------

.. automodule:: mathlas.optimization.optimizers
   :members: hooke_n_jeeves

