.. mathlas plotting module documentation

MathlasPlots
============

Description
-----------
This module includes methods used to construct and manipulate a plot with
a common Mathlas appearance.

Documentation
-------------

.. module:: mathlas.plotting.mathlas_plots
.. autoclass:: MathlasPlot
    :members:

