.. mathlas surrogate modelling Kriging module documentation

Cross validation methods
========================

Description
-----------
This module includes methods for performing cross validation.

Documentation
-------------

.. automodule:: mathlas.machine_learning.cross_validation
    :members:

