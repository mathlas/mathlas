.. mathlas surrogate modelling Kriging module documentation

Kriging estimator
=================

Description
-----------
This module includes methods used to construct and manipulate a Kriging
estimator.
The functionality includes:

    * Constructing the model from a training database.
    * Obtaining predictions and RMSE values for given points.

Documentation
-------------

.. module:: mathlas.surrogate_modelling.kriging
.. autoclass:: Kriging
    :members:

