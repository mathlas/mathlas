.. mathlas surrogate modelling Mixture of vonMises module documentation

Mixture of vonMises estimator
=============================

Description
-----------
This module includes methods used to construct and manipulate a mixture of
vonMises estimator.
The functionality includes:

    * Constructing the model from a training database.
    * Obtaining predictions and RMSE values for given points.

Documentation
-------------

.. automodule:: mathlas.surrogate_modelling.mixture_of_vonmises
    :members:

