.. mathlas repository documentation master file

Mathlas common utilities
========================

We have some very good utilities that you can use to be happy. Here are some
of them.

Surrogate modelling
-------------------

.. toctree::
   :maxdepth: 2

   surrogate_modelling/Kriging
   surrogate_modelling/RBF
   surrogate_modelling/mixture_of_vonmises

Design of Experiments
---------------------

.. toctree::
   :maxdepth: 2

   doe/Repulse_Dynamical_Sytem
   doe/LHC
   doe/Sample
   doe/seq_select

Machine Learning
----------------

.. toctree::
   :maxdepth: 2

   machine_learning/cross_validation

Optimization
------------

.. toctree::
   :maxdepth: 2

   optimization/optimizers

Plotting
--------

.. toctree::
   :maxdepth: 2

   plotting/MathlasPlots

Statistics
----------

.. toctree::
   :maxdepth: 2

   statistics/Guilbaud

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

