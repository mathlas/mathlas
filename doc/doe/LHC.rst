.. mathlas DoE module documentation

LHC
===

Description
-----------
This module includes methods used to construct and manipulate a LHC model.

Documentation
-------------

.. automodule:: mathlas.doe.lhc
    :members:

