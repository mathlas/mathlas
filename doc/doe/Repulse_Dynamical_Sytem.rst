.. mathlas DoE module documentation

Repulse Dynamical Sytem
=======================

Description
-----------
This module includes methods used to construct and manipulate a DoE model.

Documentation
-------------

.. automodule:: mathlas.doe.repulse_dynamical_system
    :members:

