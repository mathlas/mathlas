# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import base64
import tempfile
import subprocess
from pathlib import Path
from mathlas.plotting import colormaps
from mathlas.misc.ansi_color import RED, END


class LocationHeatMap:
    """
    Class that can be used to plot a location heatmap using OpenLayers 4
    """
    __basemap_variants = {'carto': ('light_all', 'dark_all',
                                    'light_nolabels', 'light_only_labels',
                                    'dark_nolabels', 'dark_only_labels'),
                          'osm': None,
                          'wikimedia': None,
                          'stamen': ('toner', 'toner-hybrid', 'toner-labels', 'toner-lines',
                                     'toner-lite', 'watercolor', 'terrain', 'terrain-background',
                                     'terrain-labels', 'terrain-lines', 'toner-background')}
    __supported_heatmaps = {'jet': colormaps.jet_hex,
                            'viridis': colormaps.viridis_hex,
                            'inferno': colormaps.inferno_hex,
                            'magma': colormaps.magma_hex,
                            'plasma': colormaps.plasma_hex}
    # Some generic marker, in case the user does not provide one
    __marker_b64_encoded = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAAB4CAYAAAC6oA/pAAA' \
                           'ABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUA' \
                           'd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAZrSURBVHic7Z1LbFRVGMd/50yZDqVT+qDhISrGm' \
                           'GiMiTHGjYlK0LRUDQsjK2NYGF0UCGg0EDCOJoomGkJbXaCJJsoGH4maCoZFNWEhC+JGTY' \
                           'gvUKAUaIHSl9OZOS5uSWnvudN+0DszwPdbzZxzT/O/v9y599xzm/sZZiKzN8nZ9EqMWQP' \
                           'uXnA3gVkMVM84tnLJAn3AcQw/49zXNA39QGZtttggE9mzridF3eh6nNkCNM1t1oqkH8wO' \
                           'Lla/zycrx3wb+GW1dz+ENZ+CuSXWeJXJMSzPsGv1wekdNrTpxv3PYe2BG1QUwK0U6GHj/' \
                           'vbpHVOPrA371gEflyhU5WN4gY7Vuye/XqK9+6HgiCIZOdg5GB+F/Di4Qqw5Y8VYSMyDef' \
                           'PBRJ+2gSyWVZd+ksGW63pSpEePRP70clkYPAkj56BwDUuajk3A/HpYuBSqIi/uR8HdSWf' \
                           'bf8E5q250faSo4QHo/RWG+q8vUQCFPAz3Q+9vMDIQtdUKjGkHMGT2JulPn8Q3PRgegP6/' \
                           '4wtbaSy6DWoafT1n+a95maW/9hF8onJZGDgWc7oKo/9osN9hFlF99mEbzMw9XDhxbZ/Er' \
                           'wTn4EJvRGdhjcVxv3fQ6Pl4g1UqI+ciDhJznwUWh9rHR66/k/lscXkY997tLLPAklBzPh' \
                           'd3pMomP+5rXWrxrR7caOeq6fj3vzp8b6hEorIEqCwBKkuAyhKgsgSoLAEqS4DKEqCyBKg' \
                           'sASpLgMoSoLIEqCwBKkuAyhKgsgSoLAEqS4DKEqCyBKgsASpLgMoSoLIEqCwBKkuAyhKg' \
                           'sgSoLAEqS4DKEqCyBKgsASpLgMoSoLIEqCwBKkuAyhKgsgSoLAEqS4DKEqCyBKgsASpLg' \
                           'MoSoLIEqCwBKkuAyhKgsgSoLAEqS4DKEqCyBKgsASpLgMoSoLIEqCwBKkuAyhKgsgSoLA' \
                           'EqS4DKEqCyBKgsARZwodbiL5S//vHvv7PA2VCzrYo7TmXj3X932gLh11RHv3X/xsC7/+a' \
                           'UBfNXqD0xD5IL4g9ViVQvCPY/zJ8WCt95B9WFX+Z9Q5AOv5c7wHVb8u5bIPzm5ZoGqKmP' \
                           'NVfFMT9ynwvYZLflg8dPYfjSO7hpBaTSccarHFLpYH+9mM/Z9WjfxDwrsQ0IvwTdJKD5D' \
                           'li4LPh8PWISUL8s2E/rnXZmobANLi9StH7fWxi2Rv7RQg5GLkB2GPLZoHTDlVDIQXbkys' \
                           'bCZHGhq8EYSCSDi1jNwuJTJWfepKt1O8DkVosObWfggXtw5gnvIFsFtU1cfUE6F9S58Zc' \
                           '+mJmmFZCqu8oMs+Z7+gZfu/Rl6lT1+QMLqc51Aw/GGmHsPJz+Uz6uph4W3T73efwcBPc4' \
                           'nW2Dlxqm/kh3P3YB3CqMi7e4WqpefnQYA/XL48kT5iOaLq66XBQUKwa5cf8aCuzAuLtii' \
                           'TM+FvwcPbemXtJLoOGmWKJcxm84ttK1+htfZ/E75kxPFf1jT2PcUzjTCszttP7cv3Dx9M' \
                           'zbJebB0ruDQmhzzxCwH/iKptTnZFZGloaZ/fLCup4UtSN3gFmOMYsxRUr7TcexClgbai/' \
                           'kofeXmUvXNK6YuLiEOInh9VnnmMyTxbk+cMcZqvk9qqzodEqzFrPhuzowR/DV+Bk6AwP/' \
                           'RI9N1sCSiDOBMc/S0frp3IScmdIs/nW2DWJ4zdtX2wzJIvOmhpujeg7T+NOeq84moHQrp' \
                           'Y2HPgIOe/vqI4TUNEJ1ra/HYdlEJlPSakqlk5XJFMC95O1LpYMKlpdjbPTVz7DHV403bk' \
                           'q7Bt/Z9iPwhbevYfnU5dy6JcEtSZhhErktccSbidI/sMjbF4HwzWFVNaQn1tASycnP0zF' \
                           'uBzufPBFfwGhKL+uDln9xZqe3r25pMKeqXx61AnCUwfnvxZqvCOV5FJYyO3CEjw5rofl2' \
                           'WNAQMdC9PNs5URyUR9a7LcNY418Oil77P0jnav8iZYko30PWjpbPgNle0QoYuwnMFS6iz' \
                           'Q1lfCJtXCDAs/4f5kM6WvxztBJS3sf3HS2HwXxWfCNzHtyrpQlUnPL/r4OtegUYjOx37g' \
                           '06286ULlA05Ze169E+MO9E9B4h29xV0jxFKL8sgKbBd4E/Qu3ObWb3/d7Su+WgMmRl1mZ' \
                           'xZuotjHEH6GrbV6ZEXipDFkBX65cYd2DiW4683VzWPB4qRxYwISiHoZP3W38td5zpVJas' \
                           'QNDb2LE3yh3Fx//gOHU6wULVJQAAAABJRU5ErkJggg=='
    marker_lons = []
    marker_lats = []
    # HTML headers required for the usage of the HTML code, with SRI hash
    html_headers = ('<script src="https://openlayers.org/en/v4.0.1/build/ol.js"\n' +
                    'integrity="sha384-DjqoiOTAYD0bxdY6K1Ep7Xd4QQc46AKzKmkgC4+rnK' +
                    'X47PniPHIjRt02uhT8C04h"\n' +
                    'crossorigin="anonymous"></script>\n' +
                    '<link rel="stylesheet" href="https://openlayers.org/en/v4.0.1/css/ol.css" ' +
                    'integrity="sha384-5ezXHctQdkW/AoPO12yIqJjdpRiHXnQIT73NpKlM8mhF2zRNK3jgSUAX' +
                    'JMMY4F34"\n' +
                    'crossorigin="anonymous">\n')

    def __init__(self, lons, lats, weights=None, attributes=None, center=None, zoom=14,
                 radius=2, blur=5, heatmap_opacity=1.0,
                 basemap='wikimedia', variant=None,
                 heatmap_style='magma',
                 marker_lons=None, marker_lats=None, marker_path=None):
        """
        Interactive map object

        Parameters
        ----------
        lons : iterable of point longitudes
               Positions in the map, which must be expressed as WGS84
        lats : iterable of point latitudes
               Positions in the map, which must be expressed as WGS84
        weights : iterable of weights (optional)
                  Weights to use in the heatmap generation in the 0-1 range.
                  If not given, all points will be assigned a uniform weight.
        attributes : dictionary (optional)
                     Extra attributes for the points, where the keys correspond
                     to the attribute name and the values are iterables of the
                     same length as `lons`  that will be assigned to the features.

                     The attributes will be exposed as Feature properties.
        center : map center coordinates in (lon, lat) format as WGS84
        zoom : integer
                Default zoom level for the map
        radius : integer
                 Radius for the map points, see:
                 https://openlayers.org/en/latest/apidoc/ol.layer.Heatmap.html
        blur : integer
               Blur radius for the map points, see:
               https://openlayers.org/en/latest/apidoc/ol.layer.Heatmap.html
        heatmap_opacity : flaot
                          Opacity of the heatmap in the 0-1 range.
        basemap : string
                  Basemap type. Allowed values are:
                    * `carto` : Copyrighted, see https://carto.com/location-data-services/basemaps/
                    * `osm` : OpenStreetMaps basemap
                    * `wikimedia` : WikiMedia basemaps
                    * `stamen` : Stamen basemaps
        variant : string (optional)
                  `carto` and `stamen` basemaps have some variants.
                  Accepted values for `carto`:
                    * `light_all`
                    * `dark_all`
                    * `light_nolabels`
                    * `light_only_labels`
                    * `dark_nolabels`
                    * `dark_only_labels`
                  Accepted values for `stamen`:
                    * `toner`
                    * `toner-hybrid`
                    * `toner-labels`
                    * `toner-lines`
                    * `toner-lite`
                    * `watercolor`
                    * `terrain`
                    * `terrain-background`
                    * `terrain-labels`
                    * `terrain-lines`
                    * `toner-background`
        heatmap_style : string
                        Heatmap colormap to use.
                        Have a look at https://www.youtube.com/watch?v=xAoljeRJ3lU to see why
                        you should think twice before using "jet".
                        Allowed values are:
                            * `jet`
                            * `viridis`
                            * `inferno`
                            * `magma`
                            * `plasma`
        marker_lons : iterable of marker longitudes
                      Coordinates must be expressed as WGS84.

                      The bottom center of the marker will be positioned in the
                      given locations.
        marker_lats : iterable of marker latitudes
                      Coordinates must be expressed as WGS84.

                      The bottom center of the marker will be positioned in the
                      given locations.
        marker_path : String or Path-like
                      Path to the file containing the marker image
        """
        if len(lons) != len(lats):
            raise ValueError('len(lons) must match len(lats)')

        if weights is not None:
            if len(weights) != len(lons):
                raise ValueError('If weights is given, its length must match that of lats/lons')

        if (marker_lons and not marker_lats) or (marker_lats and not marker_lons):
            raise ValueError('If giving marker positions you must provide '
                             'both marker_lons & marker_lats')

        for attrib in attributes:
            if len(attributes[attrib]) != len(lons):
                raise ValueError('If extra attributes are given, its length must match '
                                 'that of lats/lons ({} does not)'.format(attrib))

        if basemap not in self.__basemap_variants.keys():
            raise ValueError('Given basemap type is not supported')

        if self.__basemap_variants[basemap] is None and variant:
            raise ValueError('Basemap {} does not accept any variants'.format(basemap))
        if self.__basemap_variants[basemap] and variant not in self.__basemap_variants[basemap]:
            text = 'Basemap {} does not accept variant {}; accepted variants:\n'.format(basemap,
                                                                                        variant)
            for variant in self.__basemap_variants[basemap]:
                text += '\t* {}\n'.format(variant)

            raise ValueError(text)

        if heatmap_style not in self.__supported_heatmaps.keys():
            raise ValueError('Given heatmap style is not supported')

        # Store the values
        self.lons = list(lons)
        self.lats = list(lats)
        if marker_lons:
            self.add_markers(marker_lons, marker_lats)
        self.weights = weights
        self.attributes = attributes
        self.basemap = basemap
        self.variant = variant
        self.heatmap_style = heatmap_style
        if center is None:
            self.center = (sum(self.lons) / len(self.lons), sum(self.lats) / len(self.lats))
        elif len(center) != 2:
            raise ValueError("center must have two components")
        else:
            self.center = center
        self.zoom = zoom
        self.radius = radius
        self.blur = blur
        self.heatmap_opacity = heatmap_opacity

        if basemap == 'osm':
            self.basemap_source = 'new ol.source.OSM()'
        elif basemap == 'carto':
            self.basemap_source = ('new ol.source.XYZ({attributions: new ol.Attribution({html: \'© '
                                   '<a href="http://www.openstreetmap.org/copyright">'
                                   'OpenStreetMap</a | © '
                                   '<a href="https://carto.com/attribution">CARTO</a>\'}),'
                                   'url: \'https://cartodb-basemaps-{a-c}.global.ssl.fastly.net/' +
                                   variant + '/{z}/{x}/{y}.png\',maxZoom: 18})')
        elif basemap == 'wikimedia':
            self.basemap_source = ('new ol.source.XYZ({attributions: new ol.Attribution({html: \'© '
                                   '<a href="http://www.openstreetmap.org/copyright">'
                                   'OpenStreetMap</a> | © '
                                   '<a href="'
                                   'https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">'
                                   'Wikimedia</a>\'}),'
                                   'url: \'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png\','
                                   'maxZoom: 18})')
        elif basemap == 'stamen':
            self.basemap_source = 'new ol.source.Stamen({layer: \'' + variant + '\'})'

        if marker_path is not None:
            self.marker_path = Path(marker_path)
            self.set_marker_icon()

    def add_markers(self, marker_lons, marker_lats):
        """
        Add marker positions to the map

        Parameters
        ----------
        marker_lons : iterable of marker longitudes
                      Coordinates must be expressed as WGS84.

                      The bottom center of the marker will be positioned in the
                      given locations.
        marker_lats : iterable of marker latitudes
                      Coordinates must be expressed as WGS84.

                      The bottom center of the marker will be positioned in the
                      given locations.
        """
        self.marker_lons += list(marker_lons)
        self.marker_lats += list(marker_lats)

    def set_marker_icon(self, marker_path=None):
        """
        Set the marker icon to the one contained in the specified path

        The lower center of the icon will be positioned in the
        coordinates specified for the markers.

        Parameters
        ----------
        marker_path : Pathlike-object (optional)
                      Path to the marker to be used
                      The extension of the file will be used to determine the marker type,
                      so it must be coherent with the file's contents.
        """

        base64_mediatypes = {'.jpg': 'data:image/jpeg;base64,',
                             '.jpeg': 'data:image/jpeg;base64,',
                             '.png': 'data:image/png;base64',
                             '.svg': 'data:image/svg+xml;base64,'}

        if marker_path is not None:
            self.marker_path = Path(marker_path)

        if self.marker_path is None:
            raise RuntimeError('Marker path was not given, cannot continue')

        if self.marker_path.suffix.lower() not in base64_mediatypes.keys():
            raise ValueError('Given image type is not supported, supported types:\n\t* ' +
                             '\n\t* '.join(base64_mediatypes.keys()) +
                             '\n')

        mediatype = base64_mediatypes[self.marker_path.suffix.lower()]
        # We store the base64-encoded version of the marker
        self.__marker_b64_encoded = mediatype
        self.__marker_b64_encoded += base64.b64encode(self.marker_path.read_bytes()).decode('utf8')

    def js_code(self, target_id, controls=True):
        """
        Return the <script> tag for this particular map

        Parameters
        ----------
        target_id: string
                   ID of the node where the map will be placed
        controls: boolean
                  Flag indicating whether standard control should be added
                  to the map

        Returns
        -------
        string

        JS code to be embedded inside a <script> tag in HTML
        """
        # Define some required strings
        base_feature_point = 'new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([{}, {}])))'
        markers_string = ',\n'.join([base_feature_point.format(self.marker_lons[i],
                                                               self.marker_lats[i])
                                     for i in range(len(self.marker_lats))])
        if self.attributes is None:
            locations_string = ',\n'.join([base_feature_point.format(self.lons[i], self.lats[i])
                                           for i in range(len(self.lats))])
        else:
            base_feature_point = ('new ol.Feature({{'
                                  'geometry: new ol.geom.Point(ol.proj.fromLonLat([{}, {}])), ' +
                                  ', '.join(['{}: '.format(k) + '{}'
                                             for k in self.attributes.keys()]) +
                                  '}})')
            locations_string = ',\n'.join([base_feature_point.format(self.lons[i], self.lats[i],
                                                                     *[self.attributes[k][i]
                                                                       for k in self.attributes.keys()])
                                           for i in range(len(self.lats))])

        gradient_string = "', '".join(i for i in self.__supported_heatmaps[self.heatmap_style])

        # Define the <script> tag string
        controls_code = ''
        if controls is False:
            controls_code = ',\ncontrols: []\n'
        code = """var all_customer_locations = [""" + locations_string + """];

            var heatmap = new ol.layer.Heatmap({
                source: new ol.source.Vector({
                    features: all_customer_locations
                }),
                blur: """ + str(self.blur) + """,
                radius:  """ + str(self.radius) + """,
                opacity: """ + str(self.heatmap_opacity) + """,
                gradient: ['""" + gradient_string + """']
            });

            var tiles = new ol.layer.Tile({
                source: """ + self.basemap_source + """
            });

            var marker_features = [""" + markers_string + """];
            var markers = new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: marker_features
                }),
                style: [new ol.style.Style({
                    image: new ol.style.Icon({
                          anchor: [0.5, 0],
                          anchorOrigin: 'bottom-left',
                          anchorXUnits: 'fraction',
                          anchorYUnits: 'pixels',
                          opacity: 1.0,
                              src: '""" + self.__marker_b64_encoded + """',
                          scale: 0.4
                          })
                    })
                ]
            });

            var map = new ol.Map({
                layers: [tiles, heatmap, markers],
                target: '""" + target_id + """',
                view: new ol.View({
                    center: ol.proj.fromLonLat([""" + str(self.center[0]) + """, """ + str(self.center[1]) + """]),
                    zoom: """ + str(self.zoom) + """
                })""" + controls_code + """
            });"""

        return code

    def to_html(self, title, path=None, width=800, height=600):
        """
        Return or store the code for a complete HTML5 page with the heatmap

        Parameters
        ----------
        title : string
                Title of the webpage
        path : string, Path-like or None
               Path where the HTML page should be stored.
               If not given, the HTML code will be returned
        width : string or integer
                Width of the map (in pixels)
        height : string or integer
                 Height of the map (in pixels)

        Returns
        -------
        string: The HTML code for the heatmap if path=None
        """

        code = ('<!DOCTYPE html>\n'
                '<html>\n'
                '<head>\n'
                '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\n'
                '<title>{}</title>\n'.format(title) +
                self.html_headers +
                '</head>\n'
                '<body>\n'
                '<div id="map" style="width: {}px; height: {}px"></div>\n'.format(width, height) +
                '<script>\n' +
                self.js_code('map') +
                '</script>\n' +
                '</body>\n'
                '</html>')

        if path is None:
            return code
        else:
            if not isinstance(path, Path):
                path = Path(path)
            path.write_text(code)

    def to_png(self, path, width=800, height=600):
        """
        Store the heatmap as a PNG file

        Parameters
        ----------
        path : string, Path-like or None
               Path where the PNG page should be stored.
        width : string or integer
                Width of the map (in pixels)
        height : string or integer
                 Height of the map (in pixels)

        Returns
        -------
        string: The HTML code for the heatmap if path=None
        """

        code = ('<!DOCTYPE html>\n'
                '<html>\n'
                '<head>\n'
                '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\n' +
                self.html_headers +
                '</head>\n'
                '<body style="margin: 0;">\n'
                '<div id="map" style="width: {}px; height: {}px"></div>\n'.format(width, height) +
                '<script>\n' +
                self.js_code('map', controls=False) +
                'map.once("postrender", function() {\n' +
                '   window.setTimeout(function () {\n' +
                '       if (typeof window.callPhantom === "function") {\n' +
                '           window.callPhantom();\n' +
                '       }\n' +
                '   }, 2000);\n' +
                '})\n' +
                '</script>\n' +
                '</body>\n'
                '</html>')

        with tempfile.NamedTemporaryFile(mode='wt', delete=False, suffix='.html') as o_html:
            o_html.write(code)
            with tempfile.NamedTemporaryFile(mode='wt', delete=False, suffix='.js') as o_js:
                o_js.write('var fs = require("fs");\n' +
                           'var page = require("webpage").create();\n' +
                           'page.viewportSize = {width: ' + str(width) +
                           ', height: ' + str(height) + '};\n' +
                           'page.onCallback = function(data) {\n' +
                           'page.render("' + str(path) + '");\n' +
                           'phantom.exit(0);\n' +
                           '};\n' +
                           'var url = "' + o_html.name + '";\n' +
                           'page.open(url, function(status) {\n' +
                           'if (status !== "success") {\n' +
                           'console.log("Error opening url " + url);\n' +
                           'phantom.exit(1);\n' +
                           '}\n' +
                           '});')

        # Call phantomjs to process the script
        try:
            subprocess.run(['phantomjs', o_js.name],
                           timeout=10)
        except FileNotFoundError as e:
            sys.stderr.write(RED + 'ERROR: ' + END +
                             'PNG exporting requires PhantomJS to be available in your path\n')
            raise e
        finally:
            # Remove HTML & JS files
            Path(o_html.name).unlink()
            Path(o_js.name).unlink()


class LocationMap:
    """
    Class that can be used to plot a location map using OpenLayers 4
    """
    __basemap_variants = {'carto': ('light_all', 'dark_all',
                                    'light_nolabels', 'light_only_labels',
                                    'dark_nolabels', 'dark_only_labels'),
                          'osm': None,
                          'wikimedia': None,
                          'stamen': ('toner', 'toner-hybrid', 'toner-labels', 'toner-lines',
                                     'toner-lite', 'watercolor', 'terrain', 'terrain-background',
                                     'terrain-labels', 'terrain-lines', 'toner-background')}
    # Some generic marker, in case the user does not provide one
    __marker_b64_encoded = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAAB4CAYAAAC6oA/pAAA' \
                           'ABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUA' \
                           'd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAZrSURBVHic7Z1LbFRVGMd/50yZDqVT+qDhISrGm' \
                           'GiMiTHGjYlK0LRUDQsjK2NYGF0UCGg0EDCOJoomGkJbXaCJJsoGH4maCoZFNWEhC+JGTY' \
                           'gvUKAUaIHSl9OZOS5uSWnvudN+0DszwPdbzZxzT/O/v9y599xzm/sZZiKzN8nZ9EqMWQP' \
                           'uXnA3gVkMVM84tnLJAn3AcQw/49zXNA39QGZtttggE9mzridF3eh6nNkCNM1t1oqkH8wO' \
                           'Lla/zycrx3wb+GW1dz+ENZ+CuSXWeJXJMSzPsGv1wekdNrTpxv3PYe2BG1QUwK0U6GHj/' \
                           'vbpHVOPrA371gEflyhU5WN4gY7Vuye/XqK9+6HgiCIZOdg5GB+F/Di4Qqw5Y8VYSMyDef' \
                           'PBRJ+2gSyWVZd+ksGW63pSpEePRP70clkYPAkj56BwDUuajk3A/HpYuBSqIi/uR8HdSWf' \
                           'bf8E5q250faSo4QHo/RWG+q8vUQCFPAz3Q+9vMDIQtdUKjGkHMGT2JulPn8Q3PRgegP6/' \
                           '4wtbaSy6DWoafT1n+a95maW/9hF8onJZGDgWc7oKo/9osN9hFlF99mEbzMw9XDhxbZ/Er' \
                           'wTn4EJvRGdhjcVxv3fQ6Pl4g1UqI+ciDhJznwUWh9rHR66/k/lscXkY997tLLPAklBzPh' \
                           'd3pMomP+5rXWrxrR7caOeq6fj3vzp8b6hEorIEqCwBKkuAyhKgsgSoLAEqS4DKEqCyBKg' \
                           'sASpLgMoSoLIEqCwBKkuAyhKgsgSoLAEqS4DKEqCyBKgsASpLgMoSoLIEqCwBKkuAyhKg' \
                           'sgSoLAEqS4DKEqCyBKgsASpLgMoSoLIEqCwBKkuAyhKgsgSoLAEqS4DKEqCyBKgsASpLg' \
                           'MoSoLIEqCwBKkuAyhKgsgSoLAEqS4DKEqCyBKgsASpLgMoSoLIEqCwBKkuAyhKgsgSoLA' \
                           'EqS4DKEqCyBKgsARZwodbiL5S//vHvv7PA2VCzrYo7TmXj3X932gLh11RHv3X/xsC7/+a' \
                           'UBfNXqD0xD5IL4g9ViVQvCPY/zJ8WCt95B9WFX+Z9Q5AOv5c7wHVb8u5bIPzm5ZoGqKmP' \
                           'NVfFMT9ynwvYZLflg8dPYfjSO7hpBaTSccarHFLpYH+9mM/Z9WjfxDwrsQ0IvwTdJKD5D' \
                           'li4LPh8PWISUL8s2E/rnXZmobANLi9StH7fWxi2Rv7RQg5GLkB2GPLZoHTDlVDIQXbkys' \
                           'bCZHGhq8EYSCSDi1jNwuJTJWfepKt1O8DkVosObWfggXtw5gnvIFsFtU1cfUE6F9S58Zc' \
                           '+mJmmFZCqu8oMs+Z7+gZfu/Rl6lT1+QMLqc51Aw/GGmHsPJz+Uz6uph4W3T73efwcBPc4' \
                           'nW2Dlxqm/kh3P3YB3CqMi7e4WqpefnQYA/XL48kT5iOaLq66XBQUKwa5cf8aCuzAuLtii' \
                           'TM+FvwcPbemXtJLoOGmWKJcxm84ttK1+htfZ/E75kxPFf1jT2PcUzjTCszttP7cv3Dx9M' \
                           'zbJebB0ruDQmhzzxCwH/iKptTnZFZGloaZ/fLCup4UtSN3gFmOMYsxRUr7TcexClgbai/' \
                           'kofeXmUvXNK6YuLiEOInh9VnnmMyTxbk+cMcZqvk9qqzodEqzFrPhuzowR/DV+Bk6AwP/' \
                           'RI9N1sCSiDOBMc/S0frp3IScmdIs/nW2DWJ4zdtX2wzJIvOmhpujeg7T+NOeq84moHQrp' \
                           'Y2HPgIOe/vqI4TUNEJ1ra/HYdlEJlPSakqlk5XJFMC95O1LpYMKlpdjbPTVz7DHV403bk' \
                           'q7Bt/Z9iPwhbevYfnU5dy6JcEtSZhhErktccSbidI/sMjbF4HwzWFVNaQn1tASycnP0zF' \
                           'uBzufPBFfwGhKL+uDln9xZqe3r25pMKeqXx61AnCUwfnvxZqvCOV5FJYyO3CEjw5rofl2' \
                           'WNAQMdC9PNs5URyUR9a7LcNY418Oil77P0jnav8iZYko30PWjpbPgNle0QoYuwnMFS6iz' \
                           'Q1lfCJtXCDAs/4f5kM6WvxztBJS3sf3HS2HwXxWfCNzHtyrpQlUnPL/r4OtegUYjOx37g' \
                           '06286ULlA05Ze169E+MO9E9B4h29xV0jxFKL8sgKbBd4E/Qu3ObWb3/d7Su+WgMmRl1mZ' \
                           'xZuotjHEH6GrbV6ZEXipDFkBX65cYd2DiW4683VzWPB4qRxYwISiHoZP3W38td5zpVJas' \
                           'QNDb2LE3yh3Fx//gOHU6wULVJQAAAABJRU5ErkJggg=='
    marker_lons = []
    marker_lats = []
    # HTML headers required for the usage of the HTML code, with SRI hash
    html_headers = ('<script src="https://openlayers.org/en/v4.0.1/build/ol.js"\n' +
                    'integrity="sha384-DjqoiOTAYD0bxdY6K1Ep7Xd4QQc46AKzKmkgC4+rnK' +
                    'X47PniPHIjRt02uhT8C04h"\n' +
                    'crossorigin="anonymous"></script>\n' +
                    '<link rel="stylesheet" href="https://openlayers.org/en/v4.0.1/css/ol.css" ' +
                    'integrity="sha384-5ezXHctQdkW/AoPO12yIqJjdpRiHXnQIT73NpKlM8mhF2zRNK3jgSUAX' +
                    'JMMY4F34"\n' +
                    'crossorigin="anonymous">\n')

    def __init__(self, center, zoom=14, basemap='wikimedia', variant=None,
                 marker_lons=None, marker_lats=None, marker_path=None):
        """
        Interactive map object

        Parameters
        ----------
        center : map center coordinates in (lon, lat) format as WGS84
        zoom : integer
                Default zoom level for the map
        basemap : string
                  Basemap type. Allowed values are:
                    * `carto` : Copyrighted, see https://carto.com/location-data-services/basemaps/
                    * `osm` : OpenStreetMaps basemap
                    * `wikimedia` : WikiMedia basemaps
                    * `stamen` : Stamen basemaps
        variant : string (optional)
                  `carto` and `stamen` basemaps have some variants.
                  Accepted values for `carto`:
                    * `light_all`
                    * `dark_all`
                    * `light_nolabels`
                    * `light_only_labels`
                    * `dark_nolabels`
                    * `dark_only_labels`
                  Accepted values for `stamen`:
                    * `toner`
                    * `toner-hybrid`
                    * `toner-labels`
                    * `toner-lines`
                    * `toner-lite`
                    * `watercolor`
                    * `terrain`
                    * `terrain-background`
                    * `terrain-labels`
                    * `terrain-lines`
                    * `toner-background`
        marker_lons : iterable of marker longitudes
                      Coordinates must be expressed as WGS84.

                      The bottom center of the marker will be positioned in the
                      given locations.
        marker_lats : iterable of marker latitudes
                      Coordinates must be expressed as WGS84.

                      The bottom center of the marker will be positioned in the
                      given locations.
        marker_path : String or Path-like
                      Path to the file containing the marker image
        """
        if (marker_lons and not marker_lats) or (marker_lats and not marker_lons):
            raise ValueError('If giving marker positions you must provide '
                             'both marker_lons & marker_lats')

        if basemap not in self.__basemap_variants.keys():
            raise ValueError('Given basemap type is not supported')

        if self.__basemap_variants[basemap] is None and variant:
            raise ValueError('Basemap {} does not accept any variants'.format(basemap))
        if self.__basemap_variants[basemap] and variant not in self.__basemap_variants[basemap]:
            text = 'Basemap {} does not accept variant {}; accepted variants:\n'.format(basemap,
                                                                                        variant)
            for variant in self.__basemap_variants[basemap]:
                text += '\t* {}\n'.format(variant)

            raise ValueError(text)

        # Store the values
        if marker_lons:
            self.add_markers(marker_lons, marker_lats)
        self.basemap = basemap
        self.variant = variant
        if len(center) != 2:
            raise ValueError("center must have two components")
        else:
            self.center = center
        self.zoom = zoom

        if basemap == 'osm':
            self.basemap_source = 'new ol.source.OSM()'
        elif basemap == 'carto':
            self.basemap_source = ('new ol.source.XYZ({attributions: new ol.Attribution({html: \'© '
                                   '<a href="http://www.openstreetmap.org/copyright">'
                                   'OpenStreetMap</a | © '
                                   '<a href="https://carto.com/attribution">CARTO</a>\'}),'
                                   'url: \'https://cartodb-basemaps-{a-c}.global.ssl.fastly.net/' +
                                   variant + '/{z}/{x}/{y}.png\',maxZoom: 18})')
        elif basemap == 'wikimedia':
            self.basemap_source = ('new ol.source.XYZ({attributions: new ol.Attribution({html: \'© '
                                   '<a href="http://www.openstreetmap.org/copyright">'
                                   'OpenStreetMap</a> | © '
                                   '<a href="'
                                   'https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">'
                                   'Wikimedia</a>\'}),'
                                   'url: \'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png\','
                                   'maxZoom: 18})')
        elif basemap == 'stamen':
            self.basemap_source = 'new ol.source.Stamen({layer: \'' + variant + '\'})'

        if marker_path is not None:
            self.marker_path = Path(marker_path)
            self.set_marker_icon()

    def add_markers(self, marker_lons, marker_lats):
        """
        Add marker positions to the map

        Parameters
        ----------
        marker_lons : iterable of marker longitudes
                      Coordinates must be expressed as WGS84.

                      The bottom center of the marker will be positioned in the
                      given locations.
        marker_lats : iterable of marker latitudes
                      Coordinates must be expressed as WGS84.

                      The bottom center of the marker will be positioned in the
                      given locations.
        """
        self.marker_lons += list(marker_lons)
        self.marker_lats += list(marker_lats)

    def set_marker_icon(self, marker_path=None):
        """
        Set the marker icon to the one contained in the specified path

        The lower center of the icon will be positioned in the
        coordinates specified for the markers.

        Parameters
        ----------
        marker_path : Pathlike-object (optional)
                      Path to the marker to be used
                      The extension of the file will be used to determine the marker type,
                      so it must be coherent with the file's contents.
        """

        base64_mediatypes = {'.jpg': 'data:image/jpeg;base64,',
                             '.jpeg': 'data:image/jpeg;base64,',
                             '.png': 'data:image/png;base64',
                             '.svg': 'data:image/svg+xml;base64,'}

        if marker_path is not None:
            self.marker_path = Path(marker_path)

        if self.marker_path is None:
            raise RuntimeError('Marker path was not given, cannot continue')

        if self.marker_path.suffix.lower() not in base64_mediatypes.keys():
            raise ValueError('Given image type is not supported, supported types:\n\t* ' +
                             '\n\t* '.join(base64_mediatypes.keys()) +
                             '\n')

        mediatype = base64_mediatypes[self.marker_path.suffix.lower()]
        # We store the base64-encoded version of the marker
        self.__marker_b64_encoded = mediatype
        self.__marker_b64_encoded += base64.b64encode(self.marker_path.read_bytes()).decode('utf8')

    def js_code(self, target_id):
        """
        Return the <script> tag for this particular map

        Parameters
        ----------
        target_id: string
                   ID of the node where the map will be placed

        Returns
        -------
        string

        JS code to be embedded inside a <script> tag in HTML
        """
        # Define some required strings
        base_feature_point = 'new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([{}, {}])))'
        markers_string = ',\n'.join([base_feature_point.format(self.marker_lons[i],
                                                               self.marker_lats[i])
                                     for i in range(len(self.marker_lats))])

        # Define the <script> tag string
        code = """var tiles = new ol.layer.Tile({
                source: """ + self.basemap_source + """
            });

            var marker_features = [""" + markers_string + """];
            var markers = new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: marker_features
                }),
                style: [new ol.style.Style({
                    image: new ol.style.Icon({
                          anchor: [0.5, 0],
                          anchorOrigin: 'bottom-left',
                          anchorXUnits: 'fraction',
                          anchorYUnits: 'pixels',
                          opacity: 1.0,
                              src: '""" + self.__marker_b64_encoded + """',
                          scale: 0.4
                          })
                    })
                ]
            });

            var map = new ol.Map({
                layers: [tiles, markers],
                target: '""" + target_id + """',
                view: new ol.View({
                    center: ol.proj.fromLonLat([""" + str(self.center[0]) + """, """ + str(self.center[1]) + """]),
                    zoom: """ + str(self.zoom) + """
                })
            });"""

        return code

    def to_html(self, title, path=None, width=800, height=600):
        """
        Return or store the code for a complete HTML5 page with the map

        Parameters
        ----------
        title : string
                Title of the webpage
        path : string, Path-like or None
               Path where the HTML page should be stored.
               If not given, the HTML code will be returned
        width : string or integer
                Width of the map (in pixels)
        height : string or integer
                 Height of the map (in pixels)

        Returns
        -------
        string: The HTML code for the map if path=None
        """

        code = ('<!DOCTYPE html>\n'
                '<html>\n'
                '<head>\n'
                '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\n'
                '<title>{}</title>\n'.format(title) +
                self.html_headers +
                '</head>\n'
                '<body>\n'
                '<div id="map" style="width: {}px; height: {}px"></div>\n'.format(width, height) +
                '<script>\n' +
                self.js_code('map') +
                '</script>\n' +
                '</body>\n'
                '</html>')

        if path is None:
            return code
        else:
            if not isinstance(path, Path):
                path = Path(path)
            path.write_text(code)

    def to_png(self, path, width=800, height=600):
        """
        Store the map as a PNG file

        Parameters
        ----------
        path : string, Path-like or None
               Path where the PNG page should be stored.
        width : string or integer
                Width of the map (in pixels)
        height : string or integer
                 Height of the map (in pixels)

        Returns
        -------
        string: The HTML code for the map if path=None
        """

        code = ('<!DOCTYPE html>\n'
                '<html>\n'
                '<head>\n'
                '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\n' +
                self.html_headers +
                '</head>\n'
                '<body style="margin: 0;">\n'
                '<div id="map" style="width: {}px; height: {}px"></div>\n'.format(width, height) +
                '<script>\n' +
                self.js_code('map') +
                'map.once("postrender", function() {\n' +
                '   window.setTimeout(function () {\n' +
                '       if (typeof window.callPhantom === "function") {\n' +
                '           window.callPhantom();\n' +
                '       }\n' +
                '   }, 2000);\n' +
                '})\n' +
                '</script>\n' +
                '</body>\n'
                '</html>')

        with tempfile.NamedTemporaryFile(mode='wt', delete=False, suffix='.html') as o_html:
            o_html.write(code)
            with tempfile.NamedTemporaryFile(mode='wt', delete=False, suffix='.js') as o_js:
                o_js.write('var fs = require("fs");\n' +
                           'var page = require("webpage").create();\n' +
                           'page.viewportSize = {width: ' + str(width) +
                           ', height: ' + str(height) + '};\n' +
                           'page.onCallback = function(data) {\n' +
                           'page.render("' + str(path) + '");\n' +
                           'phantom.exit(0);\n' +
                           '};\n' +
                           'var url = "' + o_html.name + '";\n' +
                           'page.open(url, function(status) {\n' +
                           'if (status !== "success") {\n' +
                           'console.log("Error opening url " + url);\n' +
                           'phantom.exit(1);\n' +
                           '}\n' +
                           '});')

        # Call phantomjs to process the script
        try:
            subprocess.run(['phantomjs', o_js.name],
                           timeout=10)
        except FileNotFoundError as e:
            sys.stderr.write(RED + 'ERROR: ' + END +
                             'PNG exporting requires PhantomJS to be available in your path\n')
            raise e
        finally:
            # Remove HTML & JS files
            Path(o_html.name).unlink()
            Path(o_js.name).unlink()


if __name__ == '__main__':
    import sys

    lons = (-3.84049200463167, -3.8210045860331, -3.83419926502418, -3.83895132585191,
            -3.79641948194299, -3.83200348092005, -3.83906213306472, -3.8328189885773,
            -3.82307613491397, -3.82090968081321, -3.83962145682533, -3.7915313853065)
    lats = (40.2811269101776, 40.2900835556515, 40.3049902974645, 40.2931179238227,
            40.2871309991946, 40.2766775103182, 40.2938988233747, 40.2940578480902,
            40.2770753540115, 40.2793165976248, 40.2828244040673, 40.2886610988003)
    ages = (-3.84049200463167, -3.8210045860331, -3.83419926502418, -3.83895132585191,
            -3.79641948194299, -3.83200348092005, -3.83906213306472, -3.8328189885773,
            -3.82307613491397, -3.82090968081321, -3.83962145682533, -3.7915313853065)
    net_income = (-3.84049200463167, -3.8210045860331, -3.83419926502418, -3.83895132585191,
                  -3.79641948194299, -3.83200348092005, -3.83906213306472, -3.8328189885773,
                  -3.82307613491397, -3.82090968081321, -3.83962145682533, -3.7915313853065)

    plt = LocationHeatMap(lons, lats, center=(-3.817, 40.292642), zoom=14,
                          attributes={'age': ages, 'net_income': net_income},
                          basemap='wikimedia', heatmap_style='inferno', heatmap_opacity=1.0,
                          radius=5, blur=15)
    plt.add_markers((-3.828, -3.3635679, -3.7010456, -3.6783814, -3.5640463, -0.7197092,
                     -3.7162325, -4.4289575, -3.8650806, -3.7733092, -0.3957439, -4.1037315),
                    (40.292642, 40.4923592, 40.4267717, 40.4611334, 40.4294432, 38.2581343,
                     40.3245623, 36.7235608, 40.3401168, 40.2277515, 39.4439764, 40.9403236))
    # plt.set_marker_icon('/path/to/marker.svg')
    plt.to_html('Heatmap', 'map_test.html')
    # plt.to_png('map_test.png')

    sys.stdout.write("HTML map stored to {}\n".format('map_test.html'))
    sys.stdout.write("PNG map stored to {}\n".format('map_test.png'))

