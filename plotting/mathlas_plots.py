# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys

import matplotlib
import numpy as np

from mathlas.plotting import colors

if sys.platform == 'linux':
    # TODO: This won't work with wayland
    if matplotlib.get_backend() != 'Agg':
        from os import getenv
        if getenv('DISPLAY') is None:
            sys.stdout.write('Automatically activating Agg backend since X was not detected\n')
            matplotlib.use('Agg')
        del getenv
import matplotlib.pyplot as plt


class MathlasPlot(object):
    """Class for plotting using a common appearance."""

    def __init__(self, figsize=(12, 9), dpi=80):
        """
        Constructor for MathlasPlot objects.

        This method constructs a MathlasPlot object and initializes several
        definitions such as corporate colours, as well as generating a new
        figure.

        Parameters
        ----------
        figsize : tuple, optional
                  Tuple with the size of the figure to create in inches.
                  Default size in (12x9) inches.

        dpi : integer, optional
              Dots per inch. Used to compute the correspondence between
              physical size and pixels.
              Default is 80dpi.

        See Also
        --------
        create_figure

        Notes
        -----
        There is no lightweight cross-platform way of automatically determining
        the DPI value of a screen, so guesses have to be made.
        """
        # Create new figure
        self._create_figure(figsize, dpi)
        self.ax = None

        self.xlim = plt.xlim
        self.text = plt.text

        # Set up method aliases
        # We use this code instead of defining one mapping method per
        # function alias, which is cumbersome
        functionAliases = ('show', 'savefig', 'grid', 'legend', 'annotate', 'xticks',
                           'gca', 'gcf', 'axvline', 'table', 'cla', 'clf', 'close',
                           'subplot', 'setp')
        for alias in functionAliases:
            if not hasattr(self, alias):
                setattr(self, alias, getattr(plt, alias))

        # Set a default font size for the plots that is coherent with
        # the one set by _framework_treatment
        matplotlib.rc('font', size=20)

    @staticmethod
    def equal_aspect_ratio():
        """
        Alias of::

        plt.axis("equal")
        """
        plt.axis("equal")

    @staticmethod
    def suptitle(title, fontsize=25):
        """
        Set a supreme title in the current figure.

        Parameters
        ----------
        title : string
                The title message to be displayed.

        fontsize : integer or string, optional
                   The size of the title in points, or a string with its
                   qualitative size. Allowed values for the string are:
                        * ``xx-small``
                        * ``x-small``
                        * ``small``
                        * ``medium``
                        * ``large``
                        * ``x-large``
                        * ``xx-large``
                   The default font size is 20pt.
        """
        plt.suptitle(title, fontsize=fontsize)

    def boxplot(self, X, alpha=1.0, color=None, markerEdgeColor=None,
                labels=None, widths=0.5, linewidth=3, title=None, axis_labels=None,
                range_y=None, whis=(1, 99), fontsize=25,
                subplot_index=111, showfliers=True):
        """Create boxplots with Mathlas colours

        Parameters
        ----------
        X : NumPy array-like or iterable or NumPy array-likes
            If X is an iterable, plot len(X) boxplots, one for each array.
            Otherwise, plot a single boxplot with the data in X.

        alpha : float
                Transparency value

        color : matplotlib-recognised color
                Color for the lines in the plot

        markerEdgeColor : matplotlib-recognised color
                          Color for the lines in the plot

        labels : iterable of strings
                 Iterable with the labels of the different boxplots

        widths : float
                 The relative widths of each of the boxplots

        linewidth : float
                    Width of the lines in the plot

        title : string
                Title of the plot

        range_y : iterable of floats
                  The range of y-axis values

        whis : iterable of floats
               Reach of the whiskers

        fontsize : integer or string, optional
                   The size of the title in points, or a string with its
                   qualitative size. Allowed values for the string are:
                        * ``xx-small``
                        * ``x-small``
                        * ``small``
                        * ``medium``
                        * ``large``
                        * ``x-large``
                        * ``xx-large``
                   The default font size is 25pt.

        subplot_index : integer
                        Index of the subplot

        showfliers : boolean
                     Whether the outliers should be shown as points or
                     not."""
        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig

        # Create an axes instance
        ax = f.add_subplot(subplot_index)
        self.ax = ax

        b = ax.boxplot(X, patch_artist=True, widths=widths, labels=labels,
                       showfliers=showfliers, whis=whis)

        if axis_labels is not None:
            if len(axis_labels) != 2:
                raise ValueError("Number of labels for axis must be 2")
            ax.set_xlabel(axis_labels[0])
            ax.set_ylabel(axis_labels[1])

        # Set color
        if color is None:
            color = colors.mathlas_orange
        elif color == "mathlas blue":
            color = colors.mathlas_blue
        elif color == "mathlas orange":
            color = colors.mathlas_orange

        # Set the marker edge colors
        if markerEdgeColor is None:
            markerEdgeColor = colors.mathlas_blue
        elif color == 'mathlas blue':
            markerEdgeColor = colors.mathlas_orange
        elif color == 'mathlas orange':
            markerEdgeColor = colors.mathlas_blue

        # Set the colours and alpha values for the lines in the plot
        for key in b.keys():
            for l in b[key]:
                l.set(color=markerEdgeColor)
                l.set_alpha(alpha)
                l.set_linewidth(linewidth)
        for l in b['boxes']:
            l.set(facecolor=color)
        for l in b['fliers']:
            l.set_marker('o')
            l.set_markeredgecolor(markerEdgeColor)
            l.set_color(color)

        # Set title, and make sure the look conforms to the standard
        self._framework_treatment(ax, title=title,
                                  range_y=range_y,
                                  fontsize=fontsize)

    def plot(self, X, Y, color=None, transparency=False,
             range_x=None, range_y=None, axis_labels=None, title=None,
             linestyle='-', linewidth=2, label=None, fontsize=25,
             use_logscale=(False, False), remove_axis_ticks=False,
             subplot_index=111, **opts):
        """
        Plot lines and/or markers allowing for multiple x, y pairs.

        Optional figure attributes can be specified with the method parameters.

        Parameters
        ----------
        X : iterable
            X values to plot.

        Y : iterable
            Y values to plot.

        color : color, optional
            A colour for plotting. Can either be a string numerical value, as
            long as it is a valid value for Matplotlib.
            If `color` is None (default), Mathlas blue (``#005288``) is used.

        transparency : boolean, integer or float, optional
                       The meaning of the parameter depends on its type:

                            * If transparency is a boolean, it determines whether
                              halve transpareceny should be applied to the plot or not.
                            * If transparency is a float, it determines the alpha level
                              to be applied, from 0. to 1.

                       By default no transparency is applied to the plot.

        range_x : tuple, optional
            The range where the X variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        range_y : tuple, optional
            The range where the Y variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        axis_labels : tuple with strings
            A tuple of two strings: the first will be used as the label for the
            X axis and the second one will be used as the label for the Y axis.
            If None, no labels will be assigned

        title : string, optional
                A title for the plot.

        linestyle : string, optional
                    A string describing the style of the line to use.
                    It can be any value accepted by Matplotlib.

        linewidth : float, optional
                    Thickness of the line.

        label : string, optional
                Label for the plot

        fontsize : integer or string, optional
                   The size of the title in points, or a string with its
                   qualitative size. Allowed values for the string are:
                        * ``xx-small``
                        * ``x-small``
                        * ``small``
                        * ``medium``
                        * ``large``
                        * ``x-large``
                        * ``xx-large``
                   The default font size is 25pt.

        use_logscale : iterable of size 2, optional
                       Indicates if (x,y)-axis must be plotted in log scale.

        remove_axis_ticks : boolean, optional
                            Should the axis numbers/ticks be removed? Default
                            value is False.

        subplot_index : integer, optional
                        Index of the subplot where the plotting will occur.

        Raises
        ------
        AssertError
            If the length of X does not match the length of Y
        """
        # Preliminary checks
        if X is None:
            X = np.arange(0, len(Y))
        else:
            n = len(X)
            assert n == len(Y)

        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig
        if isinstance(subplot_index, int):
            ax = f.add_subplot(subplot_index)
        elif isinstance(subplot_index, (tuple, list)):
            ax = f.add_subplot(*subplot_index)
        self.ax = ax

        # If no label is provided an empty label is created
        if label is None:
            label = ""

        # Set transparency
        a = 1.
        if isinstance(transparency, bool):
            if transparency:
                a = 0.5
        elif isinstance(transparency, (int, float)):
            a = float(transparency)

        # Set color
        if color is None:
            color = colors.mathlas_blue
        elif color == "mathlas blue":
            color = colors.mathlas_blue
        elif color == "mathlas orange":
            color = colors.mathlas_orange
        else:
            color = color

        plt.plot(X, Y, color=color, alpha=a, linewidth=linewidth,
                 linestyle=linestyle, label=label, **opts)

        # Remove the axis ticks if the user requires it.
        if remove_axis_ticks:
            ax.set_yticklabels([])
            ax.set_xticklabels([])

        if use_logscale[0]:
            ax.set_xscale('log')

        if use_logscale[1]:
            ax.set_yscale('log')

        # Plot axis labels, if provided
        if axis_labels is not None:
            plt.xlabel(axis_labels[0], fontsize=fontsize)
            plt.ylabel(axis_labels[1], fontsize=fontsize)

        # Show the legend it it's not empty
        if label.strip() != '':
            plt.legend()

        self._framework_treatment(ax, title, range_x, range_y, fontsize)

    def plots(self, X, Y, color_list=None, transparency=False,
              range_x=None, range_y=None, axis_labels=None, title=None, markers=None,
              linestyles=None, linewidths=None, var_labels=None, legend_location=2, fontsize=25,
              use_logscale=(False, False), remove_axis_ticks=False, subplot_index=111,
              use_polar=False):
        """
        Variant of :py:ref:plot() for plotting multiple lines at a time.

        Optional figure attributes can be specified with the method parameters.

        Parameters
        ----------
        X : iterable
            X values to plot. If the elements of X are not iterables the same X
            will be used for all the iterables in Y.

        Y : iterable
            Y values to plot. Each element of the iterable must contain another
            iterable varibale that contains the values to be plotted.

        color_list : iterable, optional
            A colour for plotting. Can either be a string numerical value, as
            long as it is a valid value for Matplotlib.
            If `color` is None (default), Mathlas blue (``#005288``) is used.

        transparency : boolean, optional
            Whether to plot with 50 percent transparency or not.
            If False (default), an opaque plot will be performed.

        range_x : tuple, optional
            The range where the X variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        range_y : tuple, optional
            The range where the Y variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        axis_labels : tuple with strings
            A tuple of two strings: the first will be used as the label for the
            X axis and the second one will be used as the label for the Y axis.
            If None, no labels will be assigned

        title : string, optional
                A title for the plot.

        markers : list of strings, optional
                     Some strings describing the markers to use.
                     They can be any value accepted by Matplotlib. Its length
                     must be equal to the length of Y.

        linestyles : list of strings, optional
                     Some strings describing the style of the line to use.
                     They can be any value accepted by Matplotlib. Its length
                     must be equal to the length of Y.

        linewidths : list of floats, optional
                     Some values of the width of the lines. Its length
                     must be equal to the length of Y.

        var_labels : list of strings, optional
                     Labels for the plots. If provided, a legend will be
                     plotted. Its length must be equal to the length of Y.

        legend_location : int, optional
                          Location of the legend. It can be any value accepted
                          by Matplotlib.

        fontsize : integer or string, optional
                   The size of the title in points, or a string with its
                   qualitative size. Allowed values for the string are:
                        * ``xx-small``
                        * ``x-small``
                        * ``small``
                        * ``medium``
                        * ``large``
                        * ``x-large``
                        * ``xx-large``
                   The default font size is 25pt.

        use_logscale : list of booleans, optional
                       Whether plots should use logscale in the X & Y axes or not.
                       Log scale is not used by default.

        remove_axis_ticks : boolean, optional
                            Should the axis numbers/ticks be removed? Default
                            value is False.

        subplot_index : integer, optional
                        Index of the subplot where the plotting will occur.

        Returns
        -------
        The axes object for the created plot

        Raises
        ------
        TypeError
            If X or Y are not an iterable.

        ValueError
            * If X is an iterable of iterables but the number of them provided
              is different from the number of iterables in Y.
            * If the length of an element of X and its correspondent element of
              Y are of different sizes.
            * If the number of linestyles provided is different from the number
              of plots.
        """
        # Preliminary checks on type of Y
        if not isinstance(Y, (list, np.ndarray)):
            raise TypeError("Variable Y does not have a correct type")
        # Convert data and get dimensions
        X = np.asarray(X)
        Y = np.asarray(Y)
        nPlots = Y.shape[0]
        # Preliminary checks on the variable X
        if len(X.shape) == 2 and X.shape[1] != 1:
            if nPlots != len(X):
                raise ValueError("Number of list in X do not match those in Y")
            else:
                for x, y in zip(X, Y):
                    if len(x) != len(y):
                        raise ValueError("The length of some of the " +
                                         "elements of X does not match the " +
                                         "length of its correspondent " +
                                         "element of Y")
        elif len(X.shape) == 1 or (len(X.shape) == 2 and X.shape[1] == 1):
            for y in Y:
                if len(y) != len(X):
                    raise ValueError("The length of some of the elements " +
                                     "of Y does not match the length of X")
            if len(X.shape) == 2:
                X = np.array([X[:, 0]] * nPlots)
            else:
                X = np.array([X] * nPlots)
        else:
            raise TypeError("Variable X does not have a correct type")

        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig

        if isinstance(subplot_index, int):
            ax = f.add_subplot(subplot_index, polar=use_polar)
        elif isinstance(subplot_index, (tuple, list)):
            ax = f.add_subplot(*subplot_index, polar=use_polar)
        else:
            # This might fail and the user would have to deal with
            # the exception downstream
            ax = f.add_subplot(subplot_index)
        self.ax = ax

        # If no label is provided an empty label is created
        if var_labels is None:
            var_labels = [None] * len(X)

        # Set transparency
        if transparency:
            a = 0.5
        else:
            a = 1.

        # Set colors
        if color_list is None:
            color_list = colors.generate_chromatic_list(nPlots)
        else:
            colors2 = []
            for color in color_list:
                if color == "mathlas blue":
                    color = colors.mathlas_blue
                elif color == "mathlas orange":
                    color = colors.mathlas_orange
                else:
                    color = color
                colors2.append(color)
            color_list = colors2[:]

        # Set markers
        if markers is None:
            markers = [""] * len(X)
        else:
            if len(markers) != len(X):
                raise ValueError("Number of provided linestyles is " +
                                 "different from the number of plots")

        # Set linestyles
        if linestyles is None:
            linestyles = ["-"] * len(X)
        else:
            if len(linestyles) != len(X):
                raise ValueError("Number of provided linestyles is " +
                                 "different from the number of plots")

        # Set linewidths
        if linewidths is None:
            linewidths = [2.0] * len(X)
        else:
            if len(linewidths) != len(X):
                raise ValueError("Number of provided linewidths is " +
                                 "different from the number of plots")

        # Perform plots
        for x, y, c, m, ls, lw, label in zip(X, Y, color_list, markers, linestyles, linewidths, var_labels):
            plt.plot(x, y, color=c, alpha=a, linewidth=lw, marker=m, linestyle=ls, label=label)

        # Plot axis labels, if provided
        if axis_labels is not None:
            plt.xlabel(axis_labels[0], fontsize=fontsize)
            plt.ylabel(axis_labels[1], fontsize=fontsize)

        # Remove the axis ticks if the user requires it.
        if remove_axis_ticks:
            ax.set_yticklabels([])
            ax.set_xticklabels([])

        self._framework_treatment(ax, title, range_x, range_y, use_polar=use_polar)

        if use_logscale[0]:
            ax.set_xscale('log')

        if use_logscale[1]:
            ax.set_yscale('log')

        if var_labels[0] is not None:
            # Remove the line around the legend box, and instead fill it with a
            # light grey. Also only use one point for the scatterplot legend
            # because the user will get the idea after just one, they don't
            # need three.
            legend = ax.legend(frameon=True, loc=legend_location, ncol=1,
                               prop={"size": 18})
            rect = legend.get_frame()
            rect.set_facecolor(colors.light_grey)

            # Change the legend label colors to almost black, too
            texts = legend.texts
            for t in texts:
                t.set_color(colors.almost_black)

        return ax

    def hist(self, H, normed=True, color='mathlas blue', nBins=10,
             transparency=False, range_x=None, range_y=None, axis_labels=None,
             title=None, text=None, fontsize=25, subplot_index=111):
        """
        Histogram plotting routine

        Compute and draw the histogram of x. The return value is a tuple
        `(n, bins, patches)` or
        `([n0, n1, ...], bins, [patches0, patches1,...])`
        if the input contains multiple data.

        Parameters
        ----------
        H : value array
            Input values whose histogram will be plotted.

        normed : boolean, optional
                 If True, the first element of the return tuple will be the
                 counts normalized to form a probability density, i.e.,
                 n/(len(x)`dbin), i.e., the integral of the histogram will sum
                 to 1. If `stacked` is also True, the sum of the histograms is
                 normalized to 1.

                 Default is True

        color : color, optional
            A colour for plotting. Can either be a string numerical value, as
            long as it is a valid value for Matplotlib.

            If `color` is ``b`` (default), Mathlas blue (``#005288``) is used.

        nBins : integer, optional
                Number of bins to split the data into.

                Default is 10

        transparency : boolean, optional
                       Whether to plot with 50 percent transparency or not.
                       If False (default), an opaque plot will be performed.

        range_x : tuple, optional
            The range where the X variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        range_y : tuple, optional
            The range where the Y variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        axis_labels : tuple with strings
            A tuple of two strings: the first will be used as the label for the
            X axis and the second one will be used as the label for the Y axis.
            If None, no labels will be assigned

        title : string, optional
                A title for the plot.

        text : string, optional
               A message to display.

        fontsize : integer or string, optional
                   The size of the title in points, or a string with its
                   qualitative size. Allowed values for the string are:
                        * ``xx-small``
                        * ``x-small``
                        * ``small``
                        * ``medium``
                        * ``large``
                        * ``x-large``
                        * ``xx-large``
                   The default font size is 25pt.

        subplot_index : integer, optional
                        Index of the subplot where the plotting will occur.

        Returns
        -------
        out : tuple with `(n, bins, patches)`
        """
        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig
        ax = f.add_subplot(subplot_index)
        self.ax = ax

        # Set transparency
        if transparency:
            a = 0.5
        else:
            a = 1.

        # Set color
        if color == 'mathlas blue':
            color = colors.mathlas_blue
        elif color == 'mathlas orange':
            color = colors.mathlas_orange
        else:
            color = colors.mathlas_blue

        # Set range
        if range_x is None:
            rx = [min(H), max(H)]
        else:
            rx = range_x

        # Plot histogram
        info = plt.hist(H, bins=nBins, normed=normed, color=color, range=rx,
                        alpha=a, edgecolor=colors.almost_black, linewidth=0.15)

        if text is not None:
            plt.text(0.8, 0.9, text, ha='left', va='top',
                     transform=ax.transAxes, fontsize=25)

        # Plot axis labels, if provided
        if axis_labels is not None:
            plt.xlabel(axis_labels[0], fontsize=fontsize)
            plt.ylabel(axis_labels[1], fontsize=fontsize)

        self._framework_treatment(ax, title, range_x, range_y, fontsize)

        return info

    def circular_histogram(self, theta, radii, nBins=12, bottom=0, title=None,
                           show_edge_color=False, color_key="winter", transparency=False,
                           theta_labels=None, rho_labels=None, rlabel_position=None,
                           subplot_index=111):
        """
        Variant of histogram() for performing circular plots.

        Parameters
        ----------
        normed : boolean, optional
                 If True, the first element of the return tuple will be the
                 counts normalized to form a probability density, i.e.,
                 n/(len(x)`dbin), i.e., the integral of the histogram will sum
                 to 1. If `stacked` is also True, the sum of the histograms is
                 normalized to 1.

                 Default is True

        color_key : string, optional
            A colour key for plotting.
            Allowed values are:
                * ``jet``
                * ``winter``
                * ``mathlas_orange``
                * ``mathlas_blue``

            Default is ``winter``

        nBins : integer, optional
                Number of bins to split the data into.

                Default is 12

        transparency : boolean, optional
                       Whether to plot with 50 percent transparency or not.
                       If False (default), an opaque plot will be performed.

        range_x : tuple, optional
            The range where the X variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        range_y : tuple, optional
            The range where the Y variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        axis_labels : tuple with strings
            A tuple of two strings: the first will be used as the label for the
            X axis and the second one will be used as the label for the Y axis.
            If None, no labels will be assigned

        title : string, optional
                A title for the plot.

        text : string, optional
               A message to display.

        subplot_index : integer, optional
                        Index of the subplot where the plotting will occur.
        """
        # TODO: Complete/correct the docstring
        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig
        ax = f.add_subplot(subplot_index, polar=True)
        self.ax = ax
        # Set compass-like properties
        ax.set_theta_zero_location("N")
        ax.set_theta_direction(-1)

        # Set color
        choose_color = {"jet": plt.cm.jet,
                        "winter": plt.cm.winter,
                        "mathlas_orange": colors.mathlas_orange,
                        "mathlas_blue": colors.mathlas_blue}
        color = choose_color[color_key]

        # Plot histogram
        width = (2*np.pi) / nBins
        if not show_edge_color:
            bars = ax.bar(theta, radii, width=width, bottom=bottom,
                          edgecolor=color)
        else:
            bars = ax.bar(theta, radii, width=width, bottom=bottom)

        # Set transparency
        if transparency:
            a = 0.8
        else:
            a = 1.

        # Use custom colors and opacity
        if show_edge_color:
            for r, bar in zip(radii, bars):
                bar.set_facecolor(color(r / 50.))
                bar.set_alpha(a)
                bar.set_edgecolor(colors.almost_black)
        else:
            for bar in bars:
                bar.set_facecolor(color)
                bar.set_alpha(a)

        if theta_labels is not None and len(theta_labels) in [0, 8]:
            ax.set_xticklabels(theta_labels)
        if rlabel_position is not None:
            ax.set_rlabel_position(rlabel_position)
        if rho_labels is not None:
            ax.set_yticklabels(rho_labels, color="w")
        if title is not None:
            ax.set_title(title, fontsize=25)

    def scatter(self, X, Y, error=None, marker='o', marker_size=20,
                color=None, edge_color=None,
                transparency=False, axis_labels=None, title=None,
                range_x=None, range_y=None, use_logscale=(False, False),
                fontsize=25, label=None, subplot_index=111):
        """
        Create a scatter plot of X vs Y, optionally displaying error bars

        Parameters
        ----------
        X : iterable
            X values to plot.

        Y : iterable
            Y values to plot.

        error : iterable of numbers, optional
                If given, error must contain the error values at each point in X.
                By default error bars are not shown.

        marker : string, optional
                 A string identifying the marker to be shown at the points in X, Y.
                 The marker type must be valid for matplotlib. 'o' is used by default.
                 Valid values are described at:
                 http://matplotlib.org/api/markers_api.html

        marker_size : integer, optional
                      The size of the marker. marker_size defaults to 20.

        color : A color value or None, optional
                The face color for the scatter plot. If None is given, the marker
                will be shown in Mathlas Orange.

        edge_color : A color value or None, optional
                     The face color for the scatter plot. If None is given,
                     the marker edge will be a dark grey (#262626).

        transparency : boolean, integer or float, optional
                       The meaning of the parameter depends on its type:

                            * If transparency is a boolean, it determines whether
                              halve transpareceny should be applied to the plot or not.
                            * If transparency is a float, it determines the alpha level
                              to be applied, from 0. to 1.

                       By default no transparency is applied to the plot.

        axis_labels : iterable with two strings or None, optional
                      An iterable with the labels for the X and Y axes, or None for
                      no label.

        title : string or None, optional
                The plot title.

        range_x : iterable of two floats or None, optional
                  The range accross which the X variable spans.

        range_y : iterable of two floats or None, optional
                  The range accross which the Y variable spans.

        use_logscale : iterable of two booleans, optional
                       Whether plots should use logscale in the X & Y axes or not.
                       Log scale is not used by default.

        fontsize : integer or string, optional
                   The size of the title in points, or a string with its
                   qualitative size. Allowed values for the string are:
                        * ``xx-small``
                        * ``x-small``
                        * ``small``
                        * ``medium``
                        * ``large``
                        * ``x-large``
                        * ``xx-large``
                   The default font size is 25pt.

        label : String
                Label for legend.

        subplot_index : integer, optional
                        Index of the subplot where the plotting will occur.

        Raises
        ------
        AssertError
            If the length of X does not match the length of Y
        """
        # Preliminary checks
        assert len(X) == len(Y)

        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig
        ax = f.add_subplot(subplot_index)
        self.ax = ax

        # Set transparency
        a = 1.
        if isinstance(transparency, bool):
            if transparency:
                a = 0.5
        elif isinstance(transparency, (int, float)):
            a = float(transparency)

        # Set colors
        if isinstance(color, (list, np.ndarray)):
            color = color
        elif color == 'mathlas orange' or color is None:
            color = colors.mathlas_orange
        elif color == 'mathlas blue':
            color = colors.mathlas_blue

        if isinstance(color, (list, np.ndarray)):
            color = color
        elif edge_color is None:
            edge_color = colors.almost_black

        # Show the whole color range
        s = ax.scatter(X, Y, s=marker_size, alpha=a, facecolor=color,
                       edgecolor=edge_color, linewidth=0.15,
                       marker=marker, label=label)

        # Set axis scale
        if use_logscale[0]:
            ax.set_xscale('log')

        if use_logscale[1]:
            ax.set_yscale('log')

        # Plot axis labels, if provided
        if axis_labels is not None:
            plt.xlabel(axis_labels[0], fontsize=fontsize)
            plt.ylabel(axis_labels[1], fontsize=fontsize)

        if error is not None:
            plt.errorbar(X, Y, error, ecolor=color,
                         elinewidth=marker_size / 20.,
                         linestyle="None")

        self._framework_treatment(ax, title, range_x, range_y, fontsize)

        return s

    def scatte2(self, X, Y, error=None, marker='o', marker_size=20, color=None,
                transparency=False, axis_labels=None, title=None, label=None,
                range_x=None, range_y=None, fontsize=25, subplot_index=111):

        # Preliminary checks
        n = len(X)
        assert n == len(Y)

        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig
        ax = f.add_subplot(subplot_index)
        self.ax = ax

        # Set transparency
        if transparency:
            a = 0.5
        else:
            a = 1.

        # Set colors
        if color == 'mathlas orange' or color is None:
            color = colors.mathlas_orange
        elif color == 'mathlas blue':
            color = colors.mathlas_blue
        else:
            color = color

        # Show the whole color range
        ax.scatter(X, Y, s=marker_size, alpha=a, facecolor="white",
                   edgecolor=colors.mathlas_blue, linewidth=4.0,
                   marker=marker, label=label)

        # Plot axis labels, if provided
        if axis_labels is not None:
            plt.xlabel(axis_labels[0], fontsize=fontsize)
            plt.ylabel(axis_labels[1], fontsize=fontsize)

        if error is not None:
            plt.errorbar(X, Y, error, ecolor=color,
                         elinewidth=marker_size / 20.,
                         linestyle="None")

        self._framework_treatment(ax, title, range_x, range_y, fontsize)

    def contourf(self, X, Y, Z, levels=None, extend="neither",
                 axis_labels=None, title=None, range_x=None, range_y=None,
                 showColorBar=False, contour_label=None, fontsize=25,
                 reference_point=None, colorlist="fading blue", subplot_index=111):
        """
        Create a filled contour plot of the given data.

        Parameters
        ----------
        X : iterable
            X values to plot.

        Y : iterable
            Y values to plot.

        Z : iterable
            Z values for the points at X, Y

        levels : iterable, optional
                 A list of floating point numbers indicating the level curves to draw,
                 in increasing order

        extend : [ 'neither' | 'both' | 'min' | 'max' ], optional
                 Unless this is 'neither', contour levels are automatically added to
                 one or both ends of the range so that all data are included.
                 These added ranges are then mapped to the special colormap values
                 which default to the ends of the colormap range.
                 Defaults to 'neither'.

        axis_labels : iterable with two strings or None, optional
                      An iterable with the labels for the X and Y axes, or None for
                      no label.

        title : string or None, optional
                The plot title.

        range_x : iterable of two floats or None, optional
                  The range accross which the X variable spans.

        range_y : iterable of two floats or None, optional
                  The range accross which the Y variable spans.

        showColorBar : boolean, optional
                       Whether a colorbar should be shown.
                       Defaults to False.

        contour_label : string or None, optional
                        The label to be displayed in the colorbar.
                        Defaults to None and only takes effect if
                        showColorBar is True

        reference_point : integer, float or None, optional
                          The X coordinate of the reference point.
                          Defaults to None

        colorlist : string, matplotlib colors or None, optional
                    The colors to use for the different levels.
                        * If a string, like 'r' or 'red', all levels will
                          be plotted in this color.
                        * If 'fading blue' is given, plot the levels in blue shades
                        * If 'from blue to orange' is given, plot the levels in
                          colors from blue to orange
                        * If a tuple of matplotlib color args (string, float, rgb, etc),
                          different levels will be plotted in different
                          colors in the order specified.
                    Defaults to "fading blue"

        fontsize : integer or string, optional
                   The size of the title in points, or a string with its
                   qualitative size. Allowed values for the string are:
                        * ``xx-small``
                        * ``x-small``
                        * ``small``
                        * ``medium``
                        * ``large``
                        * ``x-large``
                        * ``xx-large``
                   The default font size is 25pt.

        subplot_index : integer, optional
                        Index of the subplot where the plotting will occur.

        Raises
        ------
        AssertError
            If the length of X does not match the length of Y
        """
        # Have the levels been provided?
        if levels is None:
            levels = [2e-12, 5e-12, 1e-11, 2e-11, 5e-11, 1e-10, 2e-10, 5e-10,
                      1e-9, 2e-9, 5e-9, 1e-8, 2e-8, 5e-8, 1e-7, 2e-7, 5e-7,
                      1e-6, 2e-6, 5e-6, 1e-5, 2e-5, 5e-5, 1e-4, 2e-4, 5e-4,
                      1e-3, 2e-3, 5e-3, 1e-2, 2e-2, 5e-2, 1e-1, 2e-1, 5e-1,
                      1e0, 2e0, 5e0, 1e1, 2e1, 5e1, 1e2, 2e2, 5e3,
                      1e4, 2e4, 5e4, 1e5, 2e5, 5e5, 1e6, 2e6, 5e6, 1e7]
            nlevels = len(levels)
        else:
            nlevels = len(levels)

        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig
        ax = f.add_subplot(subplot_index)
        self.ax = ax

        if reference_point is None:
            V = Z
        elif isinstance(reference_point, (int, float)):
            indx = np.argmin(np.abs(X - reference_point))
            V = Z[:, indx]
        else:
            raise ValueError("Variable reference_point is not correctly defined")

        # Select levels
        l = [l <= np.max(V) for l in levels]
        ii = 0
        while l[ii]:
            ii += 1
            if ii + 1 == len(levels):
                break
        these_levels = levels[ii - nlevels + 1:ii + 1]

        # Initialize colors
        if colorlist == "fading blue":
            leaps = [(255 - elem) / (nlevels - 1.) for elem in colors.mathlas_blue_rgb]
            these_colors = []
            for ii in range(nlevels - 1, -1, -1):
                color = [int(elem + ii * leap) for elem, leap in zip(colors.mathlas_blue_rgb, leaps)]
                these_colors.append(colors._rgb_to_hex(tuple(color)))
        elif colorlist == "from blue to orange":
            leaps = [(elem1 - elem2) / (nlevels - 1.)
                     for elem1, elem2 in zip(colors.mathlas_blue_rgb, colors.mathlas_orange_rgb)]
            these_colors = []
            for ii in range(nlevels - 1, -1, -1):
                color = [elem + ii * leap for elem, leap in zip(colors.mathlas_orange_rgb, leaps)]
                these_colors.append(colors._rgb_to_hex(tuple(color)))
        elif colorlist == "from blue to white to orange":
            these_colors = []
            for ii in range(nlevels):
                xi = ii / (nlevels - 1.)
                if xi < 0.5:
                    color = 2. * ((0.5 - xi) * np.array(colors.mathlas_blue_rgb) +
                                  xi * np.array([255., 255., 255.]))
                else:
                    color = 2. * ((xi - 0.5) * np.array(colors.mathlas_orange_rgb) +
                                  (1. - xi) * np.array([255., 255., 255.]))
                these_colors.append(colors._rgb_to_hex(tuple(color.astype(int))))
        else:
            these_colors = colorlist
        # Plot the filled contours
        cs = ax.contourf(X, Y, Z, these_levels, colors=these_colors,
                         extend=extend)

        if showColorBar:
            # Make a colorbar for the ContourSet returned by the contourf call.
            cbar = plt.colorbar(cs)
            if contour_label is not None:
                cbar.ax.set_ylabel(contour_label, fontsize=fontsize)

        # Plot axis labels, if provided
        if axis_labels is not None:
            plt.xlabel(axis_labels[0], fontsize=fontsize)
            plt.ylabel(axis_labels[1], fontsize=fontsize)

        self._framework_treatment(ax, title, range_x, range_y, fontsize)

    def candlestick(self, data, fontsize=25, subplot_index=111):
        from datetime import datetime
        from matplotlib.dates import date2num, DateFormatter
        try:
            from matplotlib.finance import candlestick_ohlc
        except ImportError:
            try:
                from mpl_finance import candlestick_ohlc
            except ImportError:
                raise RuntimeError('Candlestick plots are not supported, '
                                   'please install mpl_finance')
        # Create figure if non-existant
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig
        ax = f.add_subplot(subplot_index)
        self.ax = ax
        ax.xaxis_date()
        ax.xaxis.set_major_formatter(DateFormatter("%Y-%m-%d"))
        data['Date2'] = date2num(data.index.astype(datetime))
        tuples = [tuple(x) for x in data[['Date2', 'Open', 'High', 'Low', 'Close']].values]
        plt.ylabel("Price", fontsize=fontsize)
        plt.xlabel("Date", fontsize=fontsize)

        candlestick_ohlc(ax, tuples, width=5.e-3)

    def _create_figure(self, figsize=(12, 9), dpi=80):
        self.fig = plt.figure(figsize=figsize, dpi=dpi)

    def _framework_treatment(self, ax, title=None, range_x=None,
                             range_y=None, fontsize=25, use_polar=False):

        if not use_polar:
            # Remove top and right axes lines ("spines")
            spines_to_remove = ['top', 'right']
            for spine in spines_to_remove:
                ax.spines[spine].set_visible(False)

        # Get rid of ticks. The position of the numbers is informative enough
        # of the position of the value.
        ax.xaxis.set_ticks_position('none')
        ax.yaxis.set_ticks_position('none')

        if not use_polar:
            # For remaining spines, thin out their line and change the black to a
            # slightly off-black dark grey
            spines_to_keep = ['bottom', 'left']
            for spine in spines_to_keep:
                ax.spines[spine].set_linewidth(0.5)
                ax.spines[spine].set_color(colors.almost_black)

        # Change the labels to the off-black
        ax.xaxis.label.set_color(colors.almost_black)
        ax.yaxis.label.set_color(colors.almost_black)

        # Change the axis title to off-black
        ax.title.set_color(colors.almost_black)

        font = {'size': round(fontsize * 0.8)}

        matplotlib.rc('font', **font)

        if title is not None:
            ax.set_title(title, fontsize=round(fontsize*1.5))

        if range_x is not None:
            if use_polar:
                ax.set_rmax(range_x[1])
            else:
                plt.xlim(range_x)

        if range_y is not None:
            plt.ylim(range_y[0], range_y[1])

