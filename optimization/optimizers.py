#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import numpy as np
from mathlas.misc.ansi_color import RED, YELLOW, END
from scipy.cluster.hierarchy import linkage, fcluster


def gradient_descent(f, x, max_iters=10000, **kwargs):
    """
    Classic steepest descent

    Parameters
    ----------
    f : method
        function to minimize. Its inputs must be, at least, <x> (value at which the function must
         be evaluated) and <wanna_gradient> (a Boolean that controls if the function returns only
         its value, if False, or its value and its gradient, if True).
    x : NumPy array
        initial value of the function to minimize
    max_iters : integer, optional
                maximum number of iterations
    kwargs : dictionary
             Inputs of the function

    Returns
    -------
    x : NumPy array
        solution, point at which the optimum is reached
    fX : float
         value of the function at its minimum
    iters : integer
            number of iterations required to reach the optimum
    """
    nfeats = len(x)
    for niters in range(max_iters):
        # Evaluate function and gradient
        fX, df = f(x, wanna_gradient=True, **kwargs)
        # Update value
        x -= df
        # Stop criterion
        if np.linalg.norm(df) < 1e-4 * np.sqrt(nfeats):
            break
    return x, fX, np.linalg.norm(df), niters


def enhanced_gradient_descent(f, x, max_its=10000,
                              learning_rate=((0., 0.1), (0.75, 1.), (1., 1.)),
                              momentum=((0., 0.4), (0.25, 0.4), (0.25, 0.8), (1., 0.8)),
                              simulated_annealing=((0., 0.1), (0.25, 0.), (1., 0.)), min_grad=None,
                              **kwargs):
    """
    Gradient descent with inertia and Brownian-like exploration

    Parameters
    ----------
    f : method
        function to minimize. Its inputs must be, at least, <x> (value at which the function must
         be evaluated) and <wanna_gradient> (a Boolean that controls if the function returns only
         its value, if False, or its value and its gradient, if True).
    x : NumPy array
        initial value of the function to minimize
    max_its : integer, optional
                maximum number of iterations
    learning_rate : iterable of pairs, optional
                    pairs (iteration, value of the learning rate) to be interpolated using a linear
                     piecewise model
    momentum : iterable of pairs, optional
               pairs (iteration, value of the momentum) to be interpolated using a linear piecewise
                model
    simulated_annealing : iterable of pairs, optional
                          pairs (iteration, value of the noise) to be interpolated using a linear
                           piecewise model
    min_grad : float
               threshold of the gradient used as stop criterion
    kwargs : dictionary
             Inputs of the function

    Returns
    -------
    x : NumPy array
        solution, point at which the optimum is reached
    f_x : float
          value of the function at its minimum
    n_its : integer
            number of iterations required to reach the optimum
    """
    # Initialize some values
    n_feats = len(x)
    delta = np.zeros(x.shape)
    min_grad = 1e-4 if min_grad is None else min_grad
    # Let's loop
    for n_its in range(max_its):
        # Calculate learning parameters
        alpha = linear_piecewise_interpolation(n_its / max_its, learning_rate)
        beta = linear_piecewise_interpolation(n_its / max_its, momentum)
        gamma = linear_piecewise_interpolation(n_its / max_its, simulated_annealing)
        # Evaluate function and gradient
        f_x, df = f(x, wanna_gradient=True, **kwargs)
        # Update value
        delta = -alpha * df + beta * delta + gamma * np.max(np.abs(df)) * np.random.randn(*df.shape)
        x += delta
        # Stop criterion
        if np.linalg.norm(df) < min_grad * np.sqrt(n_feats) or np.any(np.isnan(x)):
            break

    return x, f_x, np.linalg.norm(df), n_its


def linear_piecewise_interpolation(target, landmarks):
    """
    Method to interpolate function

    Parameters
    ----------
    target : float
            value whose interval must be located
    landmarks : iterable of iterables
                list of pairs (borders of the intervals, value at the borders)

    Returns
    -------
    out : integer
          interpolated value
    """
    intervals, values = zip(*landmarks)
    if target < intervals[0]:
        return values[0]
    elif target > intervals[-1]:
        return values[-1]
    else:
        indx = np.argmax(target <= np.array(intervals))
        offset = values[indx - 1]
        slope = (values[indx] - values[indx - 1]) / (intervals[indx] - intervals[indx - 1])
        return offset + slope * (target - intervals[indx - 1])


def nonlinear_conjugate_gradient(f, x, max_its=10000, min_grad=None,
                                 method="Polak-Ribiere", **kwargs):

    """
    Non-linear version of the Conjugate Gradient algorithm

    Parameters
    ----------
    f : method
        function to minimize. Its inputs must be, at least, <x> (value at which the function must
         be evaluated) and <wanna_gradient> (a Boolean that controls if the function returns only
         its value, if False, or its value and its gradient, if True).
    x : NumPy array
        initial value of the function to minimize
    max_its : integer

    min_grad : float
               threshold of the gradient used as stop criterion
    kwargs : dictionary
             Inputs of the function

    Returns
    -------
    x : NumPy array
        solution, point at which the optimum is reached
    f_x : float
          value of the function at its minimum
    n_its : integer
            number of iterations required to reach the optimum
    """

    def fletcher_reeves(df_0, df_1, *args):
        return df_1.T.dot(df_1) / df_0.T.dot(df_0)

    def polak_ribiere(df_0, df_1, *args):
        return df_1.T.dot(df_1 - df_0) / df_0.T.dot(df_0)

    def hesteness_stiefel(df_0, df_1, delta):
        return df_1.T.dot(df_1 - df_0) / delta.T.dot(df_1 - df_0)

    if method == "Fletcher-Reeves":
        eval_beta = fletcher_reeves
    elif method == "Polak-Ribiere":
        eval_beta = polak_ribiere
    elif method == "Hesteness-Stiefel":
        eval_beta = hesteness_stiefel
    else:
        raise ValueError("Inputted <method> does not match any of the options")

    # Initialize some values
    n_its = 0
    i_p = 1 if max_its > 0 else 0
    i_n = 0 if max_its > 0 else 1
    min_grad = 1e-4 if min_grad is None else min_grad

    # Initialize some values
    n_feats = len(x)
    f0, df0 = f(x, wanna_gradient=True, **kwargs)
    n_its += i_n
    f_x = [f0]
    p = -1. * df0

    # Start iterating
    r = 1. / (1. + np.linalg.norm(df0))
    while n_its < np.abs(max_its):

        n_its += i_p
        s = p / np.linalg.norm(p)

        x2 = x + 1.0 * r * s
        f2 = f(x2, wanna_gradient=False, **kwargs)
        n_its += 0.5 * i_n

        # Regula Falsi
        rf_failed = 0
        while True:

            x1 = x + 0.5 * r * s
            f1 = f(x1, wanna_gradient=False, **kwargs)
            n_its += 0.5 * i_n

            # Normalize
            if f1 > f0 and rf_failed < 3:
                r /= 2
                f2 = f1.copy()
                rf_failed += 1
            else:
                if f2 < f1:
                    alpha = r
                else:
                    f1 -= f0
                    f2 -= f0
                    alpha = -r * (4. * f1 - f2) / 4. / (f2 - 2. * f1)
                break

        # Adaptive step
        alpha = max(min(alpha, r), -r)
        if alpha / r < 0.25:
            r /= 2.
        elif alpha / r > 0.75:
            r *= 2.
        r = max(min(r, 5e-1), 1e-6)

        # Update values
        dx = alpha * s
        x += dx
        f1, df1 = f(x, wanna_gradient=True, **kwargs)
        n_its += i_n
        f_x.append(f1)

        beta = eval_beta(df0, df1, p)

        # if method == "Fletcher-Reeves":
        #     beta = df1.T.dot(df1) / df0.T.dot(df0)
        # elif method == "Polak-Ribiere":
        #     beta = df1.T.dot(df1 - df0) / df0.T.dot(df0)
        # elif method == "Hesteness-Stiefel":
        #     beta = df1.T.dot(df1 - df0) / p.T.dot(df1 - df0)
        # else:
        #     raise ValueError("Inputted <method> does not match any of the options")

        p = -df1 + beta * p

        f0 = f1.copy()
        df0 = df1.copy()

        if np.linalg.norm(df0) < min_grad * np.sqrt(n_feats) or np.any(np.isnan(x)):
            break

    return x, f_x[-1], np.linalg.norm(df0), int(n_its)


def conjugate_gradient(f, x, voyage_length=False):
    """
    Minimize a continuous differentialble multivariate function. Starting point
    is given by "X" (D by 1), and the function named in the string "f", must
    return a function value and a vector of partial derivatives. The Polack-
    Ribiere flavour of conjugate gradients is used to compute search directions,
    and a line search using quadratic and cubic polynomial approximations and the
    Wolfe-Powell stopping criteria is used together with the slope ratio method
    for guessing initial step sizes. Additionally a bunch of checks are made to
    make sure that exploration is taking place and that extrapolation will not
    be unboundedly large. The "length" gives the length of the run: if it is
    positive, it gives the maximum number of line searches, if negative its
    absolute gives the maximum allowed number of function evaluations. You can
    (optionally) give "length" a second component, which will indicate the
    reduction in function value to be expected in the first line-search (defaults
    to 1.0). The function returns when either its length is up, or if no further
    progress can be made (ie, we are at a minimum, or so close that due to
    numerical problems, we cannot get any closer). If the function terminates
    within a few iterations, it could be an indication that the function value
    and derivatives are not consistent (ie, there may be a bug in the
    implementation of your "f" function). The function returns the found
    solution "X", a vector of function values "fX" indicating the progress made
    and "i" the number of iterations (line searches or function evaluations,
    depending on the sign of "length") used.

    Based of Carl Edward Rasmussen 2002-02-13 code
    """

    if voyage_length == False:
        voyage_length = 100
        red = 1.
    else:
        if isinstance(voyage_length, list) and len(voyage_length) == 2:
            red = voyage_length[1]
            voyage_length = voyage_length[0]
        else:
            red = 1.

    if voyage_length > 0:
        iP = 1
        iN = 0
    else:
        iP = 0
        iN = 1

    RHO = 0.01  # a bunch of constants for line searches
    SIG = 0.5   # RHO and SIG are the constants in the Wolfe-Powell conditions
    INT = 0.1   # don't reevaluate within 0.1 of the limit of the current bracket
    EXT = 3.0   # extrapolate maximum 3 times the current bracket
    MAX = 20    # max 20 function evaluations per line search
    RATIO = 100  # maximum allowed slope ratio

    i = 0                       # zero the run length counter
    ls_failed = False           # no previous line search has failed
    fX = []
    f1, df1 = f(x)              # get function value and gradient
    i += iN  # count epochs?!
    s = -df1                    # search direction is steepest
    d1 = -np.linalg.norm(s)     # this is the slope
    z1 = red / (1 - d1)         # initial step is red/(|s|+1)

    while i < abs(voyage_length):   # while not finished
        i += iP  # count iterations?

        x0 = x.copy()           # make a copy of current values
        f0 = f1.copy()
        df0 = df1.copy()

        x += z1 * s          # begin line search
        f2, df2 = f(x)
        i += iN  # count epochs?
        d2 = df2.T.dot(s)

        f3 = f1.copy()          # initialize point 3 equal to point 1
        d3 = d1.copy()
        z3 = -z1.copy()
        M = MAX if voyage_length > 0 else min(MAX, -voyage_length - i)

        success = False         # initialize quantities
        limit = -1.
        while True:
            while ((f2 > f1 + z1 * RHO * d1) or (d2 > -SIG * d1)) and (M > 0):
                limit = z1      # tighten the bracket
                try:
                    if f2 > f1:     # quadratic fit
                        z2 = z3 - (0.5 * d3 * z3 * z3) / (d3 * z3 + f2 - f3)
                    else:           # cubic fit
                        A = 6 * (f2 - f3) / z3 + 3 * (d2 + d3)
                        B = 3 * (f3 - f2) - z3 * (d3 + 2 * d2)
                        z2 = (np.sqrt(B * B - A * d2 * z3 * z3) - B) / A
                except:             # if we had a numerical problem then bisect
                    z2 = z3 / 2
                z2 = max(min(z2, INT * z3), (1 - INT) * z3)  # don't accept too close to limits
                z1 += z2                                 # update the step
                x += z2 * s
                f2, df2 = f(x)
                M -= 1
                i += iN             # count epochs?
                d2 = df2.T.dot(s)
                z3 -= z2            # z3 is now relative to the location of z2
            if (f2 > f1 + z1 * RHO * d1) or (d2 > -SIG * d1):
                break               # this is a failure
            elif d2 > SIG * d1:
                success = True
                break
            elif M == 0:
                break

            try:
                A = 6 * (f2 - f3) / z3 + 3 * (d2 + d3)  # make cubic extrapolation
                B = 3 * (f3 - f2) - z3 * (d3 + 2 * d2)
                z2 = -d2 * z3 * z3 / (B + np.sqrt(B * B - A * d2 * z3 * z3))
            except:
                if limit < -0.5:
                    z2 = z1 * (EXT - 1.)     # the extrapolate the maximum amount
                else:
                    z2 = (limit - z1) / 2.   # otherwise bisect

            if z2 < 0:          # wrong sign?
                if limit < -0.5:
                    z2 = z1 * (EXT - 1.)     # the extrapolate the maximum amount
                else:
                    z2 = (limit - z1) / 2.   # otherwise bisect
            elif (limit > -0.5) and (z2 + z1 > limit):      # extraplation beyond max?
                z2 = (limit - z1) / 2                       # bisect
            elif (limit < -0.5) and (z2 + z1 > z1 * EXT):   # extrapolation beyond limit
                z2 = z1 * (EXT - 1.0)                       # set to extrapolation limit
            elif z2 < -z3 * INT:
                z2 = -z3 * INT
            elif (limit > -0.5) and (z2 < (limit - z1) * (1. - INT)):   # too close to limit?
                z2 = (limit - z1) * (1. - INT)

            f3 = f2.copy()  # set point 3 equal to point 2
            d3 = d2.copy()
            z3 = -z2.copy()
            z1 += z2
            x += z2 * s  # update current estimates
            f2, df2 = f(x)
            i += iN         # count epochs?
            M -= 1

            d2 = df2.T.dot(s)

        if success:         # if line search succeeded
            f1 = f2
            fX.append(f1)
            print('Iteration %4i | Cost: %4.6e\r' % (i, f1))
            # Polack-Ribiere direction
            s = (df2.T.dot(df2) - df1.T.dot(df2)) / (df1.T.dot(df1)) * s - df2
            df1, df2 = df2, df1
            d2 = df1.T.dot(s)
            if d2 > 0:                      # new slope must be negative
                s = -df1                    # otherwise use steepest direction
                d2 = -s.T.dot(s)
                z1 = z1 * min(RATIO, d1 / d2)   # slope ratio but max RATIO
                d1 = d2
                ls_failed = False           # this line search did not fail
            else:           # restore point from before failed line search
                x = x0
                f1 = f0
                df1 = df0
                # line search failed twice in a row or we ran out of time, so
                # we give up
                if (ls_failed) or (i > abs(voyage_length)):
                    break

                df2, df1 = df1, df2         # swap derivatives
                s = -df1                    # try steepest
                d1 = -s.T.dot(s)
                z1 = 1 / (1 - d1)
                ls_failed = True            # this line search failed
    print('\n')

    return x, fX, np.linalg.norm(df2), i


def bfgs(f, x, voyage_length=10000, mu=0.00):

    i = 0
    if voyage_length > 0:
        iP = 1
        iN = 0
    else:
        iP = 0
        iN = 1

    n = len(x)
    B = np.identity(n)
    H = np.identity(n)
    f0, df0 = f(x)
    i += iN
    fX = [f0]

    r = 1. / (1. + np.linalg.norm(df0))

    while i < np.abs(voyage_length):

        i += iP
        p = -H.dot(df0)
        p /= np.linalg.norm(p)

        x2 = x + 1.0 * r * p
        f2 = f(x2, wanna_gradient=False)
        i += 0.5 * iN

        rf_failed = 0
        while True:

            x1 = x + 0.5 * r * p
            f1 = f(x1, wanna_gradient=False)
            i += 0.5 * iN

            # Normalize
            if f1 > f0 and rf_failed < 3:
                r /= 2
                f2 = f1.copy()
                rf_failed += 1
            else:
                if f2 < f1:
                    alpha = r
                else:
                    f1 -= f0
                    f2 -= f0
                    alpha = -r * (4. * f1 - f2) / 4. / (f2 - 2. * f1)
                break
        print('Parameters', alpha / r, r, alpha)

        alpha = max(min(alpha, r), -r)
        if alpha < 0:
            print('ALPHA', alpha, '!!!!!!!!!!!!!!!')
        if alpha / r < 0.25:
            r /= 2.
        elif alpha / r > 0.75:
            r *= 2.
        r = min(r, 1e-1)

        s = alpha * p
        x += s
        f1, df1 = f(x)
        i += iN
        fX.append(f1)
        y = df1 - df0
        B += y.dot(y.T) / y.T.dot(s) 
        B -= B.dot(np.outer(s, s).dot(B.T)) / s.T.dot(B.dot(s))

        H += (1. - mu) * ((1. + y.T.dot(H.dot(y)) / s.T.dot(y)) * np.outer(s, s) / s.T.dot(y) -
               (H.dot(y.dot(s.T)) + s.dot(y.T.dot(H))) / s.T.dot(y))
        H += mu * np.identity(n)

        print(H)

        f0 = f1.copy()
        df0 = df1.copy()
        print('Iteration %4i | Cost: %4.6e\r' % (i, f0))
        print(x, np.linalg.norm(df1))
        input("Press any key to continue")

        if np.linalg.norm(df0) < 1e-4:
            break

    return x, fX, np.linalg.norm(df0), i


def hooke_n_jeeves(f, x, optimization='max', bounds=None, span=0.1, fitness=0.01, nitermax=10000,
                   remove_duplicates=False):
    """
    Pattern search (Hooke and Jeeves) method

    Compute the relative optima (maxima or minima) of the function f starting the iteration from
    points in the parameter space defined in x

    Parameters
    ----------
    f : function object
        f(x) must return the value of the scalar function at point x

    x : NumPy array
        Starting points to be used by the method.
        x[i,j] stands for the j-th coordinate of the i-th point.
        If there is only one starting point it can be also defined in a 1 dimensional numpy array,
        in this case x[j] stands for the j-th coordinate of the point.
        The starting points must be inside the parameter space domain defined by `bounds`

    optimization : string, optional
                   'max' (default) for maximization.
                   'min' for minimization

    bounds : NumPy array, optional
             Two rows and as many columns as coordinates in the parameter space.
             First row of bounds stands for the vertix in the parameter space defined by the
             minimum value of each parameter.
             Second row of bounds stands for the vertix in the parameter space defined by the
             maximum value of each parameter.
             Default: None --> no bounds are considered.

    span : 1-dimensional NumPy array of floats or float, optional
           If a NumPy array of floats is given, span[j] stands for the span coefficient of the
           hyper-cross corresponding to the j-th coordinate:

           * If bounds are provided, initial span for coordinate j is span[j] * (max[j] - min[j]),
             where max[j] and min[j] stand for the maximum and minimum values, respectively, of the
             j-th coordinate. Thus, the span of the hyper-cross is different for each coordinate.
           * If bounds is None, initial span is equal to span (no rescaling).

           If a float is given, it stands for [span] * (number of coordinates).

           Default value is 0.1

    fitness : float, optional
              Defines stop criteria. When span is lower than (initial span) * (fitness)
              for every coordinate the iteration stops. Note that if bounds are
              provided, then the initial span is different for each coordinate and, thus, the
              criterium is different for each coordinate.


              Default value is 0.01 (e.g., if span = 0.1, then stop criteria is span < 0.001)

    nitermax : integer, optional
               maximum number of iterations. If the number of iterations before the stop criteria
               overtakes nitermax, an exception is raised.

               Default value is 10000

    remove_duplicates : boolean, optional
                        if true, optima are clustered together via Hierarchical Clustering using
                        cut distance equal to (initial span) * fitness, and only one the optimum
                        of each cluster is included in the solution.

                        Default value is False

    Returns
    -------
    x_opt : NumPy array
            The shape of opt is the same of x.
            * if x is a two dimensional array, opt[i,j] stands for the j-th coordinate of the
            optimum corresponding to the i-th starting point.
            if x is a one dimensional array, opt[j] stands for the j-th coordinate of the optimum.

    f_opt : NumPy array
            One dimensional NumPy array
            f_opt[i] stands for the objective function value at the optimal point corresponding
            with the i-th starting point.
    """
    # TODO: Define parameter space using pspace DataFrame as argument
    # Convert starting points array to float64
    x = x.astype(dtype=np.float64)

    # Cast x to a 2 dimensional np.array if needed
    xonedimensional = False
    if len(x.shape) == 1:
        xonedimensional = True
        x = np.reshape(x.astype(dtype=np.float64), newshape=(1, x.shape[0]))
    elif len(x.shape) != 2:
        raise ValueError("x must be 1D or 2D array")

    npoints = x.shape[0]  # Number of starting points
    ndim = x.shape[1]  # Number of dimensions

    # Check optimization variable
    if optimization == 'min':
        isoptimal = _isminimal
        argopt = np.argmin
    elif optimization == 'max':
        isoptimal = _ismaximal
        argopt = np.argmax
    else:
        raise ValueError("Optimization can take values 'min' or 'max'")

    if bounds is not None:
        # Check dimensions of bounds
        if bounds.shape != (2, ndim):
            raise ValueError("Shape of bounds not coherent with the definition of x")

        # Convert bounds to np.float64
        bounds = bounds.astype(dtype=np.float64)

        # Check the starting points are inside the optimization domain
        for point in x:
            for d in range(ndim):
                if not (bounds[0, d] <= point[d] <= bounds[1, d]):
                    raise ValueError("Starting points are not inside the optimization domain")

    # Define the shape where the solution corresponding with each starting point is stored
    x_opt = np.empty(shape=x.shape)
    f_opt = np.empty(shape=(npoints, ))

    # If a float is provided for span it is extended to a list of constant value
    if isinstance(span, float):
        span = np.array([span] * ndim)
    elif not isinstance(span, np.ndarray):
        raise ValueError("Span must be a float or a one-dimensional NumPy array of floats")
    elif not (span.shape == (ndim, )):
        raise ValueError("Span must be one-dimensinal with length equal to number of dimensions")

    # Compute the initial_span rescaling span if bounds are provided.
    initial_span = span
    if bounds is not None:
        initial_span *= (bounds[1] - bounds[0])

    # Compute the initial_cross. The cross define the points in the parameter space that will be
    # compared with the center in order to decide if the center is moved towards an optimal point.
    # Each row of initial_cross (and cross later) stands for the array
    # that must be added to center to obtain the candidates (see below)
    # initial_cross and cross are (2 * ndim) X ndim NumPy arrays.
    initial_cross = np.concatenate((np.diag(initial_span), np.diag(-initial_span)))

    # Compute the stop criteria: maximum number of divisions of the cross that must be performed.
    # If the number of divisions is larger than nmaxdiv the iteration must stop.
    ndivmax = int(np.log(fitness)/(-np.log(2.)))

    # Initialize counter of points
    p = -1

    # Pattern search computed starting from each starting point.
    for point in x:
        p += 1
        # Objective function value at the center point
        center = point.copy()
        try:
            fcenter = float(f(center))
        except Exception:
            raise ValueError("Function can't be evaluated at the starting points")

        # Initialization of cross variable and number of division
        cross = initial_cross.copy()
        ndiv = 0
        stop_criteria_activated = False

        for _ in range(nitermax):
            # Compute objective function value at the external points of the cross that lay
            # inside the optimization domain
            candidates = [center + row for row in cross if _isindomain(center + row, bounds)]
            if len(candidates) == 0:  # Every cross' point lays outside the domain
                cross *= 0.5
                ndiv += 1
            else:
                try:
                    fcandidates = np.array([f(candidate) for candidate in candidates])
                except Exception:
                    raise ValueError("Function can't be evaluated at some points of the parameter "
                                     "space")

                # Update the cross
                if isoptimal(fcenter, fcandidates):  # keep the center divide the cross amplitude
                    cross *= 0.5
                    ndiv += 1
                else:
                    # keep the cross amplitude move the center to the candidate whose value is
                    # optimal
                    index = argopt(fcandidates)
                    center = candidates[index]
                    fcenter = fcandidates[index]

            if ndiv > ndivmax:  # Stop criteria activated
                stop_criteria_activated = True
                x_opt[p] = center
                f_opt[p] = fcenter
                break

        if not stop_criteria_activated:  # Solution is not optimal
            sys.stderr.write(RED + "Warning:" + END +
                             " Iteration from {}-th starting point does not converge\n".format(p))

    # If remove_duplicates required, points are clustered using Hierarchical Clustering
    if f_opt.shape[0] > 1 and remove_duplicates:  # Try clustering if more than one point in x
        # Order the optima in ascending order of function value f_opt
        sort_indices = np.argsort(f_opt)
        x_opt_ord = x_opt[sort_indices, :]
        f_opt_ord = f_opt[sort_indices]
        # Normalize the parameter space using the initial span (the distance from any element of the
        # cross to the center is the same)
        x_opt_ord_norm = x_opt_ord / initial_span
        tree = linkage(x_opt_ord_norm, metric='euclidean', method='single')
        # Define clusters using cut distance equal
        clusters = fcluster(tree, fitness * 2, criterion='distance')
        # Select one point per cluster (select the point associated to a lower or higher value
        # depending on optimization)
        nclusters = np.max(clusters)
        x_opt_reduced = np.empty((nclusters, ndim))
        f_opt_reduced = np.empty((nclusters,))
        for ci in range(nclusters):
            # If maximization keep the last of the points in x_opt_ord that belong to the cluster
            # else keep the first (they are order in ascending order of f_opt)
            index = {'max': -1, 'min': 0}
            point_index = np.where(clusters == ci+1)[0][index[optimization]]
            x_opt_reduced[ci, :] = x_opt_ord[point_index, :]
            f_opt_reduced[ci] = f_opt_ord[point_index]
        # Finally sort x_opt_reduced again in ascending or descending order of f_opt_reduced
        # depending on optimization
        sort_indices = np.argsort(f_opt_reduced)
        index = {'max': -1, 'min': 1}
        x_opt = x_opt_reduced[sort_indices[::index[optimization]], :]
        f_opt = f_opt_reduced[sort_indices[::index[optimization]]]

    # If the starting point is provided in a one-dimensional NumPy array, solution is returned in a
    # one_dimensional NumPy array.
    if xonedimensional:
        x_opt = x_opt[0]
    return x_opt, f_opt


def _isindomain(point, bounds=None):
    """
    Check if point is in bounds

    Parameters
    ----------
    point : 1D NumPy array
            point coordinates

    bounds : 2D NumPy array, optional
             Parameter space bounds. The array is composed of two rows. First and second row stand
             for the minimum and maximum value of each parameter, respectively. The number of
             columns must coincide with the number of point coordinates.

             Default value ``None``

    Returns
    -------
    Boolean :

              * True if point is in the domain
              * False it the point is not in the domain

    """
    # Check point is a one-dimensional array
    if len(point.shape) != 1:
        raise ValueError("point must be a one-dimensional NumPy array")

    ndim = point.shape[0]

    if bounds is not None:
        # Check dimensions of bounds
        if bounds.shape != (2, ndim):
            raise ValueError("Shape of bounds not coherent with the definition of point")
        for dim in range(ndim):
            if not (bounds[0, dim] <= point[dim] <= bounds[1, dim]):
                return False
    return True


def _ismaximal(x, y):
    """
    Check if x is larger than every element of y

    Parameters
    ----------
    x : float

    y : NumPy array

    Returns
    -------
    Boolean
    """
    if x >= np.max(y):
        return True
    return False


def _isminimal(x, y):
    """
    Check if x is lower than every element of y

    Parameters
    ----------
    x : float

    y : NumPy array

    Returns
    -------
    Boolean
    """
    if x <= np.min(y):
        return True
    return False


