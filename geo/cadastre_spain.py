#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This package includes routines needed to interface with data from the Spanish cadastre
"""

import sys
import sqlite3
import zipfile
import inspect
from pathlib import Path
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, text
from mathlas.geo.models.carvia import Carvia
from mathlas.geo.models.parcela import Parcela
from mathlas.geo.models.province import Province
from mathlas.geo.models.base import MunicipalitiesBase
from mathlas.geo.models.municipality import Municipality
from mathlas.misc.notifications import critical, warning
from mathlas.misc.ansi_color import YELLOW, BLUE, GREEN, END

# We want to be able to operate without these packages when working offline
try:
    import requests
    from bs4 import BeautifulSoup
    from xml.dom.minidom import parseString
    from mathlas.input_output.shp import read_shp
    from mathlas.geo.coord_systems import Transform
    online_supported = True
except ImportError:
    online_supported = False


class CartographyFoundException(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class MunicipalityNotFoundException(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class ProvinceNotFoundException(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class NoDataDourceAvailableException(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class SpanishCadastreFetcher:
    ovc_session = None
    currentdir = None

    def __init__(self, online=False):
        """
        This class includes methods for fetching information from the spanish land registry.

        If valid client certificates are present in the `client_cert` directory (in the format
        described in `client_cert/README.md` then the user should be able to download
        geographical info of municipalities in SHP format.
        Regardless of the existance of such client certificates, the user should be able to
        perform an online query to the cadastre to get a list of provinces and municipalities.

        Please note that the three basque provinces and Navarra have their own land registries
        which is not reachable through this API.

        Parameters
        ----------
        online : Boolean, optional
                 Whether online operations should be allowed or not.
                 By default this class will operate offline and only be able to access the data
                 in mathlas/geo/data
        """
        # Determine the directory this file is in, store it and
        # create the data dir, if needed
        self.currentdir = Path(inspect.getfile(inspect.currentframe())).parent
        if not (self.currentdir / 'data').is_dir():
            (self.currentdir / 'data').mkdir(mode=0o755)
        self.localdb = self.currentdir / 'data' / 'municipalities.sqlite3'
        # Create/Update DB
        p = (Path(inspect.getfile(inspect.currentframe())).parent /
             'data' / 'municipalities.sqlite3')
        engine = create_engine('sqlite:///{}'.format(p), echo=False)
        maker = sessionmaker(bind=engine)
        self.dbsession = maker()
        MunicipalitiesBase.metadata.create_all(engine)

        # Warn the user if they asked for an online session but it is not available
        if any([i.is_dir() for i in (self.currentdir / 'data').glob('*')]):
            if online and not online_supported:
                warning('Cannot use online cadastre session',
                        'You asked for an online session to the spanish cadastre but '
                        'dependencies were not met; offline data from '
                        '\n\t{}\n'.format((self.currentdir / 'data').resolve()) +
                        'will be used.\n'
                        'In order to use online data, make sure that all of the '
                        'following dependencies are correctly installed:\n'
                        '\t* requests\n'
                        '\t* beautifulsoup4\n'
                        '\t* GDAL')
        else:
            if not online:
                critical('No cadastre data available',
                         'You asked for an offline session, but have no cadastre data in '
                         '\n\t{}\n'.format((self.currentdir / 'data').resolve()) +
                         'You should be able to copy the cadastre information from Mathlas-NAS at:\n'
                         '\t* \\\\10.7.2.31\\mathlas\\02_DATABASES\\Catastro (in Windows)\n'
                         '\t* smb://10.7.2.31/mathlas/02_DATABASES/Catastro (in Linux)\n'
                         'If you are using macOS: Steve Jobs hates paths. Browse the NAS with Finder.\n\n'
                         'In order to use online data, make sure that all of the '
                         'following dependencies are correctly installed:\n'
                         '\t* requests\n'
                         '\t* beautifulsoup4\n'
                         '\t* GDAL')
                raise NoDataDourceAvailableException('No cadastre data source is available')
            elif not online_supported:
                critical('No cadastre data available and cannot use online cadastre session',
                         'You asked for an online session, but dependencies were not met. '
                         'Also, you have no cadastre data in '
                         '\n\t{}\n'.format((self.currentdir / 'data').resolve()) +
                         'You should be able to copy the cadastre information from Mathlas-NAS at:\n'
                         '\t* \\\\10.7.2.31\\mathlas\\02_DATABASES\\Catastro (in Windows)\n'
                         '\t* smb://10.7.2.31/mathlas/02_DATABASES/Catastro (in Linux)\n'
                         'If you are using macOS: Steve Jobs hates paths. Browse the NAS with Finder.\n\n'
                         'In order to use online data, make sure that all of the '
                         'following dependencies are correctly installed:\n'
                         '\t* requests\n'
                         '\t* beautifulsoup4\n'
                         '\t* GDAL')
                raise NoDataDourceAvailableException('No cadastre data source is available')

        if online_supported and online:
            # Create and configure the ovc_session object
            self.ovc_session = requests.Session()
            self.ovc_session.verify = str(self.currentdir / 'certs')
            self.ovc_session.headers = {'User-Agent': 'Mozilla/5.0 (X11; Fedora;'
                                                      'Linux x86_64; rv:47.0) '
                                                      'Gecko/20100101 Firefox/47.0'}
            if ((self.currentdir / 'client_cert' / 'certificate.crt').is_file() and
                (self.currentdir / 'client_cert' / 'certificate.key').is_file()):
                self.ovc_session.cert = (str(self.currentdir / 'client_cert' / 'certificate.crt'),
                                         str(self.currentdir / 'client_cert' / 'certificate.key'))
            else:
                sys.stdout.write(YELLOW +
                                 'Client certificates not found in:\n'
                                 '\t{}\n'.format((self.currentdir / 'client_cert').resolve()) +
                                 'Online fetching will not work, but data might still be '
                                 'fetched from the local cache. Please read:\n'
                                 '\t{}\n'.format(
                                     (self.currentdir / 'client_cert' / 'README.md').resolve()) +
                                 END)

            # Download the license text and store it locally
            licensepath = self.currentdir / 'license.pdf'
            if not licensepath.is_file():
                r = self.ovc_session.get('https://www.sedecatastro.gob.es/DescargaDatos/'
                                         'SECLicenciaDescargasPDF.aspx?lang=EN')
                if r.status_code == 200:
                    with licensepath.open('wb') as o:
                        o.write(r.content)
                        sys.stdout.write('In order to use the data from the cadastre you must '
                                         'accept the license terms at:\n')
                        sys.stdout.write('\t{}\n'.format(licensepath.resolve()))
                        sys.stdout.write('Also, this certificate must have been used to download '
                                         'data from the land registry web site manually at least '
                                         'once\n')
                else:
                    sys.stderr.write(f'Could not download the license, status code {r.status_code}\n')
                    self.ovc_session = None

            # We must first browse to this page. It'll read (and verify) our
            # user certificate and set a ovc_session cookie
            self._ovc_authorized = False
            if self.ovc_session.cert:
                try:
                    r = self.ovc_session.get('https://www.sedecatastro.gob.es/Accesos/'
                                             'SECAccTitular.aspx?Dest=19')

                    if r.status_code == 200:
                        self._ovc_authorized = True
                    else:
                        sys.stderr.write('Cadastre access not authorized with the '
                                         'installed certificate')
                except requests.exceptions.SSLError:
                    sys.stderr.write('Cannot connect to the Spanish cadastre site. '
                                     'Please check that the user certificates are available '
                                     'in {}'.format((self.currentdir / 'client_cert').resolve()))

            # Could not authenticate with client cert => SHP downloading will not work
            if self._ovc_authorized is None:
                sys.stderr.write('Downloading SHP files will not work\n')

    def get_provinces(self, force_offline=False):
        """
        Return the list of available provinces from the Spanish Cadastre.

        If you have an online session, data will be fetched from the
        Spanish Cadastre website, otherwise local data will be used.

        Parameters
        ----------
        force_offline : boolean
                        Province list will be read from the local DB
                        (as opposed to the Spanish Cadastre's website)
                        even if an online session is available.

        Returns
        -------
        provinces : list
                    List of Province objects
        """

        if self.ovc_session is None or force_offline:
            provinces = [p for p in self.dbsession.query(Province)]
        else:
            # We can now go to the download form and parse it
            r = self.ovc_session.get('http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/'
                                     'OVCCallejero.asmx/ConsultaProvincia?')
            if r.status_code != 200:
                raise RuntimeError('Could not get authorization cookie')
            dom = parseString(r.text)
            nProvinces = int(dom.getElementsByTagName('cuprov')[0].firstChild.data)
            provinces = []
            for node in dom.getElementsByTagName('prov'):
                p = Province()
                p.ine_id = int(node.getElementsByTagName('cpine')[0].firstChild.data)
                p.name = node.getElementsByTagName('np')[0].firstChild.data
                provinces.append(p)
                # Insert the province into the DB if not present
                if len(self.dbsession.query(Province).filter(Province.ine_id == p.ine_id).all()) == 0:
                    self.dbsession.add(p)
                    self.dbsession.commit()

            if len(provinces) != nProvinces:
                raise ValueError('Provinces XML seems malformed')

        return provinces

    def get_municipalities(self, province, force_offline=False):
        """
        Downloads and stores a list of all municipalities from
        the Spanish Cadastre.

        Parameters
        ----------
        province: Province object, int or str
                  Unique province descriptor.

                  Using a Province object is recommended
        force_offline : boolean
                Municipality list will be read from the local DB
                (as opposed to the Spanish Cadastre's website)
                even if an online session is available.

        Returns
        -------
        municipalities : list
                         List of Municipality objects whose province
                         matches the given one.

        Raises
        ------
        ProvinceNotFoundException : If the given province could not be found
        """
        province = province.strip().upper()
        if self.ovc_session is None or force_offline:
            query = self.dbsession.query(Municipality)
            if isinstance(province, Province):
                query = query.filter(Municipality.ine_province_id == province.ine_id)
            elif isinstance(province, int):
                query = query.filter(Municipality.ine_province_id == province)
            elif isinstance(province, str):
                # Look for the province INE ID
                province_ine_id = None
                for p in self.get_provinces(force_offline=force_offline):
                    if p.name.upper() == province:
                        province_ine_id = p.ine_id
                        break

                # Check that the province was found
                if province_ine_id is None:
                    raise ProvinceNotFoundException('Could not find "{}"'.format(province))

                # Filter the initial query by province INE ID
                query = query.filter(Municipality.ine_province_id == province_ine_id)

            else:
                raise TypeError('province parameter given with unsupported type ('
                                '{}) is not supported'.format(type(province)))
            municipalities = [m for m in query]
        else:
            # Loop through all the provinces and request autocomplete names for localities
            municipalities = []
            r = None
            if isinstance(province, int):
                query = self.dbsession.query(Province).filter(Province.ine_id == province)
                if query.count() == 0:
                    raise LookupError('Could not find any province named "{}"'.format(province))
                p = query[0]
                r = requests.get(
                    'http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/'
                    'OVCCallejero.asmx/ConsultaMunicipio',
                    params={'Provincia': p.name,
                            'Municipio': ''})
            elif isinstance(province, str):
                r = requests.get(
                    'http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/'
                    'OVCCallejero.asmx/ConsultaMunicipio',
                    params={'Provincia': province,
                            'Municipio': ''})
            elif isinstance(province, Province):
                r = requests.get(
                    'http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/'
                    'OVCCallejero.asmx/ConsultaMunicipio',
                    params={'Provincia': province.name,
                            'Municipio': ''})

            if r is None:
                raise ValueError('Could not find given provincia')

            dom = parseString(r.text)
            nMunicipalities = int(dom.getElementsByTagName('cumun')[0].firstChild.data)

            for node in dom.getElementsByTagName('muni'):
                m = Municipality()
                m.name = node.getElementsByTagName('nm')[0].firstChild.data
                # Ministerio de Hacienda y Administraciones Públicas ID
                locat_node = node.getElementsByTagName('locat')[0]
                m.mhap_delegation_id = int(locat_node.getElementsByTagName('cd')[0].firstChild.data)
                m.mhap_id = int(locat_node.getElementsByTagName('cmc')[0].firstChild.data)
                # Instituto Nacional de Estadística ID
                ine_node = node.getElementsByTagName('loine')[0]
                m.ine_province_id = int(ine_node.getElementsByTagName('cp')[0].firstChild.data)
                m.ine_id = int(ine_node.getElementsByTagName('cm')[0].firstChild.data)
                municipalities.append(m)

                # Add the municipalities to the DB if not present
                if len(self.dbsession.query(Municipality).filter(Municipality.ine_id == m.ine_id,
                                                                 Municipality.ine_province_id == m.ine_province_id).all()) == 0:
                    self.dbsession.add(m)
                    self.dbsession.commit()

            # Data sanity check
            if len(municipalities) != nMunicipalities:
                raise ValueError('Municipalities XML seems malformed')

        if len(municipalities) == 0:
            raise ProvinceNotFoundException('Could not find municipalities in '
                                            '"{}"'.format(province))

        return municipalities

    def get_municipality_cartography(self, municipality):
        """
        Return the path to the shapefile associated with a municipality

        If the data is not locally available and the you specified online=True
        when creating the object, it will try to be downloaded from the
        cadastre website.

        Parameters
        ----------
        municipality : Municipality object

        Returns
        -------
        path: pathlib.Path
              The path object to the shapefile
        """

        outputdir = Path(self.currentdir) / 'data'
        outputpath = outputdir / municipality.name.replace('/', '-').replace('\\', '')

        # Here we'll just check if the directory exists
        # and complaint otherwise
        if not outputpath.is_dir():
            if self.ovc_session and self._ovc_authorized:
                # First, open the form that directs us to the download
                # this form includes the questions on what you're
                # planning to use the data for
                URL = 'https://www.sedecatastro.gob.es/DescargaDatos/SECDescargaCartoSF.aspx'
                data = {'hdTipologia': 'UA',
                        'hdDelegacion': municipality.mhap_delegation_id,
                        'hdMunicipio': municipality.mhap_id,
                        'hdCapas': 'PARCELA,CONSTRU,CARVIA,',
                        'hdFecha': self.__get_municipality_date(municipality),
                        'hdUrlRetorno': '/CYCTitular/OVCDescargaCartoSF.aspx',
                        'hdUrlDescarga': 'OVCZIPCartoSF.aspx',
                        'hdOrigen': 'SF',
                        'ctl00$body$chkAcepto': ''}
                if data['hdFecha'] is None:
                    raise CartographyFoundException('No cartography found for '
                                                    '{}'.format(municipality.name))
                r = self.ovc_session.post(URL, data=data)
                if r.status_code != 200:
                    raise RuntimeError('Error downloading data')

                # We read the resulting page and
                doc = BeautifulSoup(r.text, 'html.parser')

                form = doc.find(id='aspnetForm')
                if form is None:
                    raise RuntimeError('Could not find form in cadastre webpage')

                # Determine the post URL and the data that will be sent
                # Here we're copying the data in most of the <input> fields
                URL = r.url[:r.url.rfind('/')] + '/' + form.attrs['action']
                data = {}
                for node in form.find_all('input'):
                    if 'value' in node.attrs and 'type' in node.attrs:
                        if node.attrs['type'] in ('hidden', 'text'):
                            data[node.attrs['name']] = node.attrs['value']
                        elif node.attrs['type'] == 'radio' and 'checked' in node.attrs:
                            if node.attrs['checked'] == 'checked':
                                data[node.attrs['name']] = node.attrs['value']

                data['ctl00$body$lkbLicencia'] = 'true'

                r = self.ovc_session.post(URL, data=data)
                if r.status_code != 200:
                    raise RuntimeError('Something went wrong with the download form')

                # We look for the DIV with the download script
                doc = BeautifulSoup(r.text, 'html.parser')
                node = doc.find(id='ctl00_body_ContenedorJS1_ContenedorAjaxJs')
                for line in str(node).split(';'):
                    if 'OVCZIPCartoSF.aspx' in line:
                        URL = r.url[:r.url.rfind('/')] + '/' + line.split("'")[1]
                        r = self.ovc_session.get(URL)
                        if r.status_code != 200:
                            raise RuntimeError('Something went wrong with the download')

                        outputpath.mkdir(mode=0o755, parents=True)
                        with open('{}.zip'.format(outputpath), 'wb') as o:
                            o.write(r.content)

                        # Uncompress the zipfile
                        with zipfile.ZipFile('{}.zip'.format(outputpath)) as zip:
                            for file in zip.filelist:
                                zip.extract(file, path=str(outputpath))

                                with zipfile.ZipFile(str(outputpath / file.filename)) as ozip:
                                    d = outputpath / Path(file.filename).stem
                                    if not d.is_dir():
                                        d.mkdir(mode=0o755)
                                    ozip.extractall(path=str(d))

                                # Remove the zipfile we just extracted
                                (outputpath / file.filename).unlink()

                        # Remove the downloaded zipfile
                        Path('{}.zip'.format(outputpath)).unlink()

                        # Convert the data in the folders into SQLite format
                        # Dictionary for translating projCS value into EPSG value.
                        # See EPSG_list.md for details.
                        # Add entries as needed.
                        EPSG_conversion = {'ETRS_1989_UTM_Zone_28N': 25828,
                                           'ETRS_1989_UTM_Zone_29N': 25829,
                                           'ETRS_1989_UTM_Zone_30N': 25830,
                                           'ETRS_1989_UTM_Zone_31N': 25831,
                                           'WGS_1984_UTM_Zone_28N': 32628}

                        path = outputpath / 'cadastre.sqlite3'
                        if path.is_file():
                            path.unlink()

                        with sqlite3.connect(str(path)) as conn:
                            for d in outputpath.glob('*'):
                                if not d.is_dir():
                                    continue

                                # Read the data from the layer
                                name, data, ref = read_shp(d)

                                # Skip directories with errors
                                if name is None or data is None:
                                    continue

                                # If there are coordinates in the fields, convert them
                                # to EPSG:4326 (WGS 84, which is what you know for lat, lon)
                                if 'COORX' in data.columns and 'COORY' in data.columns:
                                    col_lon = list(data.columns).index('COORX')
                                    col_lat = list(data.columns).index('COORY')
                                    projcs = ref.GetAttrValue('projcs')
                                    t = Transform(inEPSG=EPSG_conversion[projcs])
                                    for i in range(data.shape[0]):
                                        _lon, _lat = t.apply(data.iloc[i, col_lon],
                                                             data.iloc[i, col_lat])
                                        data.set_value(i, data.columns[col_lon], _lon)
                                        data.set_value(i, data.columns[col_lat], _lat)

                                data.to_sql(name, conn)
            else:
                raise FileNotFoundError('Data for {} not '.format(municipality.name) +
                                        'found locally and cannot be downloaded without ' +
                                        'an active connection to the cadastre')

        return outputpath

    def get_municipality_session(self, municipality):
        """
        Return a SQLAlchmey Session connected to the data in the given municipality

        Parameters
        ----------
        municipality : Municipality
                       Municipality object whose data is to be queried

        Returns
        -------
        session : sqlalchemy.session
        """
        p = (self.get_municipality_cartography(municipality) / 'cadastre.sqlite3').resolve()
        engine = create_engine('sqlite:///{}'.format(p), echo=False)
        maker = sessionmaker(bind=engine)
        return maker()

    def download_provinces(self):
        """
        Download provinces from online to the local storage backend
        """
        self.dbsession.add_all(self.get_provinces())
        self.dbsession.commit()

    def download_municipalities(self):
        """
        Download provinces from online to the local storage backend
        """
        for p in self.get_provinces():
            self.dbsession.add_all(self.get_municipalities(p))
        self.dbsession.commit()

    def __get_municipality_date(self, municipality):
        """
        Get the date for the data in the cadastre for a given municipality

        Parameters
        ----------
        municipality: Municipality
                      A Municipality object

        Returns
        -------
        date: string
        """

        if self.ovc_session is None or not self._ovc_authorized:
            raise RuntimeError('Cadastre connection not active but needed')

        # Determine the ids from the given municipality list
        municipalityid = municipality.mhap_id
        provinceid = municipality.ine_province_id
        municipalityname = municipality.name

        # We can now go to the download form and parse it
        r = self.ovc_session.get(
            'https://www.sedecatastro.gob.es/DescargaDatos/SECDescargaCartoSF.aspx')
        if r.status_code != 200:
            raise RuntimeError('Could not get authorization cookie')
        doc = BeautifulSoup(r.text, 'html.parser')
        form = doc.find(id='aspnetForm')
        if form is None:
            raise RuntimeError('Could not find form in cadastre webpage')

        # Determine the post URL and the data that will be sent
        postURL = r.url[:r.url.rfind('/')] + '/' + form.attrs['action']
        data = {}
        for node in form.find_all('input'):
            if 'value' in node.attrs and 'type' in node.attrs and node.attrs['type'] == 'hidden':
                data[node.attrs['name']] = node.attrs['value']

        data['ctl00$Contenido$codigoProvincia'] = provinceid
        data['ctl00$Contenido$codigoMunicipio'] = municipalityid
        data['ctl00$Contenido$municipioSelector'] = municipalityname
        data['ctl00$Contenido$RadioButtonList1'] = 'UA'  # Urbana sin historia
        data['ctl00$Contenido$Button1'] = 'Ver+capas+disponibles'
        r = self.ovc_session.post(postURL, data=data)
        if r.status_code != 200:
            raise RuntimeError('Could not read cadastre data')
        doc = BeautifulSoup(r.text, 'html.parser')
        node = doc.find(id='ctl00_Contenido_hdnFecha')

        if 'value' not in node.attrs:
            return None

        return node.attrs['value']


class SpanishCadastreLocationFetcher:
    def __init__(self, online=False, replacements=None):
        """
        A location fetcher object

        This class implements a method for finding the location of a given address.

        Parameters
        ----------
        online: boolean, optional
                Flag indicating whether cadastre info fetching can happen online or must happen
                only with offline-available data.

                Defaults to False

        replacements : dictionary, optional
                       A dictionary with replacement values for the municipality names.
                       Keys must be (municipality, province) string pairs and values must be
                       the (municipality, province) pair which must be used instead.
        """

        # Table of municipality name/province replacements
        # Some municipalities in the SportData DB do not match their counterparts
        # in the Spanish Land Registry. This dictionary maps some of them
        if replacements is None:
            self.replacements = {}
        else:
            self.replacements = replacements

        # Create the fetcher object
        self.c = SpanishCadastreFetcher(online=online)
        # Dictionary which will hold connections to the cadastre info for each municipality
        self._municipalities = {}
        # Dictionary with all the Carvia (street) objects for a city
        self._streets = {}
        # Dictionary which, for each (municipality, province)
        # will hold a list of already matched street names
        self._matched_streets = {}
        # Dictionary which, for each (municipality, province)
        # will hold a list of already computed street centers
        self._matched_street_centers = {}

    def coords(self, province, municipality, street_name, street_no=None, verbose=False):
        """
        Try to fetch the (lat, lon) location of the given address in EPSG:4326.

        If the number can not be found in the given street, the coordinates of the centroid
        of all the lots associated with the street will be returned.

        The street name matching is fuzzy, meaning that for each input given, the routine
        will ALWAYS find a match by choosing the street name from the cadastre whose
        Levenshtein distance is smallest between all those available.

        Parameters
        ----------
        province: string
                  Name of the province where the municipality should be looked for
        municipality : string
                       Name of the municipality where the street name is located.
        street_name : string
                      Name of the string to look for. The function works better if
                      the street name contains a full version of the streeet type
                      (ie: "AVENIDA PALOMAS" works better than just "PALOMAS").
        street_no : integer or None, optional
                    The number of the lot in the given street.
                    If street_no is `None`, the mean of all the lots in the
                    matched street will be returned.
                    If street_no could not be found in a street with one or
                    more associated lots, the location of the closest number will
                    be returned and a message will be printed to stdout if
                    `verbose` is `True`.
        verbose : boolean
                  Whether debugging messages should be printed to stdout or not.

        Returns
        -------
        (lat, lon) or None if the street has no associated lots.
                   Locations are expressed in EPSG:4326 (WGS84).

        Raises
        ------
        MunicipalityNotFoundException is the given (municipality, province) values
        can not be found in the Spanish cadastre (you might be looking for municipalities
        in the Basque country or Navarra, which have their own land registries).
        """

        # Make the municipalities uppercase, and substitute name/province, if needed
        municipality = municipality.strip().upper()
        province = province.strip().upper()
        street_name = street_name.strip().upper()
        if (municipality, province) in self.replacements:
            municipality, province = self.replacements[(municipality, province)]

        # Try to fetch a session object for the municipality in the given address
        if (municipality, province) in self._municipalities:
            # Is (municipality, province) already connected? => Reuse object
            session = self._municipalities[(municipality, province)]
        else:
            # Ask the DB for Municipality objects which have
            # the same name as the one in the given Address
            query = self.c.dbsession.query(Municipality).filter(Municipality.name == municipality)

            # We can get 0, 1 or more than 1 results for a municipality name
            if query.count() == 0:
                raise MunicipalityNotFoundException('Could not find any municipality named '
                                                    '"{}" in "{}"'.format(municipality, province))
            elif query.count() == 1:
                # Found only one municipality check that the province matches
                if query[0].province.name == province:
                    session = self.c.get_municipality_session(query[0])
                    self._matched_streets[(municipality, province)] = {}
                    self._municipalities[(municipality, province)] = session
                    self._matched_street_centers[(municipality, province)] = {}
                else:
                    raise MunicipalityNotFoundException('Could not find any municipality named ' +
                                                        '"{}" in "{}" '.format(municipality, province) +
                                                        'but I found one in "{}"'.format(query[0].province.name))
            else:
                # Found many municipalities for the given name,
                # try to choose one based on province name
                session = None
                for m in query:
                    if m.province.name == province:
                        session = self.c.get_municipality_session(m)
                        self._matched_streets[(municipality, province)] = {}
                        self._municipalities[(municipality, province)] = session
                        self._matched_street_centers[(municipality, province)] = {}

                if session is None:
                    # Found no province matches
                    raise MunicipalityNotFoundException('Could not find any municipality named ' +
                                                        '"{}" in "{}" '.format(municipality,
                                                                               province) +
                                                        'but I found many in other provinces')

        # We have a session object for the given municipality, check for
        # the street with a name closest to the one in the Address object
        if street_name in self._matched_streets[(municipality, province)]:
            matched_street = self._matched_streets[(municipality, province)][street_name]
            if verbose:
                sys.stdout.write('\t"{}" → "{}" ({})'.format(street_name,
                                                             matched_street.denomina,
                                                             municipality) +
                                 BLUE + ' CACHED\n' + END)
        else:
            # Get a list of all the street names in the municipality
            if (municipality, province) in self._streets:
                streets = self._streets[(municipality, province)]
            else:
                query = session.query(Carvia)
                streets = {}
                for street in query.all():
                    for name in self._normalize_address(street.denomina):
                        streets[name] = street
                self._streets[(municipality, province)] = streets

            # Choose the closest match from `streets` to the normalized
            # and denormalized versions of the street name
            matched_street = None
            min_distance = 9.e20
            for normalized in self._normalize_address(street_name):
                # If the normalized version of the street name is included
                # in streets, we take that
                if normalized in streets.keys():
                    matched_street = streets[normalized]
                    break
                else:
                    for street in sorted(streets.keys()):
                        distance = self._distance(normalized, street)
                        if distance < min_distance:
                            min_distance = distance
                            matched_street = streets[street]

            self._matched_streets[(municipality, province)][street_name] = matched_street
            if verbose:
                sys.stdout.write('\t"{}" → "{}" ({})'.format(street_name,
                                                             matched_street.denomina,
                                                             municipality) +
                                 GREEN + ' MATCHED\n' + END)

        # streetno can now be either an integer or None.
        # If streetno is an integer:
        #       - We try to find a lot in that street with the given number and
        #         return its location. We'll return the location of the first lot
        #         we can find, which might not be completely precise, but
        #         I don't care.
        #
        #         Should that fail, we'll return the location of the closest number.
        #         For example: if you ask for number 40 in a street whose numbers
        #         range (1-26), you'll get the location of number 26 and a msg
        #         will be output when in verbose mode.
        # If streetno is None:
        #       - We'll return the mean of the positions of all the lots
        #         in the street.
        if street_no is not None:
            query = session.query(Parcela).filter(Parcela.via_id == matched_street.via,
                                                  Parcela.numero == street_no).limit(1)
            if query.count() > 0:
                # Return (lat, lon)
                return query[0].coory, query[0].coorx

            # We could not match the number, provide the closest one
            order = text('ABS(NUMERO - {}) ASC'.format(street_no))
            query = session.query(Parcela).filter(Parcela.via_id == matched_street.via).order_by(order).limit(1)
            if query.count() > 0:
                if verbose:
                    sys.stdout.write(YELLOW + 'WARNING: ' + END +
                                     'You asked for number '
                                     '{} in "{}" → "{}" '.format(street_no, street_name,
                                                                 matched_street.denomina) +
                                     '({}) '.format(municipality) +
                                     'but the closest I could find was '
                                     '#{}\n'.format(query[0].numero))
                # Return (lat, lon)
                return query[0].coory, query[0].coorx

            # There must be no lots, associated with the matched street, return None
            if verbose:
                sys.stdout.write('"{}" → "{}" ({}) '.format(street_name,
                                                            matched_street.denomina,
                                                            municipality) +
                                 'has no associated lots...\n')
            return None

        if matched_street.denomina not in self._matched_street_centers[(municipality, province)]:
            n = 0
            lat = 0.
            lon = 0.
            for lot in matched_street.parcelas:
                n += 1
                lat += lot.coory
                lon += lot.coorx

            # This is a weird case where a street has no associated lots. No idea on how to proceed
            if n == 0:
                if verbose:
                    sys.stdout.write('"{}" → "{}" ({}) '.format(street_name,
                                                                matched_street.denomina,
                                                                municipality) +
                                     'has no associated lots...\n')
                return None

            self._matched_street_centers[(municipality,
                                          province)][matched_street.denomina] = (lat / n, lon / n)

        return self._matched_street_centers[(municipality, province)][matched_street.denomina]

    @staticmethod
    def _distance(s, t):
        """
        Edit distance aka Levenshtein distance between the given elements.

        Parameters
        ----------
        s : string
            First element on the distance measurement.
        t : string
            Second element on the distance measurement.

        Returns
        -------
        d : distance between the input strings
        """

        if s == t:
            return 0
        elif len(s) == 0:
            return len(t)
        elif len(t) == 0:
            return len(s)
        v0 = [None] * (len(t) + 1)
        v1 = [None] * (len(t) + 1)
        for i in range(len(v0)):
            v0[i] = i
        for i in range(len(s)):
            v1[0] = i + 1
            for j in range(len(t)):
                cost = 0 if s[i] == t[j] else 1
                v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
            for j in range(len(v0)):
                v0[j] = v1[j]

        return v1[len(t)]

    @staticmethod
    def _normalize_address(address):
        """
        Try to normalize `address` by removing meaningless words and
        converting large names into shorter ones used in the cadastre
        """
        # A list of meaningless words
        empty_words = ["DE", "DEL", "EL", "LA", "LOS", "LAS"]
        # A list of well-known abbreviations, taken from:
        # http://www.catastro.meh.es/ws/webservices_catastro.pdf (Annex I)
        street_types = {'ACCESO': ['AC'], 'AGREGADO': ['AG'], 'ALDEA': ['AL'],
                        'ALAMEDA': ['AL'], 'ANDADOR': ['AN'], 'AREA': ['AR'],
                        'ARRABAL': ['AR'], 'AUTOPISTA': ['AU'], 'AVENIDA': ['AV'],
                        'ARROYO': ['AY'], 'BAJADA': ['BJ'], 'BLOQUE': ['BL'],
                        'BARRIO': ['BO'], 'BARRANQUIL': ['BQ'], 'BARRANCO': ['BR'],
                        'CAÑADA': ['CA'], 'COLEGIO': ['CG', 'CO'], 'CIGARRAL': ['CG'],
                        'CHALET': ['CH'], 'CINTURON': ['CI'], 'CALLEJA': ['CJ'],
                        'CALLEJON': ['CJ'], 'C\\': ['CL'], 'CALLE': ['CL'], 'CAMINO': ['CM'],
                        'CARMEN': ['CARMEN'], 'COLONIA': ['CN'], 'CONCEJO': ['CO'],
                        'CAMPA': ['CP'], 'CAMPO': ['CP'], 'CARRETERA': ['CR'],
                        'CARRERA': ['CARRERA'], 'CASERIO': ['CS'], 'CUESTA': ['CT'],
                        'COSTANILLA': ['CT'], 'CONJUNTO': ['CU'], 'CALEYA': ['CY'],
                        'CALLIZO': ['CZ'], 'DETRÁS': ['DE'], 'DIPUTACION': ['DP'],
                        'DISEMINADOS': ['DS'], 'EDIFICIOS': ['ED'], 'EXTRAMUROS': ['EM'],
                        'ENTRADA': ['EN'], 'ENSANCHE': ['EN'], 'ESPALDA': ['EP'],
                        'EXTRARRADIO': ['ER'], 'ESCALINATA': ['ES'], 'EXPLANADA': ['EX'],
                        'FERROCARRIL': ['FC'], 'FINCA': ['FN'], 'GLORIETA': ['GL'],
                        'GRUPO': ['GR'], 'GRAN VIA': ['GV'], 'HUERTA': ['HT'],
                        'HUERTO': ['HT'], 'JARDINES': ['JR'], 'LAGO': ['LA'],
                        'LADO': ['LD'], 'LADERA': ['LD'], 'LUGAR': ['LG'],
                        'MALECON': ['MA'], 'MERCADO': ['MC'], 'MUELLE': ['ML'],
                        'MUNICIPIO': ['MN'], 'MASIAS': ['MS'], 'MONTE': ['MT'],
                        'MANZANA': ['MZ'], 'POBLADO': ['PB'], 'PLACETA': ['PC'],
                        'PARTIDA': ['PD'], 'PARTICULAR': ['PI'], 'PASAJE': ['PJ'],
                        'PASADIZO': ['PJ', 'PU'], 'POLIGONO': ['PL'], 'PARAMO': ['PM'],
                        'PARROQUIA': ['PQ'], 'PARQUE': ['PQ'], 'PROLONGACION': ['PR'],
                        'CONTINUAC': ['PR'], 'PASEO': ['PS'], 'PUENTE': ['PT'],
                        'PLAZA': ['PZ'], 'QUINTA': ['QT'], 'RACONADA': ['RA'],
                        'RAMBLA': ['RB'], 'RINCON': ['RC'], 'RINCONA': ['RC'],
                        'RONDA': ['RD'], 'RAMAL': ['RM'], 'RAMPA': ['RP'],
                        'RIERA': ['RR'], 'RUA': ['RU'], 'SALIDA': ['SA'], 'SECTOR': ['SC'],
                        'SENDA': ['SD'], 'SOLAR': ['SL'], 'SALON': ['SN'],
                        'SUBIDA': ['SU'], 'TERRENOS': ['TN'], 'TORRENTE': ['TO'],
                        'TRAVESIA': ['TR'], 'TRAVESÍA': ['TR'],
                        'URBANIZACION': ['UR'], 'URBANIZACIÓN': ['UR'], 'VALLE': ['VA'],
                        'VIADUCTO': ['VD'], 'VIA': ['VI'], 'VIAL': ['VL'], 'VEREDA': ['VR']}
        address = address.upper()
        # Convert first word, if we know how to
        normalized = set()
        parts = address.split()
        if address.startswith('GRAN VIA'):
            normalized.add(' '.join(street_types['GRAN VIA'] + parts[2:]))
        elif parts[0].replace('.', '') in street_types:
            for abbreviation in street_types[parts[0].replace('.', '')]:
                normalized.add(' '.join([abbreviation] + parts[1:]))

        # Filter out empty words
        for name in normalized:
            yield ' '.join([part for part in name.split() if part not in empty_words])

        if address not in normalized:
            yield ' '.join([part for part in address.split() if part not in empty_words])


if __name__ == '__main__':
    # Setting online=True will try to download and cache cadastre data to mathlas/geo/data
    # Online fetching requires the following dependencies:
    #   * sqlalchemy
    #   * requests
    #   * beautifulsoup4
    #   * gdal
    # You also need a digital client certificate (recognised by the spanish government) in
    # mathlas/geo/client_cert; see mathlas/geo/client_cert/README.md for instructions on
    # how to put it there.

    # Let's print the list of provinces considered in the spanish cadastre like so:
    cadastre_data = SpanishCadastreFetcher(online=True)
    print('Provinces:')
    for province in cadastre_data.get_provinces(force_offline=False):
        print('\t* {}'.format(province.name))
    # Provinces:
    # 	* ALBACETE
    # 	* ALACANT
    # 	* ALMERIA
    # 	* AVILA
    # 	* BADAJOZ
    # 	* ILLES BALEARS
    # 	* BARCELONA
    # 	* BURGOS
    # 	* CACERES
    # 	* CADIZ
    # 	* CASTELLO
    # 	* CIUDAD REAL
    # 	* CORDOBA
    # 	* A CORUÑA
    # 	* CUENCA
    # 	* GIRONA
    # 	* GRANADA
    # 	* GUADALAJARA
    # 	* HUELVA
    # 	* HUESCA
    # 	* JAEN
    # 	* LEON
    # 	* LLEIDA
    # 	* LA RIOJA
    # 	* LUGO
    # 	* MADRID
    # 	* MALAGA
    # 	* MURCIA
    # 	* OURENSE
    # 	* ASTURIAS
    # 	* PALENCIA
    # 	* LAS PALMAS
    # 	* PONTEVEDRA
    # 	* SALAMANCA
    # 	* S.C. TENERIFE
    # 	* CANTABRIA
    # 	* SEGOVIA
    # 	* SEVILLA
    # 	* SORIA
    # 	* TARRAGONA
    # 	* TERUEL
    # 	* TOLEDO
    # 	* VALENCIA
    # 	* VALLADOLID
    # 	* ZAMORA
    # 	* ZARAGOZA
    # 	* CEUTA
    # 	* MELILLA

    # Print the first 10 municipalities in Madrid:
    print('First 10 municipalities in Madrid:')
    municipalities = cadastre_data.get_municipalities('Madrid', force_offline=False)
    for municipality in list(municipalities)[:10]:
        print('\t* {}'.format(municipality.name))
    # First 10 municipalities in Madrid:
    # 	* AJALVIR
    # 	* ALAMEDA DEL VALLE
    # 	* ALCALA DE HENARES
    # 	* ALCOBENDAS
    # 	* ALCORCON
    # 	* ALDEA DEL FRESNO
    # 	* ALGETE
    # 	* ALPEDRETE
    # 	* AMBITE
    # 	* ANCHUELO

    # We'll try to get the EPSG:4326 coordinates of the Mathlas office
    # Province and municipality names must exactly match (minus case and
    # initial and final spaces) those in the cadastre data, but the street
    # name and number can differ a little, subce we're performing a fuzzy
    # match.
    # Let's first look for the street name and number with an exact match
    f = SpanishCadastreLocationFetcher(online=True)
    coords = f.coords('Madrid', 'Madrid', 'Paseo de las Delicias', 30, verbose=True)
    print(coords)

    # Of course, since these things tend to be hand-input, we might get
    # misspelled names. This will still find the correct location
    coords = f.coords('Madrid', 'Madrid', 'Paseo Deliciss', 30, verbose=True)
    print(coords)

    # If the errors get worse, the location process might fail
    # It will fail because in Madrid we have "Paseo de las Delicias" and "Calle Delicias"
    # The code will find "Calle Delicias" but warn that it could not find number 30 and
    # give us the coordinates of Calle Delicias 31 (the closest match it could find).
    coords = f.coords('Madrid', 'Madrid', 'Delicias', 30, verbose=True)
    print(coords)
