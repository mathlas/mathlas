# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
This module include coordinate system manipulation routines.

It should work with either OSR (from GDAL) or pyproj
(usually comes with matplotlib basemap)
"""

from mathlas.not_for_clients.exceptions import HereBeDragonsException

with_osr = False
with_basemap = False
try:
    import osr
    with_osr = True
except ImportError:
    try:
        import mpl_toolkits.basemap.pyproj as pyproj
        with_basemap = True
    except ImportError:
        raise ImportError('Either pyproj or basemap is required')


class Transform:
    def __init__(self, inEPSG=25831, outEPSG=4326):
        """
        This class provides a routine to convert coordinates
        between the given EPSG coordinate systems.

        By default, it converts EPSG:25831 -> EPSG:4326 (Google Earth, OSM).
        Google Maps uses EPSG:3857

        You can search for EPSG codes at:
            http://spatialreference.org/ref/epsg/
        Or in the accompanying file EPSG_list.md

        Parameters
        ----------
        inEPSG : EPSG system for input coordinates, optional
        outEPSG : EPSG system for output coordinates, optional
        """

        self.inEPSG = inEPSG
        self.outEPSG = outEPSG

        if with_osr:
            # create coordinate transformation
            inSpatialRef = osr.SpatialReference()
            try:
                inSpatialRef.ImportFromEPSG(inEPSG)
            except (SystemError, TypeError):
                raise ValueError('Could not understand given inEPSG value ({})'.format(inEPSG))

            outSpatialRef = osr.SpatialReference()
            try:
                outSpatialRef.ImportFromEPSG(outEPSG)
            except (SystemError, TypeError):
                raise ValueError('Could not understand given outEPSG value ({})'.format(outEPSG))

            self._transform = osr.CoordinateTransformation(inSpatialRef,
                                                           outSpatialRef)
        elif with_basemap:
            try:
                self._inCS = pyproj.Proj("+init=EPSG:{}".format(inEPSG))
            except RuntimeError:
                raise ValueError('Could not understand given inEPSG value ({})'.format(inEPSG))
            try:
                self._outCS = pyproj.Proj("+init=EPSG:{}".format(outEPSG))
            except RuntimeError:
                raise ValueError('Could not understand given outEPSG value ({})'.format(outEPSG))
        else:
            raise HereBeDragonsException('You should not be here')

    def apply(self, coordx, coordy):
        """
        Apply the transformation to the given coordinates.

        Parameters
        ----------
        coordx : float
                 The x coordinate of the point to transform
                 In EPSG:4326 this is the longitude.
        coordy : float
                 The y coordinate of the point to transform
                 In EPSG:4326 this is the latitude

        Returns
        -------
        transformedcoords : tuple
                            (transformed_x, transformed_y) (floats)
        """

        if with_osr:
            _coordx, _coordy, _ = self._transform.TransformPoint(coordx, coordy)
        elif with_basemap:
            _coordx, _coordy = pyproj.transform(self._inCS, self._outCS, coordx, coordy)
        else:
            raise HereBeDragonsException('You should not be here')

        return _coordx, _coordy
