#!/usr/bin/env python3
import requests

# A simple GET request to a site whose CA files are stored in this directory
try:
    r = requests.get('https://www.sede.fnmt.gob.es/', verify='.')
    if r.status_code == 200:
        # Everything's fine
        print('Certificates are working well')
    else:
        # Couldn't fetch the file, but the certificates did not fail
        print('Certificates seem to be working well, but there is something wrong with this script ({})'.format(r))
except requests.exceptions.SSLError:
    print('Certificates are not working as expected')
