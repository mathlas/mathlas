Introduction
============

This directory contains the Spanish administration's Certificate Authority (CA) files required in order to ensure a secure connection to any official HTTPS sercure server.

The certificate files have been downloaded from the [FNMT site](https://www.sede.fnmt.gob.es/descargas/certificados-raiz-de-la-fnmt) and have been imported and the exported as X.509 (with trust chain) certificates in Firefox.

The `c_rehash` utility has been applied to this directory in order to create links to the certificate files with their hashes, as needed by the Python [requests](http://docs.python-requests.org/en/master/user/advanced/) module.

In windows, you can run the following command on an elevated prompt:

```cmd
mklink 1fe0bb9f.0 ACFNMTUsuarios.crt
mklink 2207fa1d.0 FNMTClase2CA-FNMT.crt
mklink 9d682116.0 ACComponentesInformáticos.crt
mklink cd8c0d63.0 ACRAIZFNMT-RCM-FNMT-RCM.crt
mklink fdc4abd4.0 ACAdministraciónPública.crt
mklink fdc4abd4.1 ACAdministraciónPública_.crt
```

You can run `sample.py` in this directory to check that the certificates are working for you.
