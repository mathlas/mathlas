# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
A class representing a parcel object in the DB
"""

from .base import CadastreInfoBase
from .carvia import Carvia
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, Float


class Parcela(CadastreInfoBase):
    __tablename__ = 'PARCELA'

    # The attributes we'll be using from the DB
    # Here we're trying to be as faithful to the existing schema
    # as possible
    ninterno = Column('NINTERNO', Float, primary_key=True)
    pcat1 = Column('PCAT1', String(7))
    pcat2 = Column('PCAT2', String(7))
    via_id = Column('VIA', ForeignKey('Carvia.VIA'))
    via = relationship(Carvia, backref='parcelas')
    numero = Column('NUMERO', Integer)
    numerodup = Column('NUMERODUP', String(1))
    numsymbol = Column('NUMSYMBOL', Integer)
    area = Column('AREA', Integer)
    fechaalta = Column('FECHAALTA', Integer)
    fechabaja = Column('FECHABAJA', Integer)
    mapa = Column('MAPA', Integer)
    delegacio = Column('DELEGACIO', Integer)
    municipio = Column('MUNICIPIO', Integer)
    masa = Column('MASA', String(5))
    hoja = Column('HOJA', String(7))
    tipo = Column('TIPO', String(1))
    parcela = Column('PARCELA', String(5))
    # coords are stored in EPSG:4326 (WGS 84)
    coorx = Column('COORX', Float)      # lon
    coory = Column('COORY', Float)      # lat
    refcat = Column('REFCAT', String(14))

    def __repr__(self):
        if self.numerodup.lower() != 'none':
            name = '{} {}{}'.format(self.via.denomina, self.numero, self.numerodup)
        else:
            name = '{}, {}'.format(self.via.denomina, self.numero)
        return '<Parcela {}: {}>'.format(int(self.ninterno), name)
