# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
A class representing a municipality object in the DB
"""

from .base import MunicipalitiesBase
from .province import Province
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String


class Municipality(MunicipalitiesBase):
    __tablename__ = 'municipalities'

    # The attributes we'll be using from the DB
    # Here we're trying to be as faithful to the existing schema
    # as possible
    # We also declare the INE municipality ID-INE province ID as primary keys
    ine_id = Column('id_ine', Integer, primary_key=True)
    ine_province_id = Column('id_ine_province', ForeignKey('provinces.id'), primary_key=True)
    province = relationship(Province, backref='municipalities')
    mhap_id = Column('id_mhap', Integer)
    mhap_delegation_id = Column('id_mhap_delegation', Integer)
    name = Column('name', String)

    def __repr__(self):
        return '<Municipality {}: {} ({})>'.format(self.ine_id, self.name, self.ine_province_id)
