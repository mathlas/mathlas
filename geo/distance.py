#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import warnings
import numpy as np


def ellipsoidal_distance(origin_lon, origin_lat, dest_lon, dest_lat):
    """
    Calculate the distance in kilometers between points

    The function can be used to compute the distance between two points
    by providing scalar values for the input arguments, but it can also
    be used for computing the distance between two sets of points by
    providing equal-length iterables (anything usable by np.asarray will
    work) or for computing the distance from a set of points to a single
    point (the distance of each point in a cloud to its centroid) by
    providing iterables on either origin/dest and a scalar for the other
    argument.

    The distance function is symmetric, so
        ellipsoidal_distance(A, B) = ellipsoidal_distance(B, A)
    calling them "origin" and "dest" is a mere formalism with no real
    meaning.

    The formula is valid for distances below 475km and the coordinates
    of the points must be expressed in WGS84/EPSG:4326.

    Taken from:
    https://en.wikipedia.org/wiki/Geographical_distance#Ellipsoidal_Earth_projected_to_a_plane

    Parameters
    ----------
    origin_lon : iterable or int or float
                 Longitude of the origin point(s)
    origin_lat : iterable or int or float
                 Longitude of the origin point(s)
    dest_lon : iterable or int or float
               Longitude of the destination point(s)
    dest_lat : iterable or int or float
               Longitude of the destination point(s)

    Returns
    -------
    dist : float or np array
           distance between the given points
    """

    # Convert all the inputs to np arrays, so that we don't have to care about their types
    origin_lon = np.asarray(origin_lon)
    origin_lat = np.asarray(origin_lat)
    dest_lon = np.asarray(dest_lon)
    dest_lat = np.asarray(dest_lat)

    mean_lat = 0.5 * (origin_lat * dest_lat)
    delta_lat = origin_lat - dest_lat
    delta_lon = origin_lon - dest_lon

    k1 = 111.13209 - 0.56605 * np.cos(2 * mean_lat) + 0.00120 * np.cos(4 * mean_lat)
    k2 = (111.41513 * np.cos(mean_lat) -
          0.09455 * np.cos(3. * mean_lat) +
          0.00012 * np.cos(5. * mean_lat))

    a = k1 * delta_lat
    b = k2 * delta_lon
    dist = np.sqrt(a * a + b * b)

    if np.max(dist) > 475:
        warnings.warn('ellipsoidal_distance is not a valid function for '
                      'computing the distance between the given points')

    return dist
