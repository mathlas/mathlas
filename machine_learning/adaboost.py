#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import gc
import sys
import time
import signal
import numpy as np
import pandas as pd
from pathlib import Path
from mathlas.object import MathlasObject
from mathlas.machine_learning.decision_tree import DecisionTree
from mathlas.statistics.probabilistic_choice_object import ProbabilisticChoiceObject


class Adaboost(MathlasObject):
    def __init__(self, training_data=None, result_column=None, rounds=40,
                 min_delta=None, max_tree_depth=1, keep_data=False):
        """
        Generate a boosting object, and return a hypothesis object

        Parameters
        ----------
        training_data : Pandas DataFrame
                        training_data should contain the data used for training the model
                        40% of the data will be used for model training, whereas the
                        remaining 60% will be used for model validation.
        result_column : string
                        The name of the column that the model will be trained to match.
        rounds : Integer
                 The number of learners which will be created.
                 Defaults to 40.
        min_delta : float, optional
                    The value of min_delta to pass to the learner (defaults to None)
        max_tree_depth : integer, optional
                         The maximum depth for the decision trees.
                         Defaults to 1.
        keep_data : boolean, optional
                   Whether training and validation data should be stored in the model.
                   By default data is not stored with the model.
        """
        # Data sanity checks
        if training_data is None:
            raise ValueError('You must provide training_data')

        if result_column not in training_data.columns:
                        raise ValueError('{} not in {}'.format(result_column,
                                                               training_data.columns))

        # Register a custom signal handler for SIGUSR1, which will dump progress info
        if sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
            handler = signal.getsignal(signal.SIGUSR1)
            self.__t0 = time.time()
            signal.signal(signal.SIGUSR1, self._print_status)

        # Initialize some variables
        self.learners = [None] * rounds
        self.learnerWeights = np.zeros(rounds)   # Placeholder values
        self.trainingData = training_data.copy()
        self.resultColumn = result_column
        validation_data = training_data.copy()

        # Perform some checks over the data
        results = self.trainingData[result_column]
        self.different_results = sorted(list(set(results)))
        if len(self.different_results) != 2:
            raise ValueError("Results column is not binary.")

        for col in self.trainingData.columns:
            if not (self.trainingData[col].dtype in ["int32", "int64", "float32", "float64"] or
                    (hasattr(self.trainingData[col], 'cat') and
                         self.trainingData[col].cat.ordered)):
                if len(set(self.trainingData[col])) != 2:
                    raise ValueError('Parameter "{}" has no '.format(col) +
                                     'ordinal relation or is not binary')

        # Create dataframe of results
        l = [[1, 0] if elem == self.different_results[0] else [0, 1] for elem in results]
        self.results = pd.DataFrame(l, index=results.index, columns=self.different_results)

        # Loop through the number of rounds and optimize the learnerWeights
        sampleWeights = pd.Series(np.ones(self.trainingData.shape[0]),
                                  index=self.trainingData.index)
        for i in range(rounds):
            # Train a stump
            self.learners[i] = DecisionTree(self.trainingData, result_column,
                                            weights=sampleWeights,
                                            maxLevels=max_tree_depth,
                                            minDelta=min_delta)
            gc.collect()

            # Determine the error obtained when categorizing the training data with the
            # newly-created stump
            self.learners[i].categorize(validation_data)
            probabilistic_results = self.turn_validation_into_probabilities(validation_data)
            error_df = np.abs(0.5 * (self.results - probabilistic_results))
            error = error_df.mul(sampleWeights, axis=0).sum().sum() / sampleWeights.sum()
            # This implementation can only handle predictors
            # whose error is better than random guessing (50%)
            if error > 0.5:
                raise RuntimeError('Trained stump has an unacceptable error '
                                   '({:.2f}), failing...'.format(error))
            frames = []
            for key in self.different_results:
                frames.append(probabilistic_results.loc[results == key, key])
            success_probability = pd.concat(frames)
            sampleWeights *= (success_probability / (2. * (1. - error)) +
                              (1. - success_probability) / (2. * error))

            # Store the learner weight
            self.learnerWeights[i] = 0.5 * np.log((1-error)/error)

            if self.learnerWeights[i] < 1e-30:
                break

        # Do not store training and validation data if told not to
        if not keep_data:
            del self.trainingData

            # Explicitly run a garbage collection cycle
            gc.collect()

        # Unregister the custom SIGUSR1 signal handler now that we're done
        if sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
            signal.signal(signal.SIGUSR1, handler)
            del self.__t0

    def turn_validation_into_probabilities(self, dataset, different_results=None):
        """
        This helper method will read data from a dataset and returns a "probabilities" DataFrame

        This is useful since the decision tree implementation can either return a fixed value
        or a probabilistic object, and both have to be operated on separately since they are
        conceptually different.

        Parameters
        ----------
        dataset : Pandas DataFrame as output by the machine learning algorithms
                  This DataFrame must contain a column named like self.resultcolumn
        different_results : iterable, optional
                  An iterable with the acceptable results.
                  All the results in dataset[self.resultcolumn] must be contained in this
                  iterable.
                  If it is not given, the object's self.different_results iterable gets
                  used instead.

        Returns
        -------
        Pandas DataFrame of size (dataset.shape[0] x len(different_results)) with floats.
        For each row, the column with the highest value in this DataFrame represents the
        most likely result.
        """
        if different_results is None:
            different_results = self.different_results

        # Store the different possible predictions from the dataset
        # dataset_results_raw can contain both string objects and
        # probabilistic objects, whereas dataset_results_labels
        # will only contain result names.
        # For example: if dataset_results_raw contained two values:
        #   * A string whose value is "Mild"
        #   * A probabilistic object with values:
        #       - "No sickness" with 20% probability
        #       - "Mild" with 50% probability
        #       - "Severe" with 30% probability
        # then dataset_results_labels = ["No sickness",
        #                                "Mild",
        #                                "Severe"]
        #
        # We also check that if different_results are given, the dataset
        # does not contain values not contained in different_results
        dataset_results_raw = set(dataset[self.resultColumn])
        dataset_results_labels = []
        for elem in dataset_results_raw:
            if isinstance(elem, ProbabilisticChoiceObject):
                dataset_results_labels.extend(list(elem.p.keys()))
            else:
                dataset_results_labels.append(elem)

        if ((different_results is not None) and
                not (np.all([elem in different_results for elem in set(dataset_results_labels)]))):
            raise ValueError("Some new key has appeared in the results")
        if different_results is None:
            different_results = dataset_results_labels

        # Compute a "probabilities" Pandas DataFrame where each row corresponds
        # to an entry in the original dataset and each row corresponds to the
        # each of the possible predictions in "dataset_results_raw"
        probabilistic_results = pd.DataFrame(np.zeros((len(dataset), len(different_results))),
                                             columns=different_results, index=dataset.index)
        for key in dataset_results_raw:
            mask = dataset[self.resultColumn] == key
            if isinstance(key, ProbabilisticChoiceObject):
                values = [key.p.get(elem, 0.) for elem in different_results]
            else:
                values = [1. if elem == key else 0. for elem in different_results]
            probabilistic_results.loc[mask, different_results] = values

        # Normalize the get real probabilities
        return probabilistic_results.apply(lambda x: x/x.sum(), axis=1)

    def categorize(self, data, n_levels=None,
                   criterion="probabilistic",
                   store_values='raw'):
        """
        Categorize the DataFrame "data" based on the trained model.

        Parameters
        ----------
        data : Pandas DataFrame whose data you want categorized
               Its schema must match that in ``self.trainingData``
        n_levels: integer
                  Number of levels of the adaboost learner (the total number of learners
                  over which the categorization should be averaged)
        criterion: string, optional
                   The categorization criterion to use. Values can be:
                        * "probabilistic" : The result with the highest sum of
                           weighted probability will be chosen as the category
                        * "majority" : The result with the highest sum of
                           weights will be chosen as the category
                   The default is to perform "probabilistic" categorization
        store_values: ['raw', 'majority'], optional
                      Determines how the categorized data should be stored.
        """
        if n_levels is None or n_levels > len(self.learners):
            learner_indices = range(len(self.learners))
        else:
            learner_indices = range(n_levels)

        # Run all the learners in the data to determine the answers
        if criterion == "probabilistic":
            results = None
            for i in learner_indices:
                if self.learnerWeights[i] > 0:
                    self.learners[i].categorize(data)
                    probabilistic_results = self.turn_validation_into_probabilities(data)
                    if results is None:
                        results = self.learnerWeights[i] * probabilistic_results
                    else:
                        results += self.learnerWeights[i] * probabilistic_results
        elif criterion == "majority":
            results = pd.DataFrame(np.zeros((len(data.index), len(self.different_results))),
                                   columns=self.different_results, index=data.index)
            for i in learner_indices:
                if self.learnerWeights[i] > 0:
                    self.learners[i].categorize(data, store_value="majority")
                    for key in self.different_results:
                        mask = data[self.resultColumn] == key
                        results.loc[mask, key] += self.learnerWeights[i]

        if store_values == 'raw':
            results /= np.sum(self.learnerWeights[learner_indices])

            # Store the probabilistic objects as results
            data[self.resultColumn] = results.apply(lambda x: ProbabilisticChoiceObject(
                    dict(zip(results.columns, x.values))), axis=1)
        elif store_values == 'majority':
            data[self.resultColumn] = results.idxmax(axis=1)
        else:       # typo detector
            raise ValueError('Given value for store_values is not valid')

    def cache(self, output_dir):
        """
        Cache the current AdaBoost object to the given path in disk.

        Parameters
        ----------
        output_dir : Path object or string
                     Output directory
        """
        output_dir = Path(output_dir)
        output_dir.mkdir(parents=True, exist_ok=True)
        # Store all attributes except "learners"
        super().cache(output_dir / 'adaBoost.pickle', unstorableAttribs=['learners'])

        for i, learner in enumerate(self.learners):
            self.learners[i].cache(output_dir / 'tree_{}.pickle'.format(i))

    def _load(self, input_dir):
        """
        Load an adaboost model from the given directory
        """
        input_dir = Path(input_dir)
        # Check the raw adaBoost object (without the associated learners)
        super()._load(input_dir / 'adaBoost.pickle')
        self.learners = [None] * len(self.learnerWeights)

        # Do the actual tree loading
        for i in range(len(self.learners)):
            try:
                self.learners[i] = DecisionTree.from_cache(input_dir / 'tree_{}.pickle'.format(i))
            except IOError:
                raise IOError('Cannot load tree file "tree_{}.pickle"'.format(i))

    def _print_status(self, signum, frame):
        """
        This method prints progress information on demand.
        """
        computed_learners = [1 for i in self.learners if i is not None]
        __t1 = time.time()
        __d = time.localtime()
        sys.stdout.write('\n\033[94mAdaBoost generation progress info:\n')
        sys.stdout.write('==================================\n')
        sys.stdout.write('\tDate: {}-{}-{} {}:{}:{}\n'.format(__d.tm_year, __d.tm_mon, __d.tm_mday,
                                                              __d.tm_hour, __d.tm_min, __d.tm_sec))
        sys.stdout.write('\tElapsed time: {}s\n'.format(__t1 - self.__t0))
        sys.stdout.write('\tTotal trees: {}\n'.format(len(self.learners)))
        sys.stdout.write('\tComputed trees: {}\n'.format(len(computed_learners)))
        sys.stdout.write('\tTree weights:')
        for i in range(len(computed_learners)):
            sys.stdout.write('\n\t\t{}'.format(self.learnerWeights[i]))
        sys.stdout.write('\033[0m\n')


if __name__ == '__main__':
    import tempfile
    from mathlas.examples.machine_learning.data.sickness_database import sickness_database

    symptoms = {"temperature": ["float", [36., 40.], lambda x, a, b: a + (b - a) * 6 * (
    0.5 * x ** 2 - (x ** 3) / 3.)],
                "headache": ["categoric", ["yes", "no"], [0.3, 0.7]],
                "vomits": ["categoric", ["never", "sometimes", "often"], [0.5, 0.4, 0.1]],
                "mucus": ["categoric", ["yes", "no"], [0.6, 0.4]]}
    sickness = {"flu": ["yes", "no"]}
    schema = {"flu": {"temperature": {"<37": "no",
                                      ">=37:<38": {"headache": {"no": {"mucus": {"yes": "yes",
                                                                                 "no": "no"}},
                                                                "yes": "yes"}},
                                      ">=38:<39": {"mucus": {"yes": "no",
                                                             "no": "yes"}},
                                      ">=39": {"mucus": {"yes": "no",
                                                         "no": "yes"}}}}}
    n = 20000
    fdb = sickness_database(n, symptoms, sickness, schema)
    fdb['vomits'] = fdb['vomits'].astype('category',
                                         categories=['never', 'sometimes', 'often'],
                                         ordered=True)

    print("DB generated")

    f = Adaboost(fdb, 'flu 0.0', min_delta=0.1)
    fdb2 = fdb.copy()
    f.categorize(fdb2, store_values='majority')
    cache_dir = Path(tempfile.gettempdir()) / 'adaboost_model'
    f.cache(cache_dir)
    f2 = Adaboost.from_cache(cache_dir)
    fdb3 = fdb.copy()
    f2.categorize(fdb3, store_values='majority')

    print('Success?: ', (fdb2 == fdb3).all().all())

    sys.exit(0)
