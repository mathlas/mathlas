# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from mathlas.object import MathlasObject
from scipy.stats import multivariate_normal
from mathlas.not_for_clients.exceptions import HereBeDragonsException


class HiddenMarkovModel(MathlasObject):
    def __init__(self, mode="train", var_type=None, **kwargs):
        """
        Multi-purpose Hidden Markov Model. It can generate samples from a given model or be trained
         from samples to obtain the values of the hyper-parameters. Both tasks can be achieved for
         discrete states or floating-point numbers, that is, it can be a classifier or a regressor.
         Also, if a path is provided, the method will be stored for further use. It can be loaded
         setting the input <mode> to "load"
        The class can try to divine what the user wants and auto-detect if a classifier or a
         regressor must be used/trained.
        In case it is being used as a generator the inputs <initial_hidden_state>,
         <transition_matrix>, and <length_of_sequences> are mandatory. If it is a generator of
         discrete states the inputs "emission_matrix" and "emitted_states" must be provided while
         for floating-point numbers the inputs "emission_means" and "emission_covariances"
         are mandatory.
        In case the user wants to train a model from the samples the following rules apply. If
         samples are strings the method will treat them as discrete classes and if they are floats
         a regressor will be trained. If integers are provided and there are less than six different
         values a classifier will be trained; if there are six or more expect a regressor.
        This is the list of mandatory inputs based on the value of the input <mode>:
         - If <mode> == "load" the only input must be <path>
         - If <mode> == "train" inputs must be <observed_samples> and <n_hidden_states>. To input
           the values of <var_type> and <seed> are, however, highly recommended. If
           <var_type> = "continuous" it is highly recommended to input <infimum_point> and
           <supremum_point> (if they are not provided it will be suppossed that all co-ordinates are
           normalized, i.e., in the interval [0,1]. If <var_type> = "categorical" it is recommended 
           to input <emmited_states>.
         - If <mode> == "generate" and <var_type> == <categorical> (this value can be inputted or be
           auto-detected) mandatory inputs are <n_sequences>, <length_of_sequences>,
           <initial_hidden_state>, <transition_matrix>, <emission_matrix>, and <emitted_states>. It
           is highly advised to input the values of <var_type> and <seed>.
         - If <mode> == "generate" and <var_type> == <continuous> (this value can be inputted or be
           auto-detected) mandatory inputs are <n_sequences>, <length_of_sequences>,
           <initial_hidden_state>, <transition_matrix>, <emission_means>, and
           <emission_covariances>. We recommend also inputting the values of <var_type> and
           <seed>.

        Parameters
        ----------
        mode : string
               In which way the HMM is going to be used. It can take values "load", "generate", or
                "train". The first mode loads a previously stored HMM. The seconds generates a
                sequence of values given the pieces of a HMM. The third calculates the
                hyper-parameters of a HMM given several sequences of observations.
        var_type : string or None, optional
                   It can take values "continuous" or "categorical". If None, the class will try
                    to auto-detect which ype have been inputted.
        path : string, optional
               path of the file where class is going to be load from (mode load) or stored to
               (modes generate or train). It is mandatory in the first case.
        seed : integer, optional
               Input only if mode is "generate" or "train". Seed used to generate random numbers.
        length_of_sequences : iterable of integers, optional
                              Input only if mode is "generate". List of lenghts of the sequences to
                               be generated.
        initial_hidden_state : NumPy array of 1-by-K, optional
                               Mandatoty input only if mode is "generate", otherwise ignored.
                                Probabilities of each latent state to be the initial state.
        transition_matrix : Numpy array of K-by-K, optional
                            Mandatory input only if mode is "generate", otherwise ignored. Each
                             entry, a_ij, of the matrix is the probability of going from the j-th
                             state to the i-th state in a given transition, i.e.,
                             p(x_{t}=i|x_{t-1}=j).
        emission_matrix : NumPy array of N-by-K, optional
                          Mandatory input only if mode is "generate" and <var_type> is inputted or
                           detected to be "categorical", otherwise ignored. Each entry, a_ij,
                           of the matrix is the probability of obtaining the i-th observable state
                           if the current latent state is the j-th.
        emitted_states : ordered iterable, optional
                        Mandatory input only if <mode> is "generate" and <var_type> is inputted or
                         detected to be "categorical". Highly recommended if <mode> is "train" and
                         <var_type> is categorical. Otherwise ignored. Labels of the emitted classes.
        emission_means : ordered iterable of K N-by-1 NumPy arrays, optional
                         Mandatory input only if mode is "generate" and <var_type> is inputted or
                          detected to be "continuous", otherwise ignored. Values of the means of the
                          Gaussian emissions for each latent state.
        emission_covariances : ordered iterable of K N-by-N NumPy arrays, optional
                               Mandatory input only if mode is "generate" and <var_type> is inputted
                               or detected to be "continuous", otherwise ignored. Values of the
                               covariance matrices of the Gaussian emissions for each latent state.
        infimum_point : N-by-1 NumPy array
                        Co-ordinates of the point whose elements are all smaller than any of those
                         of the sequence, i.e., the lower corner of the hyper-cube that contains all
                         the sequences. Used to estimate the initial values of the means and the
                         covariance matrices.
        supremum_point : N-by-1 NumPy array
                         Co-ordinates of the point whose elements are all greater than any of those
                         of the sequence. Used to estimate the initial values of the means and the
                         covariance matrices.
        observed_samples : iterable of ordered iterables, optional
                           Mandatory input only if mode is "train", otherwise ignored. List of
                            observed sequences of values.
        n_hidden_states : integer, optional
                          Mandatory input only if mode is "train", otherwise ignored. Value of
                           the number of latent, discrete states used to model the variables.
        min_em_improvement : float, optional
                             Input only if mode is "train"; default value is 1e-4. If at a given EM
                              step the improvement of the log likelihhod is lower than this the EM
                              iteration is stopped.
        keep_samples : boolean, optional
                       Flag indicating whether training data should be stored in a
                       `observed_samples` attribute or it should be discarded after training.
                       Defaults to keeping the samples.
        """

        ##########################################################################################
        # Let's start checking the basics
        self.mode = mode
        if self.mode not in ["load", "generate", "train"]:
            raise ValueError("Given value for <mode> not recognized")

        ##########################################################################################
        # If required, auto-detect type of inputs
        if self.mode == "load":
            additional_inputs = []
        elif self.mode == "generate":
            self.var_type, additional_inputs = self._autodetect_var_type_generate(var_type,
                                                                                  **kwargs)
            # Load seed of the generator of random numbers
            if "seed" in kwargs.keys():
                self.seed = kwargs["seed"]
                np.random.seed(kwargs["seed"])
            else:
                self.seed = None
                if kwargs["n_sequences"] != 0:
                    raise ValueError("A seed to generate random numbers has not been provided.")
        elif self.mode == "train":
            # There must be samples to check what to do with them
            if "observed_samples" not in kwargs.keys():
                raise ValueError("No samples are provided to train the HMM")
            # Store now paramters for further use
            self.n_sequences = len(kwargs["observed_samples"])
            self.length_of_sequences = [len(seq) for seq in kwargs["observed_samples"]]
            additional_inputs = []
            # If we want to model categorical variables and the emitted states are not provided we
            #  can look for how many different elements are in the samples
            self.var_type, out = self._autodetect_var_type_train(var_type, **kwargs)
            if self.var_type == "categorical":
                self.emitted_states = out
                self.n_emitted_states = len(self.emitted_states)
            elif self.var_type == "continuous":
                self.dim_emissions = out
            else:
                raise ValueError("Value of <var_type> is not recognized.")
            # Determine which init_method must be used
            random_cond = "seed" in kwargs.keys()
            predef_cond = ("initial_hidden_state" in kwargs.keys() and
                           "transition_matrix" in kwargs.keys() and
                           "infimum" not in kwargs.keys() and "supremum" not in kwargs.keys())
            if self.var_type == "categorical":
                predef_cond &= "emission_matrix" in kwargs.keys()
            elif self.var_type == "continuous":
                predef_cond &= ("emission_means" in kwargs.keys() and
                                "emission_covariances" in kwargs.keys())
            else:
                raise ValueError("Value of <var_type> is not recognized.")

            if random_cond and not predef_cond:
                self.init_method = "random"
                self.seed = kwargs["seed"]
                np.random.seed(kwargs["seed"])
                # Load extremes of the hyper-parallelepiped
                if self.var_type == "continuous":
                    if "infimum_point" in kwargs.keys() and "supremum_point" in kwargs.keys():
                        self.infimum = kwargs["infimum_point"]
                        self.supremum = kwargs["supremum_point"]
                    elif ("infimum_point" not in kwargs.keys() and
                          "supremum_point" not in kwargs.keys()):
                        self.infimum = np.zeros(self.dim_emissions)
                        self.supremum = np.ones(self.dim_emissions)
                    else:
                        raise ValueError(
                            "Only one of infimum/supremum have been provided, not both.")
            elif not random_cond and predef_cond:
                self.init_method = "inputted"
            else:
                raise Warning("Cannot detemine which initialization of parameters use: inputs are"
                              " contradictory.")
        else:
            raise ValueError("Inputted value of <mode> not recognized")

        ##########################################################################################
        # Check that all mandatory inputs have been provided and store them as attributes
        required_inputs = {"load": ["path"],
                           "generate": ["initial_hidden_state", "transition_matrix",
                                        "length_of_sequences"],
                           "train": ["observed_samples", "n_hidden_states"]}
        for key in (required_inputs[self.mode] + additional_inputs):
            if key not in kwargs.keys():
                raise AttributeError("Required argument <{}> not provided".format(key))
            setattr(self, key, kwargs[key])
        self.path = kwargs.get("path", None)
        if self.mode == "generate":
            # Check that all inputs are correctly formatted
            self._check_inputs_generate()
            if self.var_type == "continuous":
                self.pdf = [multivariate_normal(mean=m, cov=c).pdf
                            for m, c in zip(self.emission_means, self.emission_covariances)]
        elif self.mode == "train":
            self.min_em_improvement = kwargs.get("min_em_improvement", 1e-4)

        ##########################################################################################
        # What do we do with all this pre-processed data?
        if mode == "load":
            # ********** Load a previously instanced class **********
            self._load(self.path)
            return
        elif mode == "generate":
            # ********** Create data from a given HMM **********
            # Generate sequences
            self.n_sequences = len(self.length_of_sequences)
            self.observed_samples = []
            self.latent_samples = []
            for len_seq in self.length_of_sequences:
                hidden_states, observed_states = self.generate_data(len_seq)
                self.latent_samples.append(hidden_states)
                self.observed_samples.append(observed_states)
            # Store all information for further use
            if self.path is not None:
                self.cache(self.path)
        elif mode == "train":
            # ********** Train a HMM from provided data **********
            # Initialize parameters
            out = self._init_parameters(method=self.init_method, **kwargs)
            self.initial_hidden_state, self.transition_matrix, emission_parameters = out
            if self.var_type == "categorical":
                self.emission_matrix = emission_parameters
            elif self.var_type == "continuous":
                self.emission_means, self.emission_covariances, self.pdf = emission_parameters
            else:
                raise ValueError("Value of <var_type> is not recognized.")
            # Train model
            self._train_model()

            # Delete training data if told to
            if not kwargs.get('keep_samples', True):
                self.observed_samples = []

    @staticmethod
    def _autodetect_var_type_generate(var_type, **kwargs):
        """
        Method to detect which type of variable is beign used and provide a list of variables which
        must be expected in the dictionary of inputs if <mode>=="generate"

        Parameters
        ----------
        var_type : string or None
                   Type of variable being processed. It can take values "continuous", "categorical",
                   or None
        kwargs : dictionary
                 other arguments of the HMM

        Returns
        -------
        var_type : string or None
                   Type of variable being processed. When returned it can only take values
                   "continuous" or "categorical"
        additional_inputs : list
                            variables to be load depending on the type of variable being used
        """

        categorical_vars = ["emission_matrix", "emitted_states"]
        continuous_vars = ["emission_means", "emission_covariances"]

        # Perform auto-detection of the type of the inputted variable
        if var_type is None:
            flag_categorical = True
            flag_continuous = True
            for label in categorical_vars:
                flag_categorical &= label in kwargs.keys()
                flag_continuous &= label not in kwargs.keys()
            for label in continuous_vars:
                flag_categorical &= label not in kwargs.keys()
                flag_continuous &= label in kwargs.keys()

            if flag_categorical:
                var_type = "categorical"
            elif flag_continuous:
                var_type = "continuous"
            else:
                raise ValueError("The inputted variables do not allow auto-detection of the "
                                 "var_type to be performed")
        else:
            var_type = var_type

        # If an specific value is given for <var_type> then the variables to be used are
        #  crystal clear
        if var_type == "categorical":
            additional_inputs = categorical_vars
        elif var_type == "continuous":
            additional_inputs = continuous_vars
        else:
            raise ValueError("Value of the variable var_type is not recognized.")

        return var_type, additional_inputs

    def _check_inputs_generate(self):
        """
        Method to check all the inputted variables if the <mode> == "generate".

        Returns
        -------
        None
        """
        # Check that everything has the correct dimensions
        self.n_hidden_states = self.transition_matrix.shape[0]
        if self.n_hidden_states != self.transition_matrix.shape[1]:
            raise ValueError("The sizes of the inputted matrices are not correct.")
        if self.var_type == "categorical":
            self.n_emitted_states = len(self.emitted_states)
            if (self.n_hidden_states != self.emission_matrix.shape[1] or
                    self.n_emitted_states != self.emission_matrix.shape[0]):
                raise ValueError("The sizes of the inputted matrices are not correct.")
        elif self.var_type == "continuous":
            if (self.n_hidden_states != len(self.emission_means) or
                    self.n_hidden_states != len(self.emission_covariances)):
                raise ValueError("The sizes of the inputted matrices are not correct.")
            self.dim_emissions = len(self.emission_means[0])
            for v in self.emission_means:
                if len(v) != self.dim_emissions:
                    raise ValueError("Not all provided mean vectors are of the same dimension.")
            for m in self.emission_covariances:
                if m.shape != (self.dim_emissions, self.dim_emissions):
                    raise ValueError("Not all provided covariance matrices are of the same"
                                     " shape.")

    @staticmethod
    def _autodetect_var_type_train(var_type, **kwargs):
        """
        Method to detect which type of variable is beign used and provide some additional, required 
        variables if <mode>=="train"

        Parameters
        ----------
        var_type : string or None
                   Type of variable being processed. It can take values "continuous", "categorical", 
                   or None
        kwargs : dictionary
                 other arguments of the HMM

        Returns
        -------
        var_type : string or None
                   Type of variable being processed. When returned it can only take values 
                   "continuous" or "categorical"
        emitted_states : list
                         values of the various categories that can be emitted. Only returned if
                          <var_type>=="categorical"
        dim_emissions : integer
                        dimension of the space of emitted variables. Only returned 
                        if <var_type>=="continuous"
        """

        if (var_type is None or
                (var_type == "categorical" and "emitted_states" not in kwargs.keys())):
            # This is the maximum number of different elements that there are in the samples.
            #  Only used if the variables are integers, otherwise it is not used.
            max_n_int_states = 5
            # Samples can only be integers, floating-point numbers, or strings
            input_type = type(kwargs["observed_samples"][0][0])
            if input_type not in [int, float, str, np.ndarray]:
                raise ValueError("The type of the variables is not an expected type, namely it "
                                 "is {}".format(input_type))

            # If the samples are NumPy arrays the proble to solve is a continuous
            if input_type is np.ndarray:
                var_type = "continuous"
            else:
                # Check that every sample has the same type and, if we are dealing with
                #  integers, count how many different elements there are
                emitted_states = set([])
                n_different_values = 0
                for seq in kwargs["observed_samples"]:
                    if ((input_type is int) and (n_different_values <= max_n_int_states)) \
                            or (input_type is str):
                        emitted_states = emitted_states.union(set(seq))
                        n_different_values = len(emitted_states)
                    for elem in seq:
                        if type(elem) != input_type:
                            raise ValueError("There are at least two different types of values"
                                             " in the samples, namely {} and"
                                             " {}".format(input_type, type(elem)))

                # Perform auto-detection of the tupe of inputted variables.
                if input_type is float:
                    var_type = "continuous"
                elif input_type is str:
                    var_type = "categorical"
                elif input_type is int:
                    if n_different_values <= max_n_int_states:
                        var_type = "categorical"
                    else:
                        var_type = "continuous"
        elif var_type == "categorical":
            var_type = "categorical"
        elif var_type == "continuous":
            var_type = "continuous"
        else:
            raise ValueError("Value of <var_type> is not recognized.")

        if var_type == "categorical":
            # Store detected emitted states and/or check that the inputted states meet the
            #  detected ones.
            if "emitted_states" in kwargs.keys():
                emitted_states = kwargs["emitted_states"]
            else:
                emitted_states = list(emitted_states)
            n_emitted_states = len(emitted_states)
            # Check that variables are
            if "emitted_states" in kwargs.keys() and hasattr(locals(), "emitted_states"):
                emitted_states = kwargs["emitted_states"]
                if (set(emitted_states) - emitted_states) != set():
                    raise ValueError(
                        "There are more states in the samples than in the list of"
                        " emitted states.")
                elif (emitted_states - set(emitted_states)) != set():
                    raise Warning("There are more inputted states that those found on the"
                                  " samples. Input is ignored.")
            return var_type, emitted_states
        elif var_type == "continuous":
            # Check that all elements of every sequence have the same dimension
            dim_emissions = len(kwargs["observed_samples"][0][0])
            for sequence in kwargs["observed_samples"]:
                for elem in sequence:
                    if dim_emissions != len(elem):
                        raise ValueError("Not all the samples are of the same size")
            return var_type, dim_emissions
        else:
            raise ValueError("Value of <var_type> is not recognized.")

    def _init_parameters(self, method, **kwargs):
        """
        Method that returns the initial values (for the first iteration of the EM algorithm). The 
        parameters are the probabilities of initial states, the transition matrix, and the emission 
        parameters (emission matrix or emission means and covariances)

        Parameters
        ----------
        method : string
                 method used to initialize parameters. It can take values "inputted" or "random"
        kwargs : dictionary
                 other arguments of the HMM

        Returns
        -------
        parameters : tuple
                     initial values of the parameters of the HMM
        """

        if method == "inputted":
            return self._init_parameters_from_inputs(**kwargs)
        elif method == "random":
            if self.var_type == 'categorical':
                return self._init_parameters_at_random(self.seed, self.n_hidden_states,
                                                       self.var_type,
                                                       self.n_emitted_states,
                                                       self.supremum, self.infimum,
                                                       self.dim_emissions)
            elif self.var_type == 'continuous':
                return self._init_parameters_at_random(self.seed, self.n_hidden_states,
                                                       self.var_type, None,
                                                       self.supremum, self.infimum,
                                                       self.dim_emissions)
            else:
                raise HereBeDragonsException('You should not be here')
        else:
            raise ValueError("Initialization method not defined.")

    def _init_parameters_from_inputs(self, **kwargs):
        """
        Method that checks that the inputted parameters are feasible for the HMM model to be 
        trained and stores them for further user.

        Parameters
        ----------
        kwargs : dictionary
                 other arguments of the HMM

        Returns
        -------
        initial_hidden_state : NumPy array
                               probabilities of being at each state at the initial instant
        transition_matrix : NumPy array
                            Probability of jumpìng from one state to another on an interval of one
                            "time step"
        emission_parameters : NumPy array or tuple
                              emission matrix or emission means and covariances
        """

        # Check that all variables are available
        if self.var_type == "categorical":
            var_labels = ["initial_hidden_state", "transition_matrix", "emission_matrix"]
        elif self.var_type == "continuous":
            var_labels = ["initial_hidden_state", "transition_matrix",
                          "emission_means", "emission_covariances"]
        for v in var_labels:
            if v not in kwargs.keys():
                raise ValueError("Variable <{}> was expected as an input".format(v))

        # Check the shape of <initial_hidden_state> is correct
        if kwargs["initial_hidden_state"].shape != (self.n_hidden_states,):
            raise ValueError("Shape of the inputted <initial_hidden_state> is not correct")
        # Check the shape of <transition_matrix> is correct
        if kwargs["transition_matrix"].shape != (self.n_hidden_states,
                                                 self.n_hidden_states):
            raise ValueError("Shape of the inputted <transition_matrix> is not correct")
        for ii in range(self.n_hidden_states):
            if not np.isclose(np.sum(kwargs["transition_matrix"][:, ii]), 1.):
                raise ValueError("Columns of inputted <transition_matrix> do not add to"
                                 " one.")
        # Check the shape of the emission parameters are correct
        if self.var_type == "categorical":
            if kwargs["emission_matrix"].shape != (self.dim_emissions,
                                                   self.n_hidden_states):
                raise ValueError("Shape of the inputted <emission_matrix> is not correct")
            for ii in range(self.n_hidden_states):
                if not np.isclose(np.sum(kwargs["emission_matrix"][:, ii]), 1.):
                    raise ValueError("Columns of inputted <emission_matrix> do not add to"
                                     " one.")
        elif self.var_type == "continuous":
            if len(kwargs["emission_means"]) != self.n_hidden_states:
                raise ValueError("Provided values of <emission_means> is not correct.")
            if len(kwargs["emission_covariances"]) != self.n_hidden_states:
                raise ValueError("Provided values of <emission_covariances> is not"
                                 " correct.")
            for k in range(self.n_hidden_states):
                if kwargs["emission_means"][k].shape != (self.dim_emissions,):
                    raise ValueError("Provided values of <emission_means> is not correct.")
                if kwargs["emission_covariances"][k].shape != (self.dim_emissions,
                                                               self.dim_emissions):
                    raise ValueError("Provided values of <emission_covariances> is not"
                                     " correct.")
        else:
            raise ValueError("Value of <var_type> is not recognized.")

        # Store variables for further use
        initial_hidden_state = kwargs["initial_hidden_state"]
        transition_matrix = kwargs["transition_matrix"]
        if self.var_type == "categorical":
            emission_matrix = kwargs["emission_matrix"]
            emission_parameters = emission_matrix
        elif self.var_type == "continuous":
            emission_means = kwargs["emission_means"]
            emission_covariances = kwargs["emission_covariances"]
            pdfs = [multivariate_normal(mean=m, cov=c).pdf
                    for m, c in zip(emission_means, emission_covariances)]
            emission_parameters = (emission_means, emission_covariances, pdfs)
        else:
            raise ValueError("Value of <var_type> is not recognized.")

        return initial_hidden_state, transition_matrix, emission_parameters

    @staticmethod
    def _init_parameters_at_random(seed, n_hidden_states, var_type, n_emitted_states=None,
                                   supremum=None, infimum=None, dim_emissions=None,
                                   randomization_method="uniform"):
        """
        Method that initializes HMM-parameters at random

        Returns
        -------
        initial_hidden_state : NumPy array
                               probabilities of being at each state at the initial instant
        transition_matrix : NumPy array
                            Probability of jumpìng from one state to another on 
                            an interval of one "time step"
        emission_parameters : NumPy array or tuple
                              emission matrix or emission means and covariances
        randomization_method : string
                               Type of covariance matrices to be generated. Can take the values 
                                "uniform", "diagonal", and "non-diagonal". If <var_type> is not
                                "continuous" the value is not used and, thus, ignored. 
        """

        # Initialize random number generator
        np.random.seed(seed)
        # Initialize hyperparameters
        initial_hidden_state = np.random.rand(n_hidden_states)
        initial_hidden_state /= np.sum(initial_hidden_state)
        transition_matrix = np.random.rand(n_hidden_states, n_hidden_states)
        for k in range(n_hidden_states):
            transition_matrix[:, k] /= np.sum(transition_matrix[:, k])
        if var_type == "categorical":
            emission_matrix = np.random.rand(n_emitted_states, n_hidden_states)
            for k in range(n_hidden_states):
                emission_matrix[:, k] /= np.sum(emission_matrix[:, k])
            emission_parameters = emission_matrix
        elif var_type == "continuous":
            delta = supremum - infimum
            emission_means = [infimum + delta * np.random.rand(dim_emissions)
                              for _ in range(n_hidden_states)]
            reference_eigenvalue = 0.3 * delta
            if randomization_method == "uniform":
                emission_covariances = [reference_eigenvalue * np.eye(dim_emissions)
                                            for _ in range(n_hidden_states)]
            elif randomization_method == "diagonal":
                emission_covariances = [np.diag(infimum + delta * np.random.rand(dim_emissions))
                                        for _ in range(n_hidden_states)]
            elif randomization_method == "non-diagonal":
                emission_covariances = []
                for _ in range(n_hidden_states):
                    q, _ = np.linalg.qr(np.random.rand(dim_emissions, dim_emissions))
                    e = np.diag(infimum + delta * np.random.rand(dim_emissions))
                    emission_covariances.append(q.dot(e.dot(q.T)))
            else:
                raise ValueError("The value randomization_method is not valid.")
            pdfs = [multivariate_normal(mean=m, cov=c).pdf
                    for m, c in zip(emission_means, emission_covariances)]
            emission_parameters = (emission_means, emission_covariances, pdfs)
        else:
            raise ValueError("Value of <var_type> is not recognized.")

        return initial_hidden_state, transition_matrix, emission_parameters

    def _train_model(self):
        """
        Method that trains a HMM, given the initial values of the parameters, using the conventional 
        forward-backward, expectation-maximization algorithm

        Returns
        -------
        None
        """

        # First expectation step
        self.p_emitted_samples = self.probability_of_emitted_samples(self.observed_samples)
        alpha, beta, cte = self._expectation()
        log_likelihood = self.evaluate_log_likelihood(cte)

        while True:
            # Maximization
            if self.var_type == "categorical":
                (self.transition_matrix,
                 self.emission_matrix) = self._maximization(alpha, beta, cte)
            elif self.var_type == "continuous":
                (self.transition_matrix, self.emission_means,
                 self.emission_covariances) = self._maximization(alpha, beta, cte)
                self.pdf = [multivariate_normal(mean=m, cov=c, allow_singular=True).pdf
                            for m, c in zip(self.emission_means, self.emission_covariances)]
            else:
                raise ValueError("Value of <var_type> is not recognized.")
            # Expectation
            self.p_emitted_samples = self.probability_of_emitted_samples(self.observed_samples)
            alpha, beta, cte = self._expectation()
            updated_log_likelihood = self.evaluate_log_likelihood(cte)
            # Evaluate error and stop criterion
            error = updated_log_likelihood - log_likelihood
            log_likelihood = updated_log_likelihood
            if error < self.min_em_improvement:
                break
        # We're done
        self.log_likelihood = log_likelihood

    def generate_data(self, n_points):
        """
        Having set the Hidden Markov model, this method generates a sequence of simulated data.

        Parameters
        ----------
        n_points : integer
                   number of elements of the sequence to be generated

        Returns
        -------
        hidden_states : list of integers
                        values of the hidden states of the simulated data
        observed_states : list
                          values of the simulated, observed/emitted variables
        """

        # Initialize stuff
        hidden_states = []
        observed_states = []
        phs = self.initial_hidden_state
        for _ in range(n_points):
            # Choose a hidden state at random
            x = np.random.choice(range(self.n_hidden_states), p=phs)
            hidden_states.append(x)
            # Generate a new emission
            if self.var_type == "categorical":
                pos = self.emission_matrix[:, x]
                o = np.random.choice(range(self.n_emitted_states), p=pos)
                observed_states.append(self.emitted_states[o])
            elif self.var_type == "continuous":
                mu = self.emission_means[x]
                sigma = self.emission_covariances[x]
                observed_states.append(np.random.multivariate_normal(mu, sigma))
            # Generate probability of the next hidden state
            phs = self.transition_matrix[:, x]

        return hidden_states, observed_states

    def _expectation(self, sequences=None, normalize=True):
        """
        Calculates the expected values of the probability of some hidden states given a sequence of
         observed variables.

        Parameters
        ----------
        sequences : iterable, optional
                    values of the observed variables. If not provided the inputted values in
                     <observed_samples> (compulsory if <mode> == "train") would be used.
        normalize : Boolean, optional
                    If True expected probability distribution would be normalized to avoid
                     underflow. Do not set to False unless you train a model using small sequences
                     (with length < 100).

        Returns
        -------
        alpha, beta : list of NumPy arrays
                      forward and backwards pieces of the expected distribution (see "alpha-beta
                       algorithm" elsewhere). Ther would be as many arrays as sequences are used to
                       train the HMM; the shape of each array in N by K, where N is the length of
                       the corresponding sequence and K is the number of hidden states.
        c : list of NumPy arrays
            values of the normalizing constants
        """

        # Define how to normalize distributions
        def normalize_distribution(x):
            k = np.sum(x) + 1e-300
            return x / k, k

        if sequences is None:
            sequences = self.observed_samples
            p_emitted_samples = self.p_emitted_samples
        else:
            p_emitted_samples = self.probability_of_emitted_samples(sequences)

        alpha, beta, cte = [], [], []
        for sequence, p_y in zip(sequences, p_emitted_samples):

            # initialize some variables
            a = np.zeros((len(sequence), self.n_hidden_states))
            c = np.ones(len(sequence))
            b = np.zeros((len(sequence), self.n_hidden_states))

            # Forward step
            a[0, :] = self.initial_hidden_state * p_y[0]
            if normalize:
                a[0, :], c[0] = normalize_distribution(a[0, :])
            for n in range(1, len(sequence)):
                a[n, :] = p_y[n] * self.transition_matrix.dot(a[n - 1, :])
                if normalize:
                    a[n, :], c[n] = normalize_distribution(a[n, :])

            # Backwards step
            b[-1, :] = 1.
            for n in range(len(sequence) - 2, -1, -1):
                b[n, :] = (b[n + 1, :] * p_y[n + 1]).dot(self.transition_matrix)
                if normalize:
                    b[n, :] /= c[n + 1]
            # Store data
            alpha.append(a)
            beta.append(b)
            cte.append(c)

        return alpha, beta, cte

    def _maximization(self, alpha, beta, cte):
        """
        Updates the values of the hyperparameter of the method (transition matrix and emission
         hyperparameters) using the expected distributions.

        Parameters
        ----------
        alpha, beta : list of NumPy arrays
                      forward and backwards pieces of the expected distribution (see "alpha-beta
                       algorithm" elsewhere). Ther would be as many arrays as sequences are used to
                       train the HMM; the shape of each array in N by K, where N is the length of
                       the corresponding sequence and K is the number of hidden states.
        cte : list of NumPy arrays
              values of the normalizing constants

        Returns
        -------
        new_transition_matrix : NumPy array
                                updated-transition matrix
        mu : NumPy array, only provided if <var_type> == "categories"
             updated emission matrix
        mu, sigma : NumPy array, only provided if <var_type> == "continuous"
                    updated emission means and covariance matrices
        """

        for a, b in zip(alpha, beta):
            pi = a[1, :] * b[1, :]
        pi /= np.sum(pi)

        new_transition_matrix = np.zeros((self.n_hidden_states, self.n_hidden_states))
        for a, b, c, sequence, p_y in zip(alpha, beta, cte, self.observed_samples,
                                          self.p_emitted_samples):
            for n in range(1, len(sequence)):
                a0 = np.tile(a[n - 1], (self.n_hidden_states, 1)).T
                xi = c[n] * a0 * b[n] * self.transition_matrix * p_y[n]
                new_transition_matrix += xi
        sum_of_columns = np.sum(new_transition_matrix, axis=0)
        new_transition_matrix /= sum_of_columns
        for ii in range(self.n_hidden_states):
            if sum_of_columns[ii] == 0.0:
                new_transition_matrix[:, ii] = 1. / self.n_hidden_states

        if self.var_type == "categorical":
            mu = np.zeros(self.emission_matrix.shape)
            factor = 0.
            for a, b, sequence in zip(alpha, beta, self.observed_samples):
                for n in range(len(sequence)):
                    o = self.emitted_states.index(sequence[n])
                    mu[o, :] += a[n] * b[n]
                factor += np.sum(a * b, axis=0)
            mu /= factor

            return new_transition_matrix, mu
        elif self.var_type == "continuous":
            factor = 1e-16
            mu_mat = np.zeros((self.dim_emissions, self.n_hidden_states))
            for a, b, sequence in zip(alpha, beta, self.observed_samples):
                gamma = a * b
                for g, e in zip(gamma, sequence):
                    mu_mat += np.outer(e, g)
                factor += np.sum(a * b, axis=0)
            mu_mat /= factor
            mu = [mu_mat[:, ii] for ii in range(self.n_hidden_states)]

            sigma = [np.zeros((self.dim_emissions, self.dim_emissions))
                     for _ in range(self.n_hidden_states)]
            for a, b, sequence in zip(alpha, beta, self.observed_samples):
                gamma = a * b
                for g, e in zip(gamma, sequence):
                    for k in range(self.n_hidden_states):
                        xi = e - self.emission_means[k]
                        sigma[k] += g[k] * np.outer(xi, xi)

            for k in range(self.n_hidden_states):
                sigma[k] /= factor[k]

            return new_transition_matrix, mu, sigma

    def get_most_probable_hidden_states(self, sequences):
        """
        Calculates the most probable sequence of hidden states given a sequence of observed states
         using the Viterbi algorithm.
        
        Parameters
        ----------
        sequences : list of lists
                    all the sequences whose most probable hidden states the user wants to calculate.

        Returns
        -------
        hidden_states : list of lists
                        list of most probable sequences of hidden states.

        """
        hidden_states = []
        for sequence in sequences:
            # Intitialize variables
            seq_length = len(sequence)
            probabilities = []
            hidden_sequences = []

            # First step
            p = np.log2(self.initial_hidden_state * self.probability_of_emission(sequence[0]))
            probabilities.append(p)
            z = np.zeros(self.n_hidden_states)
            hidden_sequences.append(z)
            # Forward steps
            for n in range(1, seq_length):
                p = probabilities[-1]
                a = np.log2(self.probability_of_emission(sequence[n]))
                b = np.log2(self.transition_matrix + 1e-300) + p
                probabilities.append(a + np.amax(b, axis=1))
                hidden_sequences.append(np.argmax(b, axis=1))
            # Backward steps
            x = np.ones(seq_length, dtype=int)
            x[-1] = np.argmax(probabilities[-1])
            for n in range(seq_length - 1, 0, -1):
                x[n - 1] = hidden_sequences[n][x[n]]
            hidden_states.append(x)

        return hidden_states

    def probability_of_emission(self, y):
        """
        Method to calculate the emission probability for each hidden state

        Parameters
        ----------
        y : string, integer, float, or NumPy array

        Returns
        -------
        p : NumPy array
            probability of getting the given emission for all the hidden states
        """
        if self.var_type == "continuous":
            return np.array([pdf(y) for pdf in self.pdf])
        elif self.var_type == "categorical":
            ii = self.emitted_states.index(y)
            return self.emission_matrix[ii, :]

    def probability_of_emitted_samples(self, sequences):
        """
        Calculates the probability that a given hidden state has of emitting an observed value.

        Parameters
        ----------
        sequences : list
                    list of sequences of observed samples

        Returns
        -------
        out : list
              list of values of having emitted a given observation for each hidden state

        """

        if self.var_type == "continuous":
            p_emitted_samples = []
            for sequence in sequences:
                p = []
                for k in range(self.n_hidden_states):
                    p.append(self.pdf[k](sequence))
                # print(p, sequence)
                p_emitted_samples.append(list(zip(*p)))
        elif self.var_type == "categorical":
            p_emitted_samples = [[self.probability_of_emission(elem) for elem in sequence]
                                 for sequence in sequences]
        else:
            raise ValueError("Value of <var_type> not recognized.")

        return p_emitted_samples

    def get_probability_of_sequences(self, sequences):
        """
        Provides log10 probability of sequences

        Parameters
        ----------
        sequences : list
                    list of sequences of observed values

        Returns
        -------
        out : NumPy array
              values of the log10-likelihood of each sequence
        """

        _, _, c = self._expectation(sequences=sequences, normalize=True)
        return np.array([np.sum(np.log10(e)) for e in c])

    @staticmethod
    def evaluate_log_likelihood(cte):
        """
        Calculate the log10-value of the likelihood given the values of the constants. Only works
         if expected distributions have been normalized.

        Parameters
        ----------
        cte : list of NumPy arrays
              normalizing constants of the alphas and betas.

        Returns
        -------
        log_likelihood : float
                         log10-value of the likelihood
        """

        return np.sum([np.sum(np.log10(c + 1e-16)) for c in cte])


