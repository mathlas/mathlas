# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import gc
import sys
import time
import signal
import shutil
import numpy as np
import pandas as pd
from pathlib import Path
from mathlas.object import MathlasObject
from mathlas.misc.ansi_color import LIGHT_BLUE, END
from mathlas.machine_learning.decision_tree import DecisionTree
from mathlas.statistics.probabilistic_choice_object import ProbabilisticChoiceObject


class RandomForest(MathlasObject):
    def __init__(self, trainingData=None, resultColumn=None, nTrees=None, nSamples=None,
                 nTreeVars=None, learner='DecisionTree', treeDepth=2, minDelta=None,
                 trainingProportion=0.4, random_seed=211104, keepData=False):
        """
        Generate a random forest object

        Parameters
        ----------
        trainingData : pandas DataFrame
                       The training data you want to train the random forest with.
        resultColumn : pandas column name (integer, string)
                      The name of the column on which a hypothesis will be made
        nTrees : integer
                 The number of weak learners to generate
        nSamples : integer
                   The number of samples that will be fed into each training tree
        nTreeVars : integer
                    the number of columns from trainingData that will be used in each tree.
                    `nTrees` weak learners will be training, each trained with `nTreeVars`
                    vars used as training data.
        learner : string
                  Weak learner to use. Supported values are:
                    * `DecisionTree`
        treeDepth : integer
                    The depth of the generated tree
        trainingProportion : float, optional
                             Portion (0.-1.) of the training data that will be used for
                             training, defaults to 0.4.
                             The actual indices in the training data will be selected by
                             performing a random choice without replacement on `trainingData`.
        random_seed : integer, optional
                      random seed to pass to np.random.seed before doing
                      pseudo-random work
        keepData : boolean, optional
                   Whether training and validation data should be stored in the model.
                   By default data is not stored with the model.
                   :param trainingProportion:
        """
        super().__init__()

        # A bit of sanity checking
        if learner not in ('DecisionTree',):
            raise ValueError('Given learner is not supported')

        if trainingData is None:
            raise ValueError('You must provide trainingData')

        if resultColumn not in trainingData.columns:
            raise ValueError('{} not in {}'.format(resultColumn, trainingData.columns))

        if nTrees is None:
            raise ValueError('nTrees cannot be None')

        if nSamples is None:
            raise ValueError('nSamples cannot be None')

        if trainingProportion is None:
            raise ValueError('trainingProportion cannot be None')

        # Register a custom signal handler for SIGUSR1, which will dump progress info
        handler = None

        if sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
            handler = signal.getsignal(signal.SIGUSR1)
            self.__t0 = time.time()
            signal.signal(signal.SIGUSR1, self._print_status)

        # Store for later use
        self.resultColumn = resultColumn
        # Use some of the data for training and some for validation (40%-60% by default)
        np.random.seed(random_seed)
        nTraining = int(trainingProportion*trainingData.shape[0])
        trainingIndices = sorted(np.random.choice(trainingData.index, nTraining,
                                                  replace=False))
        validationIndices = sorted(set(trainingData.index) - set(trainingIndices))
        self.trainingData = trainingData.loc[trainingIndices]
        self.validationData = trainingData.loc[validationIndices]

        # Store all the possible outcomes of the categorization process
        self.different_results = set(trainingData[self.resultColumn])

        # Get a list with all the column names except for the result col
        availableCols = list(self.trainingData.columns)
        availableCols.pop(availableCols.index(resultColumn))
        if nTreeVars is None:
            nTreeVars = len(availableCols)
        if nTreeVars > len(availableCols):
            raise ValueError('Cannot use {} variables, '.format(nTreeVars) +
                             'only {} are available'.format(len(availableCols)))

        # Store the nTress decisionTree objects
        self.learners = [None] * nTrees
        self.learnerWeights = [0.] * nTrees
        for i in range(nTrees):
            cols = (list(np.random.choice(availableCols, nTreeVars, replace=False)) +
                    [resultColumn])
            indices = np.random.choice(self.trainingData.index, nSamples, replace=False)
            self.learners[i] = DecisionTree(self.trainingData.loc[indices, cols],
                                            resultColumn,
                                            maxLevels=treeDepth,
                                            minDelta=minDelta)

            gc.collect()

            validationData = self.validationData.copy()
            self.learners[i].categorize(validationData, store_values='raw')
            self.learnerWeights[i] = 1.0 - self.error(validationData)

        # Do not store training and validation data if told not to
        if not keepData:
            del self.trainingData
            del self.validationData

            # Explicitly run a garbage collection cycle
            gc.collect()

        # Unregister the custom SIGUSR1 signal handler now that we're done
        if sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
            signal.signal(signal.SIGUSR1, handler)
            del self.__t0

    def error(self, data):
        """
        Compute the error as the relative number of mismatched items
        Parameters
        ----------
        data : DataFrame

        Returns
        -------
        error : number of miscategorized entries / length of data
        """
        # This DF will contain the "scalar" probabilities for each row
        probabilities = self.turn_validation_into_probabilities(data)
        # mask array from validation data
        # Serves both as a mask when multiplied by other matrices and as
        # a probabilities matrix for the validation data
        validationMask = self.turn_validation_into_probabilities(self.validationData)
        # Compute the distance between each element and its real value
        error = (validationMask - validationMask * probabilities).abs()

        return np.sum(error.values) / error.shape[0]

    def turn_validation_into_probabilities(self, dataset, different_results=None):
        """
        This helper method will read data from a dataset and returns a "probabilities" DataFrame

        This is useful since the decision tree implementation can either return a fixed value
        or a probabilistic object, and both have to be operated on separately since they are
        conceptually different.

        Parameters
        ----------
        dataset : pandas DataFrame
                  Data as output by the machine learning algorithms
                  This DataFrame must contain a column named like self.resultcolumn
        different_results : iterable, optional
                  An iterable with the acceptable results.
                  All the results in dataset[self.resultcolumn] must be contained in this
                  iterable.
                  If it is not given, the object's self.different_results iterable gets
                  used instead.

        Returns
        -------
        Pandas DataFrame of size (dataset.shape[0] x len(different_results)) with floats.
        For each row, the column with the highest value in this DataFrame represents the
        most likely result.
        """
        if different_results is None:
            different_results = self.different_results

        # Store the different possible predictions from the dataset
        # dataset_results_raw can contain both string objects and
        # probabilistic objects, whereas dataset_results_labels
        # will only contain result names.
        # For example: if dataset_results_raw contained two values:
        #   * A string whose value is "Mild"
        #   * A probabilistic object with values:
        #       - "No sickness" with 20% probability
        #       - "Mild" with 50% probability
        #       - "Severe" with 30% probability
        # then dataset_results_labels = ["No sickness",
        #                                "Mild",
        #                                "Severe"]
        #
        # We also check that if different_results are given, the dataset
        # does not contain values not contained in different_results
        dataset_results_raw = set(dataset[self.resultColumn])
        dataset_results_labels = []
        for elem in dataset_results_raw:
            if isinstance(elem, ProbabilisticChoiceObject):
                dataset_results_labels.extend(list(elem.p.keys()))
            else:
                dataset_results_labels.append(elem)

        if ((different_results is not None) and
                not (np.all([elem in different_results for elem in set(dataset_results_labels)]))):
            raise ValueError("Some new key has appeared in the results")
        if different_results is None:
            different_results = dataset_results_labels

        # Compute a "probabilities" Pandas DataFrame where each row corresponds
        # to an entry in the original dataset and each row corresponds to the
        # each of the possible predictions in "dataset_results_raw"
        # The values cannot be treated as real probabilities because they are not
        # normalized and, therefore, each row will not sum up to 1
        # The maximum value can be used, nonetheless, to determine the most
        # likely prediction
        probabilistic_results = pd.DataFrame(np.zeros((len(dataset), len(different_results))),
                                             columns=different_results, index=dataset.index)
        for key in dataset_results_raw:
            mask = dataset[self.resultColumn] == key
            if isinstance(key, ProbabilisticChoiceObject):
                values = [key.p[elem] if elem in key.p else 0. for elem in different_results]
            else:
                values = [1. if elem == key else 0. for elem in different_results]
            probabilistic_results.loc[mask, different_results] = values

        return probabilistic_results

    def categorize(self, data, nLearners=None, criterion="probabilistic",
                   store_values='raw', random_seed=211104):
        """
        Categorize the DataFrame "data" based on the trained model.

        Parameters
        ----------
        data : Pandas DataFrame whose data you want categorized
               Its schema must match that in ``self.trainingData``
        nLearners: integer
                 Number of learners to use for categorizing
        criterion: string, optional
                   The categorization criterion to use. Values can be:
                        * "probabilistic" : The result with the highest sum of
                           weighted probability will be chosen as the category
                        * "majority" : The result with the highest sum of
                           weights will be chosen as the category
                   The default is to perform "probabilistic" categorization
        store_values : string, optional
                       A string indicating how should results be returned.
                       It can take two values:
                        * "raw" : probabilisticChoiceObjects objects will be returned
                        * "majority" : the category which is the most likely will
                                       be stored as output
                       'raw' is used by default
        random_seed : integer, optional
                      The random seed to pass to np.random.choice when
                      nLearners < len(self.learners)
        """
        if nLearners is None or nLearners > len(self.learners):
            learner_indices = range(len(self.learners))
        else:
            np.random.seed(random_seed)
            learner_indices = np.random.choice(range(len(self.learners)),
                                               replace=False,
                                               size=nLearners)

        # Run all the learners in the data to determine the answers
        if criterion == "probabilistic":
            results = None
            for i in learner_indices:
                if self.learnerWeights[i] > 0:
                    self.learners[i].categorize(data)
                    probabilistic_results = self.turn_validation_into_probabilities(data)
                    if results is None:
                        results = self.learnerWeights[i] * probabilistic_results
                    else:
                        results += self.learnerWeights[i] * probabilistic_results
        elif criterion == "majority":
            results = pd.DataFrame(np.zeros((len(data.index), len(self.different_results))),
                                   columns=self.different_results, index=data.index)
            for i in learner_indices:
                if self.learnerWeights[i] > 0:
                    self.learners[i].categorize(data, store_value="majority")
                    for key in self.different_results:
                        mask = data[self.resultColumn] == key
                        results.loc[mask, key] += self.learnerWeights[i]

        if store_values == 'raw':
            results /= np.sum(np.array(self.learnerWeights)[learner_indices])

            # Store the probabilistic objects as results
            data[self.resultColumn] = results.apply(lambda x: ProbabilisticChoiceObject(
                                dict(zip(results.columns, x.values))), axis=1)
        elif store_values == 'majority':
            data[self.resultColumn] = results.idxmax(axis=1)
        else:       # typo detector
            raise ValueError('Given value for store_values is not valid')

    def cache(self, output_dir):
        """
        Cache the current RandomForest object to the given path in disk.

        Parameters
        ----------
        output_dir : Path object or string
                     Output directory, will be removed if existent
        """
        # Convert the path (if required) to a Path object,
        # then create the output directory
        output_dir = Path(output_dir)
        if output_dir.is_dir():
            shutil.rmtree(output_dir, ignore_errors=True)
        elif output_dir.is_file():
            output_dir.unlink(output_dir)

        output_dir.mkdir(mode=0o755, parents=True, exist_ok=True)

        # Cache myself (using the MathlasObject.cache method) but exclude
        # the learners
        super().cache(output_dir / 'randomForest.pickle', unstorableAttribs=['learners'])

        # Cache each of the learners, in order
        for i, learner in enumerate(self.learners):
            self.learners[i].cache(output_dir / 'tree_{}.pickle'.format(i))

    def _load(self, input_dir):
        """
        Load a Random Forest model from the given directory

        Parameters
        ----------
        input_dir : Path object or string
                    Directory where the model was cached
        """
        input_dir = Path(input_dir)
        # Load the RandomForest object (minus learners) by using MathlasObject._cache()
        super()._load(input_dir / 'randomForest.pickle')
        self.learners = [None] * len(self.learnerWeights)

        # Load DecisionTree learners
        for i in range(len(self.learners)):
            p = input_dir / 'tree_{}.pickle'.format(i)
            try:
                self.learners[i] = DecisionTree.from_cache(p)
            except IOError:
                raise IOError('Cannot load tree file "{}", which is required '.format(p) +
                              'for loading RandomForest model in {}'.format(input_dir))

    def _print_status(self, signum, frame):
        """
        This method prints progress information on demand.
        """
        computed_learners = [1 for i in self.learners if i is not None]
        __t1 = time.time()
        __d = time.localtime()
        sys.stdout.write('\n{}[94mRandom Forest generation progress info:\n'.format(LIGHT_BLUE))
        sys.stdout.write('=======================================\n')
        sys.stdout.write('\tDate: {}-{}-{} {}:{}:{}\n'.format(__d.tm_year, __d.tm_mon, __d.tm_mday,
                                                              __d.tm_hour, __d.tm_min, __d.tm_sec))
        sys.stdout.write('\tElapsed time: {}s\n'.format(__t1 - self.__t0))
        sys.stdout.write('\tTotal trees: {}\n'.format(len(self.learners)))
        sys.stdout.write('\tComputed trees: {}\n'.format(len(computed_learners)))
        sys.stdout.write('\tTree weights:')
        for i in range(len(computed_learners)):
            sys.stdout.write('\n\t\t{}'.format(self.learnerWeights[i]))
        sys.stdout.write('{}\n'.format(END))


if __name__ == '__main__':
    def main():
        import tempfile
        from mathlas.examples.machine_learning.data.sickness_database import sickness_database

        symptoms = {"temperature": ["float", [36., 40.], lambda x, a, b: a + (b - a) * 6 * (
        0.5 * x ** 2 - (x ** 3) / 3.)],
                    "headache": ["categoric", ["yes", "no"], [0.3, 0.7]],
                    "vomits": ["categoric", ["never", "sometimes", "often"], [0.5, 0.4, 0.1]],
                    "mucus": ["categoric", ["yes", "no"], [0.6, 0.4]]}
        sickness = {"flu": ["yes", "no"]}
        schema = {"flu": {"temperature": {"<37": "no",
                                          ">=37:<38": {"headache": {"no": {"mucus": {"yes": "yes",
                                                                                     "no": "no"}},
                                                                    "yes": "yes"}},
                                          ">=38:<39": {"mucus": {"yes": "no",
                                                                 "no": "yes"}},
                                          ">=39": {"mucus": {"yes": "no",
                                                             "no": "yes"}}}}}
        n = 5000
        fdb = sickness_database(n, symptoms, sickness, schema)

        print("DB generated")

        rf = RandomForest(fdb, 'flu 0.0', nTrees=20, nSamples=200, nTreeVars=3, treeDepth=2,
                          minDelta=0.1)
        fdb2 = fdb.copy()
        rf.categorize(fdb2, store_values='majority')
        print('Errors: {}%'.format(100. * fdb[fdb['flu 0.0'] != fdb2['flu 0.0']].shape[0] / n))
        print(fdb[fdb['flu 0.0'] != fdb2['flu 0.0']].shape[0])
        cacheDir = Path(tempfile.gettempdir()) / 'randomForest_model'
        rf.cache(cacheDir)
        f2 = RandomForest.from_cache(cacheDir)
        fdb3 = fdb.copy()
        f2.categorize(fdb3, store_values='majority')

        print('Succes?: ', (fdb2 == fdb3).all().all())

        return 0

    sys.exit(main())

