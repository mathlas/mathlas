# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
from time import time
import numpy as np
import pandas as pd
from ..object import MathlasObject
from scipy.special import psi, beta
from mathlas.statistics.probabilistic_choice_object import ProbabilisticChoiceObject as pco
from mathlas.optimization.optimizers import (gradient_descent, enhanced_gradient_descent,
                                             nonlinear_conjugate_gradient)


class NeuralNetwork(MathlasObject):

    def __init__(self, training_data, result_column, problem_type, n_components=None):
        """
        Initializes the neural network by defining its structure and optimizes the weights

        Parameters
        ----------
        training_data : Pandas DataFrame
                        training data set of features/attributes and the results.
        result_column : string
                        name of the column that contains the results in the DataFrame.
        problem_type : string
                       type of problem to be solved. It can only take the values "regression",
                        "binary classification" or "one-of-k classification".
        """
        # Store inputs for further use
        self.training_data = training_data.copy()
        self.column_labels = training_data.columns
        self.result_label = result_column
        self.data_columns = list(set(training_data.columns) - {result_column})

        # training data is split into inputs and outputs
        self.training_array = training_data[self.data_columns].values.T
        self.n_feats, self.n_data = self.training_array.shape
        if problem_type.endswith("classification"):
            # The categories must be stored in a one-versus-others encoding
            categories = training_data[result_column].values
            self.category_labels = list(set(categories))
            self.n_categories = len(self.category_labels)
            self.n_outputs = self.n_categories
            self.results_array = np.zeros((self.n_categories, self.n_data))
            indexes = self.training_data[result_column].replace(self.category_labels, range(len(self.category_labels))).values
            self.results_array[indexes, np.arange(self.n_data)] = 1.
        elif problem_type in ["regression", "Gaussian regression", "beta regression"]:
            self.n_outputs = 1
            self.results_array = np.reshape(training_data[result_column].values, (1, self.n_data))
        else:
            raise TypeError("Variable <problem_type> takes the incorrect value %s" % problem_type)

        # Initialize variables and store inputs for further use
        self.eps = 1. #0.12
        self.training_stats = []
        self.problem_type = problem_type
        self.mu = None
        self.n_units = None
        self.weights = None
        self.optimizer = None
        self.optimization_method = None
        self.activation_functions = None

        # Select the method to evaluate the cost, its gradient, and how to extract the parameters
        #  of the components of the pdfs (if required)
        if problem_type == "regression":
            self.eval_cost = self.regression_cost
            self.eval_grad = self.usual_grad
        elif problem_type == "binary classification":
            self.eval_cost = self.binary_classification_cost
            self.eval_grad = self.usual_grad
        elif problem_type == "one-of-k classification":
            self.eval_cost = self.one_of_k_classification_cost
            self.eval_grad = self.usual_grad
        elif problem_type == "Gaussian regression":
            self.eval_cost = self.gaussian_cost
            self.eval_grad = self.gaussian_grad
            self.extract_parameters = self.extract_parameters_gaussian
        elif problem_type == "beta regression":
            self.eval_cost = self.beta_cost
            self.eval_grad = self.beta_grad
            self.extract_parameters = self.extract_parameters_beta
        else:
            raise TypeError("Variable <problem_type> takes the incorrect value %s" % problem_type)
        # Store the number of components
        if problem_type in ["Gaussian regression", "beta regression"]:
            if n_components is None:
                raise ValueError("<n_components> is a compulsory input for this <problem_type>")
            elif isinstance(n_components, int) and n_components >= 0:
                self.n_components = n_components
            else:
                raise ValueError("<n_components> must be a positive integer")

    def train(self, n_units, activation_functions, mu=0., p_dropout=(1., 1.), p_sgd=1.,
              random_seed=211104, weights=None, max_training_its=10000,
              optimization_method="enhanced gradient descent", **kwargs):
        """
        Trains a neural network given a method to optimize and some other parameters

        Parameters
        ----------
        n_units : list
                  values of the number of units in each hidden layer. Therefore, its length is the
                   number of hidden layers.
        activation_functions : list of strings
                               type of activation functions to be used between every pair of layers.
                                This values can be either "sigmoid", "linear", "tanh", or "ReLU".
                                Note that the length of this list is equal to the length of
                                <nUnits> + 1.
        mu : float, optional
             regularization parameter. If not provided it is set to zero.
        p_dropout : float or list of floats, optional
                    probability of each layer (input or hidden) of using a given unit. If a float
                     is provided the probability is the same for all layers. If a list of length 2
                     is inputted the first element is the probability for the input layer and the
                     second for the hidden layers. Otherwise a list with the probability for each
                     layer separately must be provided.
        p_sgd : float, optional
                portion of the data used to calculate the cost function using the stochastic
                 gradient descent method. It must be a number less than 1 and greater than 0.
        random_seed : integer, optional
                      value of the random seed used to initialize random numbers
        weights : list of arrays, optional
                  initial values of the weights. If not provided they are initialize to random
                   numbers to avoid symmetry problems.
        max_training_its : Integer or None
                           Number of maximum iterations that the optimization method can perform.
                            If <max_training_iters> <= 0 or is None, then weights are not
                            optimized and the initial, random values are left unmodified.
        optimization_method : string, optional
                              method used to minimize the error of the neural network
        kwargs : dictionary
                 additional parameters of the optimizer. Check the optimizers library for further
                  information.
        Returns
        -------

        """

        # Select the method to evaluate the cost
        if optimization_method == "gradient descent":
            self.optimizer = gradient_descent
        elif optimization_method == "enhanced gradient descent":
            self.optimizer = enhanced_gradient_descent
        elif optimization_method == "nonlinear conjugate gradient":
            self.optimizer = nonlinear_conjugate_gradient
        elif optimization_method in ["conjugate gradient", "bfgs"]:
            raise NotImplementedError("This options are not already implemented")
        else:
            raise ValueError("<optimization_method> takes the incorrect value " +
                             "'{}'".format(self.optimization_method))

        # Establish structure of the Neural Network
        if self.problem_type in ["regression", "binary classification", "one-of-k classification"]:
            self.n_units = [self.training_array.shape[0]] + list(n_units) + [self.n_outputs]
        elif self.problem_type in ["Gaussian regression", "beta regression"]:
            self.n_units = [self.training_array.shape[0]] + list(n_units) + \
                           [(self.n_outputs + 2) * self.n_components]
        else:
            raise ValueError("Variable <problem_type> takes an unrecognized value")
        # Initialize values
        self.mu = mu
        self._check_inputs(activation_functions, p_dropout, p_sgd)
        self._initialize_weights(random_seed, weights)
        if max_training_its <= 0:
            raise ValueError("<niter> must be a positive integer.")

        # Optimize cost w.r.t. weights
        t0 = time()
        w = self.list_to_vector(self.weights)
        w, f_value, df_value, n_its = self.optimizer(self.eval_opt_fun, w, max_training_its,
                                                     p_dropout=self.p_dropout, p_sgd=self.p_sgd,
                                                     **kwargs)
        t1 = time()
        self.training_stats = {"Optimized cost": f_value, "Gradient at the solution": df_value,
                               "#iterations": n_its, "Wall time": t1-t0}
        self.weights = self.vector_to_list(w)

    def search(self, n_units, activation_functions, mu=0., p_dropout=(1., 1.),
               p_sgd=1., random_seed=211104,
               training_params=((100, 10, "enhanced gradient descent"),
                                (10, 500, "enhanced gradient descent"),
                                (5, 4500, "enhanced gradient descent"),
                                (3, 5000, "nonlinear conjugate gradient")),
               **kwargs):
            """
            Train a neural network looking for a global optimum. Multiple searches are performed by
             training many neural networks for some iterations and then continuing the training
             only for the best models.

            Parameters
            ----------
            n_units : list
                      values of the number of units in each hidden layer. Therefore, its length is
                       the number of hidden layers.
            activation_functions : list of strings
                                   type of activation functions to be used between every pair of
                                    layers. This values can be either "sigmoid", "linear", "tanh",
                                    or "ReLU". Note that the length of this list is equal to the
                                    length of <nUnits> + 1.
            mu : float, optional
                 regularization parameter. If not provided it is set to zero.
                       numbers to avoid symmetry problems.
            p_dropout : float or list of floats, optional
                        probability of each layer (input or hidden) of using a given unit. If a
                         float is provided the probability is the same for all layers. If a list of
                         length 2 is inputted the first element is the probability for the input
                         layer and the second for the hidden layers. Otherwise a list with the
                         probability for each layer separately must be provided.
            p_sgd : float, optional
                    portion of the data used to calculate the cost function using the stochastic
                     gradient descent method. It must be a number less than 1 and greater than 0.
            random_seed : integer, optional
                          value of the random seed used to initialize random numbers
            training_params : iterable of tuples
                              tuples of (number of models, number of iterations per model,
                               optimization method for those models)
            kwargs : dictionary
                     additional parameters of the optimizer. Check the optimizers library for
                      further information.

            Returns
            -------
            None
            """

            best_model = (None, np.inf)

            # Loop for all set of parameters
            for loop_no, params in enumerate(training_params):

                # Init training parameters
                n_models, n_iterations, opt_method = params
                if loop_no != 0:
                    better_nns = (list_of_nn[:n_models], cost_of_nn[:n_models])
                list_of_nn, cost_of_nn = [], []

                # Loop for all models
                for model_no in range(n_models):
                    if loop_no == 0:
                        self.train(n_units, activation_functions, mu=mu, p_dropout=p_dropout,
                                   p_sgd=p_sgd, random_seed=random_seed + model_no,
                                   max_training_its=n_iterations, optimization_method=opt_method,
                                   **kwargs)

                    else:
                        weights = better_nns[0][model_no]
                        self.train(n_units, activation_functions, mu=mu, p_dropout=p_dropout,
                                   p_sgd=p_sgd, weights=weights, max_training_its=n_iterations,
                                   optimization_method=opt_method, **kwargs)

                    cost = self.training_stats["Optimized cost"]
                    cost = cost if not np.isnan(cost) and cost != np.inf else np.inf
                    list_of_nn.append(self.weights)
                    cost_of_nn.append(cost)

                list_of_nn, cost_of_nn = zip(
                    *sorted(zip(list_of_nn, cost_of_nn), key=lambda x: x[1]))
                list_of_nn = list(list_of_nn)
                cost_of_nn = list(cost_of_nn)
                if cost_of_nn[0] < best_model[1]:
                    best_model = (list_of_nn[0], cost_of_nn[0])
                else:
                    list_of_nn.insert(0, best_model[0])
                    cost_of_nn.insert(0, best_model[1])

            self.weights = best_model[0]
            self.training_stats = {"Optimized cost": best_model[1]}

            # self.weights = list_of_nn[0]
            # self.training_stats = {"Optimized cost": cost_of_nn[0]}

    def _check_inputs(self, activation_functions, p_dropout, p_sgd):
        """

        Parameters
        ----------
        activation_functions : list of strings
                               type of activation functions to be used between every pair of layers.
                                This values can be either "sigmoid", "linear", "tanh", or "ReLU".
                                Note that the length of this list is equal to the length of
                                <nUnits> + 1.
        p_dropout : float or list of floats, optional
                    probability of each layer (input or hidden) of using a given unit. If a float
                     is provided the probability is the same for all layers. If a list of length 2
                     is inputted the first element is the probability for the input layer and the
                     second for the hidden layers. Otherwise a list with the probability for each
                     layer separately must be provided.
        p_sgd : float, optional
                portion of the data used to calculate the cost function using the stochastic
                 gradient descent method. It must be a number less than 1 and greater than 0.

        Returns
        -------
        None
        """

        # Pre-process <p_dropout> to match a common pattern
        if isinstance(p_dropout, float):
            p_dropout = [p_dropout] * (len(self.n_units) + 1)
        elif isinstance(p_dropout, (list, tuple, np.array)):
            if len(p_dropout) == 2:
                p_dropout = [p_dropout[0]] + [p_dropout[1]] * len(self.n_units)
            else:
                if len(p_dropout) != (len(self.n_units) + 1):
                    raise ValueError("Number of elements of <p_dropout> is not correct.")
        else:
            raise ValueError("<p_dropout> is not of a valid type.")

        # Check that p_dropout values are in the interval [0, 1]
        for p in p_dropout:
            if not 0 <= p <= 1:
                raise ValueError("<p_dropout> must be a number in the interval [0, 1].")
        self.p_dropout = p_dropout

        # Check that SGD ratio is a number between 0 and 1
        if not 0 <= p_sgd <= 1:
            raise ValueError("<p_sgd> must be a number in the interval [0, 1].")
        self.p_sgd = p_sgd

        # Initialize list of methods for activation functions
        self.activation_functions = []
        for elem in activation_functions:
            if elem.upper() == "SIGMOID":
                self.activation_functions.append(self.sigmoid)
            elif elem.upper() == "LINEAR":
                self.activation_functions.append(self.linear)
            elif elem.upper() == "TANH":
                self.activation_functions.append(self.tanh)
            elif elem.upper() == "RELU":
                self.activation_functions.append(self.relu)
            else:
                raise TypeError("Variable activation_function takes the incorrect value %s" % elem)

    def _initialize_weights(self, random_seed, weights):
        """
        Generates the initial weights of all layers

        Parameters
        ----------
        random_seed : integer, optional
                      value of the random seed used to initialize random numbers
        weights : list of arrays, optional
                  initial values of the weights. If not provided they are initialize to random
                   numbers to avoid symmetry problems.

        Returns
        -------
        None
        """

        # Initialize seed for the generator of random numbers
        np.random.seed(random_seed)
        # Create a list of weights
        n_layers = len(self.n_units) - 1
        if weights is not None:
            if not isinstance(weights, list):
                raise TypeError("Input variable <weights> is not a list")
            elif len(weights) != n_layers:
                raise ValueError("Input variable <weights> does not contain the correct number of "
                                 "elements")
            for l_index in range(n_layers):
                if not isinstance(weights[l_index], np.ndarray):
                    raise ValueError("At least one element of <weights> is not a NumPy array")
                elif weights[l_index].shape != (self.n_units[l_index + 1], self.n_units[l_index] + 1):
                    raise ValueError("At least one element of <weights> do have the correct shape")
            self.weights = weights
        else:
            self.weights = []
            for l_index in range(n_layers):
                l_in = self.n_units[l_index]
                l_out = self.n_units[l_index + 1]
                self.weights.append(self.eps * (2. * np.random.rand(l_out, l_in + 1) - 1.))

    def cost_function(self, wanna_gradient, p_dropout, p_sgd):
        """
        Evaluates the value of the cost function in regression problems

        Parameters
        ----------
        wanna_gradient : Boolean
                         is gradient required?

        Returns
        -------
        J : float
            cost function
        gradJ : NumPy array, optional
                Value of the derivative of the cost function w.r.t. the values of the weights.
        """

        # Perform preliminary checks and initialize some variables
        n_layers = len(self.weights) - 1
        if not n_layers == len(self.n_units) - 2:
            raise ValueError("<weights> and <nUnits> do nto ")
        n_data = self.training_array.shape[1]

        # Stochastic selection
        n_chosen = 0
        while n_chosen == 0:
            mask = np.random.rand(n_data) < p_sgd
            n_chosen = np.sum(mask)
        X = self.training_array[:, mask]
        Y = self.results_array[:, mask]
        n_data = n_chosen

        # #### Evaluate cost function ####
        # Forward propagation until the final layer is reached
        z = []
        a = [np.asanyarray([np.ones(n_data)] + list(X))]
        ds_dz = []
        for layerNo, Theta in enumerate(self.weights):
            z.append(Theta.dot(a[-1]))
            f, df = self.activation_functions[layerNo](z[-1], True)
            sz = np.asanyarray(f)
            ds_dz.append(df)
            if layerNo != n_layers:
                a.append(np.asanyarray([np.ones(n_data)] + list(sz)))
            else:
                a.append(sz)
        # Only the result at the last layer is used to evaluate the cost
        ht = a[-1]
        J = self.eval_cost(Y, ht)
        # Add term considering smoothing terms
        if self.mu > 1e-30:
            for w in self.weights:
                J += (0.5 * self.mu / n_data) * (w[:, 1:] * w[:, 1:]).sum()

        if not wanna_gradient:
            return J
        else:
            # #### Evaluate gradient of the cost function, if required ####
            delta = [ht - Y]
            for layerNo in reversed(range(n_layers)):
                m = ds_dz[layerNo]
                delta.append(self.weights[layerNo + 1][:, 1:].T.dot(delta[-1]) * m)
            delta.reverse()

            Delta = []
            for layerNo in range(n_layers + 1):
                Delta.append(delta[layerNo].dot(a[layerNo].T))

            # Backpropagation along the neural network
            grad_J = []
            c = 0
            for Theta, D in zip(self.weights, Delta):
                c += 1
                Theta_mod = Theta.copy()
                Theta_mod[:, 0] = 0.
                if c <= n_layers:
                    grad = D / n_data + (self.mu / n_data) * Theta_mod
                else:
                    grad = D / n_data + (self.mu / n_data) * Theta_mod
                grad_J.append(grad)

            return J, grad_J

    @staticmethod
    def regression_cost(samples, predictions):
        """
        Cost for regression cases

        Parameters
        ----------
        samples : NumPy array
                  values of the predicted variable as provided in the training samples
        predictions : NumPy array
                      values of the predicted variable obtained using the neural network

        Returns
        -------
        out : float
              cost
        """
        n_data = samples.shape[1]
        return np.sum((samples - predictions) ** 2.) / n_data / 2.

    @staticmethod
    def binary_classification_cost(samples, predictions):
        """
        Cost for binary classifications

        Parameters
        ----------
        samples : NumPy array
                  values of the predicted variable as provided in the training samples
        predictions : NumPy array
                      values of the predicted variable obtained using the neural network

        Returns
        -------
        out : float
              cost
        """
        n_data = samples.shape[1]
        return -1. * (samples * np.log(predictions) + (1. - samples) * np.log(1. - predictions)).sum() / n_data

    @staticmethod
    def one_of_k_classification_cost(samples, predictions):
        """
        Cost for multiple classifications

        Parameters
        ----------
        samples : NumPy array
                  values of the predicted variable as provided in the training samples
        predictions : NumPy array
                      values of the predicted variable obtained using the neural network

        Returns
        -------
        out : float
              cost
        """
        n_data = samples.shape[1]
        a = np.exp(predictions) / np.sum(np.exp(predictions), axis=0)
        return -1. * (samples * np.log(a)).sum() / n_data

    def gaussian_cost(self, samples, predictions):
        """
        Cost for mixtures of Gaussian pdfs

        Parameters
        ----------
        samples : NumPy array
                  values of the predicted variable as provided in the training samples
        predictions : NumPy array
                      values of the predicted variable obtained using the neural network

        Returns
        -------
        out : float
              cost
        """
        n_data = samples.shape[1]
        pi, mu, sigma = self.extract_parameters(predictions)
        gamma = np.zeros([self.n_components, n_data])
        for l in range(n_data):
            t = samples[:, l]
            for k in range(self.n_components):
                n = self.normal_distribution(t, mu[:, k, l], sigma[k, l])
                gamma[k, l] = pi[k, l] * n
        return - np.sum(np.log(np.sum(gamma, 0))) / n_data

    def beta_cost(self, samples, predictions):
        """
        Cost for mixture of Beta pdfs

        Parameters
        ----------
        samples : NumPy array
                  values of the predicted variable as provided in the training samples
        predictions : NumPy array
                      values of the predicted variable obtained using the neural network

        Returns
        -------
        out : float
              cost
        """
        n_data = samples.shape[1]
        pi, a, b = self.extract_parameters(predictions)
        gamma = np.zeros([self.n_components, n_data])
        for l in range(n_data):
            t = samples[:, l]
            for k in range(self.n_components):
                n = self.beta_distribution(t, a[k, l], b[k, l])
                gamma[k, l] = pi[k, l] * n
        return - np.sum(np.log(np.sum(gamma, 0))) / n_data

    @staticmethod
    def usual_grad(samples, predictions, *args):
        """
        Evaluates the gradient for most objective functions

        Parameters
        ----------
        samples : NumPy array
                  values of the predicted variable as provided in the training samples
        predictions : NumPy array
                      values of the predicted variable obtained using the neural network
        args : tuple
               other stuff required by other methods that also provide the gradient

        Returns
        -------
        out : list
              list of values of the gradient
        """
        return [predictions - samples]

    def gaussian_grad(self, samples, predictions, gamma):
        """
        Evaluates the gradient for the mixture of Gaussian distributions

        Parameters
        ----------
        samples : NumPy array
                  values of the predicted variable as provided in the training samples
        predictions : NumPy array
                      values of the predicted variable obtained using the neural network
        gamma : Numpy array
                responsibilities of each component over the sample points

        Returns
        -------
        out : list
              list of values of the gradient
        """
        n_data = samples.shape[1]

        pi, mu, sigma = self.extract_parameters(predictions)
        sum_over_k = np.sum(gamma, 0)
        for k in range(self.n_components):
            gamma[k, :] /= sum_over_k

        de_dp = pi - gamma
        de_dm = np.zeros((self.n_outputs, self.n_components, n_data))
        de_ds = np.zeros((self.n_components, n_data))
        e = np.zeros((self.n_outputs))
        for n in range(n_data):
            t = samples[:, n]
            for k in range(self.n_components):
                s2 = sigma[k, n] ** 2
                e[:] = (mu[:, k, n] - t[:])
                de_dm[:, k, n] = gamma[k, n] * e[:] / s2
                de_ds[k, n] = gamma[k, n] * (self.n_outputs - np.sum(e ** 2) / s2)

        return [np.asanyarray(list(de_dp) + list(de_ds) +
                              list(np.reshape(de_dm, (self.n_outputs * self.n_components, n_data))))]

    def beta_grad(self, samples, predictions, gamma):
        """
        Evaluates the gradient for the mixture of beta distributions

        Parameters
        ----------
        samples : NumPy array
                  values of the predicted variable as provided in the training samples
        predictions : NumPy array
                      values of the predicted variable obtained using the neural network
        gamma : Numpy array
                responsibilities of each component over the sample points

        Returns
        -------
        out : list
              list of values of the gradient
        """
        n_data = samples.shape[1]

        pi, a, b = self.extract_parameters(predictions)
        sum_over_k = np.sum(gamma, 0)
        for k in range(self.n_components):
            gamma[k, :] /= sum_over_k

        de_dp = pi - gamma
        de_da = np.zeros((self.n_components, n_data))
        de_db = np.zeros((self.n_components, n_data))
        for n in range(n_data):
            t = samples[:, n]
            for k in range(self.n_components):
                pab = psi(a[k, n] + b[k, n] + 2)
                pa = psi(a[k, n] + 1.)
                pb = psi(b[k, n] + 1.)
                de_da[k, n] = gamma[k, n] * a[k, n] * (pa - pab - np.log(t))
                de_db[k, n] = gamma[k, n] * b[k, n] * (pb - pab - np.log(1. - t))

        return [np.asanyarray(list(de_dp) + list(de_da) + list(de_db))]

    def extract_parameters_gaussian(self, nn_output):
        """
        Transforms the output of the Neural network into the parameters of the mixture of Gaussian
         distributions

        Parameters
        ----------
        nn_output : NumPy array
                    outputs of the neural networks, the parameters of the beta distribution all
                     mixed up

        Returns
        -------
        pi : NumPy array
             weights of each component of the mixture
        mu : NumPy array
             expected values of each component of the mixture
        sigma : NumPy array
                variances of each component of the mixture
        """

        n_data = nn_output.shape[1]
        a_pi = nn_output[0:self.n_components, :]
        a_sigma = nn_output[self.n_components:2 * self.n_components, :]
        a_mu = nn_output[2 * self.n_components:, :]

        z_pi = np.exp(a_pi)
        pi = z_pi / np.sum(z_pi, 0)
        mu = np.reshape(a_mu, [self.n_outputs, self.n_components, n_data])
        sigma = np.exp(a_sigma)

        return pi, mu, sigma

    def extract_parameters_beta(self, nn_output):
        """
        Transforms the output of the Neural network into the parameters of the mixture of Beta
         distributions

        Parameters
        ----------
        nn_output : NumPy array
                    outputs of the neural networks, the parameters of the beta distribution all
                     mixed up

        Returns
        -------
        pi : NumPy array
             weights of each component of the mixture
        a, b : NumPy array
               parameters of the beta distribution for each component
        """
        a_pi = nn_output[0:self.n_components]
        a_alpha = nn_output[self.n_components:2 * self.n_components]
        a_beta = nn_output[2 * self.n_components:]

        z_pi = np.exp(a_pi)
        pi = z_pi / np.sum(z_pi, 0)
        a = np.exp(a_alpha)
        b = np.exp(a_beta)

        return pi, a, b

    def eval_opt_fun(self, w, wanna_gradient, **kwargs):
        """
        function designed to optimize (train) the weight values

        Parameters
        ----------
        w : list of floats
            the weights
        wanna_gradient : Boolean
                         Must the gradient be calculated?

        Returns
        -------
        J : float
            value of the function
        dJ : NumPy array
             gradient of the function
        """

        self.weights = self.vector_to_list(w)
        output = self.cost_function(wanna_gradient=wanna_gradient, **kwargs)

        if wanna_gradient:
            j, dj = output
            return j, self.list_to_vector(dj)
        else:
            return output

    def list_to_vector(self, list_of_matrices):
        """
        Transforms a list of matrices to a single vector

        Parameters
        ----------
        list_of_matrices : list of NumPy arrays

        Returns
        -------
        vector : a list of floats
        """
        vector = []
        for indx, w in enumerate(list_of_matrices):
            s1, s2 = w.shape
            assert s2 == self.n_units[indx] + 1
            assert s1 == self.n_units[indx + 1]
            vector += list(np.reshape(w, s1 * s2))
        return np.asanyarray(vector)

    def vector_to_list(self, vector):
        """
        Transforms a list of scalars to a list of matrices

        Parameters
        ----------
        vector : a list of floats

        Returns
        -------
        listM : list of NumPy arrays
        """
        listM = []
        c = 0
        for indx in range(len(self.n_units) - 1):
            s2 = self.n_units[indx] + 1
            s1 = self.n_units[indx + 1]
            d = s1 * s2
            m = np.reshape(vector[c:c + d], (s1, s2))
            c += d
            listM.append(m)
        return listM

    def predict(self, x, guay=True):
        """
        Performs a prediction using the

        Parameters
        ----------
        x : Pandas DataFrame
            values of the features/parameters

        Returns
        -------
        out : NumPy array
              prediction
        """

        # Check that all the columns we'll be looking for are present in the data
        if not np.all([col in x.columns for col in self.data_columns]):
            raise ValueError("Labels of the columns differ from those of the training data")

        # If the result column is not present, create it
        if self.result_label not in x.columns:
            x[self.result_label] = np.zeros(x.shape[0])

        X = x[self.data_columns].values.T
        n_predictions = X.shape[1]

        n_layers = len(self.weights) - 1
        assert(n_layers == len(self.n_units) - 2)
        a = np.concatenate((np.ones((1, n_predictions)), X))
        for layerNo, Theta in enumerate(self.weights):
            z = Theta.dot(a)
            f = self.activation_functions[layerNo](z, False)
            sz = np.asanyarray(f)
            if layerNo != n_layers:
                a = np.concatenate((np.ones((1, n_predictions)), f))
            else:
                ht = sz

        if self.problem_type == "regression":
            x[self.result_label] = ht[0]
            return ht
        elif self.problem_type == "binary classification":
            predictions = (ht / np.sum(ht, axis=0)).T
            x[self.result_label] = [pco({label: value for label, value
                                         in zip(self.category_labels, vec)})
                                    for vec in predictions]
            return pd.DataFrame(predictions, columns=self.category_labels, index=x.index)
        elif self.problem_type == "one-of-k classification":
            predictions = (np.exp(ht) / np.sum(np.exp(ht), axis=0)).T
            x[self.result_label] = [pco({label: value for label, value
                                         in zip(self.category_labels, vec)})
                                    for vec in predictions]
            return pd.DataFrame(predictions, columns=self.category_labels, index=x.index)
        elif self.problem_type == "Gaussian regression":
            if not guay:
                return ht
            pi, mu, sigma = self.extract_parameters(ht)
            predictions = np.concatenate((pi, mu[0, :, :], sigma)).T
            labels = ["pi {}". format(ii) for ii in range(self.n_components)]
            labels += ["mu {}".format(ii) for ii in range(self.n_components)]
            labels += ["sigma {}".format(ii) for ii in range(self.n_components)]
            return pd.DataFrame(predictions, columns=labels)
        else:
            raise TypeError("Variable <problem_type> takes the incorrect value %s" %
                            self.problem_type)

    def check_numerical_gradient(self, n_units, activation_functions, mu=0., p_dropout=(1., 1.),
                                 p_sgd=1., random_seed=211104, weights=None):
        """
        Method to check that the back-propagation algorithm is working properly

        Parameters
        ----------
        n_units : list
                  values of the number of units in each hidden layer. Therefore, its length is the
                   number of hidden layers.
        activation_functions : list of strings
                               type of activation functions to be used between every pair of layers.
                                This values can be either "sigmoid", "linear", "tanh", or "ReLU".
                                Note that the length of this list is equal to the length of
                                <nUnits> + 1.
        mu : float, optional
             regularization parameter. If not provided it is set to zero.
        p_dropout : float or list of floats, optional
                    probability of each layer (input or hidden) of using a given unit. If a float
                     is provided the probability is the same for all layers. If a list of length 2
                     is inputted the first element is the probability for the input layer and the
                     second for the hidden layers. Otherwise a list with the probability for each
                     layer separately must be provided.
        p_sgd : float, optional
                portion of the data used to calculate the cost function using the stochastic
                 gradient descent method. It must be a number less than 1 and greater than 0.
        random_seed : integer, optional
                      value of the random seed used to initialize random numbers
        weights : list of arrays, optional
                  initial values of the weights. If not provided they are initialize to random
                   numbers to avoid symmetry problems.

        Returns
        -------
        None
        """
        # Establish structure of the Neural Network
        if self.problem_type in ["regression", "binary classification", "one-of-k classification"]:
            self.n_units = [self.training_array.shape[0]] + list(n_units) + [self.n_outputs]
        elif self.problem_type in ["Gaussian regression", "beta regression"]:
            self.n_units = [self.training_array.shape[0]] + list(n_units) + \
                           [(self.n_outputs + 2) * self.n_components]
        else:
            raise ValueError("Variable <problem_type> takes an unrecognized value")
        # Initialize values
        self.mu = mu
        self._check_inputs(activation_functions, p_dropout, p_sgd)
        self._initialize_weights(random_seed, weights)

        # Get central point and analytical gradient
        weights = self.list_to_vector(self.weights)
        j, dj_bp = self.eval_opt_fun(weights, wanna_gradient=True,
                                     p_dropout=self.p_dropout, p_sgd=self.p_sgd)

        # Evaluate numerical gradient
        dj_fd = []
        eps = 1e-4
        for index, elem in enumerate(weights):
            weights_plus = weights.copy()
            weights_plus[index] += eps
            j_plus = self.eval_opt_fun(weights_plus, wanna_gradient=False,
                                       p_dropout=self.p_dropout, p_sgd=self.p_sgd)

            weights_minus = weights.copy()
            weights_minus[index] -= eps
            j_minus = self.eval_opt_fun(weights_minus, wanna_gradient=False,
                                        p_dropout=self.p_dropout, p_sgd=self.p_sgd)

            dj_fd.append((j_plus - j_minus) / 2. / eps)
            sys.stdout.write('Back-propagation gradient {}\n'.format(dj_bp[index]))
            sys.stdout.write('Finite differences gradient {}\n'.format(dj_fd[index]))
            sys.stdout.write('ratio {}\n'.format(dj_bp[index] / dj_fd[index]))
            sys.stdout.write('### Error ### {} ######\n'.format(np.abs(dj_bp[index] - dj_fd[index])))

    def check_accuracy(self):
        """
        Calculate accuracy of the trained neural network according to the training data
        """
        # Perform predictions for the training dataset
        X = self.training_array.copy()
        self.predict(X)
        # Each problem type requires a different treatment
        if self.problem_type == "regression":
            errors = np.abs(X[self.result_label] - self.training_array[self.result_label])
            # Calculate global errors using L1, L2, and Linf metrics
            nData = len(errors.values)
            L1E = np.sum(errors.values) / nData
            L2E = np.sqrt(np.sum(errors.values ** 2) / nData)
            LIE = np.max(errors.values)
            # Show results
            sys.stdout.write("Average error {}\n".format(L1E))
            sys.stdout.write("RMS error     {}\n".format(L2E))
            sys.stdout.write("Maximum error {}\n".format(LIE))
        elif self.problem_type.endswith("classification"):
            # TODO: Implement this feature for hard and probabilistic predictions
            pass

    @staticmethod
    def sigmoid(x, wanna_gradient=False):
        """
        Computes the sigmoid function and, if required, its gradient

        Parameters
        ----------
        x : float or NumPy array
            inputted values
        wanna_gradient : Boolean, optional
                         is gradient required? (default=False)

        Return
        ------
        f : float or NumPy array
            value of the sigmoid
        gradf : float or NumPy array
                gradient of f at x
        """
        f = 1. / (1.0 + np.exp(-x))
        if not wanna_gradient:
            return f
        else:
            gradf = f * (np.ones(f.shape) - f)
            return f, gradf

    @staticmethod
    def linear(x, wanna_gradient=False):
        """
        Computes the linear function and, if required, its gradient

        Parameters
        ----------
        x : float or NumPy array
            inputted values
        wanna_gradient : Boolean, optional
                         is gradient required? (default=False)

        Return
        ------
        f : float or NumPy array
            value of the sigmoid
        gradf : float or NumPy array
                gradient of f at x
        """
        f = x
        if not wanna_gradient:
            return f
        else:
            gradf = np.ones(f.shape)
            return f, gradf

    @staticmethod
    def tanh(x, wanna_gradient=False):
        """
        Computes the hyperbolic tangent function and, if required, its gradient

        Parameters
        ----------
        x : float or NumPy array
            inputted values
        wanna_gradient : Boolean, optional
                         is gradient required? (default=False)

        Return
        ------
        f : float or NumPy array
            value of the sigmoid
        gradf : float or NumPy array
                gradient of f at x
        """
        f = np.tanh(x)
        if not wanna_gradient:
            return f
        else:
            gradf = np.ones(f.shape) - f * f
            return f, gradf

    @staticmethod
    def relu(x, wanna_gradient=False):
        """
        Computes the rectified linear unit function and, if required, its gradient

        Parameters
        ----------
        x : float or NumPy array
            inputted values
        wanna_gradient : Boolean, optional
                         is gradient required? (default=False)

        Return
        ------
        f : float or NumPy array
            value of the sigmoid
        gradf : float or NumPy array
                gradient of f at x
        """
        f = np.maximum(x, np.zeros(x.shape))
        if not wanna_gradient:
            return f
        else:
            gradf = f.copy()
            gradf[f == x] = 1.
            return f, gradf

    @staticmethod
    def normal_distribution(t, mu, sigma):
        """
        Evaluates the pdf of the Gaussian distribution

        Parameters
        ----------
        t : float, NumPy array
            value at which the pdf must be evaluated
        mu : float
             expected value of the pdf
        sigma : float
                variance of the pdf

        Returns
        -------
        out : float, NumPy array
              values of the pdf
        """
        s2 = sigma ** 2
        f = np.sqrt(2. * np.pi * s2)
        return np.exp(-0.5 * np.linalg.norm(t - mu) ** 2. / s2) / f

    @staticmethod
    def beta_distribution(t, a, b):
        """
        Evaluates the pdf of the Beta distribution

        Parameters
        ----------
        t : float, NumPy array
            value at which the pdf must be evaluated
        a, b : float
               values of the parameters

        Returns
        -------
        out : float, NumPy array
              values of the pdf
        """
        if a < 10. and b < 10.:
            return (t ** a) * ((1. - t) ** b) / beta(a + 1., b + 1.)
        else:
            x = a * np.log(t) + b * np.log(1 - t) - np.log(beta(a + 1., b + 1.))
            return np.exp(x)

    def cache(self, path, keep_data=False):
        """
        Cache the current Neural Network object to the given path in disk.

        Parameters
        ----------
        path : string
               Output path
        keep_data : boolean
                    Boolean flag indicating whether the training data should
                     be stored in the cached file or not.
        """
        # HACK!! Cannot pickle function/method references -> convert
        # them into string representations that will be handled
        # when loading
        activation_functions = self.activation_functions
        for i, fun in enumerate(self.activation_functions):
            if fun == self.sigmoid:
                self.activation_functions[i] = 'sigmoid'
            elif fun == self.linear:
                self.activation_functions[i] = 'linear'
            elif fun == self.tanh:
                self.activation_functions[i] = 'tanh'
            elif fun == self.relu:
                self.activation_functions[i] = 'relu'
            else:
                raise NotImplementedError('Used activation function can not be cached')

        cost_function = self.cost_function
        if self.cost_function == self.cost_function_classification:
            self.cost_function = 'classification'
        elif self.cost_function == self.cost_function_regression:
            self.cost_function = 'regression'
        else:
            raise NotImplementedError('Used cost function can not be cached')

        if keep_data:
            super().cache(path)
        else:
            super().cache(path, unstorableAttribs=["trainingData", "X"])

        # Restore order
        self.activation_functions = activation_functions
        self.cost_function = cost_function

    def _load(self, path):
        """
        Load Neural Network state from a previously stored pickle object.

        The method parses the attribute list in order to mend broken references

        Parameters
        ----------
        path : string
               Path where the pickle'd object will get read from.
        """
        super()._load(path)
        # HACK!! Restore function references from strings
        for i, fun in enumerate(self.activation_functions):
            if fun == 'sigmoid':
                self.activation_functions[i] = self.sigmoid
            elif fun == 'linear':
                self.activation_functions[i] = self.linear
            elif fun == 'tanh':
                self.activation_functions[i] = self.tanh
            elif fun == 'relu':
                self.activation_functions[i] = self.relu
            else:
                raise NotImplementedError('Used activation function can not be loaded')

        if self.cost_function == 'classification':
            self.cost_function = self.cost_function_classification
        elif self.cost_function == 'regression':
            self.cost_function = self.cost_function_regression
        else:
            raise NotImplementedError('Used cost function can not be loaded')


def load_cached_nn(path):
    self = NeuralNetwork.__new__(NeuralNetwork)
    self._load(path)

    return self
