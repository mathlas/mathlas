# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Pure-Python implementation of the functions required for a decision tree.

This implementation is separated from the main decision_tree module because at some
point there was a separate C++ implementation for this code, with the python code
being provided as a fallback execution path when it could not be loaded. That
code was discontinued at some point in time.
"""

import json
import numpy as np

# Constants indicating column types
REAL = 0
ORDERED = 1
CATEGORICAL = 2


def shannon_entropy(probs):
    """
    Shannon entropy for the given probability distribution.

    The magnitude returned by this function represents how dispersed the points
    in the distribution are.
    The entropy values go 0-log(n), with n being the number of elements in the
    distribution.
    """
    return -np.sum(probs * np.log2(probs))


def probs_distribution(outcomes):
    """
    Return a column vector with the proportions in which each of
    the values in outcomes is present.
    If outcomes is:
         [0, 1, 1, 1, 2, 2]
    then this function will return a 1D array with values:
         [⅙, 0.5, ⅓]
    """
    # Get a sorted vector with all the possible outcomes
    _, probs = np.unique(outcomes, return_counts=True)

    return probs / outcomes.shape[0]


def find_optimal_split(data, outcomes, weights, thresholds, col,
                       rows, min_delta=0.1):
    # Initialize some vars
    prev_threshold = -np.inf
    optimal_threshold = None
    optimal_gain = -np.inf

    # Compute the base entropy.
    base_entropy = shannon_entropy(probs_distribution(outcomes[rows]))

    for i in range(thresholds.shape[0]):
        if thresholds[i] >= prev_threshold + min_delta:
            gain = base_entropy
            # Compute entropy of the element distribution below thresholds[i]
            # and subtract it from gain
            below = rows[data[rows, col] < thresholds[i]]
            gain -= (np.sum(weights[below]) *
                     shannon_entropy(probs_distribution(outcomes[below])) /
                     np.sum(weights[rows]))
            # Compute entropy of the element distribution above thresholds[i]
            # and subtract it from gain
            above = rows[data[rows, col] >= thresholds[i]]
            gain -= (np.sum(weights[above]) *
                     shannon_entropy(probs_distribution(outcomes[above])) /
                     np.sum(weights[rows]))

            # If we're better than the best, store this value as a candidate
            if gain > optimal_gain:
                optimal_threshold = thresholds[i]
                optimal_gain = gain

            # Store the current threshold so that we can honour min_delta
            prev_threshold = thresholds[i]

    return np.array([optimal_threshold, optimal_gain])


def find_cat_split_performance(data, outcomes, weights, col, rows):
    # The total gain (we'll subtract from this)
    gain = shannon_entropy(probs_distribution(outcomes[rows]))

    categories = np.unique(data[rows, col])
    factor = np.sum(weights[rows])
    for cat in categories:
        category_elems = rows[data[rows, col] == cat]
        gain -= (np.sum(weights[category_elems]) *
                 shannon_entropy(probs_distribution(outcomes[category_elems])) /
                 factor)

    return np.array([np.NaN, gain])


def partition_data(data, outcomes, weights,
                   min_delta, max_levels,
                   data_types, ignore_col,
                   rows, level=0):
    """
    This functions takes input data and determines the set of filters
    which provide the best split based on Shannon's entropy criteria.
    The resulting filters are returned as a json object
    This function will call itself recursively.
    """
    # Store unique results and their probabilities,
    # based on the info so-far
    possible_outcomes = [str(outcome) for outcome in np.unique(outcomes[rows])]
    probs = dict(zip(possible_outcomes, probs_distribution(outcomes[rows])))

    # Check if we should stop iterating
    if len(possible_outcomes) < 2 or level >= max_levels:
        return {"probs": probs}

    # Make a copy of ignore_col, since we're going to want to modify it but only downstream
    ignore_col = ignore_col.copy()

    # Determine the optimal split column and location
    split_performances = np.zeros((2, data.shape[1]), dtype=np.double)
    for i in range(data.shape[1]):
        if ignore_col[i] != 1:
            if data_types[i] == REAL:
                sorted_data = np.unique(data[rows, i])
                if sorted_data.shape[0] > 1:
                    # Determine the thresholds as the midpoints in sorted_data
                    thresholds = (sorted_data[:-1] + sorted_data[1:]) / 2.0
                    split_performances[:, i] = find_optimal_split(data, outcomes, weights,
                                                                  thresholds, i, rows,
                                                                  min_delta)
                else:
                    # Only one value -> don't explore this column again
                    ignore_col[i] = 1
            elif data_types[i] == ORDERED:
                sorted_data = np.unique(data[rows, i])
                if sorted_data.shape[0] > 1:
                    # All the points are thresholds, except the first one
                    thresholds = sorted_data[1:]
                    split_performances[:, i] = find_optimal_split(data, outcomes, weights,
                                                                  thresholds, i, rows,
                                                                  min_delta)
                else:
                    # Only one value -> don't explore this column again
                    ignore_col[i] = 1
            elif data_types[i] == CATEGORICAL:
                # No thresholds here, split by all the values
                split_performances[:, i] = find_cat_split_performance(data, outcomes, weights,
                                                                      i, rows)
            else:
                raise ValueError('data_types[{}] is not valid'.format(i))

    # No columns to explore? => Return since we're at the end of the tree branch
    if (ignore_col == 1).all():
        return {"probs": probs}

    # Set index_max to the column index with the highest entropy gain
    index_max = split_performances[1, :].argmax()

    # Actually split data and iterate
    if data_types[index_max] == CATEGORICAL:
        ignore_col[index_max] = 1

        # Analyze the data for each category separately
        children = {}
        categories = np.unique(data[rows, index_max])
        for i in range(categories.shape[0]):
            filtered_rows = rows[data[rows, index_max] == categories[i]]
            if filtered_rows.shape[0] > 0:
                children[str(i)] = partition_data(data, outcomes, weights,
                                                  min_delta, max_levels,
                                                  data_types, ignore_col,
                                                  filtered_rows,
                                                  level + 1)
                children[str(i)]["filter_label"] = str(int(categories[i]))

        return {"categories": list(categories),
                "column": int(index_max),
                "children": children,
                "probs": probs}
    else:
        # ORDERED or REAL
        # Iterate on the data above and below the threshold
        children = {}
        threshold = split_performances[0, index_max]

        # Add children only if they have associated data
        i = 0
        filtered_rows = rows[data[rows, index_max] < threshold]
        if filtered_rows.shape[0] > 0:
            children[str(i)] = partition_data(data, outcomes, weights,
                                              min_delta, max_levels, data_types,
                                              ignore_col,
                                              filtered_rows,
                                              level + 1)
            children[str(i)]["filter_label"] = "<" + str(threshold)
            i += 1

        filtered_rows = rows[data[rows, index_max] >= threshold]
        if filtered_rows.shape[0] > 0:
            children[str(i)] = partition_data(data, outcomes, weights,
                                              min_delta, max_levels, data_types,
                                              ignore_col,
                                              filtered_rows,
                                              level + 1)
            children[str(i)]["filter_label"] = ">=" + str(threshold)

        return {"threshold": float(threshold),
                "column": int(index_max),
                "children": children,
                "probs": probs}


def train(training_data, outcomes, weights, min_delta, max_levels, criteria):
    """Train a decision tree"""

    # A few simple dimensionality tests
    if len(training_data.shape) != 2:
        raise ValueError("training_data must be a 2D array")

    if len(outcomes.shape) != 1:
        raise ValueError("outcomes must be a 1D array")

    if outcomes.shape[0] != training_data.shape[0]:
        raise ValueError("The number of elements in outcomes must " +
                         "match the number of rows in training_data")

    outcomes = outcomes.astype(np.int64)

    if len(weights.shape) != 1:
        raise ValueError("weights must be a 1D array")

    if weights.shape[0] != training_data.shape[0]:
        raise ValueError("The number of elements in weights must " +
                         "match the number of rows in training_data")

    if len(criteria.shape) != 1:
        raise ValueError("criteria must be a 1D array")

    if criteria.shape[0] != training_data.shape[1]:
        raise ValueError("The number of elements in criteria must " +
                         "match the number of columns in training_data")

    # Initialize some vectors we'll be using
    ignore_col = np.zeros(training_data.shape[1], dtype=np.int64)
    indices = np.array(range(training_data.shape[0]))

    # Find the partition rules
    filters = partition_data(training_data, outcomes,
                             weights,
                             min_delta, max_levels,
                             criteria, ignore_col,
                             indices)

    return json.dumps(filters)
