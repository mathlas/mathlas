# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import pandas as pd
from mathlas.object import MathlasObject


class KFoldCrossValidation(MathlasObject):

    def __init__(self, train_method, evaluate_method, observations, result_column, metrics,
                 partition_method, is_multiclass_classification, number_of_partitions=3,
                 len_rank=(None,), random_seed=211104, train_args=None, partition_column=None,
                 keep_all_models=True):
        """
        Perform k-fold cross validation on the given data

        Parameters
        ----------
        train_method : callable (method or function)
                       The training method to be applied to the sub-samples
        evaluate_method : callable (method or function)
                          The categorize method to be applied to the sub-samples. This callable
                           object is supposed to return the predicted values instead of operating
                           them in-place
        observations : pandas DataFrame
                       DataFrame with the variables and their measured values
        result_column : String
                        The name of the result column
        metrics : iterable
                  A list of error metric functions to be used. They can be strings or callables.
                   Valid string values are:
                     * "precision" : precision value
                     * "DCG" : Discounted Cumulative Gain
                     * "DCG_mod" : Modified DCG
        partition_method : string
                           how the partition of the samples is going to be performed. Valid values
                            are "random", "alternate", or "consecutive"
        is_multiclass_classification : Boolean
                                       are we working on a multi-class classification problem?
        number_of_partitions : Integer
                               The number of partitions to use; aldo the k in k-fold. The data will
                                be split into `k` sections.
        len_rank : iterable of integers or Nones
                   The number of observations to consider when computing the metrics. The length of
                    this iterable must match that of `metrics`. If an item in len_rank is `None` all
                    the elements will be used
        random_seed : integer
                      Random seed to use for pseudo-random number generator
                      initialization
        train_args : iterable of iterables
                     Iterable with positional arguments for the train method, which will be passed
                      after the training data and result column
        partition_column : string or None, optional
                           If given a column name, the training data will be split randomly based on
                            the unique values in the given column. If not given (or None) the whole
                            training data will be split randomly.
        keep_all_models : Boolean
                          Must all intermediate models be kept?

        Returns
        -------
        cv_merits : Pandas DataFrame with the cross validated data
                    The indices correspond to each of the training parameters whereas the columns
                     correspond to each of the metrics.
        """
        # Store inputs and more for further use
        self.train_method = train_method
        self.evaluate_method = evaluate_method
        self.observations = observations
        self.result_column = result_column
        self.number_of_partitions = number_of_partitions
        self.partition_column = partition_column
        self.metrics = metrics
        self.partition_method = partition_method
        self.len_rank = len_rank
        self.random_seed = random_seed
        self.partition_column = partition_column
        self.ranking_metrics = {"precision", "DCG", "DCG_mod"}
        self.models = {} if keep_all_models else None
        self.global_model = None
        self.optimal_params = None
        self.index_optimal_params = None

        # Check integrity of inputs
        self.metric_names, self.working_on_a_ranking = self._check_inputs()

        # No extra params for the methods? => provide and empty list as extra args
        # You should probably not be using this function if you're doing this
        self.train_args = [[]] if train_args is None else train_args

        # Store the sampled results for later and create an initial predicted_values vector
        self.is_multiclass_classification = is_multiclass_classification
        if is_multiclass_classification:
            self.class_labels = sorted(list(set(observations[result_column])))
            indexes = observations[result_column].replace(self.class_labels, range(len(self.class_labels))).values
            n_samples, n_classes = len(observations.index), len(self.class_labels)

            self.sampled_values = np.zeros((n_samples, n_classes))
            self.sampled_values[np.arange(n_samples), indexes] = 1.
            self.sampled_values = pd.DataFrame(self.sampled_values,
                                               index=self.observations.index,
                                               columns=self.class_labels)
            predicted_values = pd.DataFrame(np.zeros((n_samples, n_classes)),
                                            index=self.observations.index,
                                            columns=self.class_labels)
        else:
            self.sampled_values = observations[result_column]
            predicted_values = pd.Series(np.zeros(self.sampled_values.shape[0]),
                                         index=self.sampled_values.index)
        # Create the matrix which will store the metrics
        self.figures_of_merit = pd.DataFrame(np.zeros((len(self.train_args),
                                                       len(self.metric_names))))

        # Initiate the random number generator with the given random seed
        np.random.seed(random_seed)
        self.subsample_locators = self._generate_partition_subsamples()

        # Iterate on the training args
        for row, args in enumerate(self.train_args):
            # Now, iterate on the the subsamples, train and evaluate the models
            # TODO: We should not be storing the train params as the keys for models
            #       as that creates a tremendous rigidness on what kinds of parameters
            #       we can pass to the train methods.
            #       We should store a separate dictionary of params, whose keys match
            #       those of the associated models.
            if keep_all_models:
                self.models[frozenset(args.items())] = []
            for i in range(self.number_of_partitions):
                # Choose all the indices not in subsample_locators[i]
                if self.partition_column:
                    # Simply negate the mask here
                    indices = ~self.subsample_locators[i]
                else:
                    # We must concatenate all the indices not in subsample_locators[i]
                    indices = np.concatenate([self.subsample_locators[j] for j in
                                              range(len(self.subsample_locators)) if j != i])

                # Train and evaluate the model with the data in observations.loc[indices]
                model = self.train_method(observations.loc[indices], result_column, args)
                value = self.evaluate_method(model, observations.loc[self.subsample_locators[i]])
                predicted_values.loc[self.subsample_locators[i]] = value
                if keep_all_models:
                    self.models[frozenset(args.items())].append(model)

            if self.working_on_a_ranking:
                # All the predictions have been stored => compute the metric function(s) value(s)
                for col, metric in enumerate(self.metrics):
                    n = len_rank[col]
                    if n is None or n > predicted_values.shape[0]:
                        n = predicted_values.shape[0]

                    # Store the figure of merit
                    self.figures_of_merit.loc[row, col] = metric(predicted_values,
                                                                 self.sampled_values, n)
            else:
                for col, metric in enumerate(self.metrics):
                    self.figures_of_merit.loc[row, col] = metric(predicted_values,
                                                                 self.sampled_values)

        # Change the indices & columns to more user-friendly values
        self.train_args = train_args
        self.figures_of_merit.index = range(len(train_args))
        self.figures_of_merit.columns = self.metric_names

    def predict(self, x, method, metric=None, train_if_not_available=False, provide_variance=False):
        """
        Predicts the values at some points using the optimal model

        Parameters
        ----------
        x : pandas DataFrame
            DataFrame with the variables
        method : string
                 how are the predictions supposed to be obtained? If "average" the average value of
                  those provided by the several models (obtained from the partitions of the data)
                  are used; if "global" a single model trained using all observations is used.
        metric : string
                 metric to optimize and provide the model to be used
        train_if_not_available : Boolean
                                 If models are not available, can they be calculated? If not an
                                  exception is returned.
        provide_variance : Boolean
                        must the prediction include the variance of the distribution or only
                         the expected value?

        Returns
        -------
        out : NumPy array
              predicted values
        """
        if self.is_multiclass_classification and method == "average":
            raise NotImplementedError("Averaged prediction cannot be used on multi-class"
                                      " classification problems")

        models = self.get_optimal_models(method, metric, train_if_not_available)

        n_models = len(models)
        x[self.result_column] = 0
        if provide_variance:
            x["variance"] = 0
        x_copy = x.copy()
        for model in models:
            y = self.evaluate_method(model, x_copy, provide_variance=provide_variance)
            if provide_variance:
                x[[self.result_column, "variance"]] += y / n_models
            else:
                x[self.result_column] += y / n_models
        return x

    def get_optimal_models(self, method, metric=None, train_if_not_available=False):
        """
        Provides a list of the optimal models according to some metric

        Parameters
        ----------
        method : string
                 how are the predictions supposed to be obtained? If "average" the average value of
                  those provided by the several models (obtained from the partitions of the data)
                  are used; if "global" a single model trained using all observations is used.
        metric : string
                 metric to optimize and provide the model to be used
        train_if_not_available : Boolean
                                 If models are not available, can they be calculated? If not an
                                  exception is returned.

        Returns
        -------
        models : list
                 the optimal models
        """
        # If no metric is provided perhaps we can use something
        if metric is None:
            if len(self.figures_of_merit.columns) == 1:
                metric = self.figures_of_merit.columns[0]
            else:
                raise ValueError("<metric> must be inputted")
        # Get optimal parameters
        if metric in ["L2 error", "two classes", "multiple classes"]:
            self.index_optimal_params = self.figures_of_merit[metric].idxmin()
        elif (metric in ["likelihood"] or
              any([metric.startswith(x) for x in ["precision", "DCG", "DCG_mod"]])):
            self.index_optimal_params = self.figures_of_merit[metric].idxmax()
        else:
            raise ValueError("Metric '{}' is not supported".format(metric))
        self.optimal_params = self.train_args[self.index_optimal_params]
        if metric not in self.figures_of_merit.keys():
            raise ValueError("The inputted value of <metric> must one of the previously calculated")
        # Retrieve method or calculate it, if required
        if method == "global":
            if self.global_model is None:
                if train_if_not_available:
                    self.global_model = self.train_method(self.observations, self.result_column,
                                                          self.optimal_params)
                else:
                    raise ValueError("Model is not available")
            models = [self.global_model]
        elif method == "average":
            if self.models is None:
                if train_if_not_available:
                    self.models = []
                    for i in range(self.number_of_partitions):
                        # Choose all the indices not in subsample_locators[i]
                        if self.partition_column:
                            # Simply negate the mask here
                            indices = ~self.subsample_locators[i]
                        else:
                            # We must concatenate all the indices not in subsample_locators[i]
                            indices = np.concatenate([self.subsample_locators[j] for j in
                                                      range(len(self.subsample_locators)) if
                                                      j != i])

                        # Train and evaluate the model with the data in observations.loc[indices]
                        model = self.train_method(self.observations.loc[indices],
                                                  self.result_column, self.optimal_params)
                        self.models[frozenset(self.optimal_params.items())].append(model)
                else:
                    raise ValueError("Model is not available")
            models = self.models[frozenset(self.optimal_params.items())]
        else:
            raise ValueError("Value of method must be 'global' or 'average'")

        return models

    def _check_inputs(self):
        """

        Returns
        -------
        metric_names : list of integers
                       names of the metric (type + parameters) used
        """

        if not isinstance(self.observations, pd.DataFrame):
            raise TypeError('`observations` must be a pandas DataFrame')

        if self.result_column not in self.observations.columns:
            raise ValueError('"{}" is not a valid column name '.format(self.result_column) +
                             'for the observations')

        if self.partition_column and self.partition_column not in self.observations.columns:
            raise ValueError('"{}" is not a valid name '.format(self.partition_column) +
                             'for the partition column')

        if not callable(self.train_method):
            raise ValueError(
                'Given method for `train` argument is not a callable function or method')

        if not callable(self.evaluate_method):
            raise ValueError(
                'Given method for `evaluate` argument is not a callable function or method')

        if (self.number_of_partitions < 2 or
                self.number_of_partitions > self.observations.shape[0] - 1):
            raise ValueError('Given value for the `k` parameter is invalid')

        # We can work with arguments which are not iterables, but are sane
        if isinstance(self.metrics, str) or callable(self.metrics):
            self.metrics = (self.metrics,)

        # Are we working on a ranking?
        has_ranking_metric = set(self.metrics).intersection(self.ranking_metrics)
        has_other_metric = set(self.metrics).difference(self.ranking_metrics)
        working_on_a_ranking = has_ranking_metric and not has_other_metric

        # Check that the values given in metrics are acceptable
        metric_names = []
        self.metrics = list(self.metrics)  # We want to operate on a copy of the given list
        # If we are working on a ranking some operations must be performed. Otherwise do some other,
        #  very important stuff
        if working_on_a_ranking:

            for i, metric in enumerate(self.metrics):
                n = self.len_rank[i]
                if metric not in ['precision', 'DCG', 'DCG_mod'] and not callable(metric):
                    raise ValueError('Given value for the `metrics` parameter is unsupported')
                else:
                    extra = ''
                    if n is not None:
                        extra = ' ({})'.format(n)

                    if callable(metric):
                        metric_names.append(metric.__name__ + extra)
                    else:
                        # Convert strings into callables and store their names
                        if metric == 'precision':
                            self.metrics[i] = self.precision
                        elif metric == 'DCG':
                            self.metrics[i] = self.dcg
                        elif metric == 'DCG_mod':
                            self.metrics[i] = self.dcg_mod
                        else:
                            raise ValueError("Inputted metric `{}` not implemented".format(metric))
                        metric_names.append(metric + extra)

            if isinstance(self.len_rank, int) or self.len_rank is None:
                self.len_rank = (self.len_rank,)

            n_metrics = len(self.metrics)
            n_ranks = len(self.len_rank)
            n_cases = max(n_metrics, n_ranks)
            if n_metrics != n_metrics:
                if n_metrics == 1:
                    self.metrics = self.metrics * n_cases
                else:
                    raise ValueError('`metrics` parameter is not coherent with `len_rank`')

            if n_ranks != n_metrics:
                if n_ranks == 1:
                    self.len_rank = self.len_rank * n_cases
                else:
                    raise ValueError('`len_rank` parameter is not coherent with `metrics`')

            return metric_names, working_on_a_ranking

        else:

            for i, metric in enumerate(self.metrics):

                if callable(metric):
                    metric_names.append(metric.__name__)
                else:
                    # Convert strings into callables and store their names
                    if metric == 'L2 error':
                        self.metrics[i] = self.l2_error
                    elif metric == "two classes":
                        self.metrics[i] = self.binary_class_error
                    elif metric == "multiple classes":
                        self.metrics[i] = self.multiple_class_error
                    else:
                        raise ValueError("Inputted metric `{}` not implemented".format(metric))
                    metric_names.append(metric)

            return metric_names, working_on_a_ranking

    def _generate_partition_subsamples(self):
        """
        Creates a partition of the dataset

        Returns
        -------
        subsample_locators : list
                             indexes of the elements in each partition
        """
        # Generate partitioning info based on either the data in
        # the given column or all the data in the observations
        if self.partition_column:
            # subsample_locators here will be a list of boolean masks
            values = self.observations[self.partition_column].drop_duplicates()
            value_chunks = self._partition(list(values))

            # Compute the subsample masks
            subsample_locators = []
            for value_chunk in value_chunks:
                mask = np.zeros(self.observations[self.partition_column].shape[0], dtype=bool)
                for value in value_chunk:
                    mask |= (self.observations[self.partition_column] == value)

                subsample_locators.append(mask)
        else:
            # subsample_locators here will be a list with indices
            subsample_locators = self._partition(list(self.observations.index))

        return subsample_locators

    def _partition(self, stuff):
        """
        Given a list of values a partition in approximately equal-size arrays is created

        Parameters
        ----------
        stuff : list, NumPy array
                whatever you want to see chunked

        Returns
        -------
        subsample_locators : list of lists
                             partitions
        """

        # Create partition of training dataset
        if self.partition_method == "alternate":
            subsample_locators = [stuff[ii::self.number_of_partitions]
                                  for ii in range(self.number_of_partitions)]
        elif self.partition_method == "random":
            np.random.shuffle(stuff)
            subsample_locators = np.array_split(stuff, self.number_of_partitions)
        elif self.partition_method == "consecutive":
            n_elements = int(len(stuff) / self.number_of_partitions) + 1
            subsample_locators = [stuff[ii:ii + n_elements]
                                  for ii in range(0, len(stuff), n_elements)]
        else:
            raise ValueError("Inputted `partition_method` is not valid")

        return subsample_locators

    @staticmethod
    def l2_error(predicted_values, sampled_values):
        """
        Computes the usual, L2 error

        Parameters
        ----------
        predicted_values : pandas Series
                           Predicted values (usually probabilities)
        sampled_values : pandas Series
                         Sampled values (usually 0-1)

        Returns
        -------
        precision : float
                    The metric.
        """
        return np.sqrt(np.sum(np.power(predicted_values - sampled_values, 2)) / len(sampled_values))

    @staticmethod
    def binary_class_error(predicted_values, sampled_values):
        """
        Computes the usual, L2 error

        Parameters
        ----------
        predicted_values : pandas Series
                           Predicted values (usually probabilities)
        sampled_values : pandas Series
                         Sampled values (usually 0-1)

        Returns
        -------
        precision : float
                    The metric.
        """
        x0 = sampled_values * np.log(predicted_values + 1e-16)
        x1 = (1. - sampled_values) * np.log(1. - predicted_values + 1e-16)
        return -np.sum(x0 + x1) / len(sampled_values)

    @staticmethod
    def multiple_class_error(predicted_values, sampled_values):
        """
        Computes the usual, L2 error

        Parameters
        ----------
        predicted_values : pandas Series
                           Predicted values (usually probabilities)
        sampled_values : pandas Series
                         Sampled values (usually 0-1)

        Returns
        -------
        precision : float
                    The metric.
        """
        if len(sampled_values) == 0:
            raise ValueError("sampled_values has a len of 0")
        return -np.sum(sampled_values.values * np.log(predicted_values.values + 1e-16)) / len(sampled_values)

    @staticmethod
    def precision(predicted_values, sampled_values, n):
        r"""
        Compute the Precision metric with the given predicted and sampled values

        In order to compute the metric, a new vector (referred to as the relevance
        vector) is computed by sorting the elements in sampledValues according to the
        predictedValues (from largest to smallest).
        That is: if predictedValues is [0.2, 0.5, 0.7] and sampledValues is [0, 1, 1]
        the relevance vector will be [1, 1, 0].
        If n is given and it is lower than len(sampledValues), only the first n elements
        of the relevance vector will be considered when computing the metric.

        The precision is computed as:

        .. math:: precision=\frac{{\displaystyle \sum_{i=1}^{n}}relevance_{i}}{D}

        Where:

        .. math:: D=\min\left(n,\;L\right)

        Where :math:`L` is the position in which the last "1" appears in the relevance
        vector (when considered complete).

        Parameters
        ----------
        predicted_values : pandas Series
                           Predicted values (usually probabilities)
        sampled_values : pandas Series
                         Sampled values (usually 0-1)
        n : integer, optional
            The number of samples/values to consider

        Returns
        -------
        precision : float
                    The metric.
        """
        if not isinstance(predicted_values, pd.Series):
            raise TypeError('predicted_values must be a pandas Series')
        if not isinstance(sampled_values, pd.Series):
            raise TypeError('sampled_values must be a pandas Series')
        if predicted_values.shape[0] != sampled_values.shape[0]:
            raise ValueError(
                'The number of elements in predictedValues and sampledValues must match')

        if n is None:
            n = sampled_values.shape[0]
        elif n > sampled_values.shape[0]:
            n = sampled_values.shape[0]

        relevance = sampled_values.loc[predicted_values.sort_values(ascending=False).index]

        # Find the index of the last "one"
        indices = np.where(relevance == 1)[0]
        if indices.shape[0] == 0:
            return 0.
        else:
            # Technically speaking, relevance[:n].shape[0] needn't be n (it could be smaller)
            l = indices[-1] + 1
            d = min(relevance[:n].shape[0], l)

            return np.sum(relevance[:n]) / d

    @staticmethod
    def dcg(predicted_values, sampled_values, n):
        r"""
        Compute the Discounted Cumulative Gain metric with the given predicted and sampled values

        In order to compute the metric, a new vector (referred to as the relevance
        vector) is computed by sorting the elements in sampledValues according to the
        predictedValues (from largest to smallest).
        That is: if predictedValues is [0.2, 0.5, 0.7] and sampledValues is [0, 1, 1]
        the relevance vector will be [1, 1, 0].
        If n is given and it is lower than len(sampledValues), only the first n elements
        of the relevance vector will be considered when computing the metric.

        The DCG is computed as:

        .. math:: DCG=\frac{{\displaystyle \sum_{i=1}^{n}}\frac{2^{relevance_{i}}-1}{\log_{2}\left(i+1\right)}}{{\displaystyle \sum_{x=1}^{D}\frac{1}{\log_{2}\left(x+1\right)}}} \\

        Where:

        .. math:: D=\min\left(n,\;L\right)

        Where :math:`L` is the position in which the last "1" appears in the relevance
        vector (when considered complete).

        Parameters
        ----------
        predicted_values : pandas Series
                           Predicted values (usually probabilities)
        sampled_values : pandas Series
                         Sampled values (usually 0-1)
        n : integer, optional
            The number of samples/values to consider

        Returns
        -------
        DCG_mod : float
                  The metric.
        """
        if predicted_values.shape[0] != sampled_values.shape[0]:
            raise ValueError(
                'The number of elements in predictedValues and sampledValues must match')

        if n is None:
            n = sampled_values.shape[0]
        elif n > sampled_values.shape[0]:
            n = sampled_values.shape[0]

        relevance = sampled_values.loc[predicted_values.sort_values(ascending=False).index]

        # The index of the last "one"
        indices = np.where(relevance == 1)[0]
        if indices.shape[0] == 0:
            return 0.
        else:
            # Technically speaking, relevance[:n].shape[0] needn't be n (it could be smaller)
            l = indices[-1] + 1
            d = min(relevance[:n].shape[0], l)

            return (np.sum((2 ** relevance[:n] - 1.) / np.log2(np.arange(2, n + 2))) /
                    np.sum(1. / np.log2(np.arange(2, d + 2))))

    @staticmethod
    def dcg_mod(predicted_values, sampled_values, n):
        r"""
        Compute a modified Discounted Cumulative Gain metric with the given predicted and sampled values

        In order to compute the metric, a new vector (referred to as the relevance
        vector) is computed by sorting the elements in sampledValues according to the
        predictedValues (from largest to smallest).
        That is: if predictedValues is [0.2, 0.5, 0.7] and sampledValues is [0, 1, 1]
        the relevance vector will be [1, 1, 0].
        If n is given and it is lower than len(sampledValues), only the first n elements
        of the relevance vector will be considered when computing the metric.

        The DCG is computed as:

        .. math:: DCG_{mod}=\frac{{\displaystyle \sum_{i=1}^{n}}\frac{2^{relevance_{i}}-1}{i}}{{\displaystyle \sum_{x=1}^{D}\frac{1}{x}}}

        Where:

        .. math:: D=\min\left(n,\;L\right)

        Where :math:`L` is the position in which the last "1" appears in the relevance
        vector (when considered complete).

        Parameters
        ----------
        predicted_values : pandas Series
                           Predicted values (usually probabilities)
        sampled_values : pandas Series
                         Sampled values (usually 0-1)
        n : integer, optional
            The number of samples/values to consider

        Returns
        -------
        DCG : float
              The metric.
        """
        if predicted_values.shape[0] != sampled_values.shape[0]:
            raise ValueError(
                'The number of elements in predictedValues and sampledValues must match')

        if n is None:
            n = sampled_values.shape[0]
        elif n > sampled_values.shape[0]:
            n = sampled_values.shape[0]

        relevance = sampled_values.loc[predicted_values.sort_values(ascending=False).index]

        # The index of the last "one"
        indices = np.where(relevance == 1)[0]
        if indices.shape[0] == 0:
            return 0.
        else:
            l = indices[-1] + 1
            d = min(n, l)

            return (np.sum(relevance[:n] * np.array([1 / e for e in range(1, n + 1)])) /
                    np.sum(1 / np.arange(1, d + 1)))

    def cache(self, path, compress=True, store_observations=True):
        if store_observations:
            super().cache(path=path, compress=compress)
        else:
            super().cache(path=path, compress=compress, unstorableAttribs=['observations'])

