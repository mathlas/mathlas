# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import numpy as np
import pandas as pd
from collections import Counter
from itertools import combinations
from mathlas.object import MathlasObject
from scipy.stats import multivariate_normal
from mathlas.misc.progress_bar import ProgressBar
from mathlas.machine_learning.hmm import HiddenMarkovModel
from mathlas.not_for_clients.exceptions import HereBeDragonsException


class HiddenMarkovClassifier(MathlasObject):

    def __init__(self, dict_samples, var_type, n_hidden_states, model_type='mathlas hmm',
                 must_estimate_parameters=True, estimation_method="estimation hmm",
                 target_gradient=None, must_estimate_initial_means=True, n_samples=1000,
                 n_models=0, keep_samples=True, seed=211104, metadata=None, *args, **kwargs):
        """
        Classifier that, given pre-classified sequences, provides a
        HMM-based model that classifies new sequences.

        Parameters
        ----------
        dict_samples : dictionary
                       keys are the labels of the classes and values are lists of sequences.
                       Each element of the sequence can be a string, an integer, a float
                       or an array of floats.
        var_type : string or None
                   Datatype of the sequences. It can take values "continuous" or "categorical".
        n_hidden_states : integer/list/NumPy array or dictionary
                          number of hidden states of the HMM models.

                            * If it is an integer/list/NumPy array, the same value(s)
                              will be used for all classes
                            * If it is a dictionary a different value can be provided
                              for each class.
        model_type : string, optional
                     HMM model type to create.
                     Allowed values:
                        * `mathlas hmm` (default). Use homegrown HMM code
        must_estimate_parameters : Boolean
                                   Should a k-means-based estimator be used to get the initial
                                   values of the hyper-parameters (in case continuous inputs
                                   are provided)? If True, data must be continuous and it must
                                   be made explicit in <var_type>
        estimation_method : string, optional
                            only used if <must_estimate_parameters> is true. How the initial
                            values of the parameters must be estimated.
                            Allowed values:
                                * ``k-means``
                                * ``estimation hmm``
                                * ``complete hmm``
                                * ``silhouette``
        target_gradient : float or None, optional
                          Value of the gradient of the figure of merit at which we aim. Can be
                          seen as the learning rate. Can be given a value of `None` for automatic
                          control.

                          Only considered if <must_estimate_parameters> is `True`.
        must_estimate_initial_means : Boolean, optional
                                      only used if <must_estimate_parameters> is true. Do the
                                      initial values of the means must be estimated or chosen
                                      at random.
        n_samples : integer, optional
                    only used if <must_estimate_parameters> is true. Number of elements 
                    used to estimate the number of hidden states
        n_models : integer, optional
                   number of different models that would be tested for each class. Several models
                    are trained to avoid local minima when looking for the highest likelihood. If
                    it is an integer, the same value would be used for all classes; if it is a
                    dictionary a different value can be provided for each class.
        keep_samples : boolean, optional
                       Flag indicating whether training data should be stored in a
                       `dict_samples` attribute or it should be discarded after training.
                       Defaults to keeping the samples.
        seed : integer, optional
               Input only if mode is "generate" or "train". Seed used to generate random numbers.
        n_samples : integer, optional
                    Its values is only used if <must_estimate_solution> is True. It is the number 
                    of samples used to estimate the means using k-means.
        metadata : anything, optional
                   Extra model metadata that will be stored with the model.
                   The metadata is completely optional and will not be used for any purpose
                   by the trained model.
        args, kwargs : tuple and dictionary
                       other parameters of the HMM method like <var_type>, <emitted_states>, 
                       <infimum_point>, and <supremum_point>
        """
        if not isinstance(dict_samples, dict):
            raise TypeError('<dict_samples> must be a dictionary ' +
                            '({} given)'.format(type(dict_samples)))

        # Store inputted data
        if keep_samples:
            self.dict_samples = dict_samples
        else:
            self.dict_samples = None
        self.n_hidden_states = n_hidden_states
        self.n_models = n_models
        self.labels = sorted(self.dict_samples.keys())
        # Preprocess inputs
        for var_label in ["n_hidden_states", "n_models"]:
            var = getattr(self, var_label)
            if isinstance(var, (int, list, tuple, np.ndarray)):
                setattr(self, var_label, {label: var for label in self.labels})
            elif isinstance(var, dict):
                if set(var.keys()) != set(self.labels):
                    raise ValueError("The keys of <{}> do not match those of"
                                     " <dict_samples>".format(var_label))
                for value in var.values():
                    if not (isinstance(value, int) and value > 0):
                        raise ValueError("Values of <{}> must be a positive "
                                         "integer".format(var_label))
            else:
                raise ValueError("Values of <{}> must be a positive integer or a "
                                 "dictionary".format(var_label))

        # Get probability of a random sequence to be of a given class
        self.log_p_class = {}
        n_seqs_per_class = {label: len(data) for label, data in dict_samples.items()}
        for label in self.labels:
            self.log_p_class[label] = np.log10(n_seqs_per_class[label] /
                                               np.sum(list(n_seqs_per_class.values())))
        # Train models
        self.models = {}
        pb = ProgressBar(total_steps=len(list(dict_samples.items())), label="Training classifier")
        for label in self.labels:
            logging.debug('Training {}'.format(label))
            self.hmm_training = HiddenMarkovModelTraining(var_type=var_type,
                                                          data=dict_samples[label],
                                                          n_hidden_states=self.n_hidden_states[label],
                                                          model_type=model_type,
                                                          must_estimate_parameters=must_estimate_parameters,
                                                          estimation_method=estimation_method,
                                                          must_estimate_initial_means=must_estimate_initial_means,
                                                          target_gradient=target_gradient,
                                                          n_samples=n_samples,
                                                          n_models=self.n_models[label], seed=seed,
                                                          keep_samples=keep_samples,
                                                          *args, **kwargs)
            pb.step()
            self.models[label] = self.hmm_training._best_hmm
        del self.hmm_training
        pb.delete()

        # Store the user-provided model metadata, if any
        self.metadata = metadata

    def predict(self, sequences):
        """
        Given some sequences the method returns the log10 probability
        of each sequence of corresponding to each class

        Parameters
        ----------
        sequences : list of list/NumPy arrays/etc.
                    sequences of values

        Returns
        -------
        log_p_of_class_given_seq : dictionary
                                   probability of each sequence of being related to the class
                                   stored as key.
        """

        # Calculate probability of getting the sequence knowing the class to which it belongs
        log_p_of_seq_given_class = {}
        for label in self.labels:
            log_p_of_seq_given_class[label] = self.models[label].get_probability_of_sequences(sequences)
        # Calculate normalizing factor avoiding underflow
        p = np.array([log_p_of_seq_given_class[label] + self.log_p_class[label]
                      for label in self.labels])
        p_ref = np.max(p, axis=0)
        log_p_seq = p_ref + np.log10(np.sum(10 ** (p-p_ref), axis=0))
        # Apply Bayes' rule
        log_p_of_class_given_seq = {}
        for label in self.labels:
            log_p_of_class_given_seq[label] = (log_p_of_seq_given_class[label] +
                                               self.log_p_class[label] - log_p_seq)

        return log_p_of_class_given_seq


class HiddenMarkovModelTraining(MathlasObject):

    def __init__(self, var_type, data, n_hidden_states, model_type='mathlas hmm',
                 must_estimate_parameters=True, estimation_method="complete hmm",
                 target_gradient=None, must_estimate_initial_means=True, n_samples=1000,
                 n_models=0, seed=211104, store_all_models=True, keep_samples=True,
                 metadata=None, **kwargs):
        """
        Explores several HMM solutions and returns the one that provides a higher value
        for the log-likelihood of the observed sequences

        Parameters
        ----------
        var_type : string or None
                   Datatype of the sequences. Allowed values:

                       * ``None``
                       * ``continuous``
                       * ``categorical``
        data : iterable of ordered iterables
               List of observed sequences of values.
        n_hidden_states : integer/list/NumPy array
                          Number of hidden states or, if ``must_estimate_parameters`` is ``True``,
                          the values of the number of hidden states that must be explored.
        model_type : string, optional
                     HMM model type to create. Allowed values:

                         * ``mathlas hmm`` (default). Use homegrown HMM code

                     Defaults to ``mathlas hmm``
        must_estimate_parameters : Boolean, optional
                                   Flag indicating whether the number of hidden states and the
                                   initial parameters should be estimated.

                                   Defaults to ``True``.
        estimation_method : string, optional
                            only used if ``must_estimate_parameters`` is ``True``. How the initial
                            values of the parameters should be estimated.
                            Allowed values:

                                * ``k-means``
                                * ``estimation hmm``
                                * ``complete hmm``
                                * ``silhouette``
        target_gradient : float or None, optional
                          Value of the gradient of the figure of merit at which we aim. Can be
                          seen as the learning rate. Can be given a value of ``None`` for automatic
                          control.

                          Only considered if ``must_estimate_parameters`` is ``True``.

        must_estimate_initial_means : Boolean, optional
                                      If ``True`` the initial values of the means will be estimated,
                                      otherwise they will be chosen at random.

                                      Only observed if ``must_estimate_paramters`` is ``True``.
        n_samples : integer, optional
                    Number of elements used to estimate the number of hidden states.

                    Only used if ``must_estimate_parameters`` is ``True``.

                    Defaults to ``1000``.
        n_models : integer, optional
                   number of different models that would be tested for each class. Several models
                   are trained to avoid local minima when looking for the highest likelihood. If
                   it is an integer, the same value would be used for all classes; if it is a
                   dictionary a different value can be provided for each class.

                   Defaults to 0
        seed : integer, optional
               Seed used to generate random numbers.

               Defaults to 211104.
        store_all_models : Boolean, optional
                           Flag indicating whether all calculated models (and not only optimal ones)
                           should be stored for further use.

                           Defaults to True
        keep_samples : boolean, optional
               Flag indicating whether training data should be stored in a
               ``observed_samples`` attribute or it should be discarded after training.

               Defaults to ``True`` (keeping the samples).
        metadata : anything, optional
                   Extra model metadata that will be stored with the model.
                   The metadata is completely optional and will not be used for any purpose
                   by the trained model.
        kwargs : dictionary
                 Extra parameters to pass to the HiddenMarkovModel model generator.
        """
        # Check if we were given a valid model type
        if model_type not in ('mathlas hmm',):
            raise ValueError('{} is not a valid '.format(model_type) +
                             'HiddenMarkovModelTraining model type')

        self.model_type = model_type
        # Initialize variables
        self.var_type = var_type
        self.observed_samples = data
        self.n_hidden_states = n_hidden_states
        self.must_estimate_parameters = must_estimate_parameters
        self.store_all_models = store_all_models
        self.n_models = n_models
        self.seed = seed
        if must_estimate_parameters:
            self.n_samples = n_samples
            self.estimation_method = estimation_method
            self.target_gradient = target_gradient
            if self.estimation_method not in ("k-means", "estimation hmm",
                                              "complete hmm", "silhouette"):
                raise ValueError('<estimation_method> can only take values "k-means", '
                                 '"estimation hmm", "complete hmm " or "silhouette"')
            self.must_estimate_initial_means = must_estimate_initial_means
        if store_all_models:
            self.hmms = {}

        if must_estimate_parameters:
            if var_type != "continuous":
                raise ValueError("Parameters can only be estimated for continuous variables")
            if not isinstance(n_hidden_states, (tuple, list, np.ndarray)):
                raise ValueError("If parameters must be estimated <n_hidden_states> must be a "
                                 "tuple/list/NumPy array")
            # Do initial estimation of the parameters
            out = self._estimate_parameters()
            n_clusters, initial_hidden_state, transition_matrix, mu, covariance_matrix = out
            # Create HMM model
            if self.model_type == 'mathlas hmm':
                # Train a HiddenMarkovModel
                best_hmm = HiddenMarkovModel(mode="train", var_type=var_type,
                                             observed_samples=self.observed_samples,
                                             n_hidden_states=n_clusters,
                                             initial_hidden_state=initial_hidden_state,
                                             transition_matrix=transition_matrix,
                                             emission_means=mu,
                                             emission_covariances=covariance_matrix,
                                             keep_samples=False)
                # Store the trained model's log-likelihood
                best_ll = best_hmm.log_likelihood
            else:
                raise HereBeDragonsException('You should not be here')

            # Store the model if asked to
            if store_all_models:
                self.hmms['estimated'] = best_hmm
        else:
            best_hmm = None
            best_ll = -np.inf
            n_clusters = n_hidden_states

        # Calculate models using random initial parameters
        for ii in range(n_models):
            if self.model_type == 'mathlas hmm':
                hmm = HiddenMarkovModel(mode='train', var_type=var_type, observed_samples=data,
                                        n_hidden_states=n_clusters, seed=self.seed + ii,
                                        keep_samples=False, **kwargs)

                if hmm.log_likelihood > best_ll:
                    best_ll = hmm.log_likelihood
                    best_hmm = hmm
            else:
                raise HereBeDragonsException('You should not be here')

            if store_all_models:
                self.hmms[seed + ii] = hmm
        self._best_hmm = best_hmm

        # Store the user-provided model metadata, if any
        self.metadata = metadata

        # Delete the training data if told to
        if not keep_samples:
            self.observed_samples = None

    def get_probability_of_sequences(self, sequences):
        """
        Provides log10(probability) of the sequences for the best stored model

        Parameters
        ----------
        sequences : list
                    list of sequences of observed values

        Returns
        -------
        out : NumPy array
              values of the log10-likelihood of each sequence in ``sequences``
        """

        if self.model_type == 'mathlas hmm':
            return self._best_hmm.get_probability_of_sequences(sequences)
        else:
            raise HereBeDragonsException('You should not be here')

    def get_most_probable_hidden_states(self, sequences):
        """
        Calculates the most probable sequence of hidden states given a sequence of observed states
         using the Viterbi algorithm.

        Parameters
        ----------
        sequences : list of lists
                    all the sequences whose most probable hidden states the user wants to calculate.

        Returns
        -------
        hidden_states : list of lists
                        list of most probable sequences of hidden states.

        """
        if self.model_type == 'mathlas hmm':
            return self._best_hmm.get_most_probable_hidden_states(sequences)
        else:
            raise HereBeDragonsException('You should not be here')

    def _estimate_parameters(self, perform_sanity_check=False):
        """
        Estimates the values of the parameters of a Hidden Markv Model using k-means and the
        Bayesina Information Content score. Several levels of sophistication can be achieved
        depending on the value of the attribute <estimation_method>. It can take values
        "k-means", "estimation hmm", "complete hmm", or "silhouette" depending on what you want
        to estimate: only emission parameters with "k-means" or both emission and transition for
        "estimation hmm" and "complete hmm" (depending on if final values are refined or not
        using the optimization algorithm of HMMs).
        
        Parameters
        ----------
        perform_sanity_check : Boolean
                               Should dimensions of the data samples be checked? 

        Returns
        -------
        optimum_n_hidden_states : integer
                                  number of hidden states for which the results are optimum
        initial_hidden_state : NumPy array of 1-by-K
                               Probabilities of each latent state to be the initial state.
        transition_matrix : Numpy array of K-by-K
                            Each entry, a_ij, of the matrix is the probability of going from the
                            j-th state to the i-th state in a given transition,
                            i.e., p(x_{t}=i|x_{t-1}=j).
        emission_means : ordered iterable of K N-by-1 NumPy arrays
                         Values of the means of the Gaussian emissions for each latent state.
        emission_covariances : ordered iterable of K N-by-N NumPy arrays
                               Values of the covariance matrices of the Gaussian emissions for each
                               latent state.
        """
        
        def skip_case(bic_values, parameters, current_estimator, reasons):
            """
            Method to skip current case (for a given #clusters)

            Parameters
            ----------
            bic_values : dict of lists
                         keys are the estimation methods and values are the BIC values for each
                          #clusters.
            parameters : list of tuples
                         estimated parameters for each #clusters
            current_estimator : string
                                label of the current estimation method
            reasons : string
                      why has the current cas must be skipped

            Returns
            -------
            bic_values, parameters : modified inputs
            """
            
            estimation_methods = ["k-means", "silhouette", "estimation hmm", "complete hmm"]

            logging.debug('Training with {} hidden states '.format(n_clusters) +
                          'skipped because {}'.format(reasons))
            index = estimation_methods.index(current_estimator)
            for em in estimation_methods[index:]:
                bic_values[em].append(np.nan)
            parameters.append(None)

            return bic_values, parameters

        # Initialize parameters
        dim_emissions = len(self.observed_samples[0][0])
        if perform_sanity_check:
            for sequence in self.observed_samples:
                for point in sequence:
                    if len(point) != dim_emissions:
                        raise ValueError("Not all points are of the same dimension")

        # Group all data in a single sequence where order would not matter
        if isinstance(self.observed_samples[0], list):
            data = []
            for sequence in self.observed_samples:
                data += sequence
            data = np.array(data)
        else:
            data = np.concatenate(self.observed_samples)
        n_data = data.shape[0]

        # Calculate several models and return that with maximum BIC
        parameters = []
        bic_values = {"k-means": [], "estimation hmm": [], "complete hmm": [], "silhouette": []}
        for n_clusters in self.n_hidden_states:
            mu, cluster, distances, have_clusters_merged = self._k_means(data, n_clusters)
            # Only continue if clusters have not merged
            if have_clusters_merged:
                bic_values, parameters = skip_case(bic_values, parameters, "k-means",
                                                   "clusters have merged")
                continue
            # Estimation of emission parameters and calculation of K-means BIC
            pi, covariance_matrix, bic = self._estimate_emission_parameters(data, mu, n_clusters,
                                                                            cluster)
            # Continue only if K-means BIC is a float
            if np.isnan(bic) or bic in [np.inf, -np.inf]:
                bic_values, parameters = skip_case(bic_values, parameters, "k-means",
                                                   "k-means BIC is not a float")
                continue

            # Store data and stop if user asked for k-means or silhoutte
            bic_values["k-means"].append(bic)
            if self.estimation_method == "k-means":
                parameters.append((pi, mu, covariance_matrix))
                continue
            elif self.estimation_method == "silhouette":
                parameters.append((pi, mu, covariance_matrix))
                n_samples = distances.shape[0]
                silhouette = self._silhouette(cluster[:n_samples], distances)
                bic_values["silhouette"].append(silhouette)
                continue

            # Try to estimate the transition parameters
            try:
                out = self._estimate_transition_parameters(data, pi, mu, covariance_matrix,
                                                           n_clusters)
                initial_states, transition_matrix, bic = out
                if np.isnan(bic) or bic in [np.inf, -np.inf]:
                    bic_values, parameters = skip_case(bic_values, parameters, "estimation hmm",
                                                       "estimation-hmm BIC is not a float")
                    continue
            except (ValueError, RuntimeError):
                bic_values, parameters = skip_case(bic_values, parameters, "estimation hmm",
                                                   "something failed while estimating transition"
                                                   " parameters")
                continue

            # If user choose estimation-hmm store data and continue
            bic_values["estimation hmm"].append(bic)
            if self.estimation_method == "estimation hmm":
                parameters.append((pi, mu, covariance_matrix, initial_states, transition_matrix))
                continue

            # Training might diverge, but we're ok with that as long as we have further options
            try:
                hmm_train = HiddenMarkovModel(mode="train", var_type="continuous",
                                              observed_samples=self.observed_samples,
                                              n_hidden_states=n_clusters,
                                              initial_hidden_state=initial_states,
                                              transition_matrix=transition_matrix,
                                              emission_means=mu,
                                              emission_covariances=covariance_matrix)
            except (ValueError, RuntimeError):
                bic_values, parameters = skip_case(bic_values, parameters, "complete hmm",
                                                   "something failed while training complete hmm")
                continue

            n_params = n_clusters * (dim_emissions + dim_emissions * (dim_emissions + 1) / 2 +
                                     1 + (n_clusters + 1) / 2)
            factor = 0.5 * n_params * np.log10(n_data)
            bic = hmm_train.log_likelihood - factor
            bic_values["complete hmm"].append(bic)
            # The thing with the emission means (mu) is because we want a copy
            # of the data, not a reference to mu (which changes)
            parameters.append((pi, [mu_i.copy() for mu_i in mu], covariance_matrix,
                               initial_states, transition_matrix))

        # Handle the case where we have no models
        if all([p is None for p in parameters]):
            raise RuntimeError('Could not choose the best model since no model could be trained')

        # Get model with maximum BIC-likelihood
        if self.target_gradient is None:
            # Get maximum value
            bic_values[self.estimation_method] = [-np.inf if np.isnan(v) else v
                                                  for v in bic_values[self.estimation_method]]
            index = np.argmax(bic_values[self.estimation_method])
        else:
            # Get value of the gradient which is positive and nearest to the targeted value
            ii, jj = 0, 1

            values = np.array(bic_values[self.estimation_method])
            if len(values[values != - np.inf]) == 1:
                values[values == -np.inf] = np.inf
                values[np.isnan(values)] = np.inf
                index = np.argmin(values)
            else:
                results = []
                while jj < len(self.n_hidden_states):
                    if values[jj] != -np.inf:
                        g = (values[jj] - values[ii]) / (self.n_hidden_states[jj] - self.n_hidden_states[ii])
                        results.append((ii, np.abs(np.max([g, 0.]) - self.target_gradient)))
                        ii = jj
                    jj += 1
                index = min(results, key=lambda x: x[1])[0]

        # Get optimal parameters
        optimum_n_hidden_states = self.n_hidden_states[index]
        if self.estimation_method in ["k-means", "silhouette"]:
            pi, emission_means, emission_covariances = parameters[index]
            out = self._estimate_transition_parameters(data, pi, emission_means,
                                                       emission_covariances,
                                                       optimum_n_hidden_states)
            initial_states, transition_matrix, bic = out
        else:
            _, emission_means, emission_covariances, initial_states, transition_matrix = parameters[index]

        return (optimum_n_hidden_states, initial_states, transition_matrix,
                emission_means, emission_covariances)

    def _k_means(self, data, n_clusters):
        """
        Clustroid-based K-means in which initial values can be estimated at random or via 
        furthest-points method. If, at some point, several clusters merge the iteration is 
        stopped and the result is returned with a raised warning flag.

        Parameters
        ----------
        data : list of NumPy arrays
               list of values to be clustered
        n_clusters : integer
                     number of clusters to be created

        Returns
        -------
        mu : list of NumPy arrays
             values of the clustroids of each cluster
        cluster : list of integers
                  list as long as the number of data that stores the number of cluster at which 
                  each point is assigned
        have_clusters_merged : Boolean
        """
        # Initialize some variables
        n_data = data.shape[0]
        np.random.seed(self.seed)
        np.random.shuffle(data)
        n_samples = np.min([self.n_samples, n_data])
        samples = data[:n_samples, :]
        if not self.must_estimate_initial_means:
            # Calculate initial means at random
            indices = np.random.choice(range(n_data), size=n_clusters, replace=False)
            distances = None
        else:
            # Calculate initial means as the furthest points
            distances = np.zeros((n_samples, n_samples))
            for i1, i2 in combinations(range(n_samples), 2):
                distances[i1, i2] = np.linalg.norm(samples[i1] - samples[i2])
                distances[i2, i1] = distances[i1, i2]

            indices = list(np.unravel_index(distances.argmax(), distances.shape))
            while len(indices) < n_clusters:
                d = 0.
                for ii in indices:
                    d += distances[ii, :]

                # The new clustroid cannot have been already selected
                index = d.argmax()
                while index in indices:
                    d[index] = -1
                    index = d.argmax()
                indices.append(index)
        mu = [samples[index] for index in indices]

        # k-means
        have_clusters_merged = False
        while True:
            # Expectation
            distances2clustroid = np.zeros((n_data, n_clusters))
            for index, point in enumerate(data):
                for k, m in enumerate(mu):
                    distances2clustroid[index, k] = np.linalg.norm(point - m)
            cluster = np.argmin(distances2clustroid, axis=1)
            # Maximization
            if len(Counter(cluster)) != n_clusters:
                have_clusters_merged = True
                break
            mu1 = []
            for c in range(n_clusters):
                indices = np.array(range(n_data))[cluster == c]
                mu1.append(np.sum([data[index] for index in indices], axis=0) / len(indices))
                distances2clustroid = np.zeros((n_data, n_clusters))
            for index, point in enumerate(data):
                for k, m in enumerate(mu1):
                    distances2clustroid[index, k] = np.linalg.norm(point - m)
            indices = np.argmin(distances2clustroid, axis=0)
            mu2 = [data[index] for index in indices]
            # Calculate error
            err = 0.
            for m0, m2 in zip(mu, mu2):
                err = max(err, np.linalg.norm(m0 - m2, np.inf))
            if err > 1e-3:
                mu = mu2
            else:
                break

        return mu, cluster, distances, have_clusters_merged

    @staticmethod
    def _estimate_emission_parameters(data, mu, n_clusters, cluster):
        """
        Estimate values of the emission parameters

        Parameters
        ----------
        data : list of NumPy arrays
               list of values to be clustered
        mu : list of NumPy arrays
             values of the clustroids of each cluster
        n_clusters : integer
                     number of clusters to be created
        cluster : list of integers
                  list as long as the number of data that stores the number of cluster at which 
                  each point is assigned

        Returns
        -------
        pi : list of floats
             values of the importance of each Gaussian
        covariance_matrix : list of NumPy arrays
                            covariances of the Gaussian models
        bic : float
              Bayesian Information Content of the model
        """
        # Initialize data
        n_data, dim_emissions = data.shape
        pi = np.zeros(n_clusters)
        covariance_matrix = [1e-3 * np.eye(dim_emissions, dim_emissions)
                             for _ in range(n_clusters)]
        for c, point in zip(cluster, data):
            pi[c] += 1.
            xi = (point - mu[c]).reshape((dim_emissions, 1))
            covariance_matrix[c] += xi.T * xi
        covariance_matrix = [s / n for s, n in zip(covariance_matrix, pi)]
        pi /= n_data

        for ii, c in enumerate(covariance_matrix):
            spectrum, _ = np.linalg.eigh(c)
            t = spectrum.dtype.char.lower()
            factor = {'f': 1E3, 'd': 1E6}
            cond = factor[t] * np.finfo(t).eps
            min_eig = spectrum[0]
            max_eig = spectrum[-1]
            if cond * max_eig > min_eig:
                factor = (spectrum[-1] * cond - spectrum[0]) * 1.5
                covariance_matrix[ii] = c + factor * np.eye(*c.shape)

        # Calculate value of the BIC-modified likelihood
        pdfs = [multivariate_normal(mean=m, cov=c).pdf for m, c in zip(mu, covariance_matrix)]
        probabilities = np.array([pdf(data) for pdf in pdfs])
        n_params = n_clusters * (dim_emissions + dim_emissions * (dim_emissions + 1) / 2)
        factor = 0.5 * n_params * np.log10(n_data)
        bic = np.sum(np.log10(pi.dot(probabilities + 1e-16))) - factor

        return pi, covariance_matrix, bic

    def _estimate_transition_parameters(self, data, pi, mu, covariance_matrix, n_clusters):
        """
        Estimate values of the transition parameters

        Parameters
        ----------
        data : list of NumPy arrays
               list of values to be clustered
        pi : list of floats
             values of the importance of each Gaussian
        mu : list of NumPy arrays
             values of the clustroids of each cluster
        covariance_matrix : list of NumPy arrays
                            covariances of the Gaussian models
        n_clusters : integer
                     number of clusters to be created

        Returns
        -------
        initial_states : NumPy array
                         Probability of the HMM of being at each state at the very beginning 
                         of the sequence
        transition_matrix : NumPy array
                            Probability of performing a transition from one state to another
        bic : float
              Bayesian Information Content of the model
        """

        # Initialize data
        n_data, dim_emissions = data.shape

        # Estimate transition matrix
        pdfs = [multivariate_normal(mean=m, cov=c).pdf for m, c in zip(mu, covariance_matrix)]
        initial_states = np.zeros((n_clusters,))
        transition_matrix = np.zeros((n_clusters, n_clusters))
        for sequence in self.observed_samples:
            probabilities = np.array([pi[index] * pdf(sequence) for index, pdf in enumerate(pdfs)])
            likelihood = np.sum(np.array(probabilities), axis=0)
            # We check if any of the probability sums add to 0
            # That should not happen, so we raise an exception
            if (likelihood == 0.).any():
                raise RuntimeError('Some probabilities add to 0, which should not happen')
            responsibilities = [p / likelihood for p in probabilities]
            initial_states += np.array([r[0] for r in responsibilities])
            for ii in range(len(sequence) - 1):
                u = [r[ii] for r in responsibilities]
                v = [r[ii + 1] for r in responsibilities]
                transition_matrix += np.outer(v, u)
        initial_states /= np.sum(initial_states)
        for ii in range(n_clusters):
            sum_of_columns = np.sum(transition_matrix[:, ii])
            if sum_of_columns == 0.0:
                transition_matrix[:, ii] = 1. / n_clusters
            else:
                transition_matrix[:, ii] /= sum_of_columns

        # Calculate value of the BIC-modified likelihood
        n_params = n_clusters * (dim_emissions + dim_emissions * (dim_emissions + 1) / 2 +
                                 1 + (n_clusters + 1) / 2)
        factor = 0.5 * n_params * np.log10(n_data)
        if self.model_type == 'mathlas hmm':
            hmm_gen = HiddenMarkovModel(mode="generate", var_type="continuous", seed=211104,
                                        n_sequences=0, length_of_sequences=[],
                                        initial_hidden_state=initial_states,
                                        transition_matrix=transition_matrix,
                                        emission_means=mu, emission_covariances=covariance_matrix)
            bic = np.sum(hmm_gen.get_probability_of_sequences(self.observed_samples) + 1e-16) - factor
        else:
            raise HereBeDragonsException('You should not be here')

        return initial_states, transition_matrix, bic

    @staticmethod
    def _silhouette(cluster, D):
        """
        Evaluate the Silhouette index of a partition.

        Evaluate the Silhouette index of a certain partition into clusters
        and provide its average value.

        Parameters
        ----------
        cluster : list
                  cluster number assigned to each point

        D : NumPy array
            NumPy array of distances. Element `i`-`j` must contain the distance
            from item `i` to `j`.

        Returns
        -------
        out : float
              silhouette index of the partition.
        """

        # Create list of the indexes of those elements in the same cluster
        clusters = [[i for i, x in enumerate(cluster) if x == elem] for elem in set(cluster)]

        # For just one cluster you do not need these method
        if len(clusters) == 1:
            return 0.

        # Evaluate Silhouette indices
        S = []
        avs = 0.
        for ic, c in enumerate(clusters):
            s = []
            for elem in c:
                # One-point cluster? silhouette index is not defined so we
                # assign a zero value
                if len(c) == 1:
                    s0 = 0.
                else:
                    # Average distance to the points of the same cluster
                    a = np.sum(D[elem, c]) / (len(c) - 1.)
                    # Average distance to the points of the nearest cluster
                    b = 1e300
                    for jc, c2 in enumerate(clusters):
                        if jc != ic:
                            b = min(b, np.sum(D[elem, c2]) / len(c2))
                    # silhouette index
                    s0 = (b - a) / max(a, b)
                s.append(s0)
                # Aggregate values
                avs += s[-1]
            S.append(s)
        nPoints = sum([len(s) for s in S])
        avs /= float(nPoints)

        return avs

    @classmethod
    def from_cache(cls, path):
        obj = super().from_cache(path)

        return obj


class TrainedHiddenMarkovClassifier(MathlasObject):
    def __init__(self, models, log_p_class, list_of_vars,
                 transition_matrix=None, p_process=None,
                 et=None, em=None, metadata=None):
        """
        A classifier which, given trained HiddenMarkovModelTraining objects,
        can be used to classify new sequences.

        This class can perform raw HMM-based predictions or -if given values for
        ``transition_matrix``, ``et`` and ``em``- mixed model predictions.

        You can switch between pure-HMM based predictions and mixed-model predictions
        by manually setting the ``transition_matrix``, ``p_process`` ``et`` &
        ``em`` attributes on the instantiated object.

        Parameters
        ----------
        models : dict of HiddenMarkovModelTraining objects
                 A dict containing the trained models, whose keys are the names
                 of the different classes and whose values are the model
                 objects.
        log_p_class : dict of floats
                      log10(probability) of a random sequence being of a given class.
        list_of_vars : dict of strings
                       A dict containing the names of the variables to use in each
                       for evaluating each of the models.
                       The keys are the names of the different classes and the values
                       must be the names of the variables to be used when evaluating
                       each of the models.
        transition_matrix : pandas DataFrame or None, optional
        p_process : dictionary of floats, optional
        et : float or None, optional
        em : float or None, optional
        metadata : anything, optional
           Extra model metadata that will be stored with the model.
           The metadata is completely optional and will not be used for any purpose
           by the trained model.
        """
        super().__init__()

        # Make sure that all the objects we've been given are HMM models
        try:
            assert all([isinstance(i, HiddenMarkovModelTraining) for i in models.values()])
        except AssertionError:
            raise ValueError('Values in `models` must be of type HiddenMarkovModelTraining')

        # Checks for the case where a mixed model (see final report) and not pure-HMM should be used
        if any((transition_matrix is not None,
                p_process is not None,
                et is not None, em is not None)):
            # If given any of transition_matrix, et or em,
            # make sure that we've been given all of them
            if not all((transition_matrix is not None,
                        p_process is not None,
                        et is not None, em is not None)):
                raise ValueError('If a value is given for either `transition_matrix`, `p_process`'
                                 '`et` or `em`, all of them must be given')

        # Create some internal attributes we'll be using
        self.__transition_matrix = None
        self.__transition_dataframe = None
        self.__p_p = None
        self.__tm = None
        self.__et = None
        self.__warned = False

        # Store the user-provided parameters
        self.labels = sorted(models.keys())
        self.models = models
        self.log_p_class = log_p_class
        self.list_of_vars = list_of_vars
        self.transition_matrix = transition_matrix
        self.p_process = p_process
        self.et = et
        self.em = em
        self.metadata = metadata

    def predict(self, sequences, previous_log_p_class_given_seq=None):
        """
        Given some sequences the method returns the log10 probability
        of each sequence of corresponding to each class.

        If performing a mixed-model prediction the sequences must be provided in order.

        Parameters
        ----------
        sequences : list of Pandas DataFrames.
                    sequences of values to be used to evaluate the internal HMM models.

                    The sequences must be pandas DataFrames and the columns must contain all
                    the variables in ``list_of_vars`` when creating the
                    ``TrainedHiddenMarkovClassifier`` object.
        previous_log_p_class_given_seq : dictionary or ``None``, optional
                                         Dictionary of log10(p(process|seq)), where the keys
                                         correspond to the different process names and the values
                                         should be floats for the prediction performed for the last
                                         sequence preceding the first one in ``sequences``.

                                         If ``None`` a pure HMM-based prediction
                                         will be performed for the first sequence.

                                         This parameter should only be provided in case that the
                                         transition parameters for the mixed model have been set.

                                         Defaults to ``None``.

        Returns
        -------
        log_p_of_class_given_seq : dictionary of floats
                                   probability of each sequence of being related to the class
                                   stored as key.
        """
        # Parameter check
        if previous_log_p_class_given_seq is not None:
            if self.__tm is None or self.em is None or self.__p_p is None:
                raise ValueError('`previous_log_p_class_given_seq` given, but mixed-model '
                                 'parameters are not available, so a mixed-model prediction '
                                 'is not possible and providing `previous_log_p_class_given_seq` '
                                 'makes no sense.')

            if previous_log_p_class_given_seq.keys() != set(self.labels):
                raise ValueError('"{}" is not a known label'.format(previous_log_p_class_given_seq))

        # Check sequences length
        if len(sequences) < 1:
            raise ValueError('`sequences` must contain some valid sequences')

        # Perform the pure HMM-based prediction
        log_p_of_class_given_seq_hmm = self.__predict_hmm(sequences=sequences)

        # If either self.__tm or self.em are None, we provide the HMM prediction directly
        if self.__tm is None or self.em is None or self.__p_p is None:
            if((self.__tm is not None or self.em is not None or self.__p_p is not None) and
                   not self.__warned):
                msg = 'Warning: Some but not all mixed-model params are available, ' \
                      'performing a pure HMM-based prediction'
                logging.warning(msg)
            return log_p_of_class_given_seq_hmm

        # Mixed model prediction
        probabilities = [log_p_of_class_given_seq_hmm[label] for label in self.labels]
        log_p_hmm = list(zip(*probabilities))
        if previous_log_p_class_given_seq is None:
            p_m = np.array([10 ** v for v in log_p_hmm[0]])
            p_of_class_given_seq = [p_m]
        else:
            # Compute the mixed probabilities based on the given previous probabilities
            p0 = [10**previous_log_p_class_given_seq[label] for label in self.labels]
            p_t0 = self.__tm.dot(p0)
            p_m0 = np.array([10 ** v for v in log_p_hmm[0]])
            p_change = [(u ** self.em) * (v ** (1-self.em)) / w for u, v, w in zip(p_t0, p_m0,
                                                                                   self.__p_p)]
            prev_state = np.argmax(p0)
            p_equal = p_m0[prev_state]
            p_change[prev_state] = 0.
            f = np.sum(p_change) / np.max([1 - p_equal, 1e-100])
            p_total = [v / f for v in p_change]
            p_total[prev_state] = p_equal
            p_of_class_given_seq = [np.array(p_total)]

        for ii in range(len(sequences) - 1):
            p_t = self.__tm.dot(p_of_class_given_seq[ii])
            p_m = np.array([10 ** v for v in log_p_hmm[ii + 1]])
            p_change = [(u ** self.em) * (v ** (1-self.em)) / w for u, v, w in zip(p_t, p_m,
                                                                                   self.__p_p)]
            prev_state = p_of_class_given_seq[-1].argmax()
            p_equal = p_m[prev_state]
            p_change[prev_state] = 0.
            f = np.sum(p_change) / np.max([1 - p_equal,  1e-100])
            p_total = [v / f for v in p_change]
            p_total[prev_state] = p_equal
            p_of_class_given_seq.append(np.array(p_total))

        # We return a dictionary of scalars if given one seq, and a dictionary of numpy arrays
        # if given more than one seq
        if len(sequences) == 1:
            log_p_of_class_given_seq = {p: l for p, l in
                                        zip(self.labels, np.log10(p_of_class_given_seq[0]))}
        else:
            log_p_of_class_given_seq = {p: l for p, l in
                                        zip(self.labels, np.log10(p_of_class_given_seq).T)}

        return log_p_of_class_given_seq

    @property
    def transition_matrix(self):
        return self.__transition_dataframe

    @transition_matrix.setter
    def transition_matrix(self, x):
        # Process the given value
        if x is None:
            self.__transition_dataframe = None
            self.__transition_matrix = None
        else:
            # Check that the given transition matrix is a DataFrame
            if not isinstance(x, pd.DataFrame):
                raise TypeError('`transition_matrix` must be a pandas DataFrame')

            # Check that the labels in the transition matrix match the keys in <models>
            if (set(x.columns) != self.models.keys() or x.shape[0] != len(self.models) or
                        set(x.index) != self.models.keys() or x.shape[1] != len(self.models)):
                raise ValueError('`transition_matrix` does not seem to match the list of models')

            # We store the DataFrame and some derived values, but it might get reordered
            self.__transition_dataframe = x.loc[self.labels, self.labels].copy()
            self.__transition_matrix = self.__transition_dataframe.values

        self.__update_tm()

    @property
    def et(self):
        return self.__et

    @et.setter
    def et(self, x):
        # Set __et and update the value for self.__tm
        self.__et = x
        self.__update_tm()

    @property
    def p_process(self):
        if self.__p_p is None:
            return None
        else:
            return {self.labels[i]: self.__p_p[i] for i in range(len(self.labels))}

    @p_process.setter
    def p_process(self, x):
        if x is None:
            self.__p_p = None
        else:
            # Check whether p_process is a dict
            if not isinstance(x, dict):
                raise TypeError('`p_process` must be a dictionary of floats')
            if x.keys() != self.models.keys():
                raise ValueError('`p_process` does not seem to match the list of processes')
            self.__p_p = [x[label] for label in self.labels]

    def __update_tm(self):
        """
        Update the value of tm. Called whenever the transition matrix or et change
        """
        if self.__et is None or self.__transition_matrix is None:
            self.__tm = None
            return

        # Generate transition matrix as:
        #   Normalized transition matrix * (1-f) + Uniform transition matrix * f
        # Where:
        #   f = 1/(transition matrix)^et
        s = 1 / self.__transition_matrix.sum(axis=0)
        self.__tm = ((self.__transition_matrix * s) * (1 - s ** self.__et) +
                     np.ones((len(self.labels),)) / (len(self.labels) - 1.) * (s ** self.__et))
        np.fill_diagonal(self.__tm, 0)

    def __predict_hmm(self, sequences):
        # Calculate probability of getting the sequence knowing the class to which it belongs
        log_p_of_seq_given_class = {}
        sequences_cache = {}
        for label in self.labels:
            # I have to do this weird thing with tuples since lists cannot be
            # used as keys in dictionaries and tuples don't work as DataFrame
            # keys in the way on would expect them to
            key = tuple(self.list_of_vars[label])
            try:
                seqs = sequences_cache[key]
            except KeyError:
                seqs = [seq[self.list_of_vars[label]] for seq in sequences]
                sequences_cache[key] = seqs

            log_p_of_seq_given_class[label] = self.models[label].get_probability_of_sequences(seqs)

        # Calculate normalizing factor avoiding underflow
        p = np.array([log_p_of_seq_given_class[label] + self.log_p_class[label]
                      for label in self.labels])
        p_ref = np.max(p, axis=0)
        log_p_seq = p_ref + np.log10(np.sum(10 ** (p-p_ref), axis=0))
        # Apply Bayes' rule
        log_p_of_class_given_seq = {}
        for label in self.labels:
            log_p_of_class_given_seq[label] = (log_p_of_seq_given_class[label] +
                                               self.log_p_class[label] - log_p_seq)

        return log_p_of_class_given_seq
