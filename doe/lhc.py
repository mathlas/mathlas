# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from pandas import DataFrame, Series
from random import shuffle, sample, seed
from itertools import combinations, product, permutations
from mathlas.doe._lhc import __evaluate_distances, __update_distances


def LHC(N, D=None, fixedPoints=None, maximization_criteria="harmonic mean",
        random_seed=211104, nSeeds=1000, nIters=100000):
    """
    Distribute points in a N-space with Latin-hypercubes

    This functions distributes a set of N points in a D-space by applying a
    Latin Hypercubes distribution and performing a greedy optimization on their
    locations.
    The user can -optionally- provide a list of fixed points whose positions
    will not be modified that will be part of the solution.

    Parameters
    ----------
    N : integer
        Number of points to generate. If `fixedPoints` is given, the points
        there don't count for the total number of points.
        In other words: if the number of fixed points is `Nf`, the total number
        of points returned by this function is `N+Nf`.

    D : integer, optional
        Dimension of the space where the points are given.

        If D is not given, the dimension of the space will be obtained from the
        number of parameters per point in fixedPoints (which must be given in
        that case).

    fixedPoints : NumPy array, pandas DataFrame or list of lists, optional
        Points that will be included in the output in the given coordinates.

        If D is not given, fixedPoints must be given.

    maximization_criteria : string, optional
                            If it takes the (default) value "harmonic mean" the
                            the harmonic mean of the squared distances is used.
                            If "minimum" the minimum distance is evaluated.

    random_seed : integer or None, optional
                  seed used by the random number generator. If None is provided
                  Python generates a seed (case cannot be replicated).

                  The default seed is ``211104``.

    nSeeds : integer, optional
             The whole optimization process will be repeated `nSeeds` times
             for that many different initial seeds.

    nIters : integer, optional
             The maximum number of times that each point seed distribution
             will be modified in order to find a better alternative.


    Returns
    -------
    out : NumPy array or pandas DataFrame
          If `fixedPoints` is given and it is a DataFrame, the output will be
          a DataFrame whose columns are named after those is `fixedPoints`.
          The output will, otherwise, be a NumPY array.

          In either case, an object of size `(N+Nf)xD` with the coordinates of
          the selected points will be returned.

    Raises
    ------
    ValueError
        If the fixed points don't have `D` coordinates.
    Value Error
        If the variable <maximization_criteria> takes a value different from
        "harmonic mean" or "minimum".
    """

    # Random number generator is initialized if a seed is available
    if random_seed is not None:
        seed(random_seed)
    columns = None
    # Choose the objective function
    if maximization_criteria == "harmonic mean":
        evaluate_objective_function = __evaluate_harmonic_mean
    elif maximization_criteria == "minimum":
        evaluate_objective_function = __evaluate_minimum
    else:
        raise ValueError("The value of the variable <maximization_criteria> " +
                         "is not harmonic mean nor minimum")
    # Number of fixed points
    Nf = 0
    if fixedPoints is not None:
        # If fixedPoints is a DataFrame, store the column names and
        # convert to NumPy array
        if isinstance(fixedPoints, list):
            fixedPoints = np.array(fixedPoints)
        elif isinstance(fixedPoints, DataFrame):
            columns = fixedPoints.columns
            fixedPoints = fixedPoints.values
        elif isinstance(fixedPoints, Series):
            columns = fixedPoints.index
            n = fixedPoints.shape[0]
            fixedPoints = fixedPoints.values.reshape((1, n))

        Nf = fixedPoints.shape[0]

        if D is None:
            D = fixedPoints.shape[1]
        else:
            if fixedPoints.shape[1] != D:
                raise ValueError("{} doesn't have ".format(fixedPoints) +
                                 "dimension {}".format(D))

    # Check that we have D
    if D is None:
        raise ValueError('Dimension of the space not given')

    # Initialize global optimum storage variables
    outer_max_objective = -1e30
    outer_optimum_coords = None

    for _ in range(nSeeds):
        # Generate a random initial Latin Hypercubes distribution of N+Nf
        # points in a D-space. The last Nf rows correspond to the fixed points
        # Coords will hold the coordinates of one point per row
        coords = np.zeros((N+Nf, D))
        coords[-Nf:, :] = fixedPoints
        p = list(np.linspace(0, 1., N+Nf))
        for i in range(D):
            p_i = p[:]
            shuffle(p_i)
            if Nf > 0:
                for j in range(-Nf, 0):
                    closest = min(p_i, key=lambda x: abs(x-coords[j, i]))
                    p_i.pop(p_i.index(closest))
                coords[:-Nf, i] = p_i
            else:
                coords[:, i] = p_i

        # Optimize the distribution of the points by minimizing
        # the objective function
        D2 = __evaluate_distances(coords)
        inner_max_objective = evaluate_objective_function(D2, Nf)
        inner_optimum_coords = coords.copy()
        must_calculate_candidates = True
        for i in range(nIters):
            if must_calculate_candidates:
                # One of the nearest points is moved
                E2 = D2 + 1e9 * np.eye(D2.shape[0])
                E2[-Nf:, -Nf:] = 1e9
                l = np.unravel_index(np.argmin(E2), D2.shape)
                # Initialize list of candidates to swap
                unused_pairs = [(p1, p2, d) for p1, p2, d in
                                product(l, range(N), range(D))
                                if ((p2 not in l) and p1 < N)]

            # We get one out of the list of candidates.
            p1, p2, d = sample(unused_pairs, 1)[0]
            # Swap the coordinates of p1 and p2
            mod_coords = coords.copy()
            mod_coords[p1, d] = coords[p2, d]
            mod_coords[p2, d] = coords[p1, d]
            # Evaluate the objective function for the modified coordinates
            # and store those as a candidate solution, if they're better than
            # the current solution
            mod_D2 = __update_distances(mod_coords, D2, (int(p1), int(p2)))
            evaluation = evaluate_objective_function(mod_D2, Nf)
            if evaluation > inner_max_objective:
                inner_max_objective = evaluation
                inner_optimum_coords = mod_coords.copy()
                coords = mod_coords.copy()
                D2 = mod_D2.copy()
                must_calculate_candidates = True
            else:
                # This pair leads to worse results. Erase it and keep on
                # trying
                indx = unused_pairs.index((p1, p2, d))
                unused_pairs.pop(indx)
                must_calculate_candidates = False
            # If the list of candidates is empty stop because a local minimum
            # has been found
            if unused_pairs == []:
                break

        if inner_max_objective > outer_max_objective:
            outer_max_objective = inner_max_objective
            outer_optimum_coords = inner_optimum_coords.copy()

    if columns is not None:
        outer_optimum_coords = DataFrame(outer_optimum_coords, columns=columns)

    return outer_optimum_coords


def variation_of_lhc(x, reverse_feat, permute_feats):
    """
    This method creates a variation of a given LHC design by taking advantage of the existing
     symmetries to deliver a new design, as "optimum" (in the sense of minimum "distance",
     whichever its definition is) as the original.

     The difference with the :py:meth:`~mathlas.doe.lhc.variation_of_lhc_limited` method
     is that this method allows for fine-tuned control on how the variations are created.

    Parameters
    ----------
    x : NumPy array of Pandas DataFrame
        previously calculated LHC design
    reverse_feat : list
                   must the feat be reversed? This list must be as long as the dimension of the
                    design space.
    permute_feats : list
                    order of the variables after permutation. This list must be a permutation of
                     the list [0, 1, 2, ..., dimension of the design space]

    Returns
    -------
    new_x : NumPy array of Pandas DataFrame
            variation of the original LHC design

    """
    columns = indices = None
    if isinstance(x, DataFrame):
        columns = x.columns
        indices = x.index
        x = x.values

    n_feats = x.shape[1]

    if len(reverse_feat) != n_feats:
        raise ValueError("Variable <reverse_feat> must have as many elements as there are feats")
    if not np.all([isinstance(v, (bool, np.bool_)) for v in reverse_feat]):
        raise ValueError("Elements of <reverse_feat> must be Booleans")
    if len(permute_feats) != n_feats:
        raise ValueError("Variable <permute_feats> must have as many elements as there are feats")
    if ((not np.all([isinstance(v, (int, np.int_)) for v in permute_feats])) or
            (len(set(permute_feats)) != len(permute_feats)) or
            (sorted(permute_feats) != list(range(n_feats)))):
        raise ValueError("Variable <permute_feats> must a permutation of range(n_feats)")

    # Reverse variables if required
    y = x.copy()
    for index, must_reverse_feat in enumerate(reverse_feat):
        if must_reverse_feat:
            y[:, index] = 1. - y[:, index]

    # Permute axis
    y = y[:, permute_feats]

    if columns is not None or indices is not None:
        new_x = (DataFrame(y, columns=columns, index=indices))
    else:
        new_x = y

    return new_x


def variation_of_lhc_limited(X, limit=None):
    """
    This method creates a variation of a given LHC design by taking advantage of the existing
     symmetries to deliver a new design, as "optimum" (in the sense of minimum "distance",
     whichever its definition is) as the original.

     This method will compute and return up to `limit` variations of the LHC design in `X`
     without the user having fine-tuned control over how the variations are computed
     (see :py:meth:`~mathlas.doe.lhc.variation_of_lhc`)

    Parameters
    ----------
    X : NumPy array
        One LHC distribution, as returned by :py:meth:`~mathlas.doe.lhc.LHC`

    limit : integer, optional
            The maximum number of variations to return.
            Please be aware that this method might return less variations than
            `limit` if it cannot find enough variations.

            If `limit` is ``None``, all the possible variations will be
            returned.

    Returns
    -------
    variations : list of NumPy arrays
                 A list containing NumPy arrays -each of the same shape as `X`-
                 containing the point coordinates of the LHC variations.
    """

    # As in many other places, we separate the metadata from the values
    # in the DataFrame, so that we can treat the array as a normal NumPy
    # array. We'll convert back to DataFrame at output, if needed
    Xmodified = []
    columns = indices = None
    if isinstance(X, DataFrame):
        columns = X.columns
        indices = X.index
        X = X.values

    nParameters = X.shape[1]

    # All the possible variations of signs and order
    variations = list(product(permutations(range(nParameters)),
                              product(["+", "-"], repeat=nParameters)))

    if limit is None:
        limit = len(variations)
    elif limit < 1:
        raise ValueError('Limit is invalid')

    limit = min(limit, len(variations))

    for l, signs in sample(variations, limit):
        Y = X.copy()
        for indx, s in enumerate(signs):
            if s == "-":
                Y[:, indx] = 1. - Y[:, indx]

        # Swap columns
        Y = Y[:, l]

        # Don't yield duplicate configurations
        y = set([tuple(elem) for elem in Y])
        for x in Xmodified:
            if y == set([tuple(elem) for elem in x]):
                continue

        if columns is not None or indices is not None:
            Xmodified.append(DataFrame(Y, columns=columns, index=indices))
        else:
            Xmodified.append(Y)

    return Xmodified


def __evaluate_harmonic_mean(D2, Nf=0):
    """
    Evaluate the harmonic mean of the squared distances.

    Parameters
    ----------
    D2 : NumPy 2D array
         A NumPy 2D array with the squared distances matrix

    Nf : Integer
         Number of fixed points

    Returns
    -------
    f : float
        Harmonic mean of the squared distances
    """
    N = D2.shape[0] - Nf
    f = 0.
    for ii, jj in combinations(range(N), 2):
        f += 1. / D2[ii, jj]
    f = N * (N - 1.) / 2. / f

    return f


def __evaluate_minimum(D2, Nf=0):
    """
    Gets the minimum squared distance between any pair of points.

    Parameters
    ----------
    D2 : NumPy 2D array
         A NumPy 2D array with the squared distances matrix

    Nf : Integer
         Number of fixed points

    Returns
    -------
    f : float
        The minimum squared distance between any two points.
    """
    N = D2.shape[0]
    E2 = D2 + 1e30 * np.eye(N)
    E2[-Nf:, -Nf:] = 1e30
    return np.min(E2)


if __name__ == '__main__':
    from mathlas.plotting.mathlas_plots import MathlasPlot

    # Degenerated LHC distribution
    X = np.sort(LHC(10, D=2, nSeeds=1, nIters=1), axis=0)
    plt = MathlasPlot()
    plt.scatter(X[:, 0], X[:, 1], marker_size=100, color='mathlas orange',
                axis_labels=('$x_1$', '$x_2$'), title='Degenerated LHC')
    plt.show()
    # Unoptimized LHC distribution
    X = LHC(10, D=2, nSeeds=1, nIters=1)
    plt = MathlasPlot()
    plt.scatter(X[:, 0], X[:, 1], marker_size=100, color='mathlas orange',
                axis_labels=('$x_1$', '$x_2$'), title='Unoptimized LHC')
    plt.show()
    # Optimized LHC distribution
    X = LHC(10, D=2)
    plt = MathlasPlot()
    plt.scatter(X[:, 0], X[:, 1], marker_size=100, color='mathlas orange',
                axis_labels=('$x_1$', '$x_2$'), title='Optimized LHC')
    plt.show()
