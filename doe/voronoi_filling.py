# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
from copy import copy
from itertools import product
from scipy.spatial import Voronoi
from mathlas.plotting.mathlas_plots import MathlasPlot
# from mathlas.plotting.advanced_mathlas_plots import AdvancedMathlasPlot


def measure_voronoi_quality(X):
     
    eps = 1e-10
    N, D = X.shape
    
    # Modify points if required
    for ii, dd in product(range(N), range(D)):
        if X[ii, dd] <= 0.:
            X[ii, dd] = 1e-10
        elif X[ii, dd] >= 1.:
            X[ii, dd] = 1 - 1e-10
     
    # Extend domain
    listOfX = [X]
    for dd in range(D):
        # Mirror symmetry along x=0
        Xc = copy(X)
        Xc[:, dd] *= -1
        listOfX.append(Xc)
        # Mirror symmetry along x=1 
        Xc = copy(X)
        Xc[:, dd] -= 2
        Xc[:, dd] *= -1
        listOfX.append(Xc)
    # Concatenate all the lists
    Y = np.concatenate(listOfX)
    
    # Perform Voronoi tessellation   
    vor = Voronoi(Y)

    # Get indices of the vertices inside the unit box
    vertices_indices = []
    for indx, v in enumerate(vor.vertices):
        key = True
        for dd in range(D):
            key &= (-eps <= v[dd] <= (1. + eps))
        if key:
            vertices_indices.append(indx)

    # Evaluate maximum distances in each tile 
    dist = []
    dirs = np.zeros((N, D))
    for indx, list_of_vertices in enumerate(vor.regions):
        key = True
        for v in list_of_vertices:
            key &= v in vertices_indices
        if key and len(list_of_vertices) != 0:
            p_indx = list(vor.point_region).index(indx)
            c = vor.points[p_indx, :]
            mask = np.isclose(X, c)
            index_of_X = list(np.all(mask, 1)).index(True)
            maxD = -1.
            for v in list_of_vertices:
                p = vor.vertices[v]
                cp = p - c
                d = np.linalg.norm(cp)
                if d > maxD:
                    maxD = d
                    dir = cp
            dist.append(d)
            dirs[index_of_X, :] = dir
      
    # plt = AdvancedMathlasPlot()
    # plt.voronoi_plot(vor, 'Voronoi plot', axis_labels=['x', 'y'], range_x=(0, 1), range_y=(0, 1))
    # plt.show()
    # plt.close()
    
    return max(dist), np.mean(dist), np.std(dist), dirs, vor
    

def optimize_position(n, dimensions, fixed_points=None,
                      n_iter=1000, n_seed=5,
                      r0=1e-1, r1=1e-3):
    if fixed_points is None:
        n_fixed = 0
    else:
        n_fixed = fixed_points.shape[0]

    # Set some initial placeholder values that should get overwritten after
    # the first time the following loop is executed
    fbest = np.inf
    optimal_distribution = None

    for _ in range(n_seed):
        random_points = np.random.rand(n, dimensions)
        if fixed_points is None:
            points = random_points
        else:
            points = np.concatenate((fixed_points, random_points))
        for ii in range(n_iter):
            f, _, _, dirs, vor = measure_voronoi_quality(points)
            dirs[:n_fixed, :] = 0.
            r = r0 + (r1 - r0) * ii / (n_iter - 1.)
            points += r * dirs

        if f < fbest:
            fbest = f
            optimal_distribution = points
            
    return optimal_distribution


def plot_2d_design(X, n_fixed=0):
    
    plt = MathlasPlot()
    plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color="k")
    plt.scatter(X[:n_fixed, 0], X[:n_fixed, 1], range_x=(-.1, 1.1), range_y=(-.1, 1.1),
                marker_size=100, color="mathlas blue")
    plt.scatter(X[n_fixed:, 0], X[n_fixed:, 1], range_x=(-.1, 1.1), range_y=(-.1, 1.1),
                marker_size=100, color="mathlas orange")
    plt.equal_aspect_ratio()
    plt.show()


if __name__ == '__main__':
    np.random.seed(211104)

    for ii in [5, 9, 13, 17]:
        best_distribution = optimize_position(ii, 2)
        plot_2d_design(best_distribution, 0)

    X = np.array([[.5, .5]])
    n_fixed = X.shape[0]
    for ii in [5, 9, 13, 17]:
        best_distribution = optimize_position(ii, 2, X)
        plot_2d_design(best_distribution, n_fixed)

    X = np.array([[0.5, 0.5], [0.5, 0.], [0.5, 1.], [0., 0.5], [1., 0.5],
                  [.5 + .5 * np.cos(np.pi / 4.), .5 + .5 * np.sin(np.pi / 4.)],
                  [.5 + .5 * np.cos(3 * np.pi / 4.), .5 + .5 * np.sin(3 * np.pi / 4.)],
                  [.5 + .5 * np.cos(5 * np.pi / 4.), .5 + .5 * np.sin(5 * np.pi / 4.)],
                  [.5 + .5 * np.cos(7 * np.pi / 4.), .5 + .5 * np.sin(7 * np.pi / 4.)]])
    n_fixed = X.shape[0]
    for ii in [5, 9, 13, 17]:
        best_distribution = optimize_position(ii, 2, X)
        plot_2d_design(best_distribution, n_fixed)

    X = np.random.rand(15,2)
    optimize_position(X)

    X0 = np.array([[0., 1. / 3.], [1. / 3., 0.], [1., 2. / 3.], [2. / 3., 1.]])
    optimize_position(X0)
    print(measure_voronoi_quality(X0))
    X0 = np.array([[0., 0.], [1. / 3., 1. / 3.], [1., 2. / 3.], [2. / 3., 1.]])
    optimize_position(X0)
    print(measure_voronoi_quality(X0))

    fbest = np.inf
    for _ in range(100):
        X0 = np.random.rand(4, 2)
        f = measure_voronoi_quality(X0)[0]
        if f < fbest:
            fbest = f
            best_distribution = copy(X0)
    print(fbest)

    plt = MathlasPlot()
    plt.scatter(best_distribution[:, 0], best_distribution[:, 1])
    plt.xlim((0., 1.))
    plt.ylim((0., 1.))
    plt.show()
