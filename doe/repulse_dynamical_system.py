# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from itertools import combinations, product


def repulse_dynamical_system(n, d, seed=None, fixedPoints=None, dt=1e-3,
                             max_error=1e-6, max_iterations=5000,
                             n_simulations=10):
    """
    Creates a distribution of points that fills the space using a global
     approach. Each point repels all the others and the transitory is
     integrated until a stationary solution is found.

    This method is not recommended if the number of points to be used is small;
     in that case, points are all grouped in the border of the hyper-cubic
     domain. How small this number must be so this method is not appropriate
     depends on the dimension of the space.

    Return the array of with the positions of the points. Each row is a vector
     of size d (user-defined) with all the co-ordinates of a point. All
     co-ordinates can take values in the range [0, 1].

    Parameters
    ----------
    n : integer
        Number of point to place in the space of features.
    d : integer
        dimension of the space of parameters
    seed : integer
           Random seed to use
    fixedPoints : NumPy array, optional
                  Points defined by the user that are fixed in the space. Their
                  position will not be changed by the optimization algorithm
                  but they influence the position of the others.
    dt : float, optional
         Value of the "time" step
    max_error : float, optional
                Maximum error allowed for a simulation to be considered as
                properly converged
    max_iterations : float, optional
                     Number of maximum iterations allowed by simulation
    n_simulations : integer, optional
                    Number of simulations used to obtain the solution

    Returns
    -------
    X : NumPy array
        Array of the positions of the points. Each row is a vector of size d
        with all the co-ordinates of a point. All co-ordinates take values in
        the range [0, 1].

    Examples
    --------
    >>> repulse_dynamical_system(5, 2, seed=211104)
    [[ 0.          1.        ]
     [ 1.          1.        ]
     [ 0.49144091  0.51329626]
     [ 1.          0.        ]
     [ 0.          0.        ]]

    """

    # Check that fixedPoints has the correct format and size
    if fixedPoints is not None:
        try:
            if fixedPoints.shape[1] != d:
                raise ValueError("The dimension of fixedPoints must " +
                                 "be the d")
        except:
            raise TypeError("Variable fixedPoints is not an array")

    # Generate seed for reproducibility
    try:
        if seed is not None:
            np.random.seed(seed)
    except:
        raise TypeError("Variable <seed> is not an integer")
    # Perform optimization process
    threshold = 1e-5
    _, Xbest = _optimize_positions(n, d, fixedPoints, threshold, dt,
                                   max_error, max_iterations,
                                   n_simulations)

    # Make-up results
    for ii, dd in product(range(n), range(d)):
        if Xbest[ii, dd] > 1. - threshold * 10:
            Xbest[ii, dd] = 1.
        elif Xbest[ii, dd] < threshold * 10:
            Xbest[ii, dd] = 0.

    return Xbest


def _optimize_positions(N, D, fixedPoints, threshold, dt, max_error,
                        max_iterations, n_simulations):
    """
    Creates a distribution of points that fills the space using a global
     approach. Each point repels all the others and the transitory is
     integrated until a stationary solution is found.

    This method is not recommended if the number of points to be used is small;
     in that case, points are all grouped in the border of the hyper-cubic
     domain. How small this number must be so this method is not appropriate
     depends on the dimension of the space.

    Return the array of with the positions of the points. Each row is a vector
     of size D (user-defined) with all the co-ordinates of a point. All
     co-ordinates can take values in the range [0, 1].

    Parameters
    ----------
    N : integer
        Number of point to place in the space of features.
    D : integer
        dimension of the space of parameters
    fixedPoints : NumPy array
                  Points defined by the user that are fixed in the space. Their
                  position will not be changed by the optimization algorithm
                  but they influence the position of the others.
    dt : float
         Value of the "time" step
    max_error : float
                Maximum error allowed for a simulation to be considered as
                properly converged
    max_iterations : float
                     Number of maximum iterations allowed by simulation
    n_simulations : integer
                    Number of simulations used to obtain the solution

    Returns
    -------
    fbest : float
            Best value of the objective function found
    best_distribution : NumPy array
            Best positions of the points

    """
    # Get number of fixed points
    if fixedPoints is not None:
        N0 = fixedPoints.shape[0]
    else:
        N0 = 0

    # Initialize loop
    Xbest = None
    fbest = 1e30
    for _ in range(n_simulations):
        # Generate initial distribution
        Y = np.random.rand(N, D)
        if N0 == 0:
            X = Y
        else:
            X = np.concatenate((Y, fixedPoints))

        # Generate initial values
        X0 = X[:]
        X1 = X[:]
        err = 1e30
        it = 0
        # Loop until simulation is converged
        while err > max_error and it < max_iterations:
            # Evaluate forces
            F = _evaluate_forces(X, N0)
            # Solve Verlet scheme
            X2 = 2. * X1 - X0 + (dt * dt) * F
            # Correct co-ordinates to avoid that any point leaves the
            #  hyper-cube
            for ii, dd in product(range(N), range(D)):
                if X2[ii, dd] > 1. - threshold:
                    X2[ii, dd] = 1. - threshold
                elif X2[ii, dd] < threshold:
                    X2[ii, dd] = threshold
            # Evaluate erros
            err = np.linalg.norm(X2-X1) / np.sqrt(N * D) / dt
            # Update variabless
            X0[:] = X1[:]
            X1[:] = X2[:]
            it += 1

        # Evaluate the objective function and select the optimum until now
        f = _evaluate_objective_function(X)
        if f < fbest:
            Xbest = X2[:]
            fbest = f

    return fbest, Xbest


def _evaluate_forces(X, nFixed, kc=0.1, kr=100.):
    """
    Evaluate the forces that act between points in the space.

    Return the array of forces

    Parameters
    ----------
    X : NumPy array-like
        Array of the co-ordinates of the points. Each row is a point.
    nFixed : integer
        Number of points of the array which should not be moved but create
        their own repulsive force.
    kc : float, optional
         constant that modifies the repulsion forces of the constraints
    kr : float, optional
         constant of the repulsion forces of the points

    Returns
    -------
    F : NumPy array-like
        Array of the same size as X that stores the magnitude of the forces
        in each direction for each point. The values of last <nFixed> rows
        are zero.

    """

    # Initialize some variables
    N, D = X.shape
    F = np.zeros((N, D))
    # Evaluate distances
    D3 = _evaluate_distances(X)
    # Loop for each point
    for ii, dd in product(range(N - nFixed), range(D)):
        # Evaluate the forces of the contraints
        F[ii, dd] -= kc * np.log(X[ii, dd])
        F[ii, dd] += kc * np.log(1. - X[ii, dd])
        # Evaluate repulsion forces of the points
        for jj in range(N):
            if ii != jj:
                F[ii, dd] += kr * (X[ii, dd] - X[jj, dd]) / D3[ii, jj]

    return F


def _evaluate_distances(X):
    """
    Evaluate the distances between points at the third power.

    Return the array of Distances

    Parameters
    ----------
    X : NumPy array-like
        Array of the co-ordinates of the points. Each row is a point.

    Returns
    -------
    D : NumPy array-like
        Array of N by N size (where N is the number of rows of X). The element
        d_{ij} is the third power of distance between the point x_i and x_j,
        that is, X[i, :] and X[j, :].

    """
    # Initialize some variables
    N = X.shape[0]
    # Evaluate distances
    D3 = np.zeros((N, N))
    # Evaluate distances for all points
    for ii, jj in combinations(range(N), 2):
        D3[ii, jj] = D3[jj, ii] = np.linalg.norm(X[ii, :] - X[jj, :]) ** 3.
    return D3


def _evaluate_objective_function(X):
    """
    Evaluate the value of the objective function and return it. This functions
     is the sum of the squared distance for all pairs of points. It imposes a
     high penalty over poits whose positions are very near and mostly ignores
     those thata are far. The effect of the constraints are neglected.

    Parameters
    ----------
    X : NumPy array-like
        Array of the co-ordinates of the points. Each row is a point.

    Returns
    -------
    f : float
        Value of the objective function.
    """
    # Initialize some variables
    N = X.shape[0]
    f = 0.
    # Evaluate distances
    D3 = _evaluate_distances(X)
    # Loop for all pairs
    for ii, jj in combinations(range(N), 2):
        f += 2. / D3[ii, jj] ** (2. / 3.)
    return f / N


if __name__ == "__main__":
    from mathlas.plotting.mathlas_plots import MathlasPlot

    X = repulse_dynamical_system(15, 2, seed=211104)
    plt = MathlasPlot()
    plt.scatter(X[:, 0], X[:, 1], marker_size=100, color='mathlas orange',
                axis_labels=('$x_1$', '$x_2$'), title='Repulse dynamical system')
    plt.show()
