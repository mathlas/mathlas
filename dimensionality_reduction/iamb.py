# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import operator
from time import time
from mathlas.misc import ansi_color


def iamb(database, target, get_joint_distribution,
         eps_g=1e-5, eps_s=1e-4, var_limit=None,
         calculate_global_distribution=False,
         initial_vars=(),
         verbose=False):
    """
    Calculates the Markov Blanket of the target variable using the
    Incremental Association Markov Blanket algorithm

    Parameters
    ----------
    database : Pandas DataFrame
                Data whose
    target : string
             targeted variable whose MB is going to be calculated
    get_joint_distribution : method
                             function p = f(df) that, given a Dataframe, provides the
                              ProbabilityDistribution Object that contains the model
                              of the joint probabilities of all the considered variables.
    eps_g : float
            threshold used in the Growing phase
    eps_s : float
            threshold used in the Shrinking phase
    var_limit : integer
                Hard upper limit for the number of variables which can be added.
    calculate_global_distribution : Boolean, optional
                                    flag that indicates if the joint probability distribution is
                                     only calculated once, using all the variables, or at each step
                                     of the iteration using only the variables required to perform
                                     that step.
    initial_vars : tuple, list
                   initial list of variables (if any) to continue from.
    verbose : Boolean, optional
              If True where the process exactly is and how much time took to
              build the probability distribution objects is shown via screen.

    Returns
    -------
    markov_blanket : set
                     labels of the variables that form the Markov Blanket
    """
    # Initializes some variables
    markov_blanket = set([])
    if calculate_global_distribution:
        if database is not None:
            probabilities = get_joint_distribution(database, list(database.columns))
        else:
            probabilities = get_joint_distribution()
        variables = set(probabilities.variables).difference({target})
    else:
        variables = set(database.columns).difference({target})

    # If given initial_vars, consider them
    if initial_vars is not None and len(initial_vars) > 0:
        markov_blanket |= set(initial_vars)
        variables -= set(initial_vars)
        if verbose:
            sys.stdout.write('\tInitial variables in the Markov blanket\n')
            for v in initial_vars:
                sys.stdout.write('\t\t- {}\n'.format(v))

    # Growing phase
    while len(variables) != 0:
        if verbose:
            sys.stdout.write(ansi_color.YELLOW +
                             "\tGrowing phase. Tier {}\n".format(len(markov_blanket) + 1) +
                             ansi_color.END)
        # Evaluate all conditional mutual information
        cmi = {}
        if verbose:
            t = time()
        if len(markov_blanket) == 0:
            for variable in sorted(list(variables)):
                if not calculate_global_distribution:
                    required_variables = [variable, target]
                    probabilities = get_joint_distribution(database, required_variables)
                cmi[variable] = probabilities.get_mutual_information([target], [variable])
        else:
            for variable in sorted(list(variables)):
                if not calculate_global_distribution:
                    required_variables = list(markov_blanket.union({variable, target}))
                    probabilities = get_joint_distribution(database, required_variables)
                cmi[variable] = probabilities.get_conditional_mutual_information([target],
                                                                                 [variable],
                                                                                 list(markov_blanket))
        if verbose:
            t -= time()
            sys.stdout.write("Mutual informations calculated in {:.1f} seconds.\n".format(-t))
        # Add variable that provides maximum CMI if its value is higher than some threshold
        label, value = max(cmi.items(), key=operator.itemgetter(1))
        if value > eps_g:
            markov_blanket = markov_blanket.union([label])
            variables = variables.difference({label})
            if verbose:
                sys.stdout.write("Added variable <{}> whose CMI is {:.5f},"
                                 " larger than the threshold {:.5f}.\n".format(label, value, eps_g))
        else:
            break

        # Start with the shrinking phase if we hit the variable hard limit
        if var_limit is not None and len(markov_blanket) >= var_limit:
            if verbose:
                sys.stdout.write('Variable hard limit reached, exiting\n')
            return markov_blanket

    # Shrinking phase
    for variable in sorted(list(markov_blanket)):
        if verbose:
            sys.stdout.write("\t\033[93mShrinking phase."
                             " Tier {}\033[0m\n".format(len(markov_blanket)))
        if not calculate_global_distribution:
            required_variables = list(markov_blanket.union({variable, target}))
            probabilities = get_joint_distribution(database, required_variables)
        cmi = probabilities.get_conditional_mutual_information([target], [variable],
                                                               list(markov_blanket.difference(set(variable))))
        if cmi < eps_s:
            markov_blanket = markov_blanket.difference(set(variable))

    return markov_blanket


def lambda_iamb(database, target, get_joint_distribution,
                eps_g=1e-5, eps_s=1e-4, lambda_value=None, var_limit=None,
                calculate_global_distribution=False,
                initial_vars=(),
                verbose=False):
    """
    Calculates the Markov Blanket of the target variable using the
    λ-Incremental Association Markov Blanket algorithm

    Parameters
    ----------
    database : Pandas DataFrame
                Data whose
    target : string
             targeted variable whose MB is going to be calculated
    get_joint_distribution : method
                             function p = f(df) that, given a Dataframe, provides the
                              ProbabilityDistribution Object that contains the model of the joint
                              probabilities of all the considered variables.
    eps_g : float
            threshold used in the Growing phase
    eps_s : float
            threshold used in the Shrinking phase
    lambda_value : float
                   lambda value of the lambda-IAMB algorithm. It must be a float between 0 and 1.
    var_limit : integer
                Hard upper limit for the number of variables which can be added.
    calculate_global_distribution : Boolean, optional
                                    flag that indicates if the joint probability distribution is
                                     only calculated once, using all the variables, or at each step
                                     of the iteration using only the variables required to perform
                                     that step.
    initial_vars : tuple, list
                   initial list of variables (if any) to continue from.
    verbose : Boolean, optional
              If True where the process exactly is and how much time took to
              build the probability distribution objects is shown via screen.

    Returns
    -------
    markov_blanket : set
                     labels of the variables that form the Markov Blanket
    """
    # Check that variables are correctly inputted
    lambda_key = lambda_value is not None
    if lambda_key and (not isinstance(lambda_value, float) or not 0. <= lambda_value <= 1.):
        raise ValueError("Variable <lambda_iamb> must be a float in the range [0, 1].")

    # Initializes some variables
    markov_blanket = set([])
    if calculate_global_distribution:
        if database is not None:
            probabilities = get_joint_distribution(database, list(database.columns))
        else:
            probabilities = get_joint_distribution()
        variables = set(probabilities.variables).difference({target})
    else:
        variables = set(database.columns).difference({target})

    # If given initial_vars, consider them
    if initial_vars is not None and len(initial_vars) > 0:
        markov_blanket |= set(initial_vars)
        variables -= set(initial_vars)
        if verbose:
            sys.stdout.write('\tInitial variables in the Markov blanket\n')
            for v in initial_vars:
                sys.stdout.write('\t\t- {}\n'.format(v))

    # Growing phase
    while len(variables) != 0:
        if verbose:
            sys.stdout.write(ansi_color.YELLOW +
                             "\tGrowing phase. Tier {}\n".format(len(markov_blanket) + 1) +
                             ansi_color.END)
        # Evaluate all conditional mutual information
        cmi = {}
        if verbose:
            t = time()

        if len(markov_blanket) == 0:
            for variable in sorted(list(variables)):
                if not calculate_global_distribution:
                    required_variables = [variable, target]
                    probabilities = get_joint_distribution(database, required_variables)
                cmi[variable] = probabilities.get_mutual_information([target], [variable])
        else:
            for variable in sorted(list(variables)):
                if not calculate_global_distribution:
                    required_variables = list(markov_blanket.union({variable, target}))
                    probabilities = get_joint_distribution(database, required_variables)
                cmi[variable] = probabilities.get_conditional_mutual_information([target],
                                                                                 [variable],
                                                                                 list(markov_blanket))

        if verbose:
            t -= time()
            sys.stdout.write("Mutual informations calculated in {:.1f} seconds.\n".format(-t))
        # Add variable that provides maximum CMI if its value is higher than some threshold
        label, value = max(cmi.items(), key=operator.itemgetter(1))
        if value > eps_g:
            markov_blanket = markov_blanket.union([label])
            variables = variables.difference({label})
            if verbose:
                sys.stdout.write("Added variable <{}> whose CMI is {:.5f},"
                                 " larger than the threshold {:.5f}.\n".format(label, value, eps_g))
            if lambda_key:
                cmi[label] = -1e300
                label2, value2 = max(cmi.items(), key=operator.itemgetter(1))
                h_t1 = probabilities.get_conditional_entropy([target], [label] + list(markov_blanket))
                h_t2 = probabilities.get_conditional_entropy([target], [label2] + list(markov_blanket))
                h_t = probabilities.get_conditional_entropy([target], list(markov_blanket))
                if value2 > eps_g and (h_t2 - lambda_value * h_t1 < (1. - lambda_value) * h_t):
                    markov_blanket = markov_blanket.union([label2])
                    variables = variables.difference({label2})
                    if verbose:
                        sys.stdout.write("Added variable <{}> whose CMI is {:.5f}, "
                                         "larger than the threshold {:.5f}.\n".format(label2,
                                                                                      value2,
                                                                                      eps_g))

        else:
            break

        # Start with the shrinking phase if we hit the variable hard limit
        if var_limit is not None and len(markov_blanket) >= var_limit:
            if verbose:
                sys.stdout.write('Variable hard limit reached, exiting\n')
            return markov_blanket

    # Shrinking phase
    for variable in sorted(list(markov_blanket)):
        if verbose:
            sys.stdout.write("\t\033[93mShrinking phase."
                             " Tier {}\033[0m\n".format(len(markov_blanket)))
        if not calculate_global_distribution:
            required_variables = list(markov_blanket.union({variable, target}))
            probabilities = get_joint_distribution(database, required_variables)
        cmi = probabilities.get_conditional_mutual_information([target], [variable],
                                                               list(markov_blanket.difference(set(variable))))
        if cmi < eps_s:
            markov_blanket = markov_blanket.difference(set(variable))

    return markov_blanket
