#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pickle
from mathlas.doe.lhc import LHC, variation_of_lhc
from mathlas.plotting.mathlas_plots import MathlasPlot


if __name__ == "__main__":
    # Define benchmark parameters
    seed = 211104
    list_of_n_centers = [10, 15, 20]

    ############################################################################################

    # Calculate centers of the RBFs
    for n_centers in list_of_n_centers:
        try:
            design = pickle.load(open("LHC_{}.pickle".format(n_centers), "rb"))
        except:
            design = LHC(n_centers, 2, random_seed=seed)
            pickle.dump(design, open("LHC_{}.pickle".format(n_centers), "wb"))

        x_reversed_design = variation_of_lhc(design, reverse_feat=[True, False],
                                             permute_feats=[0, 1])
        permuted_design = variation_of_lhc(design, reverse_feat=[False, False],
                                           permute_feats=[1, 0])

        plt = MathlasPlot(figsize=(20, 10))
        plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color='k', subplot_index=131)
        plt.scatter(design[:, 0], design[:, 1], marker_size=100,
                    range_x=(-0.05, 1.05), range_y=(-0.05, 1.05), title="Original design",
                    subplot_index=131)
        plt.equal_aspect_ratio()

        plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color='k', subplot_index=132)
        plt.scatter(x_reversed_design[:, 0], x_reversed_design[:, 1], marker_size=100,
                    range_x=(-0.05, 1.05), range_y=(-0.05, 1.05), title="x-reversed design",
                    subplot_index=132)
        plt.equal_aspect_ratio()

        plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color='k', subplot_index=133)
        plt.scatter(permuted_design[:, 0], permuted_design[:, 1], marker_size=100,
                    range_x=(-0.05, 1.05), range_y=(-0.05, 1.05), title="permuted-axis design",
                    subplot_index=133)
        plt.equal_aspect_ratio()
        plt.show()
