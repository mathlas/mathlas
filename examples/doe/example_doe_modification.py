# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from mathlas.doe.lhc import multipleLHC
from mathlas.plotting.mathlas_plots import MathlasPlot


# Number of points to be used ################################################
N = 10
##############################################################################


# A random case
X = multipleLHC(N, 2)
# Plot it
plt = MathlasPlot()
for indx, x in enumerate(X):
    plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color='k',
             subplot_index=241 + indx)
    plt.scatter(x[:, 0], x[:, 1], marker_size=100,
                range_x=(-0.05, 1.05), range_y=(-0.05, 1.05),
                subplot_index=241 + indx)
    plt.equal_aspect_ratio()
plt.show()
