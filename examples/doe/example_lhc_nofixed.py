# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.doe.lhc import LHC
import numpy as np
from mathlas.plotting.mathlas_plots import MathlasPlot


# Number of points to be used ################################################
N = 15
saveFigure = True  # If True it saves figure to pgn.file
seed_random = np.random.randint(0, 20000)  # Random seed for LHC in random case
seed_optimize = seed_random  # Random seed for LHC in optimize case. If
# seed_random = seed_optimize, both figures correspond to the same case.
##############################################################################

# A random case
X = LHC(N, 2, nIters=1, nSeeds=1, random_seed=seed_random)
# Plot it
plt = MathlasPlot()
plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color='k',
         subplot_index=121)
plt.scatter(X[:, 0], X[:, 1], marker_size=100,
            range_x=(-0.05, 1.05), range_y=(-0.05, 1.05), subplot_index=121)
plt.equal_aspect_ratio()

# Optimize a configuration
X = LHC(N, 2, random_seed=seed_optimize)
# Plot it
plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color='k',
         subplot_index=122)
plt.scatter(X[:, 0], X[:, 1], marker_size=100,
            range_x=(-0.05, 1.05), range_y=(-0.05, 1.05), subplot_index=122)
plt.equal_aspect_ratio()

# Save plotted figure if required
if saveFigure:
    plt.savefig('./example_lhc_n0.png')

# Show the results
plt.show()
