#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import numpy as np
from mathlas.surrogate_modelling.kriging import Kriging
from mathlas.plotting.advanced_mathlas_plots import AdvancedMathlasPlot


def humps(x):

    return 1. / ((x - .3) ** 2 + .01) + 1. / ((x - .9) ** 2 + .04) - 6


if __name__ == "__main__":

    # Genenaret meshes
    x_mesh = np.linspace(0., 1., 201)
    humps_mesh = humps(x_mesh)
    # Generate smaples to train Kriging
    nPoints = 10
    sampleFeats = np.linspace(0., 1., nPoints)
    sampleFeats = np.array([0., 0.05, 0.1, 0.23, 0.28, 0.30, 0.50,
                            0.62, 0.7, 0.8, 0.85, 1.0])
    sampleResponses = humps(sampleFeats)
    # Train Kriging model
    k_model = Kriging(sampleFeats, sampleResponses, regressionType="quadratic")
    # Get error at samples
    predictedResponses = k_model.get_prediction(sampleFeats)
    error = np.linalg.norm(predictedResponses.T - sampleResponses)
    sys.stdout.write("The reconstruction error at the sampled points is {}\n".format(error))

    # Predict estimator and uncertainty
    predictedResponses, mseResponses = k_model.get_prediction(x_mesh, provideRMSE=True)

    # Compute the X value where rmse is maximum
    sys.stdout.write("Point wher RMSE is maximum is {}\n".format(x_mesh[np.argmax(mseResponses)]))

    # Plot rmse
    plt = AdvancedMathlasPlot()
    plt.plot(x_mesh, humps_mesh, subplot_index=211, axis_labels=['', 'Humps'])
    plt.scatter(sampleFeats, sampleResponses, marker_size=100,
                subplot_index=211)
    plt.plot(x_mesh, mseResponses[:, 0], subplot_index=212,
             axis_labels=['X', 'RMSE'])
    sample_rmse = np.array([0.0] * len(sampleFeats))
    plt.scatter(sampleFeats, sample_rmse, marker_size=100, subplot_index=212)
    plt.savefig('./rmse.png')

    # Plot them
    plt = AdvancedMathlasPlot()
    plt.plot(x_mesh, humps_mesh)
    plt.plot_prediction_and_uncertainty(x_mesh, np.array(predictedResponses)[:, 0],
                                        np.array(3. * mseResponses)[:, 0])
    plt.scatter(sampleFeats, sampleResponses, marker_size=100)

    # Get and plot the PDF
    y_mesh = np.linspace(-20., 120., 281)
    pdf = k_model.get_pdf(x_mesh, y_mesh, productOfVariables=True)
    plt = AdvancedMathlasPlot()
    plt.contour_plot_from_meshes(x_mesh, y_mesh, pdf[:, :, 0], reference_point=0.5)
    plt.scatter(sampleFeats, sampleResponses, marker_size=100)

    # Get and plot the quantiles for Y<20 and Y<60 at all points of the mesh
    Q20 = k_model.get_cdf_quantile(x_mesh, 20.)
    Q60 = k_model.get_cdf_quantile(x_mesh, 60.)
    plt = AdvancedMathlasPlot()
    plt.plots(x_mesh, [Q20, Q60], range_y=(-0.05, 1.05),
              var_labels=["p(Y<20|X)", "p(Y<60|X)"])

    # Get and plot median and the interval from quantile .1 to .9
    Y10 = k_model.get_cdf_point(x_mesh, .1)
    Y50 = k_model.get_cdf_point(x_mesh, .5)
    Y90 = k_model.get_cdf_point(x_mesh, .9)
    plt = AdvancedMathlasPlot()
    plt.plot(x_mesh, humps_mesh)
    plt.plot_prediction_and_uncertainty(x_mesh, Y50, Y90 - Y50, Y50 - Y10)

    # Get how the objective function behaves in neighborhood of the solution
    log_t = np.linspace(-1, 2, 101)
    list_t = 10 ** log_t
    s2 = []
    dc = []
    s = []
    for ii in list_t:
        k_model.theta = np.matrix([[ii]])
        k_model.getCLF()
        s2.append(np.log10(k_model.squaredSigma[0]))
        dc.append(k_model.covLogDet / nPoints)
        s.append(s2[-1] + dc[-1])
    # Now plot it
    plt = AdvancedMathlasPlot()
    plt.plots([log_t, log_t, log_t], [s2, dc, s],
              axis_labels=["$\log_{10}(\\theta)$ value",
                           ""],
              var_labels=["$\log_{10}(\sigma^2)$", "$log_{10}(|\Phi|) / N$",
                          "$\log_{10}(\sigma^2)$ + $\log_{10}(|\Phi|) / N$"],
              colors=["mathlas orange", "mathlas orange", "mathlas blue"],
              linestyles=["-", "--", "-"], subplot_index=121)
    plt.plots([log_t[50:], log_t[50:], log_t[50:]], [s2[50:], dc[50:], s[50:]],
              axis_labels=["$\log_{10}(\\theta)$ value",
                           ""],
              var_labels=["$\log_{10}(\sigma^2)$", "$log_{10}(|\Phi|) / N$",
                          "$\log_{10}(\sigma^2)$ + $\log_{10}(|\Phi|) / N$"],
              colors=["mathlas orange", "mathlas orange", "mathlas blue"],
              linestyles=["-", "--", "-"], subplot_index=122)

    # Set a different value of the parameters
    k_model.theta = np.matrix([[200.]])
    k_model.getCLF()
    # Get error at samples
    predictedResponses = k_model.get_prediction(sampleFeats)
    error = np.linalg.norm(predictedResponses.T - sampleResponses)
    sys.stdout.write("The reconstruction error at the sampled points is {}\n".format(error))
    # Predict estimator and uncertainty
    predictedResponses, mseResponses = k_model.get_prediction(x_mesh, provideRMSE=True)
    # Plot them
    plt = AdvancedMathlasPlot()
    plt.plot(x_mesh, humps_mesh)
    plt.plot_prediction_and_uncertainty(x_mesh, np.array(predictedResponses)[:, 0],
                                        np.array(3. * mseResponses)[:, 0])
    plt.scatter(sampleFeats, sampleResponses, marker_size=100)
    plt.show()


