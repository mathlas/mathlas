#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.surrogate_modelling.mixture_density_model import MixtureDensityModel
from itertools import product


def generate_points(N=1000, show_plots=False):

    np.random.seed(211104)

    n1 = N / 2
    n2 = N / 2

    # Define parameters of the Gaussians
    m1 = np.array([4., 1.])
    l1 = np.array([1., 0.25])
    a1 = 30. * np.pi / 180.
    G1 = np.array([[np.cos(a1), -np.sin(a1)], [np.sin(a1), np.cos(a1)]])
    C1 = np.diag(l1).dot(G1)

    m2 = np.array([-4., 1.])
    l2 = np.array([2., 0.25])
    a2 = 100. * np.pi / 180.
    G2 = np.array([[np.cos(a2), -np.sin(a2)], [np.sin(a2), np.cos(a2)]])
    C2 = np.diag(l2).dot(G2)

    # Create simple, stretched Gaussian
    X1 = m1 + np.random.randn(n1, 2).dot(C1)

    # Create another simple, stretched Gaussian
    X2 = m2 + np.random.randn(n2, 2).dot(C2)

    # Concatenate data
    X = np.concatenate((X1, X2), axis=0)

    # Shuffle data
    indx = range(N)
    np.random.shuffle(indx)
    X = X[indx, :]

    if show_plots:
        plt = MathlasPlot()
        plt.scatter(X[:, 0], X[:, 1])
        plt.show()

    return X


if __name__ == "__main__":

    # Generate data
    X = generate_points()
    Y = X.copy()
    Y[:, 1] = np.exp(Y[:, 1] - 4.)
    Z = X.copy()
    Z[:, 0] = (Z[:, 0] + 4.) % 10.
    Z[:, 1] = Z[:, 1] % 6.

    print("Training model")
#     mdm_norm = MDM(X, 2, path="./model_gauss_simple.pickle")
    mdm_norm = MixtureDensityModel(path="./model_gauss_simple.pickle")
    print("The number of components selected " +
          "by the method is {}".format(mdm_norm.n_density_functions))

    meshX = np.linspace(-8, 8, 201)
    meshY = np.linspace(-8, 6, 201)
    P = []
    for x, y in product(meshX, meshY):
        P.append([x, y])
    pdf = mdm_norm.get_pdf(np.array(P))
    pdf = np.reshape(pdf, (201, 201)).T
    pdf1 = mdm_norm.get_marginalized_pdf(meshX, [0])
    pdf2 = mdm_norm.get_marginalized_pdf(meshY, [1])
    ycut = 0.
    xcut = -4.
    pdfa = mdm_norm.get_conditional_pdf(np.array([ycut]), meshX, [0])
    pdfb = mdm_norm.get_conditional_pdf(np.array([xcut]), meshY, [1])
    pdfa = np.reshape(pdfa, (pdfa.shape[1],))
    pdfb = np.reshape(pdfb, (pdfb.shape[1],))

    plt = MathlasPlot()
    plt.contourf(meshX, meshY, pdf, subplot_index=222)
    plt.scatter(X[:, 0], X[:, 1], range_x=(-8, 8), range_y=(-8., 6.),
                transparency=True, subplot_index=222)
    plt.plot(meshX, pdf1, subplot_index=224)
    plt.plot(pdf2, meshY, subplot_index=221)
    plt.suptitle("Marginalized distributions")

    plt = MathlasPlot()
    plt.contourf(meshX, meshY, pdf, subplot_index=222)
    plt.scatter(X[:, 0], X[:, 1], range_x=(-8, 8), range_y=(-8., 6.),
                transparency=True, subplot_index=222)
    plt.plot([-8, 8], [ycut, ycut], subplot_index=222)
    plt.plot([xcut, xcut], [-8, 6], subplot_index=222)
    plt.plot(meshX, pdfa, subplot_index=224)
    plt.plot(pdfb, meshY, subplot_index=221)
    plt.suptitle("Conditional distributions")

    plt.show()

    print("Training model")
#     mdm_lognorm = MDM(Y, 2,
#                       listOfDensityFunctions=["Gaussian", "Log-normal"],
#                       path="./model_lognormal_simple.pickle")
    mdm_lognorm = MixtureDensityModel(path="./model_lognormal_simple.pickle")
    print("The number of components selected " +
          "by the method is {}".format(mdm_lognorm.n_density_functions))

    meshX = np.linspace(-8, 8, 201)
    meshY = np.linspace(0, 0.5, 201)
    P = []
    for x, y in product(meshX, meshY):
        P.append([x, y])
    pdf = mdm_lognorm.get_pdf(np.array(P))
    pdf = np.reshape(pdf, (201, 201)).T
    pdf1 = mdm_lognorm.get_marginalized_pdf(meshX, [0])
    pdf2 = mdm_lognorm.get_marginalized_pdf(meshY, [1])
    ycut = 0.02
    xcut = -4.0
    pdfa = mdm_lognorm.get_conditional_pdf(np.array([ycut]), meshX, [0])
    pdfb = mdm_lognorm.get_conditional_pdf(np.array([xcut]), meshY, [1])
    pdfa = np.reshape(pdfa, (pdfa.shape[1],))
    pdfb = np.reshape(pdfb, (pdfb.shape[1],))

    plt = MathlasPlot()
    plt.contourf(meshX, meshY, pdf, subplot_index=222)
    plt.scatter(Y[:, 0], Y[:, 1], range_x=(-8, 8), range_y=(0., 0.5),
                transparency=True, subplot_index=222)
    plt.plot(meshX, pdf1, subplot_index=224)
    plt.plot(pdf2, meshY, subplot_index=221)
    plt.suptitle("Marginalized distributions")

    plt = MathlasPlot()
    plt.contourf(meshX, meshY, pdf, subplot_index=222)
    plt.scatter(Y[:, 0], Y[:, 1], range_x=(-8, 8), range_y=(0., 0.5),
                transparency=True, subplot_index=222)
    plt.plot([-8, 8], [ycut, ycut], subplot_index=222)
    plt.plot([xcut, xcut], [0.0, 0.5], subplot_index=222)
    plt.plot(meshX, pdfa, subplot_index=224)
    plt.plot(pdfb, meshY, subplot_index=221)
    plt.suptitle("Conditional distributions")

    plt.show()

    print("Training model")
#     mdm_periodic = MDM(Z, 2, listOfDensityFunctions=["Periodic", "Periodic"],
#                        spanOfFeats={0: 10., 1: 6.},
#                        path="./model_periodic_simple.pickle")
    mdm_periodic = MixtureDensityModel(path="./model_periodic_simple.pickle")
    print("The number of components selected " +
          "by the method is {}".format(mdm_periodic.n_density_functions))

    meshX = np.linspace(0, 10, 101)
    meshY = np.linspace(0, 6, 101)
    P = []
    for x, y in product(meshX, meshY):
        P.append([x, y])
    pdf = mdm_periodic.get_pdf(np.array(P))
    pdf = np.reshape(pdf, (101, 101)).T
    pdf1 = mdm_periodic.get_marginalized_pdf(meshX, [0])
    pdf2 = mdm_periodic.get_marginalized_pdf(meshY, [1])
    ycut = 2.
    xcut = 8.
    pdfa = mdm_periodic.get_conditional_pdf(np.array([ycut]), meshX, [0])
    pdfb = mdm_periodic.get_conditional_pdf(np.array([xcut]), meshY, [1])
    pdfa = np.reshape(pdfa, (pdfa.shape[1],))
    pdfb = np.reshape(pdfb, (pdfb.shape[1],))

    plt = MathlasPlot()
    plt.contourf(meshX, meshY, pdf, subplot_index=222)
    plt.scatter(Z[:, 0], Z[:, 1], range_x=(0., 10.), range_y=(0., 6.),
                transparency=True, subplot_index=222)
    plt.plot(meshX, pdf1, subplot_index=224)
    plt.plot(pdf2, meshY, subplot_index=221)
    plt.suptitle("Marginalized distributions")

    plt = MathlasPlot()
    plt.contourf(meshX, meshY, pdf, subplot_index=222)
    plt.scatter(Z[:, 0], Z[:, 1], range_x=(0., 10.), range_y=(0., 6.),
                transparency=True, subplot_index=222)
    plt.plot([0., 10], [ycut, ycut], subplot_index=222)
    plt.plot([xcut, xcut], [0, 6], subplot_index=222)
    plt.plot(meshX, pdfa, subplot_index=224)
    plt.plot(pdfb, meshY, subplot_index=221)
    plt.suptitle("Conditional distributions")

    plt.show()
