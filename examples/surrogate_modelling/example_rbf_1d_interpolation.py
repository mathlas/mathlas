#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.surrogate_modelling.rbf import RBF
from mathlas.plotting.advanced_mathlas_plots import AdvancedMathlasPlot
import numpy as np


def humps(x):
    return 1 / ((x - 0.3) ** 2 + 0.01) + 1 / ((x - 0.9) ** 2 + 0.04) - 6


def test1D(rbf_kind, param=None):
    # 1D sample
    x = np.reshape(np.linspace(0., 2., 7), (7, 1))
    y = np.reshape(humps(x), (7, 1))

    X = np.reshape(np.linspace(0., 2., 201), (201, 1))
    Y = np.reshape(humps(X), (201, 1))

    rbf_model = RBF(x, y, rbf_kind, param=param)
    tildeY, S = rbf_model.get_prediction(X, provideRMSE=True)
    tildey = rbf_model.get_prediction(x)

    rbf_model = RBF(x, y, rbf_kind, param=param, centers=x)
    tildey2 = rbf_model.get_prediction(x)

    print("The reconstruction error at the sampled points is")
    print(tildey - y)

    print("The difference between providing centers or not is")
    print(tildey - tildey2)

    plt = AdvancedMathlasPlot()
    plt.plot(X, Y, label="Original function")
    plt.plot(X, tildeY, linestyle="--", label="Reconstructed function",
             color="mathlas orange")
    plt.scatter(x, y, marker_size=100, label='Control points',
                color="mathlas blue")
    plt.legend()
    plt.show()

    plt = AdvancedMathlasPlot()
    plt.plot(X, Y, label='Humps')
    plt.plot_prediction_and_uncertainty(X, tildeY, 3. * S)
    plt.scatter(x, y, marker_size=100, label='Control points')
    plt.legend()
    plt.show()

    # Get and plot the PDF
    Ymesh = np.linspace(-20., 120., 281)
    pdf = rbf_model.get_pdf(X, Ymesh, productOfVariables=True)
    plt = AdvancedMathlasPlot()
    plt.contour_plot_from_meshes(X[:, 0], Ymesh, pdf[:, :, 0],
                                 reference_point=0.5)
    plt.scatter(x, y, marker_size=100)
    plt.show()
    exit()

    # Get and plot the quantiles for Y<20 and Y<60 at all points of the mesh
    Q20 = rbf_model.get_cdf_quantile(X, 20.)
    Q60 = rbf_model.get_cdf_quantile(X, 60.)
    plt = AdvancedMathlasPlot()
    plt.plots(X, [Q20, Q60], range_y=(-0.05, 1.05),
              var_labels=["p(Y<20|X)", "p(Y<60|X)"])
    plt.show()

    # Get and plot median and the interval from quantile .1 to .9
    Y10 = rbf_model.get_cdf_point(X, .1)
    Y50 = rbf_model.get_cdf_point(X, .5)
    Y90 = rbf_model.get_cdf_point(X, .9)
    plt = AdvancedMathlasPlot()
    plt.plot(X, Y)
    plt.plot_prediction_and_uncertainty(X, Y50, Y90 - Y50, Y50 - Y10)
    plt.scatter(x, y, marker_size=100, title=r"Median$\pm$40%")
    plt.show()


if __name__ == "__main__":

    test1D("Gaussian", param=2.)
    test1D("Cubic spline")
