#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
from itertools import chain
from mathlas.geo.models.carvia import Carvia
from mathlas.geo.models.municipality import Municipality
from mathlas.geo.cadastre_spain import SpanishCadastreFetcher


def distance(s, t):
    """
    Edit distance aka Levenshtein distance between the given elements.

    Parameters
    ----------
    s : string
        First element on the distance measurement.
    t : string
        Second element on the distance measurement.

    Returns
    -------
    d : distance between the input strings
    """

    if s == t:
        return 0
    elif len(s) == 0:
        return len(t)
    elif len(t) == 0:
        return len(s)
    v0 = [None] * (len(t) + 1)
    v1 = [None] * (len(t) + 1)
    for i in range(len(v0)):
        v0[i] = i
    for i in range(len(s)):
        v1[0] = i + 1
        for j in range(len(t)):
            cost = 0 if s[i] == t[j] else 1
            v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
        for j in range(len(v0)):
            v0[j] = v1[j]

    return v1[len(t)]


def suggest_street_name(street_name, streets, n=3,
                        show_results=False,
                        provide_options=True):
    """
    Sugeest the street names in streets which most closely resemble
    the name given in "street_name".

    Parameters
    ----------
    street_name : string
                  An approximate name for the street.
    streets : list of Carvia objects
              A list of known "good" street names.
    n : integer, optional
        The number of suggestions to provide. Defaults to 3.
    show_results : boolean, optional
                   Whether suggestions should be printed
                   to stdout. Only works when provide_options
                   is True.
    provide_options : boolean, optional
                      Whether the routine should return a list
                      of suggestions if a perfect match is not
                      found.

    Returns
    -------
    suggestions : list of lists.
                  Ordered list of suggestions.
                  Each sublist will contain:

                    * d : integer
                          The Levenshtein distance between the
                          "clean" version of the given name and
                          `v`.
                    * v : string
                          An entry in `streets`.
    """
    # TODO: We could benefit from normalizing the names in `streets`
    street_names = {}
    for s in streets:
        for option in normalize_address(str(s)):
            street_names[option] = s
    if street_name in street_names.keys():
        return [[0, street_names[street_name]]]
    else:
        if provide_options:
            pairs = []
            for street in streets:
                pairs.append((distance(street_name, str(street)), street))
            spairs = sorted(pairs, key=lambda x: x[0])
            d, v = zip(*spairs)

            if show_results:
                print(street_name)
                for ii in range(3):
                    print(d[ii], v[ii])
                input()

            return list(zip(d[:n], v[:n]))
        else:
            return []


def normalize_address(address):
    """
    Try to normalize `address` by removing meaningless words and
    converting large names into shorter ones used in the cadastre
    """
    # A list of meaningless words
    empty_words = ["DE", "DEL", "EL", "LA", "LOS", "LAS"]
    # A list of knwon abbreviation, taken from:
    # http://www.catastro.meh.es/ws/webservices_catastro.pdf (Annex I)
    street_types = {'ACCESO': ['AC'], 'AGREGADO': ['AG'], 'ALDEA': ['AL'],
                    'ALAMEDA': ['AL'], 'ANDADOR': ['AN'], 'AREA': ['AR'],
                    'ARRABAL': ['AR'], 'AUTOPISTA': ['AU'], 'AVENIDA': ['AV'],
                    'ARROYO': ['AY'], 'BAJADA': ['BJ'], 'BLOQUE': ['BL'],
                    'BARRIO': ['BO'], 'BARRANQUIL': ['BQ'], 'BARRANCO': ['BR'],
                    'CAÑADA': ['CA'], 'COLEGIO': ['CG', 'CO'], 'CIGARRAL': ['CG'],
                    'CHALET': ['CH'], 'CINTURON': ['CI'], 'CALLEJA': ['CJ'],
                    'CALLEJON': ['CJ'], 'C\\': ['CL'], 'CALLE': ['CL'], 'CAMINO': ['CM'],
                    'CARMEN': ['CARMEN'], 'COLONIA': ['CN'], 'CONCEJO': ['CO'],
                    'CAMPA': ['CP'], 'CAMPO': ['CP'], 'CARRETERA': ['CR'],
                    'CARRERA': ['CARRERA'], 'CASERIO': ['CS'], 'CUESTA': ['CT'],
                    'COSTANILLA': ['CT'], 'CONJUNTO': ['CU'], 'CALEYA': ['CY'],
                    'CALLIZO': ['CZ'], 'DETRÁS': ['DE'], 'DIPUTACION': ['DP'],
                    'DISEMINADOS': ['DS'], 'EDIFICIOS': ['ED'], 'EXTRAMUROS': ['EM'],
                    'ENTRADA': ['EN'], 'ENSANCHE': ['EN'], 'ESPALDA': ['EP'],
                    'EXTRARRADIO': ['ER'], 'ESCALINATA': ['ES'], 'EXPLANADA': ['EX'],
                    'FERROCARRIL': ['FC'], 'FINCA': ['FN'], 'GLORIETA': ['GL'],
                    'GRUPO': ['GR'], 'GRAN VIA': ['GV'], 'HUERTA': ['HT'],
                    'HUERTO': ['HT'], 'JARDINES': ['JR'], 'LAGO': ['LA'],
                    'LADO': ['LD'], 'LADERA': ['LD'], 'LUGAR': ['LG'],
                    'MALECON': ['MA'], 'MERCADO': ['MC'], 'MUELLE': ['ML'],
                    'MUNICIPIO': ['MN'], 'MASIAS': ['MS'], 'MONTE': ['MT'],
                    'MANZANA': ['MZ'], 'POBLADO': ['PB'], 'PLACETA': ['PC'],
                    'PARTIDA': ['PD'], 'PARTICULAR': ['PI'], 'PASAJE': ['PJ'],
                    'PASADIZO': ['PJ', 'PU'], 'POLIGONO': ['PL'], 'PARAMO': ['PM'],
                    'PARROQUIA': ['PQ'], 'PARQUE': ['PQ'], 'PROLONGACION': ['PR'],
                    'CONTINUAC': ['PR'], 'PASEO': ['PS'], 'PUENTE': ['PT'],
                    'PLAZA': ['PZ'], 'QUINTA': ['QT'], 'RACONADA': ['RA'],
                    'RAMBLA': ['RB'], 'RINCON': ['RC'], 'RINCONA': ['RC'],
                    'RONDA': ['RD'], 'RAMAL': ['RM'], 'RAMPA': ['RP'],
                    'RIERA': ['RR'], 'RUA': ['RU'], 'SALIDA': ['SA'], 'SECTOR': ['SC'],
                    'SENDA': ['SD'], 'SOLAR': ['SL'], 'SALON': ['SN'],
                    'SUBIDA': ['SU'], 'TERRENOS': ['TN'], 'TORRENTE': ['TO'],
                    'TRAVESIA': ['TR'], 'TRAVESÍA': ['TR'],
                    'URBANIZACION': ['UR'], 'URBANIZACIÓN': ['UR'], 'VALLE': ['VA'],
                    'VIADUCTO': ['VD'], 'VIA': ['VI'], 'VIAL': ['VL'], 'VEREDA': ['VR']}
    address = address.upper()
    # Convert first word, if we know how to
    normalized = set()
    parts = address.split()
    if address.startswith('GRAN VIA'):
        normalized.add(' '.join(street_types['GRAN VIA'] + parts[2:]))
    elif parts[0].replace('.', '') in street_types:
        for abbreviation in street_types[parts[0].replace('.', '')]:
            normalized.add(' '.join([abbreviation] + parts[1:]))

    # Filter out empty words
    for name in normalized:
        yield ' '.join([part for part in name.split() if part not in empty_words])

    if address not in normalized:
        yield ' '.join([part for part in address.split() if part not in empty_words])


def main():
    # Read data
    c = SpanishCadastreFetcher(online=True)
    # Find a single municipality and find the path to its data
    query = c.dbsession.query(Municipality).filter(Municipality.name == 'ALMENDRALEJO')
    if query.count() != 1:
        sys.stderr.write('Municipality not found, quitting\n')
        return 1

    session = c.get_municipality_session(query[0])
    # Get a list of all the street names in the municipality
    query = session.query(Carvia)
    # for street in streets:
    #     print(street)
    street = 'Travesía de la ZARZA'
    min_d = [9e20, None]
    for normalized in normalize_address(street):
        suggestions = suggest_street_name(normalized, query.all(), 5)
        # We store the street whose suggestion best fits the given query string
        # But we could be suggesting a few strings and let the user decide
        min_d = min(min(suggestions, key=lambda x: x[0]), min_d, key=lambda x: x[0])

    print('Best match: {}'.format(min_d))

if __name__ == "__main__":
    sys.exit(main())
