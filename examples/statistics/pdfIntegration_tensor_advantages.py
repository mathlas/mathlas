#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.chrono import Chrono
import sys
import numpy as np


if __name__ == "__main__":

    chrono = Chrono()
    a = np.arange(60.).reshape(3, 4, 5)
    b = np.arange(24.).reshape(4, 3, 2)

    ###########################################################################

    sys.stdout.write('Product using tensordot ...\n')
    chrono.click()
    for ii in range(10000):
        c = np.tensordot(a, b, axes=([1, 0], [0, 1]))
    chrono.click()
    sys.stdout.write('Time required: {} seconds\n'.format(chrono.lapse_str))

    ###########################################################################

    # A slower but equivalent way of computing the same...
    sys.stdout.write('Product using conventional loops ...\n')
    chrono.click()
    for ii in range(10000):
        d = np.zeros((5, 2))
        for i in range(5):
            for j in range(2):
                for k in range(3):
                    for n in range(4):
                        d[i, j] += a[k, n, i] * b[n, k, j]
    chrono.click()
    sys.stdout.write('Time required: {} seconds\n'.format(chrono.lapse_str))

    ###########################################################################

    sys.stdout.write("{} \n".format(c == d))
