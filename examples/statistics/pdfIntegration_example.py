# -*- coding: utf-8 -*-

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.statistics.pdf_integration import PdfIntegration
from mathlas.chrono import Chrono
import sys
import numpy as np


def f(xi):

    return xi[:, 0] * xi[:, 1] + xi[:, 0] * xi[:, 2] + xi[:, 1] * xi[:, 2]


def g(xi):

    return xi[:, 0] * xi[:, 1] * xi[:, 2]


def h(xi):

    return xi[:, 0] * xi[:, 1] / 3. + xi[:, 1] / 9.


def adapter(x, model):

    # Convert data to correct type for one or more dimensions
    if not isinstance(x, np.ndarray):
        raise TypeError("Values of the co-ordinates must be provided as a " +
                        "NumPy array")
    elif len(x.shape) == 1:
        x = np.array([x])
    elif len(x.shape) > 2:
        raise ValueError("Inputs does not have the correct shape.")

    return model(x)


def evaluate_errors(F0, F1):

    v = F0[0]
    if F1[1] == F0[1]:
        v_seq = F1[0]
    else:
        v_seq = F1[0].T
    maxE = np.max(v - v_seq)
    aveE = np.linalg.norm(v - v_seq) / np.prod(v.shape)

    return maxE, aveE


if __name__ == "__main__":

    sys.stdout.write('Initializing parameters ...\n')

    nPoints = 31
    span = (0, 1)
    chrono = Chrono()

    ###########################################################################

    sys.stdout.write('Defining meshes...\n')
    chrono.click()
    pdfInt = PdfIntegration()
    pdfInt.defineMesh("t", span, nPoints)
    pdfInt.defineMesh("x", span, nPoints)
    pdfInt.defineMesh("y", span, nPoints)
    pdfInt.defineMesh("z", span, nPoints)
    chrono.click()
    sys.stdout.write('Time required: {} seconds\n'.format(chrono.lapse_str))

    ###########################################################################

    sys.stdout.write('Evaluating functions (sequential) ...\n')
    chrono.click()
    pdfInt.evaluatePdf_seq(adapter, ["t", "x", "z"], "f", **{"model": f})
    pdfInt.evaluatePdf_seq(adapter, ["x", "y", "z"], "g", **{"model": g})
    pdfInt.evaluatePdf_seq(adapter, ["t", "y"], "h", **{"model": h})
    chrono.click()
    sys.stdout.write('Time required: {} seconds\n'.format(chrono.lapse_str))

    ###########################################################################

    sys.stdout.write('Evaluating functions (one step) ...\n')
    chrono.click()
    pdfInt.evaluatePdf_one(adapter, ["t", "x", "z"], "f", **{"model": f})
    pdfInt.evaluatePdf_one(adapter, ["x", "y", "z"], "g", **{"model": g})
    pdfInt.evaluatePdf_one(adapter, ["t", "y"], "h", **{"model": h})
    chrono.click()
    sys.stdout.write('Time required: {} seconds\n'.format(chrono.lapse_str))

    ###########################################################################

    sys.stdout.write('Performing integration (tensordot) ...\n')
    chrono.click()
    vseq, rseq = pdfInt.fastIntegrate(["f", "g"], ["x", "z"], "h seq")
    chrono.click()
    sys.stdout.write('Time required: {} seconds\n'.format(chrono.lapse_str))

    sys.stdout.write('Compare results (tensordot) ...\n')
    maxE, aveE = evaluate_errors(pdfInt.functions["h"],
                                 pdfInt.functions["h seq"])

    sys.stdout.write('Average error is {}\n'.format(aveE))
    sys.stdout.write('Maximum error is {}\n'.format(maxE))

    ###########################################################################

    sys.stdout.write('Performing integration (sequential) ...\n')
    chrono.click()
    vseq, rseq = pdfInt.integrate(["f", "g"], [], ["x", "z"], "h seq")
    chrono.click()
    sys.stdout.write('Time required: {} seconds\n'.format(chrono.lapse_str))

    sys.stdout.write('Compare results (sequential) ...\n')
    maxE, aveE = evaluate_errors(pdfInt.functions["h"],
                                 pdfInt.functions["h seq"])

    sys.stdout.write('Average error is {}\n'.format(aveE))
    sys.stdout.write('Maximum error is {}\n'.format(maxE))
