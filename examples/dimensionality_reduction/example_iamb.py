# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
from time import time
from mathlas.misc import ansi_color
from mathlas.dimensionality_reduction.iamb import iamb, lambda_iamb
from mathlas.statistics.probability_distribution import get_probabilities
from examples.dimensionality_reduction.asian_network import (produce_data_asian_network,
                                                             produce_probability_asian_network)


def analyze_resulting_markov_blanket(actual_mb, calculated_mb, all_vars, len_actual_mb, time,
                                     prefix):

    TP = len(calculated_mb.intersection(actual_mb))
    FP = len(calculated_mb.difference(actual_mb))
    FN = len(actual_mb.difference(calculated_mb))
    TN = len(all_vars.difference(calculated_mb.union(actual_mb)))

    if len_actual_mb == TP and FN == 0 and FP == 0:
        c1 = ansi_color.BOLD + ansi_color.CYAN
    else:
        if FN + FP < len_actual_mb:
            c1 = ansi_color.BOLD + ansi_color.YELLOW
        else:
            c1 = ansi_color.BOLD + ansi_color.RED
    c2 = ansi_color.END

    string = prefix + " "
    for v in [TP, TN, FP, FN]:
        string += c1 + "{0:2}".format(v) + c2 + " "
    sys.stdout.write(string + " {0:.2e} \n".format(time))


if __name__ == "__main__":

    # Define solutions to the problem of finding the Markov blankets of several variables
    markov_blankets = {"a": {"t"},
                       "s": {"l", "b"},
                       "t": {"a", "e", "l"},
                       "l": {"s", "e", "t"},
                       "b": {"s", "d", "e"},
                       "e": {"t", "l", "x", "d", "b"},
                       "x": {"e"},
                       "d": {"e", "b"}}

    # Generate databases
    time_via_counting = {}
    time_via_dt = {}
    nsamples_list = [100, 1000, 10000, 100000, 1000000]
    database = {}
    for n_samples in nsamples_list:
        database[n_samples] = produce_data_asian_network(n_samples)

    # Calculate
    for var, solution in markov_blankets.items():
        # Initialize variables
        remaining_variables = set(database[nsamples_list[0]].columns).difference({var})
        len_mb = len(solution)
        sys.stdout.write("\nTarget variable is {}. Cardinal of its MB is {}.\n".format(var, len_mb))
        sys.stdout.write("                      TP TN FP FN  CPU cost\n")

        for flag in [True, False]:
            surname = "Global" if flag else "Local "

            # Calculate MB for analytical model
            t0 = time()
            mb = iamb(None, var, produce_probability_asian_network, calculate_global_distribution=True)
            t1 = time() - t0
            analyze_resulting_markov_blanket(solution, mb, remaining_variables, len_mb, t1,
                                             "Analytical     {}".format(surname))
        # Calculate MB for various samples sizes
        for n_samples in nsamples_list:
            # Cases using the counting-generated probability distributions
            for flag in [True, False]:
                surname = "Global" if flag else "Local "
                # Calculate MB for counting model
                t0 = time()
                mb = iamb(database[n_samples], var, get_probabilities, calculate_global_distribution=flag)
                t1 = time() - t0
                analyze_resulting_markov_blanket(solution, mb, remaining_variables, len_mb, t1,
                                                 "Counts {0:7} {1}".format(n_samples, surname))
            # Cases using the counting-generated probability distributions
            for flag in [True, False]:
                surname = "Global" if flag else "Local "
                # Calculate MB for counting model
                t0 = time()
                mb = lambda_iamb(database[n_samples], var, get_probabilities, calculate_global_distribution=flag, lambda_value=0.5)
                t1 = time() - t0
                analyze_resulting_markov_blanket(solution, mb, remaining_variables, len_mb, t1,
                                                 "H      {0:7} {1}".format(n_samples, surname))
            # # Cases using the decision-tree-generated probability distributions
            # for flag in [True, False]:
            #     surname = "Global" if flag else "Local "
            #     # Calculate MB for the decision tree model
            #     t0 = time()
            #     mb = iamb(database[n_samples], var, ProbabilityDistributionLikeDT,
            #               calculate_global_distribution=flag)
            #     t1 = time() - t0
            #     analyze_resulting_markov_blanket(solution, mb, remaining_variables, len_mb, t1,
            #                                      "DT     {0:7} {1}".format(n_samples, surname))
