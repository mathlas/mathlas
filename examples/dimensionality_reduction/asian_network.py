# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import numpy as np
import pandas as pd
from itertools import product
from mathlas.statistics.probability_distribution import get_probabilities, ProbabilityDistribution


def produce_data_asian_network(npoints, random_seed=211104):

    np.random.seed(random_seed)
    df = pd.DataFrame(np.zeros((npoints, 8)), index=range(npoints),
                      columns=["a", "t", "s", "l", "b", "e", "x", "d"])
    # visit Asia
    df["a"] = np.random.choice([False, True], p=[0.99, 0.01], size=npoints)
    # tuberculosis
    t_at = np.random.choice([False, True], p=[0.95, 0.05], size=npoints)
    t_af = np.random.choice([False, True], p=[0.99, 0.01], size=npoints)
    mask_at = df["a"].values
    mask_af = np.logical_not(mask_at)
    df.loc[mask_at, "t"] = t_at[mask_at]
    df.loc[mask_af, "t"] = t_af[mask_af]
    # smoking
    df["s"] = np.random.choice([False, True], p=[0.50, 0.50], size=npoints)
    # lung cancer
    l_st = np.random.choice([False, True], p=[0.90, 0.10], size=npoints)
    l_sf = np.random.choice([False, True], p=[0.99, 0.01], size=npoints)
    mask_st = df["s"].values
    mask_sf = np.logical_not(mask_st)
    df.loc[mask_st, "l"] = l_st[mask_st]
    df.loc[mask_sf, "l"] = l_sf[mask_sf]
    # bronchitis
    b_st = np.random.choice([False, True], p=[0.40, 0.60], size=npoints)
    b_sf = np.random.choice([False, True], p=[0.70, 0.30], size=npoints)
    mask_bt = df["s"].values
    mask_bf = np.logical_not(mask_bt)
    df.loc[mask_bt, "b"] = b_st[mask_bt]
    df.loc[mask_bf, "b"] = b_sf[mask_bf]
    # either tuberculosis or lung cancer
    mask_t = np.logical_or(df["t"].values, df["l"].values)
    mask_f = np.logical_not(mask_t)
    df.loc[mask_t, "e"] = np.array([True] * len(mask_t))
    df.loc[mask_f, "e"] = np.array([False] * len(mask_t))
    # dyspnoea
    d_et_bt = np.random.choice([False, True], p=[0.10, 0.90], size=npoints)
    d_et_bf = np.random.choice([False, True], p=[0.30, 0.70], size=npoints)
    d_ef_bt = np.random.choice([False, True], p=[0.20, 0.80], size=npoints)
    d_ef_bf = np.random.choice([False, True], p=[0.90, 0.10], size=npoints)
    mask_et_bt = np.logical_and(df["e"].values, df["b"].values)
    mask_et_bf = np.logical_and(df["e"].values, np.logical_not(df["b"].values))
    mask_ef_bt = np.logical_and(np.logical_not(df["e"].values), df["b"].values)
    mask_ef_bf = np.logical_and(np.logical_not(df["e"].values), np.logical_not(df["b"].values))
    df.loc[mask_et_bt, "d"] = d_et_bt[mask_et_bt]
    df.loc[mask_et_bf, "d"] = d_et_bf[mask_et_bf]
    df.loc[mask_ef_bt, "d"] = d_ef_bt[mask_ef_bt]
    df.loc[mask_ef_bf, "d"] = d_ef_bf[mask_ef_bf]
    # positive x-ray
    x_et = np.random.choice([False, True], p=[0.02, 0.98], size=npoints)
    x_ef = np.random.choice([False, True], p=[0.95, 0.05], size=npoints)
    mask_et = df["e"].values
    mask_ef = np.logical_not(mask_et)
    df.loc[mask_et, "x"] = x_et[mask_et]
    df.loc[mask_ef, "x"] = x_ef[mask_ef]

    return df


def produce_probability_asian_network(*args, **kwargs):

    probabilities = {}
    labels = ["a", "s", "t", "l", "b", "e", "x", "d"]
    for state in product([True, False], repeat=8):
        a, s, t, l, b, e, x, d = state
        p = 1.
        # visit Asia?
        f = .01
        f = f if a else 1 - f
        p *= f
        # Tuberculosis?
        f = .05 if a else .01
        f = f if t else 1 - f
        p *= f
        # Smoking?
        f = .5
        f = f if s else 1 - f
        p *= f
        # Lung cancer?
        f = .10 if s else .01
        f = f if l else 1 - f
        p *= f
        # Bronchitis?
        f = .60 if s else .30
        f = f if b else 1 - f
        p *= f
        # Either tuberculosis or Lung cancer?
        f = 1. if (l or t) else 0.
        f = f if e else 1 - f
        p *= f
        # positive X-ray?
        f = .98 if e else .05
        f = f if x else 1 - f
        p *= f
        # Dyspnoea?
        f = .90 if (e and b) else (.70 if (e and not b) else (.80 if (not e and b) else .10))
        f = f if d else 1 - f
        p *= f

        # Store result
        probabilities[state] = p

    return ProbabilityDistribution(probabilities, labels)


if __name__ == "__main__":

    df = produce_data_asian_network(10000)
    analytic_distribution = produce_probability_asian_network()

    target = "l"
    v1, v2, v3 = "e", "a", "b"
    variables = [target, v1, v2]
    variables_to_marginalize = variables.copy()
    variables_to_marginalize.pop(variables_to_marginalize.index(target))

    # Calculate probability distributions and compare them
    p_tea = get_probabilities(df, variables)
    p_t = get_probabilities(df, [target])
    p_t2 = p_tea.marginalize(variables_to_marginalize)
    sys.stdout.write("Marginalized probability distribution {}\n".format(p_t))
    assertion = 'No' if p_t2 == p_t else 'Yes'
    sys.stdout.write("Does it matter how you calculate it? {}\n".format(assertion))

    # Calculate entropies and compare them
    h_t = p_t.get_entropy()
    h_t2 = p_tea.marginalize(variables_to_marginalize).get_entropy()
    sys.stdout.write("Entropy of target probability distribution {}\n".format(h_t))
    assertion = 'No' if np.isclose(h_t, h_t2) else 'Yes'
    sys.stdout.write("Does it matter how you calculate it? {}\n".format(assertion))

    # Calculate mutual information and compare them
    i_te = p_tea.get_mutual_information([target], [v1])
    p_te = get_probabilities(df, [target, v1])
    i_te2 = p_te.get_mutual_information([target], [v1])
    sys.stdout.write("Mutual information of target probability distribution {}\n".format(i_te))
    assertion = 'No' if np.isclose(i_te, i_te2) else 'Yes'
    sys.stdout.write("Does it matter how you calculate it? {}\n".format(assertion))

    # Calculate conditional mutual information and compare them
    c_tea = p_tea.get_conditional_mutual_information([target], [v1], [v2])
    p_teab = get_probabilities(df, [target, v1, v2, v3])
    c_tea2 = p_tea.get_conditional_mutual_information([target], [v1], [v2])
    sys.stdout.write("Conditional mutual information of target probability distribution {}\n".format(c_tea))
    assertion = 'No' if np.isclose(c_tea, c_tea2) else 'Yes'
    sys.stdout.write("Does it matter how you calculate it? {}\n".format(assertion))

    # Calculate Kullback-Leibler divergence
    variables = analytic_distribution.variables
    for case in [(1000, "1k"), (10000, "10k"), (100000, "100k"), (1000000, "1M")]:
        nsamples, label = case
        df = produce_data_asian_network(nsamples)
        counting_model = get_probabilities(df, variables)
        kld = analytic_distribution.get_kullback_leibler_divergence(counting_model)
        sys.stdout.write("The value of the KL divergence for a model"
                         " using {} points is {}\n".format(label, kld))
