# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
import numpy as np
import pandas as pd
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.machine_learning.neural_network import NeuralNetwork
from mathlas.machine_learning.cross_validation import KFoldCrossValidation
from mathlas.examples.machine_learning.aux_ops import (train_nn, predict_nn_regression,
                                                       eval_bishop_function, eval_oefa_function)


if __name__ == "__main__":

    for case in ["straight Bishop", "reversed Bishop", "oefa"]:

        if case == "straight Bishop":
            df = eval_bishop_function(n_points=1000)
            n_components = 1
        elif case == "reversed Bishop":
            df = eval_bishop_function(n_points=1000)
            n_components = 3
            df.columns = ["output", "input"]
        elif case == "oefa":
            df = eval_oefa_function(n_points=1000)
            n_components = 2
        else:
            raise ValueError("Case not recognized")
        x = np.linspace(df["input"].min(), df["input"].max(), 401)
        prediction = pd.DataFrame(x, columns=["input"])

        ########################################################################################

        nn = NeuralNetwork(df, "output", problem_type="Gaussian regression",
                           n_components=n_components)
        # nn.check_numerical_gradient(n_units=(5,), activation_functions=["sigmoid", "linear"])
        nn.search(n_units=(5,), activation_functions=["sigmoid", "linear"],
                  training_params=((100, 1, "enhanced gradient descent"),
                                   (50, 5, "enhanced gradient descent"),
                                   (20, 5, "nonlinear conjugate gradient"),
                                   (10, 15, "nonlinear conjugate gradient"),
                                   (1, 8, "nonlinear conjugate gradient")))

        y_a = nn.predict(prediction)

        y_d = nn.predict(df, guay=False)
        mask = np.random.rand(1000) < 1.5
        c = nn.gaussian_cost(nn.results_array[:, mask], y_d)
        print("TRAINED", c)
        y_d[2, :] = 0.5
        c = nn.gaussian_cost(nn.results_array[:, mask], y_d)
        print("LOLES", c)
        y_d[1, :] = 0.25
        c = nn.gaussian_cost(nn.results_array[:, mask], y_d)
        print("LOLES", c)

        ########################################################################################

        plt = MathlasPlot()
        plt.scatter(df["input"].values, df["output"].values)
        plt.plots(x, [y_a["mu 0"].values,
                      y_a["mu 0"].values + 3. * y_a["sigma 0"].values,
                      y_a["mu 0"].values - 3. * y_a["sigma 0"].values],
                  colors=["mathlas blue", "mathlas orange", "mathlas orange"])
        plt.show()
        input("here")

        ########################################################################################

        nUnits = [(3,), (6,), (3, 3), (3, 6), (6, 3), (6, 6), (3, 6, 3)]

        train_args = []
        for nu in nUnits:
            train_args.append({"problem_type": "regression", "n_units": nu,
                               "activation_functions": tuple(len(nu) * ["sigmoid"] + ["linear"])})

        ########################################################################################

        t0 = time.time()
        cv = KFoldCrossValidation(train_nn, predict_nn_regression, df, "output",
                                  number_of_partitions=2, metrics="L2 error",
                                  partition_method="alternate", train_args=train_args)
        t1 = time.time()
        training_time = t1 - t0

        ########################################################################################
        figures_of_merit = cv.figures_of_merit["L2 error"].copy()
        figures_of_merit.index = [d["n_units"] for d in cv.figures_of_merit.index]

        optimal_params = cv.figures_of_merit["L2 error"].idxmin()
        sys.stdout.write('\nCurrent case: {}\n'.format(case))
        sys.stdout.write("\tWall time spent in training method: {:.2f}\n".format(training_time))
        sys.stdout.write('\t\tOptimal CV params: {}\n'.format(optimal_params))
        sys.stdout.write('\t\tCross Validation figures of merit:\n')
        sys.stdout.write('{}\n'.format(figures_of_merit.to_string()))

        ########################################################################################

        plt = MathlasPlot()
        plt.scatter(df["input"].values, df["output"].values)
        for args in train_args:
            for model in cv.models[frozenset(args.items())]:
                cp = predict_nn_regression(model, prediction.copy())
                if args == optimal_params:
                    plt.plot(x, cp, color="g", linewidth=.2, transparency=True)
                else:
                    plt.plot(x, cp, color="k", linewidth=.2, transparency=True)
        cp_g = cv.predict(prediction.copy(), method="global", train_if_not_available=True)
        cp_a = cv.predict(prediction.copy(), method="average", train_if_not_available=False)
        plt.plots(x, [cp_a["output"].values, cp_g["output"].values],
                  var_labels=["Averaged model", "Global model"])

    plt.show()

    sys.exit(0)
