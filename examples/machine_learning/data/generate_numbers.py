# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


def draw_zero(random_seed):

    # Initialize random numbers
    np.random.seed(random_seed)
    # Rotation sign. Direction in which the zero is plotted
    rot_sign = np.random.choice([+1, -1], p=[0.3, 0.7])
    # Where does the dwaring begins
    theta_ref = np.pi / 6. * np.random.rand() + np.random.choice([0., np.pi], p=[0.5, 0.5])
    # Semi-axis vary linearly
    d0 = np.random.rand()
    d1 = np.random.rand()
    a0 = 0.8 + 0.2 * d0
    a1 = 0.8 + 0.2 * d1
    b0 = 0.3 + 0.7 * d0
    b1 = 0.3 + 0.7 * d1
    # Rotation of the drawing
    theta_rot = np.pi / 6. * (2. * np.random.rand() - 1.)
    A = np.array([[np.cos(theta_rot), -np.sin(theta_rot)],
                  [np.sin(theta_rot), np.cos(theta_rot)]])
    # Number of points to be plotted
    n_points = int(75 + 50 * np.random.rand())
    n_points_base = int(n_points - 5 + 15 * np.random.rand())
    coords = []
    for i in range(n_points):
        theta = rot_sign * i * 2 * np.pi / (n_points_base - 1) - theta_ref
        a = a0 + (a1 - a0) * i / (n_points - 1.)
        b = b0 + (b1 - b0) * i / (n_points - 1.)
        xi = np.array([b * np.sin(theta), a * np.cos(theta)])
        x = A.dot(xi)
        coords.append(x)

    return np.array(coords)


def draw_one(random_seed):

    # Initialize random numbers
    np.random.seed(random_seed)
    # Angle of downward line
    theta_down = np.pi / 10. * (1.5 * np.random.rand() - 0.5)
    # Should draw the upwards line?
    upwards_line = np.random.rand() < 0.4
    # Parameter of upward line
    if upwards_line:
        theta_up = np.pi / 5. * np.random.rand()
        length_up = 0.2 + 0.8 * np.random.rand()

    # Number of points to be plotted
    if upwards_line:
        n_points = int(50 + 50 * np.random.rand())
        n_up = int(n_points * (length_up / (2. + length_up)))
    else:
        n_points = int(40 + 40 * np.random.rand())
        n_up = 0

    coords = []
    for i in range(n_points):
        if i < n_up:
            theta = theta_up + theta_down
            r = length_up * (1. - i / n_up)
            x = -r * np.sin(theta)
            y = 1. - r * np.cos(theta)
        else:
            theta = theta_down
            r = 2 * (i - n_up) / (n_points - n_up - 1.)
            x = -r * np.sin(theta)
            y = 1. - r * np.cos(theta)
        coords.append([x, y])

    return np.array(coords)


def draw_six(random_seed):

    # Initialize random numbers
    np.random.seed(random_seed)
    # Where does the drawing begins
    theta_start = -np.pi / 6. * (np.random.rand() + 1./ 2.)
    # Where does the circle-like part ends
    theta_detached = -np.pi / 12. * (3 * np.random.rand() + 2.)
    # Semi-axis vary linearly
    d0 = np.random.rand()
    d1 = np.random.rand()
    a0 = 0.4 + 0.2 * d0
    a1 = 0.4 + 0.2 * d1
    b0 = 0.3 + 0.3 * d0
    b1 = 0.3 + 0.3 * d1
    # Rotation of the drawing
    theta_rot = np.pi / 8. * np.random.rand()
    A = np.array([[np.cos(theta_rot), -np.sin(theta_rot)],
                  [np.sin(theta_rot), np.cos(theta_rot)]])
    # Size of the steps in the final stroke
    delta_stroke = 0.035 + 0.01 * np.random.rand()
    # Rate of "curviness" of the final stroke
    slope_stroke = 0.015 * np.random.rand()
    # Number of points to be plotted
    n_points_circle = int(50 + 50 * np.random.rand())
    n_points_stroke = int(25 + 25 * np.random.rand())
    coords = []
    delta_theta = theta_detached - theta_start
    for i in range(n_points_circle):
        theta = theta_start + (2. * np.pi + delta_theta) * i / (n_points_circle - 1)
        a = a0 + (a1 - a0) * i / (n_points_circle - 1.)
        b = b0 + (b1 - b0) * i / (n_points_circle - 1.)
        xi = np.array([b * np.sin(theta), - 0.5 + a * np.cos(theta)])
        x = A.dot(xi)
        coords.append(x)
    x0, y0 = coords[-1]
    x1, y1 = coords[-2]
    theta_stroke = np.arctan2(y1-y0, x1-x0)
    for i in range(n_points_stroke):
        theta = theta_stroke - slope_stroke * i
        x = [x0 - i * delta_stroke * np.cos(theta), y0 - i * delta_stroke * np.sin(theta)]
        coords.append(x)
    coords = list(reversed(coords))

    return np.array(coords)

def draw_seven(random_seed):

    # Initialize random numbers
    np.random.seed(random_seed)
    # Angle of horizontal line
    theta_hor = np.pi / 12. * (2. * np.random.rand() - 1.)
    # Length of horizontal line
    length_hor = 0.8 + 0.5 * np.random.rand()
    # Angle of downward line
    theta_down = np.pi / 6. * (1. - np.random.rand())
    # Should draw the crossing line?
    crossing_line = np.random.rand() < 0.7
    # Parameter of upward line
    if crossing_line:
        theta_crossing = np.pi / 24. * (2. * np.random.rand() - 1.) + theta_hor
        length_crossing = 0.15 + 0.05 * np.random.rand()
        height_crossing = 0.2 * (2. * np.random.rand() - 1.)

    # Number of points to be plotted
    if crossing_line:
        n_points = int(100 + 50 * np.random.rand())
        n_crossing = int(n_points * (length_crossing / (2. + length_crossing + length_hor)))
        n_hor = int(n_points * length_hor / (2. + length_crossing + length_hor))
    else:
        n_points = int(75 + 50 * np.random.rand())
        n_crossing = 0
        n_hor = int(n_points * length_hor / (2. + length_hor))

    coords = []
    for i in range(n_points):
        if i < n_hor:
            theta = theta_hor
            r = length_hor * (1. - i / n_hor)
            x = 0.5 * length_hor - r * np.cos(theta)
            y = 1. - r * np.sin(theta)
            x0, y0 = x, y
        elif i < n_points - n_crossing:
            theta = theta_down
            r = 2. * (i - n_hor) / (n_points - n_hor - n_crossing)
            x = 0.5 * length_hor - r * np.sin(theta)
            y = 1. - r * np.cos(theta)
            x1, y1 = x, y
        else:
            theta = theta_crossing
            r = length_crossing * (2. * (n_points - i) / (n_crossing - 1.) - 1.)
            f = (height_crossing - y0) / (y1 - y0) * (x1 - x0) + x0 + length_crossing * 0.75
            x = f - r * np.cos(theta)
            y = height_crossing - r * np.sin(theta)

        coords.append([x, y])

    return np.array(coords)


def draw_eight(random_seed):

    # Initialize random numbers
    np.random.seed(random_seed)
    # Rotation sign. Direction in which the zero is plotted
    rot_sign = np.random.choice([+1, -1], p=[0.3, 0.7])
    # Where does the dwaring begins
    theta_ref = np.pi * (0.5 - 0.25 * np.random.rand()) + np.random.choice([0., np.pi], p=[0.5, 0.5])
    # Semi-axis vary linearly
    a0 = 0.5 + 0.5 * np.random.rand()
    a1 = 0.7 + 0.3 * np.random.rand()
    # Rotation of the drawing
    theta_rot = np.pi / 10. * (2. * np.random.rand() - 1.)
    A = np.array([[np.cos(theta_rot), -np.sin(theta_rot)],
                  [np.sin(theta_rot), np.cos(theta_rot)]])
    # Number of points to be plotted
    n_points = int(75 + 50 * np.random.rand())
    n_points_base = int(n_points - 5 + 15 * np.random.rand())
    coords = []
    for i in range(n_points):
        n = i / (n_points_base - 1)
        theta = rot_sign * 2 * np.pi * (n - 0.25) - theta_ref

        a = a0 + (a1 - a0) * i / (n_points - 1.)
        f = 1 + np.sin(theta) ** 2
        xi = np.array([a * np.sin(theta) * a * np.cos(theta) / f, np.cos(theta) / f])
        x = A.dot(xi)
        coords.append(x)

    return np.array(coords)


def update_line(f, line, time_text, time_template, flag):
    """
    animation function.  This is called sequentially

    Parameters
    ----------
    f : method
        provides points iterativelly
    line : Matplotlib plot object
           line to be plotted
    time_text : Matplotlib text object
                text to be shown
    time_template : string
                    templete of the text to be shown
    flag : Boolean
           if True scatter is used. Otherwise, plot is used

    Returns
    -------
    line, time_text : Matplotlib plot and text objects
                      stuff to be plotted
    """
    xi, eta, i = f[0], f[1], f[2]
    if flag:
        data = np.hstack((xi[:, np.newaxis], eta[:, np.newaxis]))
        line.set_offsets(data)
    else:
        line.set_data(xi, eta)
    time_text.set_text(time_template.format(i * 1e-2))
    return line, time_text


def return_points():
    """
    This function provides a piece of the sequence

    Returns
    -------
    x, y : NumPy array
           List of data
    i : integer
        number of the current iteration
    """

    global coords
    n_points = coords.shape[0]
    x, y, i = 0, 0, 0
    while i < n_points - 1:
        if not pause:
            i += 1
            x = coords[:i, 0]
            y = coords[:i, 1]

        yield x, y, i


def onClick(event):
    """
    If the user clicks the window, value of <pause> shifts.
    """
    global pause
    pause ^= True


if __name__ == "__main__":
    """
    This example plots number at random. The number are generated using a model of number writing.
     Hey, and if you click on the image the process of writing the number pauses until you click
     again!
    """
    import sys
    # Seed of the random number generator
    seed = 211104
    # Must use scatterplots or plots?
    use_scatter = True
    generators = {"0": draw_zero, "1": draw_one, "6": draw_six, "7": draw_seven, "8": draw_eight}
    probabilities = {"0": 0.20, "1": 0.20, "6": 0.20, "7": 0.20, "8": 0.20}
    # Initialize variables
    a, p = zip(*probabilities.items())
    np.random.seed(seed)
    # Loop until user closes program
    while True:
        # Get seed
        s = int(np.random.rand() * 1e7)
        # Choose number at random at get data
        number = np.random.choice(a, p=p)
        coords = generators[number](s)
        # Initialize figure
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.axis("equal")
        ax = plt.axes(xlim=(-1.3, 1.3), ylim=(-1.3, 1.3))
        fig.suptitle("Number {}".format(number))
        # Initialize plot of the data
        if use_scatter:
            drawing = ax.scatter([], [], s=5)
        else:
            drawing, = ax.plot([], [], "b", lw=2, ms=10)
        # Initialize text
        message_template = 'Time = {:.2f}'  # prints running simulation time
        message = ax.text(0.05, 0.9, '', transform=ax.transAxes)
        # Let's plot data
        pause = False
        fig.canvas.mpl_connect('button_press_event', onClick)

        # Blit = True does not work in macOS
        if sys.platform == 'darwin':
            blit = False
        else:
            blit = True

        ani = animation.FuncAnimation(fig, update_line, return_points,
                                      interval=10, blit=blit, repeat=False,
                                      fargs=(drawing, message, message_template, use_scatter))
        plt.show()


