#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import inspect
import numpy as np
import pandas as pd
from time import time
from pathlib import Path
from itertools import product
from collections import Counter
from mathlas.plotting.mathlas_plots import MathlasPlot
import matplotlib.pyplot as plt
from mathlas.machine_learning.neural_network import NeuralNetwork
from mathlas.statistics.probabilistic_choice_object import ProbabilisticChoiceObject as pco


def eval_vanilla_function(npoints=1000, seed=211104, case="circle", noise=0.):
    """
    Evaluate tests function and create a database.

    Parameters
    ----------
    npoints : integer
              Number of samples of database to be created

    seed : integer or None
           value of the random seed used to create the samples

    case : string
           Name of the function to use. It can take values "lower square", "centered square",
           "triangle", "circle", "ellipse", "U", "toy1", and "toy2".
           If any of the last two cases are used all other parameters are ignored.
           Default value is "circle".

    noise : float
            value of the Gaussian noise

    Returns
    -------
    db : Pandas DataFrame
         database
    """

    if case == "lower square":
        f = lambda x: x[0] < 0.5 and x[1] < 0.5
    elif case == "centered square":
        f = lambda x: 0.25 < x[0] < 0.75 and 0.25 < x[1] < 0.75
    elif case == "triangle":
        f = lambda x: x[0] - x[1] < 0.
    elif case == "circle":
        f = lambda x: (x[0]-0.5) ** 2 + (x[1]-0.5) ** 2 < 0.4 ** 2
    elif case == "ellipse":
        f = lambda x: (x[0]-0.25) ** 2 + ((x[1]-0.4) / 0.8) ** 2 < 0.5 ** 2
    elif case == "U":
        f = lambda x: ((0.2 <= x[0] <= 0.4 and 0.4 <= x[1] <= 0.8) or
                       (0.6 <= x[0] <= 0.8 and 0.4 <= x[1] <= 0.8) or
                       (0.2 <= x[0] <= 0.8 and 0.2 <= x[1] <= 0.4))
    elif case == "lemniscate":
        f = lambda x: (((x[0] - 0.5) ** 2 + 0.2 * (x[1] - 0.5) ** 2) ** 2 <=
                       0.25 * (x[0] - 0.5) ** 2 - 0.2 * (x[1] - 0.5) ** 2)
    elif case == "toy1":
        db = pd.DataFrame([[0.2, 0.2, "s"], [0.1, 0.5, "s"], [0.2, 0.9, "s"],
                           [0.4, 0.4, "o"], [0.5, 0.4, "o"], [0.45, 0.6, "s"],
                           [0.7, 0.3, "o"], [0.7, 0.7, "s"], [0.7, 0.8, "s"],
                           [0.9, 0.2, "o"], [0.9, 0.45, "o"], [0.9, 0.8, "o"]],
                          columns=["x1", "x2", "c"])
        return db
    elif case == "toy2":
        db = pd.DataFrame([[0., 0., "s"],
                           [0.33, 0., "s"],
                           [0.66, 0., "s"],
                           [1., 0., "s"],
                           [0., 0.33, "s"],
                           [0.33, 0.33, "o"],
                           [0.66, 0.33, "o"],
                           [1., 0.33, "s"],
                           [0., 0.66, "s"],
                           [0.33, 0.66, "o"],
                           [0.66, 0.66, "o"],
                           [1., 0.66, "s"],
                           [0., 1., "s"],
                           [0.33, 1., "s"],
                           [0.66, 1., "s"],
                           [1., 1., "s"]],
                          columns=["x1", "x2", "c"])
        return db

    if seed is None:
        x = np.linspace(0., 1., npoints)
        X = np.array(list(product(x, x)))
    else:
        np.random.seed(seed)
        X = np.random.rand(npoints, 2)
    Y = []
    for x in X:
        if f(x):
            y = 1
        else:
            y = 0
        x[0] = np.min([np.max([x[0] + noise * np.random.randn(), 0.]), 1.])
        x[1] = np.min([np.max([x[1] + noise * np.random.randn(), 0.]), 1.])
        Y.append([x[0], x[1], y])

    return pd.DataFrame(Y, columns=["x1", "x2", "c"])


def eval_multiple_function(npoints=1000, seed=211104, case="circle", noise=0.):
    """
    Evaluate tests function and create a database.

    Parameters
    ----------
    npoints : integer
              Number of samples of database to be created

    seed : integer
           value of the random seed used to create the samples

    case : string
           Name of the function to use. It can take values "lower square", "centered square",
           "triangle", "circle", "ellipse", "U", "toy1", and "toy2".
           If any of the last two cases are used all other parameters are ignored.
           Default value is "circle".

    noise : float
            value of the Gaussian noise

    Returns
    -------
    db : Pandas DataFrame
         database
    """

    if case == "circle":
        f = lambda x: False
        g = lambda x: (x[0] - 0.5) ** 2 + (x[1] - 0.5) ** 2 < 0.4 ** 2
    elif case == "lemniscate":
        f = lambda x: False
        g = lambda x: (((x[0] - 0.5) ** 2 + 0.2 * (x[1] - 0.5) ** 2) ** 2 <=
                       0.25 * (x[0] - 0.5) ** 2 - 0.2 * (x[1] - 0.5) ** 2)

    elif case == "split circle":
        f = lambda x: (x[0] - 0.5) ** 2 + (x[1] - 0.5) ** 2 < 0.4 ** 2
        g = lambda x: (x[1] - x[0])*(x[1] + x[0] - 1.) > 0
    elif case == "split lemniscate":
        f = lambda x: (((x[0] - 0.5) ** 2 + 0.2 * (x[1] - 0.5) ** 2) ** 2 <=
                       0.25 * (x[0] - 0.5) ** 2 - 0.2 * (x[1] - 0.5) ** 2)
        g = lambda x: ((x[1]-0.5) - 1.25 * (x[0]-0.5))*((x[1]-0.5) + 1.25 * (x[0]-0.5)) > 0
    else:
        raise ValueError("Case not implemented")

    if seed is None:
        x = np.linspace(0., 1., npoints)
        X = np.array(list(product(x, x)))
    else:
        np.random.seed(seed)
        X = np.random.rand(npoints, 2)
    Y = []
    for x in X:
        v1, v2 = f(x), g(x)
        if v1 and v2:
            y = "black"
        elif v1 and not v2:
            y = "blue"
        elif not v1 and v2:
            y = "green"
        else:
            y = "red"
        x[0] = np.min([np.max([x[0] + noise * np.random.randn(), 0.]), 1.])
        x[1] = np.min([np.max([x[1] + noise * np.random.randn(), 0.]), 1.])
        Y.append([x[0], x[1], y])

    return pd.DataFrame(Y, columns=["x1", "x2", "c"])


def add_one_of_k_encoding(df, result_column):
    class_labels = sorted(list(set(df[result_column])))
    n_samples, n_classes = len(df.index), len(class_labels)
    indexes = df[result_column].replace(class_labels, range(n_classes)).values
    sampled_values = np.zeros((n_samples, n_classes))
    sampled_values[np.arange(n_samples), indexes] = 1.
    sampled_values = pd.DataFrame(sampled_values, index=df.index, columns=class_labels)

    return pd.concat((df, sampled_values), axis=1)


def read_naca4412():

    current_dir = Path(inspect.getfile(inspect.currentframe())).parent
    path = current_dir / Path("data/NACA4412_Cp_613.xlsx")

    # For when Sheet1's format is identical to Sheet2
    data = {}
    for sheet_label in [u'Re=1e5', u'Re=8.2e6']:
        data[sheet_label] = pd.read_excel(path, sheet_label,
                                          index_col=None, na_values=['NA'])
        x = data[sheet_label]["x"]
        y = data[sheet_label]["y"]
        s = [0.]
        for ii in range(len(x) - 1):
            dx = (x[ii + 1] - x[ii]) / 100.
            dy = (y[ii + 1] - y[ii]) / 100.
            s.append(s[-1] + np.sqrt(dx ** 2 + dy ** 2))
        data[sheet_label]["s"] = s

    return data


def eval_naca4412_aoa0():

    # Read data
    naca4412 = read_naca4412()
    cp_high = naca4412["Re=8.2e6"]
    cp_low = naca4412["Re=1e5"]
    cp_chosen = cp_high

    # Parse data
    selected_case = u"AoA = 0º"
    df = cp_chosen[["s", selected_case]]

    return df


def eval_oefa_function(n_points=1000):
    """
    Generate data according to the example in
    http://www.ofai.at/cgi-bin/get-tr?download=1&paper=oefai-tr-98-16.ps.gz

    Parameters
    ----------
    n_points : integer
               number of points simulated

    Returns
    -------
    data : Pandas DataFrame
           data in two columns: `input` and `output`
    """
    data = np.zeros([n_points, 2])
    for index in range(n_points):
        x = 0.6 if index == 0 else y
        mu = 3. * x * (1. - x)
        sigma = 0.05 * (x ** 2 + 0.1)
        g1 = (mu + 0.01) + sigma * np.random.randn()
        g2 = (mu - 0.1) + sigma * np.random.randn()
        y = g1 if np.random.rand() < 0.2 else g2
        data[index, 0] = x
        data[index, 1] = y

    return pd.DataFrame(data, columns=["input", "output"])


def eval_bishop_function(n_points=1000):
    """
    Generate data according to Bishop's example

    Parameters
    ----------
    n_points : integer
               number of points simulated

    Returns
    -------
    data : Pandas DataFrame
           data in two columns: `input` and `output`
    """
    data = np.zeros([n_points, 2])
    np.random.seed(211104)
    x = np.random.rand(n_points)
    noise = 0.1 * (2. * np.random.rand(n_points) - 1.)
    y = x + 0.3 * np.sin(2. * np.pi * x)
    data[:, 0] = x
    data[:, 1] = (y + noise)

    return pd.DataFrame(data, columns=["input", "output"])


def plot_2contours(validation_sets, storage):
    """
    Plot the results. Samples as a scatterplot and theoretical/prediction distribution as contours.

    Parameters
    ----------
    validation_sets : Pandas DataFrame
                      theoretical/prediction database

    storage : pathlib.Path, string or None
              Path where the plot should be stored or `None`
              if you want the plot to be displayed.

    Returns
    -------
    None

    """

    plt = MathlasPlot(figsize=(15, 5))

    n_plots = len(validation_sets)

    for ii, validation_set in enumerate(validation_sets):
        v = validation_set.values
        n = int(np.sqrt(v.shape[0]))
        plt.contourf(v[:, 0].reshape((n, n)), v[:, 1].reshape((n, n)), v[:, 2].reshape((n, n)),
                     showColorBar=True, colorlist="from blue to white to orange",
                     levels=np.linspace(0., 1., 51), subplot_index=(100 + 10 * n_plots + ii + 1),
                     extend='both')

        plt.equal_aspect_ratio()

    if storage is None:
        plt.show()
    else:
        plt.savefig('{}'.format(storage))
        plt.close()


def scatterplot(df, validation_sets, storage, secret=False):

    f = plt.figure(figsize=(18, 9), dpi=80)
    n_plots = len(validation_sets)
    ax = f.add_subplot(100 + 10 * (n_plots + 1) + 1)
    values = df.values
    mask = values[:, 2] == "red"
    if np.sum(mask) > 0:
        plt.scatter(values[mask, 1], values[mask, 0], color="r")
    mask = values[:, 2] == "green"
    if np.sum(mask) > 0:
        plt.scatter(values[mask, 1], values[mask, 0], color="g")
    mask = values[:, 2] == "blue"
    if np.sum(mask) > 0:
        plt.scatter(values[mask, 1], values[mask, 0], color="b")
    plt.axis("equal")
    for ii, validation_set in enumerate(validation_sets):
        category_labels = validation_set.columns
        n_colors = np.sum([c in category_labels for c in ["red", "green", "blue"]])
        if n_colors not in [2, 3]:
            raise ValueError("Not enough colors to be plotted.")
        for c in ["red", "green", "blue"]:
            if c not in validation_set:
                validation_set[c] = pd.Series(np.zeros(len(validation_set.index)),
                                              index=validation_set.index)
        color = validation_set[["red", "green", "blue"]].values

        n = int(np.sqrt(color.shape[0]))
        ax = f.add_subplot(100 + 10 * (n_plots + 1) + ii + 2)
        c = color.reshape((n, n, 3))
        if secret:
            m = np.max(c)
            c /= m
        ax.imshow(c)
        plt.axis("equal")

    if storage is not None:
        plt.savefig(storage)
        plt.close()


def evaluate_confusion_matrix(model, actual_classes, label, show_results=True, **kwargs):
    """
    Evaluates the confusion matrix for some predictions and show it, if required.

    Parameters
    ----------
    model : object
            model of the prediction
    actual_classes : Pandas DataFrame
                     real values of the classes
    label : string, integer
            label of the column that contains the results
    show_results : Boolean
                   must results be shown?
    kwargs : dictionary
             stuff required by the `predict` method of the model

    Returns
    -------
    confusion_matrix : Pandas DataFrame
                       The confusion matrix itself, yep
    f1_score : float
               The F-1 score
    predicted_classes : Pandas DataFrame
                        predicted values of the target variable
    """

    # Use the neural network to classify
    t0 = time()
    predicted_classes = actual_classes.copy()
    predicted_classes["c"] = None
    model.predict(predicted_classes, **kwargs)
    t1 = time()
    for ii, l in enumerate(model.category_labels):
        predicted_classes[l] = predicted_classes['c'].apply(lambda x: np.float64(x.values[ii])
                                                                if (isinstance(x, pco))
                                                                else np.float64(x))

    classification_time = t1 - t0

    confusion_matrix = pd.DataFrame(np.zeros((model.n_categories, model.n_categories + 3)),
                                    index=model.category_labels,
                                    columns=model.category_labels + ["Precision", "Recall", "F1-score"])
    t0 = time()
    most_probable_outcome = np.array([max([(k, v) for k, v in d.p.items()], key=lambda x: x[1])[0]
                                      for d in predicted_classes[label]])
    for category in model.category_labels:
        sys.stdout.write("Checking results for category {}\n".format(category))
        mask = actual_classes[label].values == category
        res = Counter(most_probable_outcome[mask])
        for n in model.category_labels:
            confusion_matrix.loc[category, n] = res.get(n, 0.)

    confusion_matrix *= 100. / predicted_classes.values.shape[0]

    f1_score = 0.
    for n in model.category_labels:
        precision = confusion_matrix.loc[n, n] / np.sum(confusion_matrix.loc[:, n])
        recall = confusion_matrix.loc[n, n] / np.sum(confusion_matrix.loc[n, :])
        confusion_matrix.loc[n, "Precision"] = precision * 100.
        confusion_matrix.loc[n, "Recall"] = recall * 100.
        confusion_matrix.loc[n, "F1-score"] = 100. * 2 * precision * recall / (precision + recall)
        f1_score += 1. / confusion_matrix.loc[n, "F1-score"]
    f1_score = model.n_categories / f1_score
    tf = time()
    confusion_time = tf - t0

    # Show results, if required
    if show_results:
        sys.stdout.write("{}\n".format(confusion_matrix))
        sys.stdout.write("Harmonic F-1 score: {}\n".format(f1_score))
        sys.stdout.write("Wall time spent in classifying: {:.2f}\n".format(classification_time))
        sys.stdout.write("Wall time spent in calculating confusion matrix: {:.2f}\n".format(confusion_time))

    return confusion_matrix, f1_score, predicted_classes


def train_nn(observations, result_column, args):
    """
    Trains a Neural Network given some samples and the arguments required by the class

    Parameters
    ----------
    observations : Pandas DataFrame
                   Samples used to train the model
    result_column : string
                    label of the column in which the target function is stored
    args : dictionary
           arguments of the class

    Returns
    -------
    model : NeuralNetwork object
            The trained model
    """
    if not isinstance(args, dict):
        raise ValueError("`args` must be a dictionary")
    model = NeuralNetwork(observations, result_column, problem_type=args["problem_type"])
    args_mod = {k: v for k, v in args.items() if k != "problem_type"}
    model.search(**args_mod)
    return model


def predict_nn_regression(model, observations, provide_variance=False):
    """
    Predicts the values at some points given a Neural Network

    Parameters
    ----------
    model : NeuralNetwork object
            The trained model
    observations : Pandas DataFrame
                   samples to be predicted

    Returns
    -------
    out : NumPy array
          one-dimensional array with the results
    """
    return model.predict(observations)[0, :]


def predict_nn_classification(model, observations, provide_variance=False):
    """
    Predicts the values at some points given a Neural Network

    Parameters
    ----------
    model : NeuralNetwork object
            The trained model
    observations : Pandas DataFrame
                   samples to be predicted

    Returns
    -------
    out : Pandas DataFrame
          Predictions
    """

    return model.predict(observations).values


