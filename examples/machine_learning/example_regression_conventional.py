#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
import numpy as np
import pandas as pd
from itertools import product
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.plotting.advanced_mathlas_plots import AdvancedMathlasPlot
from mathlas.surrogate_modelling.bayesian_regression import BayesianRegression
from mathlas.machine_learning.cross_validation import KFoldCrossValidation
from mathlas.examples.machine_learning.aux_ops import eval_bishop_function, eval_naca4412_aoa0


def train(observations, result_column, args):
    """
    Trains a Neural Network given some samples and the arguments required by the class

    Parameters
    ----------
    observations : Pandas DataFrame
                   Samples used to train the model
    result_column : string
                    label of the column in which the target function is stored
    args : dictionary
           arguments of the class

    Returns
    -------
    model : NeuralNetwork object
            The trained model
    """
    if not isinstance(args, dict):
        raise ValueError("`args` must be a dictionary")
    model = BayesianRegression(observations, result_column)
    model.train(**args)
    return model


def predict(model, observations, provide_variance=False):
    """
    Predicts the values at some points given a Neural Network

    Parameters
    ----------
    model : NeuralNetwork object
            The trained model
    observations : Pandas DataFrame
                   samples to be predicted

    Returns
    -------
    out : NumPy array
          one-dimensional array with the results
    """
    return model.predict(observations, provide_variance=provide_variance)

if __name__ == "__main__":

    cases = ["naca4412", "coarse Bishop", "large Bishop"]

    for case in cases:
        if case == "coarse Bishop":
            df = eval_bishop_function(n_points=50)
            axis_labels = ("input", "output")
            range_y = (-0.2, 1.2)
        elif case == "large Bishop":
            df = eval_bishop_function(n_points=1000)
            axis_labels = ("input", "output")
            range_y = (-0.2, 1.2)
        elif case == "naca4412":
            df = eval_naca4412_aoa0()
            axis_labels = ("s, arc-length", u"$c_p$")
            range_y = (-1.2, 1.2)
        else:
            raise ValueError("Value of <case> not recognized")

        df.columns = ["input", "output"]

        # Create mesh for prediction purposes
        x = np.linspace(df["input"].min(), df["input"].max(), 401)
        x = np.array([x]).T
        prediction = pd.DataFrame(x, columns=["input"])

        #########################################################################

        list_of_basis = product(["Cubic spline", "Gaussian"],
                                [1e-3, 5e-3, 0.01, 0.05, 0.1, 0.5, 1., 5.])
        list_of_centres = [5, 9, 13, 17, 21]

        train_args = []
        for basis, centres in product(list_of_basis, list_of_centres):
            b, n = basis
            train_args.append({"rbf_label": b, "scale_parameter": n, "centres": centres,
                               "training_method": "conventional"})

        ########################################################################################

        t0 = time.time()
        cv = KFoldCrossValidation(train, predict, df, "output",
                                  number_of_partitions=3, metrics="L2 error",
                                  partition_method="alternate", train_args=train_args)
        t1 = time.time()
        training_time = t1 - t0

        ########################################################################################
        figures_of_merit = cv.figures_of_merit["L2 error"].copy()

        optimal_params = cv.figures_of_merit["L2 error"].idxmin()
        sys.stdout.write('\nCurrent case: {}\n'.format(case))
        sys.stdout.write("\tWall time spent in training method: {:.2f}\n".format(training_time))
        sys.stdout.write('\t\tOptimal CV params: {}\n'.format(optimal_params))

        ########################################################################################

        plt = MathlasPlot()
        for args in train_args:
            for model in cv.models[frozenset(args.items())]:
                cp = predict(model, prediction.copy())
                plt.plot(x, cp, color="k", linewidth=.1, transparency=True)
        y_g = cv.predict(prediction.copy(), method="global", train_if_not_available=True,
                         provide_variance=True)
        y_a = cv.predict(prediction.copy(), method="average", train_if_not_available=False,
                         provide_variance=True)
        plt.plots(x, [y_a["output"].values, y_g["output"].values],
                  var_labels=["Averaged model", "Global model"])
        plt.scatter(df["input"].values, df["output"].values, axis_labels=axis_labels,
                    title="{}\n opt. params: {}".format(case, optimal_params), range_y=range_y)

        plt = AdvancedMathlasPlot()
        plt.plot_prediction_and_uncertainty(y_g["input"].values, y_g["output"].values,
                                            3 * np.sqrt(y_g["variance"].values))
        plt.scatter(df["input"].values, df["output"].values, axis_labels=axis_labels,
                    title="{} - Global model\n opt. params: {}".format(case, optimal_params),
                    range_y=range_y)

        plt = AdvancedMathlasPlot()
        plt.plot_prediction_and_uncertainty(y_a["input"].values, y_a["output"].values,
                                            3 * np.sqrt(y_a["variance"].values))
        plt.scatter(df["input"].values, df["output"].values, axis_labels=axis_labels,
                    title="{} - Averaged model\n opt. params: {}".format(case, optimal_params),
                    range_y=range_y)

    plt.show()

    sys.exit(0)
