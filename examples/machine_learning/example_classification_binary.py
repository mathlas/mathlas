#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
import pickle
import numpy as np
from itertools import product
from mathlas.doe.lhc import LHC
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.machine_learning.cross_validation import KFoldCrossValidation
from mathlas.surrogate_modelling.bayesian_classification import BayesianClassification
from mathlas.examples.machine_learning.aux_ops import eval_vanilla_function, plot_2contours


def train(observations, result_column, args):
    """
    Trains a Neural Network given some samples and the arguments required by the class

    Parameters
    ----------
    observations : Pandas DataFrame
                   Samples used to train the model
    result_column : string
                    label of the column in which the target function is stored
    args : dictionary
           arguments of the class

    Returns
    -------
    model : NeuralNetwork object
            The trained model
    """
    if not isinstance(args, dict):
        raise ValueError("`args` must be a dictionary")
    model = BayesianClassification(observations, result_column, rescaling="normalizing")
    _args = {}
    for key in args.keys():
        if key == 'centres':
            _args['centres'] = np.array(args['centres'])
        else:
            _args[key] = args[key]
    model.train(**_args)
    return model


def predict(model, observations, provide_variance=False):
    """
    Predicts the values at some points given a Neural Network

    Parameters
    ----------
    model : NeuralNetwork object
            The trained model
    observations : Pandas DataFrame
                   samples to be predicted

    Returns
    -------
    out : NumPy array
          one-dimensional array with the results
    """
    return model.predict(observations, provide_variance=False)


if __name__ == "__main__":

    n_samples = 1000
    noise = 0.04
    seed = 211104
    list_of_basis_types = ["Gaussian"]
    list_of_scale_parameters = [0.01, 0.05, 0.1, 0.5]
    list_of_n_centers = [10, 15, 20, 25, 30]
    cases = ["lemniscate", "U", "circle", "triangle", "ellipse"]

    ############################################################################################

    # Calculate centers of the RBFs
    position_of_centres = {}
    for n_centers in list_of_n_centers:
        try:
            position_of_centres[n_centers] = pickle.load(open("LHC_{}.pickle".format(n_centers),
                                                              "rb"))
        except:
            position_of_centres[n_centers] = LHC(n_centers, 2, random_seed=seed)
            pickle.dump(position_of_centres[n_centers], open("LHC_{}.pickle".format(n_centers),
                                                             "wb"))
        plt = MathlasPlot()
        plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color='k')
        plt.scatter(position_of_centres[n_centers][:, 0], position_of_centres[n_centers][:, 1],
                    marker_size=100, range_x=(-0.05, 1.05), range_y=(-0.05, 1.05))
        plt.equal_aspect_ratio()

    ############################################################################################

    for case in cases:
        sys.stdout.write("\n ***** Checking case {} ***** \n".format(case))
        # Initialize training and validation databases
        db = eval_vanilla_function(npoints=n_samples, case=case, noise=noise)
        actual_classes = eval_vanilla_function(npoints=251, seed=None, case=case)
        predicted_classes = actual_classes.copy()

        bc = BayesianClassification(db, "c")
        bc.check_derivative_binary(rbf_label="Gaussian", scale_parameter=0.1,
                                   centres=position_of_centres[15])

    ############################################################################################

    for case in cases:
        # Initialize training and validation databases
        db = eval_vanilla_function(npoints=n_samples, case=case, noise=noise)
        actual_classes = eval_vanilla_function(npoints=251, seed=None, case=case)
        predicted_classes = actual_classes.copy()

        ############################################################################################

        train_args = []
        args = product(list_of_basis_types, list_of_scale_parameters, position_of_centres.values())
        for basis_type, scale_param, centres in args:
            train_args.append({"rbf_label": basis_type,
                               "scale_parameter": scale_param,
                               "centres": tuple(map(tuple, centres))})

        ########################################################################################

        t0 = time.time()
        cv = KFoldCrossValidation(train, predict, db, "c",
                                  number_of_partitions=3, metrics="two classes",
                                  partition_method="alternate", train_args=train_args,
                                  is_multiclass_classification=False)
        t1 = time.time()
        training_time = t1 - t0

        ########################################################################################

        optimal_params = cv.figures_of_merit["two classes"].idxmin().copy()
        optimal_params["centres"] = len(optimal_params["centres"])
        sys.stdout.write('\nCurrent case: {}\n'.format(case))
        sys.stdout.write("\tWall time spent in training method: {:.2f}\n".format(training_time))
        sys.stdout.write('\t\tOptimal CV params: {}\n'.format(optimal_params))

        ########################################################################################

        y_a = cv.predict(predicted_classes.copy(), method="average", train_if_not_available=False,
                         provide_variance=False)
        y_g = cv.predict(predicted_classes.copy(), method="global", train_if_not_available=True,
                         provide_variance=False)
        plot_2contours([actual_classes, y_g], None)
        plot_2contours([actual_classes, y_a], None)
    plt.show()
