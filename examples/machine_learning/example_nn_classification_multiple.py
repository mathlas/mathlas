#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
from mathlas.machine_learning.neural_network import NeuralNetwork
from mathlas.machine_learning.cross_validation import KFoldCrossValidation
from mathlas.examples.machine_learning.aux_ops import train_nn, predict_nn_classification
from mathlas.examples.machine_learning.aux_ops import (eval_multiple_function, scatterplot,
                                                       evaluate_confusion_matrix)


if __name__ == "__main__":
    """
    Simple script to test Neural Network structures and other parameters
    """

    ########################################################################################
    # Define benchmark parameters
    case = "split lemniscate"
    n_samples = 1000
    noise = 0.04
    df = eval_multiple_function(npoints=n_samples, case=case, noise=noise)
    actual_classes = eval_multiple_function(npoints=251, seed=None, case=case)

    #########################################################################

    nUnits = [(5,), (10,), (5, 5), (10, 10), (5, 10, 5)]
    train_args = []
    for nu in nUnits:
        train_args.append({"problem_type": "one-of-k classification", "n_units": nu,
                           "activation_functions": tuple(len(nu) * ["sigmoid"] + ["linear"])})

    ########################################################################################

    t0 = time.time()
    cv = KFoldCrossValidation(train_nn, predict_nn_classification, df, "c",
                              metrics="multiple classes", partition_method="random",
                              train_args=train_args, is_multiclass_classification=True)
    t1 = time.time()
    training_time = t1 - t0

    ########################################################################################
    figures_of_merit = cv.figures_of_merit["multiple classes"].copy()
    figures_of_merit.index = [d["n_units"] for d in cv.figures_of_merit.index]

    optimal_params = cv.figures_of_merit["multiple classes"].idxmin()
    sys.stdout.write('\nCurrent case: {}\n'.format(case))
    sys.stdout.write("\tWall time spent in training method: {:.2f}\n".format(training_time))
    sys.stdout.write('\t\tOptimal CV params: {}\n'.format(optimal_params))
    sys.stdout.write('\t\tCross Validation figures of merit:\n')
    sys.stdout.write('{}\n'.format(figures_of_merit.to_string()))

    ########################################################################################

    kwargs = {"method": "global", "train_if_not_available": True}
    models = cv.get_optimal_models(method="global", train_if_not_available=True)
    _, _, predicted_classes = evaluate_confusion_matrix(models[0], actual_classes, "c")
    scatterplot(df, [actual_classes, predicted_classes], None, True)
    predicted_classes = cv.predict(predicted_classes, method="average", train_if_not_available=False)
    scatterplot(df, [actual_classes, predicted_classes], None, True)
    sys.exit(0)
