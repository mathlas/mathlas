#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
import numpy as np
import pandas as pd
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.machine_learning.cross_validation import KFoldCrossValidation
from mathlas.examples.machine_learning.aux_ops import (train_nn, predict_nn_regression,
                                                       eval_bishop_function, eval_naca4412_aoa0)


if __name__ == "__main__":

    cases = ["coarse Bishop", "large Bishop", "naca4412"]

    for case in cases:
        if case == "coarse Bishop":
            nUnits = [(3,), (6,), (3, 3), (3, 6), (6, 3), (6, 6), (3, 6, 3)]
            df = eval_bishop_function(n_points=50)
            axis_labels = ("input", "output")
        elif case == "large Bishop":
            nUnits = [(3,), (6,), (3, 3), (3, 6), (6, 3), (6, 6), (3, 6, 3)]
            df = eval_bishop_function(n_points=1000)
            axis_labels = ("input", "output")
        elif case == "naca4412":
            nUnits = [(5,), (10,), (5, 5), (10, 10), (5, 10, 5)]
            df = eval_naca4412_aoa0()
            axis_labels = ("s, arc-length", u"$c_p$")
        else:
            raise ValueError("Value of <case> not recognized")

        df.columns = ["input", "output"]

        # Create mesh for prediction purposes
        x = np.linspace(df["input"].min(), df["input"].max(), 401)
        x = np.array([x]).T
        prediction = pd.DataFrame(x, columns=["input"])

        #########################################################################

        train_args = []
        for nu in nUnits:
            train_args.append({"problem_type": "regression", "n_units": nu,
                               "activation_functions": tuple(len(nu) * ["sigmoid"] + ["linear"])})

        ########################################################################################

        t0 = time.time()
        cv = KFoldCrossValidation(train_nn, predict_nn_regression, df, "output",
                                  number_of_partitions=2, metrics="L2 error",
                                  partition_method="alternate", train_args=train_args)
        t1 = time.time()
        training_time = t1 - t0

        ########################################################################################
        figures_of_merit = cv.figures_of_merit["L2 error"].copy()
        figures_of_merit.index = [d["n_units"] for d in cv.figures_of_merit.index]

        optimal_params = cv.figures_of_merit["L2 error"].idxmin()
        sys.stdout.write('\nCurrent case: {}\n'.format(case))
        sys.stdout.write("\tWall time spent in training method: {:.2f}\n".format(training_time))
        sys.stdout.write('\t\tOptimal CV params: {}\n'.format(optimal_params))
        sys.stdout.write('\t\tCross Validation figures of merit:\n')
        sys.stdout.write('{}\n'.format(figures_of_merit.to_string()))

        ########################################################################################

        plt = MathlasPlot()
        for args in train_args:
            for model in cv.models[frozenset(args.items())]:
                cp = predict_nn_regression(model, prediction.copy())
                plt.plot(x, cp, color="k", linewidth=.5, transparency=True)
        y_g = cv.predict(prediction.copy(), method="global", train_if_not_available=True)
        y_a = cv.predict(prediction.copy(), method="average", train_if_not_available=False)
        plt.plots(x, [y_a["output"].values, y_g["output"].values],
                  var_labels=["Averaged model", "Global model"])
        plt.scatter(df["input"].values, df["output"].values, axis_labels=axis_labels,
                    title="{} - opt. structure: {}".format(case, optimal_params["n_units"]))
    plt.show()

    sys.exit(0)
