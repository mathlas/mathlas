# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import numpy as np
import pandas as pd
from time import time
from collections import Counter
from mathlas.machine_learning.hmm import HiddenMarkovModel
from mathlas.machine_learning.hmm_addons import HiddenMarkovClassifier


def get_parameters_of_hmm(class_no):
    """
    Initialize the parameters of HMMs

    Parameters
    ----------
    class_no : integer
               class number

    Returns
    -------
    initial_hidden_state, transition_matrix, means, comat : NumPy arrays
                                                            parameters of the HMMs
    """

    if class_no == 0:
        initial_hidden_state = np.array([0.5, 0.25, 0.25])
        transition_matrix = np.array([[0.4, 1, 0.4], [0.2, 0.0, 0.4], [0.4, 0.0, 0.2]])
        means = [None for _ in range(3)]
        comat = [None for _ in range(3)]
        means[0] = np.array([0., -0.75])
        comat[0] = np.array([[0.25, 0.], [0., 0.25]])
        means[1] = np.array([0., 3.])
        comat[1] = np.array([[1., 0.], [0., 1.]])
        means[2] = np.array([1.5, 0])
        comat[2] = np.array([[0.5, 0.], [0., 0.5]])
    elif class_no == 1:
        initial_hidden_state = np.array([0.25, 0.75])
        transition_matrix = np.array([[0.2, 0.7], [0.8, 0.3]])
        means = [None for _ in range(2)]
        comat = [None for _ in range(2)]
        means[0] = np.array([1., -1.])
        comat[0] = np.array([[0.5, 0.], [0., 0.5]])
        means[1] = np.array([-2., 2.])
        comat[1] = np.array([[1., 0.], [0., 0.5]])
    elif class_no == 2:
        initial_hidden_state = np.array([0.1, 0.1, 0.2, 0.6])
        transition_matrix = np.array([[0.4, 0.1, 0.2, 0.0], [0.2, 0.1, 0.1, 0.5],
                                      [0.2, 0.4, 0.6, 0.0], [0.2, 0.4, 0.1, 0.5]])
        means = [None for _ in range(4)]
        comat = [None for _ in range(4)]
        means[0] = np.array([1.5, 1.5])
        comat[0] = np.array([[1.0, 0.], [0., 1.0]])
        means[1] = np.array([1.5, -1.5])
        comat[1] = np.array([[1.0, 0.], [0., 0.5]])
        means[2] = np.array([-1.5, 1.5])
        comat[2] = np.array([[0.5, 0.], [0., 1.0]])
        means[3] = np.array([-1.5, -1.5])
        comat[3] = np.array([[0.5, 0.], [0., 0.5]])
    else:
        raise ValueError("Model number not implemented")

    return initial_hidden_state, transition_matrix, means, comat


if __name__ == "__main__":
    """
    Example of use of the HMM-based classifier
    """
    ##############################################################################################
    n_classes = 3
    n_tests = 1000
    min_len_sequences = 10
    max_len_sequences = 100
    classes = list(range(n_classes))
    delta_len = max_len_sequences - min_len_sequences
    ##############################################################################################

    # Define Hidden Markov Machine
    hmm = {}
    for ii in classes:
        ihs, tm, mu, cov_mat = get_parameters_of_hmm(ii)
        hmm[ii] = HiddenMarkovModel(mode="generate", var_type="continuous", seed=211104,
                                    n_sequences=5, length_of_sequences=[200, 1000, 500, 1000, 1300],
                                    initial_hidden_state=ihs, transition_matrix=tm,
                                    emission_means=mu, emission_covariances=cov_mat)
        f = hmm[ii].get_probability_of_sequences
        sys.stdout.write("Log-likelihood of the original HMM for class {} is {}\n"
                         "".format(ii, np.sum(f(hmm[ii].observed_samples))))
    # Generate data
    data = {}
    for ii in classes:
        data[ii] = hmm[ii].observed_samples
        for jj in range(len(data[ii])):
            data[ii][jj] = np.concatenate([e.reshape(1, 2) for e in data[ii][jj]])

    t0 = time()
    hmc = HiddenMarkovClassifier(data, var_type="continuous", n_hidden_states=list(range(2, 9)),
                                 n_models=0, estimation_method="estimation hmm", n_samples=1000,
                                 target_gradient=None)
    tf = time()
    training_time = tf - t0
    # Show some preliminary results
    for ii in classes:
        sys.stdout.write("Log-likelihood of the trained HMM for class {} is {}\n"
                         "".format(ii, hmc.models[ii].log_likelihood))
    for ii in classes:
        sys.stdout.write("Selected #hidden states of the trained HMM for class {} is {}\n"
                         "".format(ii, hmc.models[ii].n_hidden_states))

    # Test the classifier and create confussion matrix
    confusion_matrix = pd.DataFrame(np.zeros((len(classes), len(classes) + 3)), index=classes,
                                    columns=classes + ["Precision", "Recall", "F1-score"])
    t0 = time()
    for ii in classes:
        sys.stdout.write("Checking results for number {}\n".format(ii))
        ihs, tm, mu, cov_mat = get_parameters_of_hmm(ii)
        len_sequences = min_len_sequences + delta_len * np.random.rand(n_tests)
        len_sequences = len_sequences.astype(int)
        hmm_gen = HiddenMarkovModel(mode="generate", var_type="continuous", seed=211104,
                                    n_sequences=n_tests, length_of_sequences=len_sequences,
                                    initial_hidden_state=ihs, transition_matrix=tm,
                                    emission_means=mu, emission_covariances=cov_mat)
        sequences = hmm_gen.observed_samples
        # Perform prediction
        p_of_class_given_seq = hmc.predict(sequences)
        labels, probabilities = zip(*p_of_class_given_seq.items())
        most_probable_outcome = np.argmax(np.array(probabilities), axis=0)
        res = Counter([labels[ii] for ii in most_probable_outcome])
        for jj in classes:
            confusion_matrix.loc[ii, jj] = res.get(jj, 0.)
    confusion_matrix *= 100. / n_tests

    f1_score = 0.
    for ii in classes:
        alpha = confusion_matrix.loc[ii, ii]
        precision = confusion_matrix.loc[ii, ii] / np.sum(confusion_matrix.loc[:, ii])
        recall = confusion_matrix.loc[ii, ii] / np.sum(confusion_matrix.loc[ii, :])
        confusion_matrix.loc[ii, "Precision"] = precision * 100.
        confusion_matrix.loc[ii, "Recall"] = recall * 100.
        confusion_matrix.loc[ii, "F1-score"] = 100. * 2 * precision * recall / (precision + recall)
        f1_score += 1. / confusion_matrix.loc[ii, "F1-score"]
    f1_score = len(classes) / f1_score
    tf = time()
    confusion_time = tf - t0

    # Show results
    sys.stdout.write("{}\n".format(confusion_matrix))
    sys.stdout.write("Harmonic F-1 score: {}\n".format(f1_score))
    sys.stdout.write("Wall time spent in training method: {:.2f}\n".format(training_time))
    sys.stdout.write("Wall time spent in calculating confusion matrix: {:.2f}\n".format(confusion_time))
