#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
import numpy as np
import tensorflow as tf
from mathlas.examples.machine_learning.aux_ops import eval_vanilla_function, plot_2contours


def tf_neural_network(trainingData, trainingLabels, prediction_dataset, random_seed=211104):
    batch_size = 500
    num_nodes = 15

    graph = tf.Graph()
    with graph.as_default():
        # We'd rather be able to replicate the results...
        tf.set_random_seed(random_seed)
        # Input data. For the training data, we use a placeholder that will be fed
        # at run time with a training minibatch.
        tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size,
                                                             trainingData.shape[1]))
        tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, trainingLabels.shape[1]))
        tf_prediction_dataset = tf.constant(prediction_dataset)
        β = tf.constant(0.)

        # Construct the weights and biases arrays for the first layer
        weights1 = tf.Variable(tf.truncated_normal([trainingData.shape[1], num_nodes]))
        biases1 = tf.Variable(tf.zeros([num_nodes]))
        logits1 = tf.sigmoid(tf.matmul(tf_train_dataset, weights1) + biases1)

        # Variables for the second layer
        weights2 = tf.Variable(tf.truncated_normal([num_nodes, trainingLabels.shape[1]]))
        biases2 = tf.Variable(tf.zeros([trainingLabels.shape[1]]))
        logits2 = tf.matmul(logits1, weights2) + biases2

        # Training computation.
        base_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits2,
                                                                           tf_train_labels))
        regularization = 0.5 * (tf.nn.l2_loss(weights1) + tf.nn.l2_loss(weights2))
        loss = base_loss + β * regularization

        # Optimizer.
        global_step = tf.Variable(0)
        learning_rate = tf.train.exponential_decay(0.5, global_step, 100, 0.96)
        optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)

        # The predictions for the prediction dataset
        predicted_labels = tf.sigmoid(tf.matmul(tf.sigmoid(tf.matmul(tf_prediction_dataset,
                                                                     weights1) + biases1),
                                                weights2) + biases2)

    num_steps = 20000

    with tf.Session(graph=graph) as session:
        tf.initialize_all_variables().run()

        for step in range(num_steps):
            # Determine an offset
            # offset = (step * batch_size) % (trainingLabels.shape[0] - batch_size)
            offset = np.random.randint(0, trainingLabels.shape[0] - batch_size)
            # Generate a minibatch.
            batch_data = trainingData[offset:(offset + batch_size), :]
            batch_labels = trainingLabels[offset:(offset + batch_size), :]
            # Prepare a dictionary telling the session where to feed the minibatch.
            # The key of the dictionary is the placeholder node of the graph to be fed,
            # and the value is the numpy array to feed to it.
            feed_dict = {tf_train_dataset: batch_data,
                         tf_train_labels: batch_labels}
            _, l, bl, rl = session.run([optimizer, loss, base_loss, regularization],
                                       feed_dict=feed_dict)
            if step % 1000 == 0:
                print('Step {}:'.format(step))
                print('\tBase loss: {}'.format(bl))
                print('\tRegularization loss: {}'.format(rl))
                print('\tLoss: {}'.format(l))

        data = predicted_labels.eval()
        data /= np.sum(data, axis=1).reshape([-1, 1])

    return data


if __name__ == "__main__":
    """
    Simple script to test Neural Network structures and other parameters
    """

    ################################################################################################
    # Define benchmark parameters
    case = "ellipse"
    nsamples = 5000
    noise = 0.04
    nUnits = (15,)

    # Evaluate the lemniscate function and convert the labels vector into a matrix
    # (one vector per row)
    training_data = eval_vanilla_function(npoints=nsamples, case=case, noise=noise)
    training_labels = np.zeros((nsamples, 2), dtype=np.float32)
    training_labels[np.arange(nsamples), training_data['c']] = 1
    training_data = training_data[['x1', 'x2']].values
    # Evaluate the function for a few points and separate coordinates and labels
    actual_classes = eval_vanilla_function(npoints=251, seed=None, case=case)
    predicted_classes = actual_classes.copy()

    ########################################################################################

    # Train the decision tree
    t0 = time.time()
    validation_classes = tf_neural_network(training_data, training_labels,
                                           predicted_classes[['x1', 'x2']].astype(np.float32).values)
    print(validation_classes)
    predicted_classes['c'] = validation_classes[:, 1]
    t1 = time.time()
    sys.stdout.write("\tNN structure:        {}\n".format(nUnits))
    sys.stdout.write("\tTraining time:       {:.3f}s\n".format(t1 - t0))
    # Calculate accuracy and plot if required
    mean_error = 100. * np.mean(np.abs(predicted_classes['c'] - actual_classes['c']))
    std_error = 100. * np.std(np.abs(predicted_classes['c'] - actual_classes['c']))
    sys.stdout.write("\tError mean:          {:.3f}%\n".format(mean_error))
    sys.stdout.write("\tError std:           {:.3f}%\n\n".format(std_error))
    plot_2contours([actual_classes, predicted_classes], None)
    plt.show()

    sys.exit(0)
