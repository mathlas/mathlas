#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import numpy as np
import pandas as pd
import xgboost as xgb
from itertools import product
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.machine_learning.cross_validation import KFoldCrossValidation


# specify parameters via dictionary:
#       https://github.com/dmlc/xgboost/blob/master/doc/parameter.md
#       http://xgboost.readthedocs.io/en/latest/python/python_api.html#module-xgboost.training
def train(training_data, result_col, train_args):
    """
    Train method for cross-validation

    Parameters
    ----------
    training_data : pandas DataFrame
    result_col : string
                 The name of the column containing the output variable results
    train_args : dictionary


    Returns
    -------
    model : Trained model
    """
    data_cols = [col for col in training_data.columns if col != result_col]
    if train_args["training method"] == 'XGBoost':
        args = train_args.copy()
        args.pop("training method")
        nbr = args.pop("num_boost_rounds")
        # Extract the forecasting period from the name of the result column
        dtrain = xgb.DMatrix(training_data[data_cols].values.astype(np.float64),
                             label=training_data[result_col].values)
        return xgb.train(args, dtrain, nbr)
    else:
        raise NotImplementedError('Unsupported model type "{}"'.format(training_data[0]))


def evaluate(model, data, provide_variance=None):
    """
    Evaluates the given model on the given data

    Parameters
    ----------
    model : Trained booster model
    data : pandas DataFrame
           All-numeric

    Returns
    -------
    Model prediction
    """
    cols = [col for col in data.columns if not col.startswith('unsubscription')]
    return model.predict(xgb.DMatrix(data[cols].values.astype(np.float64)))


def generate_data(n_samples, p_keep=0.1, seed=211104):
    """
    Generate samples/customers with two features (age, time they have been subscribed to the
     service) and simulates if they unsubscribe or not. It provides both a Boolean to mark if they
     unsubscribe and the probability, assigned by the model, of not unsubscribe from the service.

    Parameters
    ----------
    n_samples : integer
                number of samples (customers) to be generated
    p_keep : float
             number in the range [0, 1] that controls the number of samples kept. If `p_keep`=1 all
              generated samples will be kept and there will be complete customer's histories on the
              returned dataframe. Otherwise, same parts of the history will be omitted.
    seed : integer
           number used to initialize the generator of random numbers

    Returns
    -------
    dfu : Pandas DataFrame
          DF that contains the customer features plus a flag that indicates if she unsubscribes
           that month (1) or not (0).
    dfp : Pandas DataFrame
          DF that contains the customer features plus the probability that our model assigns the
           customer to not unsubscribe that month.

    """

    def p(x, y):
        return 0.25 * (np.cos(2. * np.pi * (x-18.) / (75.-18.)) + 3) * np.exp(-y / 60.)

    np.random.seed(seed)
    customers = []
    new_customer = True
    while len(customers) < n_samples:
        if new_customer:
            age = np.random.randint(low=18, high=75)
            sen = 0
        else:
            age, sen, _, _ = prev_customer
            sen += 1
            if sen % 12 == 0:
                age += 1

        prb = p(age, sen)
        uns = int(np.random.rand() > prb)
        prev_customer = [age, sen, uns, 1. - prb]
        if np.random.rand() < p_keep:
            customers.append(prev_customer)
        new_customer = uns == 1

    df = pd.DataFrame(customers, columns=["age", "seniority", "unsubscription", "probability"])
    return df[["age", "seniority", "unsubscription"]], df[["age", "seniority", "probability"]]


if __name__ == '__main__':
    """
    Example of use of KFoldCrossValidation and XGBoost to get a model for ranking purposes.
    """

    ###############################################################################################
    # Number of samples used to train the model
    n_training_samples = 100000
    # Number of samples used to validate the trained model
    n_validation_samples = 10000
    # Number of random predictions used to calculate baseline precision
    n_random_predictions = 1000
    # How are the predictions supposed to be obtained? If "average" the average value of those
    #  provided by the several models (obtained from the partitions of the data) are used; if
    #  "global" a single model trained using all observations is used.
    kind_of_opt_model = "global"
    # Metric used to calculate the optimal case
    opt_metric = "precision (200)"
    # List of metrics to be used in the cross-validation process
    metrics = ['precision', 'DCG'] * 6
    # List of values of the length of the rankings to be used in the cross-validation process
    len_ranks = [50, 50, 100, 100, 200, 200, 300, 300, 400, 400, 500, 500]
    # List of training methods to choose from when using the cross-validation
    training_methods = ['XGBoost']
    # List of values that he maximum depth (XGBoost) can take when using the cross-validation
    max_depth = [3, 6, 9]
    # List of learning rate (eta in XGBoost) values  when using the cross-validation
    eta = [0.15, 0.3, 0.45]
    # List of values of the boosting rounds when using the cross-validation
    num_boost_rounds = [5, 15, 25]
    # List of values of the parameter for balancing positive and negative weights (scale_pos_weight
    #  in XGBoost) when using the cross-validation
    scale_pos_weight = [0, 3, 6, 9]

    ###############################################################################################
    # Train model using cross-validation
    train_args = []
    for tm, md, e, nbr, spw in product(training_methods, max_depth, eta, num_boost_rounds,
                                       scale_pos_weight):
        train_args.append({"training method": tm, 'max_depth': md, 'eta': e,
                           'scale_pos_weight': spw, "num_boost_rounds": nbr, 'silent': 1,
                           'objective': 'binary:logistic', 'nthread': 4})

    sys.stdout.write("Generating training data\n")
    df_t, df_p = generate_data(n_training_samples, seed=211104)

    sys.stdout.write("Using cross-validation to look for optimal parameters\n")
    cv = KFoldCrossValidation(train, evaluate, df_t, "unsubscription", partition_method="random",
                              is_multiclass_classification=False, metrics=metrics,
                              len_rank=len_ranks, train_args=train_args)
    sys.stdout.write("Training global, optimal model\n")
    cv.get_optimal_models(method=kind_of_opt_model, metric=opt_metric, train_if_not_available=True)
    sys.stdout.write('\t\tOptimal CV params: {}\n'.format(cv.optimal_params))
    sys.stdout.write('\t\tCross Validation figures of merit: ')
    sys.stdout.write('{}\n'.format(cv.figures_of_merit.loc[cv.index_optimal_params, opt_metric]))

    ###############################################################################################
    # Verify model accuracy and plot results
    sys.stdout.write("Generating validation data")
    df_u, df_p = generate_data(n_validation_samples, seed=13102010)
    df_u0 = df_u.copy()
    prediction = cv.predict(df_u, method=kind_of_opt_model, metric=opt_metric)
    df_p["prediction"] = prediction["unsubscription"]
    n = len(set(prediction["unsubscription"]))
    sys.stdout.write("Number of different probabilities in the prediction: {}\n". format(n))

    sys.stdout.write("Calculating errors\n")
    df_p["error"] = df_p["prediction"] - df_p["probability"]
    df_p = df_p.sort_values(by="prediction", ascending=False)
    errors = []
    for x in [50, 100, 200, 300, 400, 500]:
        errors.append(df_p["error"].values[:x])
    errors.append(df_p["error"].values)
    e = []
    for _ in range(10):
        e.extend(list(np.random.rand(n_validation_samples) - df_p["probability"]))
    errors.append(e)

    sys.stdout.write("Calculating precision of predictions\n")
    predicted_values = prediction["unsubscription"]
    sampled_values = df_u0["unsubscription"]
    len_ranks = [25 * (x + 1) for x in range(20)]
    predicted_precisions = [cv.precision(predicted_values, sampled_values, n=x) for x in len_ranks]

    sys.stdout.write("Calculating precision of randomness\n")
    random_precisions = []
    for _ in range(n_random_predictions):
        s = pd.Series(np.random.rand(n_validation_samples))
        random_precisions.append([cv.precision(s, sampled_values, n=x) for x in len_ranks])
    random_precisions = np.mean(np.array(random_precisions), axis=0)

    plt = MathlasPlot()
    plt.plots(len_ranks, [predicted_precisions, random_precisions],
              var_labels=["Modeled", "Random"], axis_labels=["Length of the ranking", "Precision"],
              range_y=[0, 1], range_x=[min(len_ranks) - 1, max(len_ranks) + 1])

    plt = MathlasPlot()
    plt.boxplot(errors, range_y=(-1, 1), labels=[50, 100, 200, 300, 400, 500, "all", "random"],
                title="(Predicted - actual) probabilities")

    plt.show()

    sys.exit(0)
