import sys
import numpy as np
from mathlas.machine_learning.hmm import HiddenMarkovModel


if __name__ == "__main__":

    use_random_model = False

    if use_random_model:
        seed = 211104
        K = 3
        N = 2
        T = 10

        np.random.seed(seed)
        A = np.random.rand(K, K)
        B = np.random.rand(N, K)
        pi = np.random.rand(K)
        for k in range(K):
            A[:, k] /= np.sum(A[:, k])
        for k in range(K):
            B[:, k] /= np.sum(B[:, k])
        pi /= np.sum(pi)
        # X, Y = generate_data(n_points=100, L=A, O=B, L0=pi)
    else:
        pi = np.array([0.5, 0.5])
        A = np.array([[0.5, 0.4], [0.5, 0.6]])
        B = np.array([[0.2, 0.3], [0.3, 0.2], [0.3, 0.2], [0.2, 0.3]])
        X = []
        Y = [np.array(["G", "G", "C", "A", "C", "T", "G", "A", "A"])]

    # Generate data
    hmm = HiddenMarkovModel(mode="generate", var_type="categorical", length_of_sequences=[10, 15, 5],
                            initial_hidden_state=pi, transition_matrix=A, emission_matrix=B,
                            emitted_states=["A", "C", "G", "T"], seed=211104)

    hmm = HiddenMarkovModel(mode="generate", var_type="categorical", length_of_sequences=10 * [1000],
                            initial_hidden_state=pi, transition_matrix=A, emission_matrix=B,
                            emitted_states=["A", "C", "G", "T"], seed=211104)
    for ii in range(2):
        hmm2 = HiddenMarkovModel(mode="train", var_type="categorical", n_hidden_states=3,
                                 emitted_states=["A", "C", "G", "T"],
                                 observed_samples=hmm.observed_samples, seed=211104 + ii)
        sys.stdout.write("log likelihood, {}\n".format(hmm2.log_likelihood))
        if hmm2.n_hidden_states == hmm.n_hidden_states:
            et = hmm.transition_matrix - hmm2.transition_matrix
            ee = hmm.emission_matrix - hmm2.emission_matrix
            sys.stdout.write("difference in transition matrix, {}\n".format(et))
            sys.stdout.write("difference in emission matrix, {}\n".format(ee))
        else:
            sys.stdout.write("transition matrix, {}\n".format(hmm2.transition_matrix))
            sys.stdout.write("emission matrix, {}\n".format(hmm2.emission_matrix))

    true_sol = [0, 0, 0, 1, 1, 1, 1, 1, 1]
    calculated_sol = hmm.get_most_probable_hidden_states(Y)
    sys.stdout.write("The result of the Viterbi algorithm should be {}.\n".format(true_sol))
    sys.stdout.write("The result of the Viterbi algorithm is        {}.\n".format(calculated_sol))

