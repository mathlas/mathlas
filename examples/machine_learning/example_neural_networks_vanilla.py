#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import time
import sqlite3
import numpy as np
import pandas as pd
from itertools import product
import matplotlib.pyplot as plt
from mathlas.machine_learning.neural_network import NeuralNetwork
from mathlas.statistics.probabilistic_choice_object import ProbabilisticChoiceObject
from mathlas.examples.machine_learning.aux_ops import eval_vanilla_function, plot_2contours


if __name__ == "__main__":
    """
    Benchmark of Neural Networks
    """

    ################################################################################################
    # Define benchmark parameters
    nn_structures = [(5,), (10,), (15,), (25,), (5, 5), (10, 10), (15, 15), (25, 25),
                     (5, 10, 5), (5, 15, 5), (10, 15, 10), (5, 25, 5), (10, 25, 10), (15, 25, 15),
                     (10, 15, 25, 15, 10)]
    nn_types = ["", "ReLU"]
    cases = ["lemniscate", "U", "circle", "triangle", "ellipse"]
    noises = [0., 0.04]                                 # Standard deviation of the noise in the
                                                        #   data
    nsamples = [1000, 5000]                             # Number of samples to be used
    must_plot_cases = True
    must_store_plots = True                             # To store plots both <must_plot_cases> and
                                                        #   <must_store_plots> must be True. If
                                                        #   only <must_plot_cases> is True, figures
                                                        #   will be shown but not stored.
    verbosity = True

    # Check that the path exists and, if not, create it
    base_dname = "results/"
    os.makedirs(base_dname, exist_ok=True)

    # Turn structures of the neural network into strings
    nn_labels = []
    nn_cases = []
    for nUnits, nn_type in product(nn_structures, nn_types):
        nn_cases.append((nUnits, nn_type))
        nn_labels.append("-".join(["{}".format(elem) for elem in nUnits]) + " - {}".format(nn_type))

    # Initialize storage databases
    nn_errors = pd.DataFrame(np.zeros((len(nn_labels), len(cases))),
                             index=nn_labels, columns=cases)
    nn_error_stdvs = pd.DataFrame(np.zeros((len(nn_labels), len(cases))),
                                  index=nn_labels, columns=cases)
    nn_times = pd.DataFrame(np.zeros((len(nn_labels), len(cases))), index=nn_labels,
                            columns=cases)

    ###############################################################################################
    # Start benchmark
    for samples, noise in product(nsamples, noises):
        benchmark_dname = os.path.join(base_dname, "nsamples_{}_noise_{}".format(samples, noise))
        if not os.path.exists(benchmark_dname):
            os.makedirs(benchmark_dname)
        suffix = "noise_{}_samples_{}".format(noise, samples)
        path = os.path.join(benchmark_dname, "nn_benchmark_{}.sqlite".format(suffix))
        for case in cases:
            text = '{} - Samples: {} - Noise: {}'.format(case, samples, noise)
            sys.stdout.write("\033[1m{}\n".format(text))
            sys.stdout.write("{}\033[0m\n\n".format('='*len(text)))
            case_dname = os.path.join(benchmark_dname, case)
            if not os.path.exists(case_dname):
                os.makedirs(case_dname)
            # Initialize training and validation databases
            db = eval_vanilla_function(npoints=samples, case=case, noise=noise)
            actual_classes = eval_vanilla_function(npoints=251, seed=None, case=case)
            predicted_classes = actual_classes.copy()

            ########################################################################################

            # # Benchmark of neural network
            if verbosity:
                sys.stdout.write("NN:\n")

            for nn_case, label in zip(nn_cases, nn_labels):
                nUnits, nn_type = nn_case
                if nn_type == "":
                    activation_functions = ["sigmoid"] * (len(nUnits) + 1)
                elif nn_type == "ReLU":
                    activation_functions = ["RELU"] * (len(nUnits) + 1)

                # Check if plot exists and, therefore, the case has already been calculated
                if must_plot_cases:
                    if must_store_plots:
                        fname = "nn_{}_{}_nlayers_{}_structure_{}.png".format(case, suffix,
                                                                              len(nUnits),
                                                                              label)
                        store_path = os.path.join(case_dname, fname)
                        if os.path.isfile(store_path):
                            sys.stdout.write("Skipping file {}\n".format(store_path))
                            write_nn_stats = False
                            continue
                        else:
                            write_nn_stats = True
                    else:
                        store_path = None

                # Train the neural network
                t0 = time.time()
                nn = NeuralNetwork(db, "c", "binary classification")
                nn.train(nUnits, activation_functions)
                t1 = time.time()
                nn_times.loc[label, case] = t1 - t0
                if verbosity:
                    sys.stdout.write("\tNN structure:        {}\n".format(label))
                    sys.stdout.write("\tTraining time:       {:.3f}s\n".format(t1 - t0))

                # Use the neural network to classify
                t0 = time.time()
                predicted_classes["c"] = None
                nn.predict(predicted_classes)
                t1 = time.time()
                predicted_classes['c'] = predicted_classes['c'].apply(lambda x: x.values[1] if (
                    isinstance(x, ProbabilisticChoiceObject)) else x)
                if verbosity:
                    sys.stdout.write("\tNeural Network:      {}\n".format(label))
                    sys.stdout.write("\tClassification time: {:.3f}s\n".format(t1 - t0))

                # Calculate accuracy and plot if required
                mean_error = 100. * np.mean(np.abs(predicted_classes["c"] -
                                                   actual_classes["c"]))
                std_error = 100. * np.std(np.abs(predicted_classes["c"] -
                                                 actual_classes["c"]))
                nn_errors.loc[label, case] = mean_error
                nn_error_stdvs.loc[label, case] = std_error
                if verbosity:
                    sys.stdout.write("\tError mean:          {:.3f}%\n".format(mean_error))
                    sys.stdout.write("\tError std:           {:.3f}%\n\n".format(std_error))
                if must_plot_cases:
                    plot_2contours([actual_classes, predicted_classes], store_path)
                    plt.show()

        # Store results
        conn = sqlite3.connect(path)
        if write_nn_stats:
            nn_errors.to_sql("NN, errors", conn, if_exists="replace")
            nn_error_stdvs.to_sql("NN, error stdvs", conn, if_exists="replace")
            nn_times.to_sql("NN, time", conn, if_exists="replace")
        conn.close()

    sys.exit(0)
