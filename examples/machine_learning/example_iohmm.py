import numpy as np
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.machine_learning.iohmm import InputOutputHiddenMarkovModel


def generate_data(n_points=1000, seed=211104, case=1):

    np.random.seed(seed)
    inputs, pz, py = [], [], []
    latent, outputs = [], []
    for index in range(n_points):
        theta = index * 4. * np.pi / (n_points - 1)
        if case == 0:
            radius = theta
        elif case == 1:
            radius = 2. * np.sin(4. * theta)
        elif case == 2:
            radius = 2.
        elif case == 3:
            radius = 1. / (1. - 0.9 * np.cos(theta))
        elif case == 4:
            radius = 1. / np.sqrt(theta + 0.1)
        inputs.append([radius * np.cos(theta), radius * np.sin(theta)])

        q_z1 = np.array([inputs[-1][0], inputs[-1][1]])
        q_z2 = np.array([np.sqrt(inputs[-1][0] ** 2 + inputs[-1][1] ** 2),
                         np.abs(inputs[-1][0]) + np.abs(inputs[-1][1])])

        p_z1 = np.exp(q_z1) / np.sum(np.exp(q_z1))
        p_z2 = np.exp(q_z2) / np.sum(np.exp(q_z2))

        if index == 0:
            pz.append(p_z1)
            zv = [1, 0]
            latent.append(1)
        else:
            pz.append(pz[-1][0] * p_z1 + pz[-1][1] * p_z2)
            p = zv[0] * p_z1 + zv[1] * p_z2
            zs = np.random.choice(range(2), p=p)
            zv = [0, 0]
            zv[zs] = 1.
            latent.append(zs)

        q_y1 = np.array([10., np.abs(inputs[-1][1]), np.abs(inputs[-1][0])])
        q_y2 = np.array([np.sum(inputs[-1]), np.sqrt(np.abs(inputs[-1][0])), inputs[-1][1] ** 2])

        p_y1 = np.exp(q_y1) / np.sum(np.exp(q_y1))
        p_y2 = np.exp(q_y2) / np.sum(np.exp(q_y2))

        py.append(pz[-1][0] * p_y1 + pz[-1][1] * p_y2)
        p = zv[0] * p_y1 + zv[1] * p_y2
        ys = np.random.choice(range(3), p=p)
        outputs.append(ys)

    inputs = np.array(inputs)
    pz = np.array(pz)
    py = np.array(py)
    latent = np.array(latent)
    outputs = np.array(outputs)

    return inputs, pz, py, latent, outputs


if __name__ == "__main__":

    plot_sequences = False

    X, Y = [], []
    for case in range(5):
        n_points = 1000
        x, pz, py, z, y = generate_data(n_points=n_points, case=case)
        X.append(x)
        Y.append(y)

        if plot_sequences:
            plt = MathlasPlot()
            plt.plot(x[:, 0], x[:, 1], title="inputs")
            plt.equal_aspect_ratio()
            plt = MathlasPlot()
            plt.plots(range(n_points), [pz[:, 0], pz[:, 1]], title="hidden units")
            plt = MathlasPlot()
            plt.plots(range(n_points), [py[:, 0], py[:, 1], py[:, 2]], title="outputs")
            plt = MathlasPlot()
            plt.scatter(range(n_points), z, title="hidden units")
            plt = MathlasPlot()
            plt.scatter(range(n_points), y, title="outputs")
            plt.show()

    iohmm = InputOutputHiddenMarkovModel(mode="train", task="classification", seed=211104,
                                         observed_inputs=X, observed_outputs=Y, n_hidden_states=2,
                                         nUnits={"initial": [], "transition": [], "emission": []},
                                         activation_functions={"initial": ["sigmoid"],
                                                               "transition": ["sigmoid"],
                                                               "emission": ["sigmoid"]})
    iohmm.generate_data(X[0])
    iohmm.calculate_transition_matrix(X[0][0])
    print(iohmm.get_most_probable_hidden_states(X, Y))

