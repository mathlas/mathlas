#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
import numpy as np
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.machine_learning.neural_network import NeuralNetwork
from mathlas.statistics.probabilistic_choice_object import ProbabilisticChoiceObject as pco
from mathlas.examples.machine_learning.aux_ops import eval_vanilla_function


if __name__ == "__main__":
    """
    Simple script to test Neural Network structures and other parameters
    """

    ################################################################################################
    # Define benchmark parameters
    case = "lemniscate"
    nsamples = 1000
    noise = 0.04
    nUnits = (15,)
    activation_functions = ["sigmoid"] * (len(nUnits) + 1)

    db = eval_vanilla_function(npoints=nsamples, case=case, noise=noise)
    actual_classes = eval_vanilla_function(npoints=251, seed=None, case=case)
    predicted_classes = actual_classes.copy()

    maxiters = 10000
    factors = np.linspace(0., 1., 21)

    ########################################################################################
    actual_n1s, actual_n2s, training_times = [], [], []
    mean_errors, std_errors, rms_errors, max_errors = [], [], [], []
    for f in factors:

        n1 = np.max([int(np.round(f * maxiters)), 1])
        n2 = np.max([maxiters - n1, 1])

        # Train the decision tree
        t0 = time.time()
        nn = NeuralNetwork(db, "c", nUnits, "classification", activation_functions,
                           max_training_iters=None)
        nn.train_network(niter=n1, optimization_method="enhanced gradient descent")
        nn.train_network(niter=n2, optimization_method="nonlinear conjugate gradient")
        t1 = time.time()

        # Use the neural network to classify
        predicted_classes["c"] = None
        nn.predict(predicted_classes)
        predicted_classes['c'] = predicted_classes['c'].apply(lambda x: x.values[1]
                                                              if (isinstance(x, pco)) else x)

        # Calculate accuracy and plot if required
        mean_error = 100. * np.mean(np.abs(predicted_classes["c"] - actual_classes["c"]))
        std_error = 100. * np.std(np.abs(predicted_classes["c"] - actual_classes["c"]))
        rms_error = 100. * np.sqrt(np.mean((predicted_classes["c"] - actual_classes["c"]) ** 2))
        max_error = 100. * np.max(np.abs(predicted_classes["c"] - actual_classes["c"]))

        # Store data
        actual_n1s.append(nn.training_stats[0]["#iterations"])
        actual_n2s.append(nn.training_stats[1]["#iterations"])
        training_times.append(t1 - t0)
        mean_errors.append(mean_error)
        std_errors.append(std_error)
        rms_errors.append(rms_error)
        max_errors.append(max_error)

    plt = MathlasPlot()
    plt.plot(factors, training_times, range_x=(0., 1.),
             axis_labels=["%iters on gradient descent", "Training time (s)"], subplot_index=221)
    plt.plots(factors, [actual_n1s, actual_n2s], range_x=(0., 1.),
              axis_labels=["%iters on gradient descent", "Iters"],
              var_labels=["Enhanced GD", "Nonlinear CG"], subplot_index=222)
    plt.plots(factors, [mean_errors, rms_errors, max_errors], range_x=(0., 1.),
              axis_labels=["%iters on gradient descent", "Errors"],
              var_labels=["L1", "L2", "Inf"], subplot_index=223)
    plt.show()

    sys.exit(0)
