#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
import numpy as np
from mathlas.machine_learning.neural_network import NeuralNetwork
from mathlas.statistics.probabilistic_choice_object import ProbabilisticChoiceObject as pco
from mathlas.examples.machine_learning.aux_ops import eval_vanilla_function, plot_2contours, evaluate_confusion_matrix, scatterplot


if __name__ == "__main__":
    """
    Simple script to test Neural Network structures and other parameters
    """

    ################################################################################################
    # Define benchmark parameters
    case = "lemniscate"
    nsamples = 5000
    noise = 0.04
    nUnits = (15,)
    activation_functions = ["sigmoid"] * (len(nUnits) + 1)

    db = eval_vanilla_function(npoints=nsamples, case=case, noise=noise)
    actual_classes = eval_vanilla_function(npoints=251, seed=None, case=case)
    predicted_classes = actual_classes.copy()

    ########################################################################################

    # Train the decision tree
    t0 = time.time()
    nn = NeuralNetwork(db, "c", nUnits, "one-of-k classification", activation_functions,
                       max_training_iters=None)
    nn.train_network(niter=10000, optimization_method="enhanced gradient descent")
    sys.stdout.write("\t#iterations:         {:.3f}\n".format(nn.training_stats[0]["#iterations"]))
    nn.train_network(niter=9000, optimization_method="nonlinear conjugate gradient")
    sys.stdout.write("\t#iterations:         {:.3f}\n".format(nn.training_stats[1]["#iterations"]))
    t1 = time.time()
    sys.stdout.write("\tNN structure:        {}\n".format(nUnits))
    training_time = t1 - t0

    # Show time required to train the model
    sys.stdout.write("Wall time spent in training method: {:.2f}\n".format(training_time))

    _, _, predicted_classes = evaluate_confusion_matrix(nn, actual_classes, "c")

    scatterplot([actual_classes, predicted_classes], None)

    sys.exit(0)
