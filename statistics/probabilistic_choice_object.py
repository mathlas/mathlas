# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


class ProbabilisticChoiceObject:
    def __init__(self, p):
        """
        An object whose value property is returned following the
        probability distribution p

        Parameters
        ----------
        p : dictionary
        """
        if abs(sum(p.values()) - 1.0) > 0.01:
            raise ValueError("Added probability of '{}' is not 1.0".format(p))

        self.p = p
        self.keys, self.values = zip(*sorted(list(p.items())))

    def __repr__(self):
        """
        This'll give you something like '70.0% No, 30.0% Yes'
        """
        return ', '.join(['{:.1f}% {}'.format(100 * self.values[i], k)
                          for i, k in enumerate(self.keys)])

    @property
    def majority_vote(self):
        """
        Return the value with the highest probability
        """
        return self.keys[self.values.index(max(self.values))]
