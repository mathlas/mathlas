# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from probdist import ProbDist, normalization_type


def evaluateModel(x, model_class, model_type, probDistType=None,
                  positions=None, var_ranges=None, randomVars=None):
    """
    Calculates the value of a model given the co-ordinates in (mean, standar
    devaition) format instead of the original distribution parameters.

    Parameters
    ----------
    x : NumPy array
        values of the variables.

    model_class : class object
                  The class of the model that contains the function/method to
                  evaluate.

    model_type : string
                 Key of the model to use. This value is not case sensitive.

    probDistType : string
                   Type of distribution used to model gusts of wind.

    positions : list of 2-tuples
                list of integers that indicates the indexes of the pairs
                (mean, standard deviation) that must be transformed into the
                original co-ordinates.

    var_ranges : list of 2-tuples.
                 List of pairs of values with the lower and upper values of
                 each variable to be converted takes. Each pair corresponds to
                 a pair in positions.

    Returns
    -------
    out : NumPy array
          Values of the functions at the points.

    Raises
    ------
    TypeError
        * If the input `x`is not a NumPy array.

    ValueError
        * If the shape of the input `x` is not correct.
        * If the value of `model_type` does not take an accepted value, namely,
          "mixture density model", "probability distribution", or "beta
          interpolation".
    """
    # Convert data to correct type for one or more dimensions
    if not isinstance(x, np.ndarray):
        raise TypeError("Values of the co-ordinates must be provided as a " +
                        "NumPy array")
    elif len(x.shape) == 1:
        x = np.array([x])
    elif len(x.shape) > 2:
        raise ValueError("Inputs does not have the correct shape.")

    # If a Gaussian function is required
    if model_type.lower() == "gaussian":
        factor = np.sqrt(2. * np.pi) * x[:, 2]
        return np.exp(-0.5 * ((x[:, 0] - x[:, 1]) / x[:, 2]) ** 2) / factor

    # Inputs must verify certain conditions
    if ((model_type.lower() in ["conditional mixture density model",
                                "marginalized mixture density model"]) and
            randomVars is None):
        raise ValueError("This value of <model_type> requires a value of " +
                         "<randomVars> different from None")
    if ((positions is None) and (var_ranges is not None)):
        raise ValueError("Positions are not provided")

    # Generate notRadomVars if randomVars is provided
    if randomVars is not None:
        notRandomVars = [elem for elem in range(x.shape[1])
                         if elem not in randomVars]

    # Generate var_ranges if None is provided
    if positions is None and var_ranges is None:
        positions = []
        var_ranges = []
    elif var_ranges is None:
        var_ranges = [None] * len(positions)

    # Transform data
    y = x.copy()
    jacobian = 1.
    for pos, var_range in zip(positions, var_ranges):
        # Initialize probability distribution
        pd = ProbDist(probDistType, normalization_type[probDistType],
                      var_range)
        # Perform conversion
        mean, stdev = x[:, pos[0]], x[:, pos[1]]
        eta = pd.fit_from_statistics(mean, stdev)
        # Rearrange data
        y[:, pos[0]] = eta[0]
        y[:, pos[1]] = eta[1]
        # Calculate Jacobian for this transformation
        jacobian *= pd.jacobian(mean, stdev, eta[0], eta[1])
    # Calculate function
    if model_type.lower() == "mixture density model":
        return model_class.get_pdf(y) * jacobian
    elif model_type.lower() == "conditional mixture density model":
        return jacobian * model_class.get_conditional_pdf(y[:, notRandomVars],
                                                        y[:, randomVars],
                                                          randomVars)
    elif model_type.lower() == "marginalized mixture density model":
        return jacobian * model_class.get_marginalized_pdf(y, randomVars)
    elif model_type.lower() == "beta interpolation":
        return model_class.get_pdf(y[:, :-1], y[:, -1]) * jacobian
    else:
        raise ValueError("<model_type> value not recognized.")