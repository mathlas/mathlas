import numpy as np
from scipy.misc import factorial
from mathlas.plotting.mathlas_plots import MathlasPlot


def lambda_u(a, b, c, n):

    # Avoids values of a out of the valid interval
    l_sub_star = 0 if c > 0 else np.max([0., 1. - 1. / b])
    a_sub_star = np.max([1. / 2. / n, 1. - b]) if c > 0 else np.max([0., 1. - b])
    a_sup_star = np.max([1., b]) if c > 0 else 1.
    if a <= a_sub_star:
        return l_sub_star
    if a >= a_sup_star:
        return 1

    l_up = lambda y: np.max([1. + a + b * (y - 1.), 1. - c])
    l_down = lambda y: 1. - a + b * (y - 1.)

    p = np.int(np.floor(l_up(0) * n) + 1)
    q = np.int(np.floor(l_down(1) * n))

    fu = [(p + ii) / n for ii in range(n + 1)]
    fv = [ii / n for ii in range(n + 1)]

    u = [(f - 1. - a) / b + 1. for f in fu]
    u = [f for f in u if f <= 1.]
    v = [(f - 1. + a) / b + 1. for f in fv]
    v = [f for f in v if f <= 1.]

    r = [np.max([j for j, vj in enumerate(v) if vj < uk] + [-10]) for uk in u]

    n_u = len(u)
    n_v = len(v)

    alpha, beta = [], []
    k_v, k_u = 0, p
    while True:
        flag_v = True
        if k_v < n_v:
            try:
                v_k = v[k_v]
                c0 = np.exp(k_v * np.log(v_k) - np.log(factorial(k_v)))
                d0 = 0.
                for i in range(p, k_v + 1):
                    # d0 += np.exp((k_v - i) * np.log(v_k - u[i - p]) - np.log(factorial(k_v - i))) * alpha[i - p]
                    d0 += (((v_k - u[i - p]) ** (k_v - i)) / factorial(k_v - i)) * alpha[i - p]
                d1 = 0.
                for j in range(k_v):
                    # d1 += np.exp((k_v - j) * np.log(v_k - v[j]) - np.log(factorial(k_v - j))) * beta[j]
                    d1 += (((v_k - v[j]) ** (k_v - j)) /factorial(k_v - j)) * beta[j]
                beta.append(c0 - d0 - d1)
                k_v += 1
                flag_v = False
            except:
                flag_v = True

        flag_u = True
        if k_u < n_u + p:
            try:
                u_k = u[k_u - p]
                c0 = np.exp(k_u * np.log(u_k) - np.log(factorial(k_u)))
                d0 = 0.
                for i in range(p, k_u):
                    # d0 += np.exp((k_u - i) * np.log(u_k - u[i - p]) - np.log(factorial(k_u - i))) * alpha[i - p]
                    d0 += (((u_k - u[i - p]) ** (k_u - i)) / factorial(k_u - i)) * alpha[i - p]
                d1 = 0.
                if len(v) != 0 and v[0] < u_k:
                    for j in range(r[k_u - p] + 1):
                        # d1 += np.exp((k_u - j) * np.log(u_k - v[j]) - np.log(factorial(k_u - j))) * beta[j]
                        d1 += (((u_k - v[j]) ** (k_u - j)) / factorial(k_u - j)) * beta[j]
                alpha.append(c0 - d0 - d1)
                k_u += 1
                flag_u = False
            except:
                flag_u = True

        if flag_v and flag_u:
            raise ValueError("ALL IS WRONG")

        if k_u == n_u + p and k_v == n_v:
            break

    pochhammer = lambda n, k: np.prod([np.float64(ii) for ii in range(n-k+1, n+1)])

    pab = 0.
    for i in range(p, n + 1):
        # pab += np.exp((n - i) * np.log(1. - u[i - p]) - np.log(factorial(n - i))) * alpha[i - p]
        pab += pochhammer(n, i) * ((1. - u[i - p]) ** (n - i)) * alpha[i - p]
    for j in range(q + 1):
        # pab += np.exp((n - j) * np.log(1. - v[j]) - np.log(factorial(n - j))) * beta[j]
        pab += pochhammer(n, j) * ((1. - v[j]) ** (n - j)) * beta[j]
    # pab *= factorial(n)

    # plt = MathlasPlot()
    # x = np.linspace(0., 1., 101)
    # upper = [l_up(xi) for xi in x]
    # lower = [l_down(xi) for xi in x]
    # plt.plot(x, upper)
    # plt.plot(x, lower, range_x=(0., 1.), range_y=(0., 1.))
    # plt.show()

    return 1 - pab


def guilbaud_s_a_function(a0, b, c, m2, m1):

    l_sub_star = 0 if c > 0 else np.max([0., 1. - 1. / b])
    a_sub_star = np.max([1. / 2. / m2, 1. - b]) if c > 0 else np.max([0., 1. - b])
    a_sup_star = np.max([1., b]) if c > 0 else 1.

    l_u_target = lambda_u(a0, b, c, m1)

    x = [a_sub_star, 0.5 * (a_sub_star + a_sup_star), a_sup_star]
    y = [lambda_u(a, b, c, m2) - l_u_target for a in x]

    if y[0] * y[2] > 0:
        raise ValueError("Initial values of the bisection method are of the same sign.")

    while True:
        if y[0] * y[1] < 0:
            x = [x[0], 0.5 * (x[0] + x[1]), x[1]]
            y = [y[0], lambda_u(x[1], b, c, m2) - l_u_target, y[1]]
        else:
            x = [x[1], 0.5 * (x[1] + x[2]), x[2]]
            y = [y[1], lambda_u(x[1], b, c, m2) - l_u_target, y[2]]

        if x[2] - x[0] < 1e-10:
            break

    return x[1]


if __name__ == "__main__":


    triplets = [(0.01, 5, 0.66853), (0.02, 2, 0.90), (0.02, 5, 0.62718), (0.02, 10, 0.45662), (0.02, 20, 0.32866),
                (0.02, 38, 0.24089), (0.20, 4, 0.49), (0.05, 8, 0.45427), (0.10, 10, 0.36866)]
    for alpha, n, solution in triplets:
        eps = guilbaud_s_a_function(1. - alpha / 2, 1., 1., n, 1)
        print("For (alpha, n) = ({}, {}) our solution is {:5f} while it must be {}".format(alpha, n, eps, solution))

    b = 1.
    for n in [1, 2, 5, 10, 20]:
        a_list = np.linspace(0.01, 0.95, 200)
        p01, p05, p10 = [], [], []
        for a in a_list:
            p01.append(lambda_u(a, b, 0.1, n))
            p05.append(lambda_u(a, b, 0.5, n))
            p10.append(lambda_u(a, b, 1.0, n))
        plt = MathlasPlot()
        plt.plots(a_list, [p01, p05, p10], var_labels=["c=0.1", "c=0.5", "c=1.0"], title="n={}".format(n))
        plt.show()

    #################
    # Ejemplo Mathlas
    #################
    print("j = 1")
    a_1 = 0.436068389
    b = 0.882496903
    c = 0.446428514
    N0 = 14
    N1 = 7
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 2")
    a_1 = 0.418838552
    b = 1.095446515
    c = 0.568181745
    N0 = 10
    N1 = 11
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 4")
    a_1 = 0.484559637
    b = 1.129214855
    c = 0.63131305
    N0 = 8
    N1 = 9
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 5")
    a_1 = 0.581512759
    b = 1.258667932
    c = 0.677155173
    N0 = 13
    N1 = 7
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 6")
    a_1 = 0.471896442
    b = 1.22759131
    c = 0.677155173
    N0 = 12
    N1 = 13
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 9")
    a_1 = 0.583203592
    b = 1.267917159
    c = 0.684713567
    N0 = 9
    N1 = 10
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 10")
    a_1 = 0.604244328
    b = 1.20608011
    c = 0.601835781
    N0 = 8
    N1 = 9
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 11")
    a_1 = 0.63173525
    b = 1.176301886
    c = 0.544566635
    N0 = 7
    N1 = 8
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 14")
    a_1 = 0.932171168
    b = 1.453315879
    c = 0.521144711
    N0 = 4
    N1 = 5
    l_sub_star = 0 if c > 0 else np.max([0., 1. - 1. / b])
    a_sub_star = np.max([1. / 2. / N1, 1. - b]) if c > 0 else np.max([0., 1. - b])
    a_sup_star = np.max([1., b]) if c > 0 else 1.
    print(a_sub_star, a_sup_star)
    if a_1 < a_sub_star or a_1 > a_sup_star:
        print("ojo no cumple criterio")
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 13")
    a_1 = 0.588744358
    b = 1.527828977
    c = 0.93908462
    N0 = 6
    N1 = 6
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 15")
    a_1 = 0.588744358
    b = 1.219996218
    c = 0.63125186
    N0 = 5
    N1 = 4
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 16")
    a_1 = 0.539928949
    b = 1.1604963
    c = 0.620567351
    N0 = 4
    N1 = 2
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))

    print("j = 17")
    a_1 = 0.394182031
    b = 1.131843544
    c = 0.620567351
    N0 = 2
    N1 = 1
    lamb = lambda_u(a_1, b, c, N1)
    print("Objective Lambda:", lamb)
    a_0 = guilbaud_s_a_function(a_1, b, c, N0, N1)
    print("a_0:", a_0)
    print("Lambda (a_0):", lambda_u(a_0, b, c, N0))
    exit(3009)
    # #

    #
    # exit(246)
    #
    # alpha = .90
    # a = 1. - alpha / 2.
    # b = 1.
    # c = 0.2
    # n1 = 1
    # n2 = 10
    #
    # a = 0.1
    # b = 1.0
    #
    # for n in [1, 2, 5, 10, 20]:
    #     a_list = np.linspace(0.01, 0.95, 200)
    #     p01, p05, p10 = [], [], []
    #     for a in a_list:
    #         p01.append(lambda_u(a, b, 0.1, n))
    #         p05.append(lambda_u(a, b, 0.5, n))
    #         p10.append(lambda_u(a, b, 1.0, n))
    #
    #     plt = MathlasPlot()
    #     plt.plots(a_list, [p01, p05, p10], var_labels=["c=0.1", "c=0.5", "c=1.0"], title="n={}".format(n))
    #     plt.show()
    #
    # exit(234)
    #
    # l_sub_star = 0 if c > 0 else np.max([0., 1. - 1. / b])
    # a_sub_star = np.max([1. / 2. / n1, 1. - b]) if c > 0 else np.max([0., 1. - b])
    # a_sup_star = np.max([1., b]) if c > 0 else 1.
    #
    # lambda_u(a_sub_star, b, c, n2)
    # a_sub_star = np.max([1. / 2. / n2, 1. - b]) if c > 0 else np.max([0., 1. - b])
    # lambda_u(a_sub_star, b, c, n1)
    #
    # exit(123)
    #
    #
    #
    # lambda_u(a, b, c, n1)
    #
    # a = 0.1
    # b = 1.
    # c = 1.0
    # n = 1
    # lambda_u(a, b, c, n)
    #
    # a = 0.1
    # b = 1.
    # c = 1.0
    # n = 8
    # lambda_u(a, b, c, n)
    #
    # a = 0.1
    # b = 1.
    # c = 0.7
    # n = 8
    # lambda_u(a, b, c, n)


    print(lambda_u(0.15, 1, 1, 8))