# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import pandas as pd
import sys
from mathlas.statistics.product_limit import pl
from mathlas.statistics.durbin import lambda_u, guilbaud_s_a_function


def guilbaud_test(surv, data, alpha, sampling='All', time=None, return_iterations_data=False):
    """
    Applies Guilbaud's version of Kolmogorov-Smirnov (KS) test for truncated and censored data.

    The function applies the Guilbaud's version of KS test for left truncated and/or right censored
    data. The method returns a boolean (True if the test is successfully passed) and the
    intermediate data at each iteration if required.

    As in the KS test, a region around the survival function to be tested is defined by means of an
    upper and lower bounds. If the experimental survival function based on data lays inside the
    limits the test is considered passed.

    The experimental survival function is computed using the product limit algorithm [1] that takes
    into account left-truncated and right-censored data. The upper and lower bounds are defined
    around the survival function to be tested using the Guilbaud's method [2].

    Parameters
    ----------
    surv : function
           Survival function to be tested. It takes a float equal or greater than zero and returns
           a float value between 0 and 1 both included. The survival function should be a
           decreasing function.

    data : Pandas DataFrame
           Experimental survival data. Each individual is stored in a row. The DataFrame must
           contain three columns, namely:

            * `"entry"` float which stands for the date in which the individual is introduced in the
              study (if larger than zero, this means the data is left-truncated)
            * `"exit"` float which stands for the date in which the individual is removed from the
              study (it can be because the individual dies or because the individual is extracted in
              the study and is not longer taken into account, that is, right-censored)
            * `"censored"` boolean which is True if the data is censored and False if the data
              related to a death at date `exit`

    alpha : float
            significance level 0 < alpha < 1

    sampling : string, optional
           Defines the sampling mode:

           * If sampling = 'All' (default) every entry and exit time is considered
           * If sampling = 'Deaths' only death times are considered (not implemented yet)
           * If sampling = 'Censored' only censored times are considered (not implemented yet)
           * If sampling = 'Userdefined' time sampling is provided by the user via `time`
             argument (not implemented yet)

    time : 1D NumPy array, optional
           Defines the time values used to compute the product limit survival function. If sampling
           different from 'All' it must be provided by the user.

    return_iterations_data : boolean
                             If True returns the iteration DataFrame that includes the value of the
                             intermediate variables at each iteration.

    Returns
    -------
    test_result : boolean
                  True if the test passed, False otherwise

    iterations_data : Pandas DataFrame, optional
                      Only returned if return_iterations_data is True.
                      It is a DataFrame with one row per iteration and the following columns:

                      * `"t"` (float) time value.
                      * `"S"` (float) value of the survival function :math:`S` at :math:`t_j`
                      * `"pl"` (float) value of the experimental survival function
                        :math:`\hat{S}(t)` computed
                        via the product limit algorithm [1]
                      * `"S_bot"` (float) value of the acceptance region lower bound
                        :math:`S_*(t_j)`
                      * `"S_up"` (float) value of the acceptance region upper bound
                        :math:`S^*(t_j)`
                      * `"epsilon"` (float) value of the band width that defines the lower
                        and upper bound (see [2]).
                      * `"N0"` (integer) number of individuals that are monitored in the
                        time interval :math:`[t_j,t_{j+1})`
                      * `"N1"` (integer) number of individuals that were monitored during
                        the time interval :math:`[t_{j-1}, t_j)` and survive at :math:`t_j`
                      * `"a_1"` (float) value of
                        :math:`a_1(t_j) = \varepsilon(t_j)/\hat{S}(t_j)`
                      * `"b"` (float) value of :math:`b(t_j) = S(t_j)/\hat{S}(t_j)`
                      * `"c"` (float) value of :math:`c(t_j) = S_*(t_j)/\hat{S}(t_j)`
                      * `"lambda_U"` (float) value of :math:`\lambda_{U,j} =
                        \lambda_U(a_1(t_j),b(t_j),c(t_j),N_1(t_j))`
                      * `"a_0"` (float) value of :math:`a_0(t_j)` that meets
                        :math:`\lambda_{U,j} = \lambda_U(a_0(t_j),b(t_j),c(t_j),N_0(t_j))`
                        (see [2])
                      * `"test_result"` (boolean) True if :math:`S_*(t_j)\le \hat{S}(t_j)`
                        and :math:`\hat{S}(t_{j-1})\le S^*(t_j)`

    References
    ----------
    [1] Kaplan, E. L. & Meier, P., Nonparametric Estimation from Incomplete Observations Journal of
    the American Statistical Association, American Statistical Association, 1958, 53, 457-481.

    [2] Guilbaud, O., Exact Kolmogorov-type tests for left-truncated and/or right-censored data
    Journal of the American Statistical Association, 1988, 83, 213-221.
    """
    # Check data
    if not isinstance(data, pd.DataFrame):
        raise ValueError("Input argument data must be a Pandas DataFrame")

    if not data.columns.equals(pd.Index(["entry", "exit", "censored"])):
        raise ValueError("Data wrong format. Columns names do not match the required ones")

    for num, column in zip(data.dtypes[0:2], data.columns[0:2]):
        if not num == float:
            # Try to convert to float
            try:
                data[column] = data[column].astype(float)
            except:
                raise ValueError("data {} column must be float and it was unable to"
                                 " convert it.\n".format(column))
            sys.stderr.write("Warning: data {} column converted to float.\n".format(column))

    if not data.dtypes[2] == bool:
        raise ValueError("data 'censored' column must be boolean")

    # Check sampling
    if not sampling == 'All':
        raise ValueError("Sampling option not implemented yet")

    # Compute the experimental survival function based on data via the product limit algorithm
    result_pl = pl(data, sampling, time, True)
    t = result_pl["t"].values
    S_hat = result_pl["pl"].values
    N0 = result_pl["N0"].values
    N1 = result_pl["N1"].values

    # Compute objective survival function
    S = surv(t)

    # Initialize iteration arrays
    S_bot = np.ones(t.shape[0])
    S_up = np.ones(t.shape[0])
    epsilon = np.zeros(t.shape[0])
    a_1 = np.zeros(t.shape[0])
    b = np.zeros(t.shape[0])
    c = np.zeros(t.shape[0])
    lamb = np.zeros(t.shape[0])
    a_0 = np.zeros(t.shape[0])
    condition = np.zeros(t.shape[0], dtype=int)
    condition[0] = 1
    a_0[0] = guilbaud_s_a_function(1. - alpha / 2., 1., 1., N0[0], 1)

    for index in range(1, t.shape[0]):
        epsilon[index] = S_hat[index - 1] * a_0[index - 1]  # Compute epsilon[j]
        # Compute S_bot[j]
        S_bot[index] = max([0., min([S_bot[index - 1], S[index] - epsilon[index]])])
        S_up[index] = min([S_up[index - 1], S[index] + epsilon[index]])  # Compute S_up[j]
        # Check the product limit is between the boundaries:
        if (S_hat[index] >= S_bot[index]) and (S_hat[index - 1] <= S_up[index]):
            condition[index] = 1
        else:  # Product limit ot of bounds
            break

        if index < t.shape[0]-1:  # The following is not computed in the last iteration
            a_1[index] = epsilon[index] / S_hat[index]  # Compute a_1[j]
            b[index] = S[index] / S_hat[index]
            c[index] = S_bot[index] / S_hat[index]
            lamb[index] = lambda_u(a_1[index], b[index], c[index], N1[index])
            a_0[index] = guilbaud_s_a_function(a_1[index], b[index], c[index], N0[index], N1[index])

    result_test = True if condition[-1] == 1 else False

    if return_iterations_data:
        df = pd.DataFrame(np.column_stack((t, S, S_hat, S_bot, S_up, epsilon, N0, N1, a_1, b, c,
                                           lamb, a_0, condition)),
                          columns=["t", "S", "pl", "S_bot", "S_up", "epsilon", "N0", "N1",
                                   "a_1", "b", "c", "lambda_U", "a_0", "test_result"])
        df['N0'] = df['N0'].astype(int)
        df['N1'] = df['N1'].astype(int)
        df['test_result'] = df['test_result'].astype(bool)
        return result_test, df
    else:
        return result_test

if __name__ == "__main__":
    pass
