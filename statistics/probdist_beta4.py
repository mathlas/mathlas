# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from scipy.special import polygamma, betainc
from scipy.special import beta as betaf


def get_pdf(samples, hyperparameters):
    """
    Evaluates the values of the Beta pdf for a given set of points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the pdf is to be evaluated

    hyperparameters : iterable
                      Iterable with alpha, beta, minX, and maxX.

    Returns
    -------
    pdf : float or iterable
          Value of the pdf. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.

    Notes
    -----
    If there ara values in `X` that are lower than `minX` or larger than `maxX`
    the code will crash. No check is performed. User must check it.
    """
    # Unzip hyperparameters
    alpha, beta, minX, maxX = hyperparameters

    # Evaluate pdf
    XI = (samples - minX) / (maxX - minX)
    B = betaf(alpha, beta)
    if B == 0:
        ly = ((alpha - 1.) * np.log(XI + 1e-300))
        ly += ((beta - 1.) * np.log(1. - XI + 1e-300))
        a = alpha - 1.
        b = beta - 1.
        c = alpha + beta - 1.
        lb = (a * np.log(a) - a + 0.5 * np.log(2. * np.pi * a) +
              b * np.log(b) - b + 0.5 * np.log(2. * np.pi * b) -
              c * np.log(c) + c - 0.5 * np.log(2. * np.pi * c))
        return np.exp(ly - lb)
    else:
        y = (XI ** (alpha - 1.)) * ((1. - XI) ** (beta - 1.))
        return y / B


def get_cdf_quantile(samples, hyperparameters):
    """
    Evaluates the quantiles of the Beta distribution for a given set of
    points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the cdf is to be evaluated

    hyperparameters : iterable
                      Iterable with alpha, beta, minX, and maxX.

    Returns
    -------
    cdf : float or iterable
          Value of the quantile. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.

    Notes
    -----
    If there ara values in `X` that are lower than `minX` or larger than `maxX`
    the code will crash. No check is performed. User must check it.
    """
    # Unzip hyperparameters
    alpha, beta, minX, maxX = hyperparameters
    # Evaluate cdf
    Z = []
    for x in samples:
        xi = (x - minX) / (maxX - minX)
        if xi < 0.:
            v = 0.
        elif xi > 1.:
            v = 1.
        else:
            v = betainc(alpha, beta, xi)
        Z.append(v)
    return np.array(Z)


def get_cdf_point(quantiles, hyperparameters):
    """
    Evaluates the point at which the quantile takes the given value for the
    Beta distribution.

    Parameters
    ----------
    quantiles : iterable
                One-dimensional NumPy array with values or float for those quantile
                 whose point must be calculated.

    hyperparameters : iterable
                      Iterable with alpha, beta, minX, and maxX.

    Returns
    -------
    X : float or iterable
        Value of the point. If X is a float, return value is a float.
        Otherwise the return value is a NumPy array.
    """
    # Unzip hyperparameters
    alpha, beta, minX, maxX = hyperparameters
    # Solve the incomplete beta using a bisection method
    result = []
    for q in quantiles:
        x0, x2 = 0., 1.
        y0 = betainc(alpha, beta, x0) - q
        y2 = betainc(alpha, beta, x2) - q
        if y0 * y2 > 0:
            raise ValueError("Something strange has happened")
        err = x2 - x0
        while err > 1e-4:
            x1 = 0.5 * (x0 + x2)
            y1 = betainc(alpha, beta, x1) - q

            if y0 * y1 > 0:
                x0 = x1
                y0 = y1
            else:
                x2 = x1
                y2 = y1
            err = x2 - x0
        result.append(x1 * (maxX - minX) + minX)

    return np.array(result)


def fit_from_samples(samples):
    """
    Fits the data to a beta distribution using an heuristic method based on
    iteratively solving two `2x2` non-linear systems.

    Parameters
    ----------
    samples : iterable
              One-dimensional dataset.

    Returns
    -------
    alpha : iterable or float
            first exponent
    beta : iterable or float
           second exponent
    minV : iterable or float
           minimum values
    maxV : iterable or float
           maximum values
    loglikelihood : float
                    natural logarithm of the likelihood function
    """
    # Assing initial values to the maximum and the minimum
    minX, maxX = min(samples), max(samples)
    gap = (maxX - minX)
    minX -= 5e-4 * gap
    maxX += 5e-4 * gap
    # Evaluate some useful variables
    N = len(samples)
    a, b = 0., 0.
    g0, g1 = 1., 1.
    for x in samples:
        xi = (x - minX) / (maxX - minX)
        a += np.log(xi)
        b += np.log(1. - xi)
        g0 *= xi ** (1. / N)
        g1 *= (1. - xi) ** (1. / N)
    a /= N
    b /= N
    # Initialze values of alpha and beta
    alpha = 0.5 + 0.5 * g0 / (1. - g0 - g1)
    beta = 0.5 + 0.5 * g1 / (1. - g0 - g1)

    # Fitting is performed using a homebrewed strategy.
    # Two nested processes are carried on.
    # The outer loop stops if the objective funcion is enough small.
    should_outer_loop = True
    while should_outer_loop:

        # The first inner loop calculates alpha and beta by assuming that
        #  the maximum and the minimum are known.
        should_loop = True
        while should_loop:
            # Calculate some useful values
            a0, a1 = polygamma(0, alpha), polygamma(1, alpha)
            b0, b1 = polygamma(0, beta), polygamma(1, beta)
            c0, c1 = polygamma(0, alpha + beta), polygamma(1, alpha + beta)
            # Evaluate function
            f = np.array([[a0 - c0 - a], [b0 - c0 - b]])
            # Evaluate Jacobian of the function
            jf = np.array([[a1 - c1, -c1], [-c1, b1 - c1]])
            # Modify values and decide if continue iterating
            delta = np.linalg.solve(jf, f)
            alpha -= delta[0, 0]
            beta -= delta[1, 0]
            should_loop = np.linalg.norm(delta) > 1e-6

        # Evaluate if the solution is a local maximum or not
        a1 = polygamma(1, alpha)
        b1 = polygamma(1, beta)
        c1 = polygamma(1, alpha + beta)
        if not (a1 - c1 > 0 and b1 - c1 > 0):
            raise ValueError("Solution found is not a local maximum")

        should_loop = True
        while should_loop:
            # Calculate some useful values for the evaluation of the
            #  function
            h0, h1 = 1., 1.
            for x in samples:
                xi = (x - minX) / (maxX - minX)
                h0 += 1. / xi
                h1 += 1. / (1. - xi)
            h0 = N / h0
            h1 = N / h1
            # Evaluate function
            f = np.array([[(alpha - 1.) / (alpha + beta - 1.) - h0],
                          [(beta - 1.) / (alpha + beta - 1.) - h1]])
            # Calculate some useful values for the evaluation of the
            #  jacobian
            d1, d2 = 0., 0.
            i0, i1 = 0., 0.
            for x in samples:
                xi = (x - minX) / (maxX - minX)
                d1 += 1. / (x - minX)
                d2 += 1. / (maxX - x)
                i0 += (maxX - x) / (x - minX) ** 2
                i1 += (minX - x) / (maxX - x) ** 2
            d1 /= N
            d2 /= N
            H0 = (h0 ** 2) / N
            H1 = (h1 ** 2) / N
            # Evaluate Jacobian of the function
            jf = np.array([[N * d1 * H0, H0 * i0],
                           [H1 * i1, -N * d2 * H1]])
            # Modify values and decide if continue iteration
            delta = np.linalg.solve(jf, f)
            maxX -= delta[0, 0]
            minX -= delta[1, 0]
            should_loop = np.linalg.norm(delta) > 1e-6

        # The complete function is evaluated to check if the outer loop
        #  should stop
        # Calculate some useful values
        a0, a1 = polygamma(0, alpha), polygamma(1, alpha)
        b0, b1 = polygamma(0, beta), polygamma(1, beta)
        c0, c1 = polygamma(0, alpha + beta), polygamma(1, alpha + beta)
        a, b = 0., 0.
        h0, h1 = 1., 1.
        for x in samples:
            xi = (x - minX) / (maxX - minX)
            a += np.log(xi)
            b += np.log(1. - xi)
            h0 += 1. / xi
            h1 += 1. / (1. - xi)
        a /= N
        b /= N
        h0 = N / h0
        h1 = N / h1
        # Evaluate function
        f = np.array([[a0 - c0 - a],
                      [b0 - c0 - b],
                      [(alpha - 1.) / (alpha + beta - 1.) - h0],
                      [(beta - 1.) / (alpha + beta - 1.) - h1]])
        should_outer_loop = np.linalg.norm(f) > 1e-3

    # Calculate likelihood
    log_likelihood = - N * np.log(betaf(alpha, beta))
    lg0, lg1 = 0., 0.
    for x in samples:
        xi = (x - minX) / (maxX - minX)
        lg0 += np.log(xi)
        lg1 += np.log(1. - xi)
    log_likelihood += (alpha - 1.) * lg0 + (beta - 1.) * lg1
    log_likelihood /= N

    # Return results
    return alpha, beta, minX, maxX, log_likelihood


def fit_from_statistics(mean, stdev, minX, maxX):
    """
    Fits the statistics to a Beta distribution.

    Parameters
    ----------
    mean : iterable
           Average values

    stdev : iterable
            Standard deviation

    minX : iterable
           Lower bounds for the distributions.

    maxX : iterable
           Upper bounds for the distributions.

    Returns
    -------
    alpha : iterable or float
            first exponent
    beta : iterable or float
           second exponent
    minV : iterable or float
           minimum values
    maxV : iterable or float
           maximum values

    Notes
    -----
    The length of `alpha`, `beta`, `minV`, and `maxV` must be equal.
    """
    if ((len(mean) != len(stdev)) or (len(mean) != len(minX)) or
            (len(mean) != len(maxX)) or (len(stdev) != len(minX)) or
            (len(stdev) != len(maxX)) or (len(minX) != len(maxX))):
        raise ValueError("Inputs must be of the same size.")
    # Choose data to fit
    # Assign values to the maximum and the minimum
    alpha, beta, minV, maxV = [], [], [], []
    for m, std in zip(mean, stdev):
        # Initialize some parameters
        v = std * std
        ba = 1. / (m + 1e-6) - 1.
        constant = ba / (1. + ba) ** 2 / (v + 1e-6)
        # Iterate until a solution is found
        a = 2.
        should_loop = True
        while should_loop:
            new_a = constant / (1. + ba + 1. / a)
            should_loop = np.abs(new_a - a) > 1e-3
            a = new_a
        alpha.append(a)
        beta.append(ba * a)
        minV.append(minX)
        maxV.append(maxX)

    return np.array(alpha), np.array(beta), np.array(minV), np.array(maxV)


def fit_from_samples_old(X):
    """
    Fits the data to a beta distribution solving a `4x4` non-linear system of
    equations.

    Parameters
    ----------
    X : iterable
        One-dimensional dataset.

    Returns
    -------
    alpha : iterable or float
            first exponent
    beta : iterable or float
           second exponent
    minV : iterable or float
           minimum values
    maxV : iterable or float
           maximum values
    loglikelihood : float
                    natural logarithm of the likelihood function
    """
    # Assing initial values to the maximum and the minimum
    minX, maxX = min(X), max(X)
    gap = (maxX - minX)
    minX -= 5e-4 * gap
    maxX += 5e-4 * gap
    # Evaluate some useful variables
    N = len(X)
    a, b = 0., 0.
    g0, g1 = 1., 1.
    for x in X:
        xi = (x - minX) / (maxX - minX)
        a += np.log(xi)
        b += np.log(1. - xi)
        g0 *= xi ** (1. / N)
        g1 *= (1. - xi) ** (1. / N)
    a /= N
    b /= N
    # Initialze values of alpha and beta
    alpha = 0.5 + 0.5 * g0 / (1. - g0 - g1)
    beta = 0.5 + 0.5 * g1 / (1. - g0 - g1)

    should_loop = True
    while should_loop:
        # Calculate some useful values for the evaluation of the function
        a0, a1 = polygamma(0, alpha), polygamma(1, alpha)
        b0, b1 = polygamma(0, beta), polygamma(1, beta)
        c0, c1 = polygamma(0, alpha + beta), polygamma(1, alpha + beta)

        a, b = 0., 0.
        h0, h1 = 1., 1.
        for x in X:
            xi = (x - minX) / (maxX - minX)
            a += np.log(xi)
            b += np.log(1. - xi)
            h0 += 1. / xi
            h1 += 1. / (1. - xi)
        a /= N
        b /= N
        h0 = N / h0
        h1 = N / h1

        # Evaluate function
        f = np.array([[a0 - c0 - a],
                      [b0 - c0 - b],
                      [(alpha - 1.) / (alpha + beta - 1.) - h0],
                      [(beta - 1.) / (alpha + beta - 1.) - h1]])
        # Calculate some useful values for the evaluation of the Jacobian
        d1, d2 = 0., 0.
        i0, i1 = 0., 0.
        for x in X:
            xi = (x - minX) / (maxX - minX)
            d1 += 1. / (x - minX)
            d2 += 1. / (maxX - x)
            i0 += (maxX - x) / (x - minX) ** 2
            i1 += (minX - x) / (maxX - x) ** 2
        d1 /= N
        d2 /= N
        H0 = (h0 ** 2) / N
        H1 = (h1 ** 2) / N
        k2 = (alpha + beta - 1.) ** 2
        # Evaluate Jacobian of the function
        jf = np.array([[a1 - c1, -c1, 1. / gap, -1. / gap + d1],
                       [-c1, b1 - c1, 1. / gap - d2, -1. / gap],
                       [beta / k2, (1. - alpha) / k2,
                        N * d1 * H0, H0 * i0],
                       [(1. - beta) / k2, alpha / k2,
                        H1 * i1, -N * d2 * H1]])
        # Modify values and decide if continue iteration
        delta = np.linalg.solve(jf, f)
        alpha -= delta[0, 0]
        beta -= delta[1, 0]
        maxX -= delta[2, 0]
        minX -= delta[3, 0]
        should_loop = np.linalg.norm(delta) > 1e-6

    # Calculate likelihood
    loglikelihood = - N * np.log(betaf(alpha, beta))
    lg0, lg1 = 0., 0.
    for x in X:
        xi = (x - minX) / (maxX - minX)
        lg0 += np.log(xi)
        lg1 += np.log(1. - xi)
    loglikelihood += (alpha - 1.) * lg0 + (beta - 1.) * lg1
    loglikelihood /= N

    # Return results
    return alpha, beta, minX, maxX, loglikelihood
