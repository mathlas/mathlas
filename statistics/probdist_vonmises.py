# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
from scipy.special import iv


def get_pdf(samples, hyperparameters):
    """
    Evaluates the values of the von Mises pdf for a given set of points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the pdf is to be evaluated

    hyperparameters : iterable
                      Iterable with mu and kappa.

    Returns
    -------
    pdf : float or iterable
          Value of the pdf. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.

    Notes
    -----
    Values must be normalized in the interval [0, 1]. No check is performed.
    User must check it.
    """
    # Unzip hyperparameters
    mu, k = hyperparameters
    # Evaluate pdf
    T = 2. * np.pi * samples
    e = k * np.cos(T - mu)
    f = 2. * np.pi * iv(0, k)
    return 2. * np.pi * np.exp(e) / f


def get_cdf_quantile(samples, hyperparameters):
    """
    Evaluates the quantiles of the von Mises distribution for a given set of
    points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the cdf is to be evaluated

    hyperparameters : iterable
                      Iterable with mu and kappa.

    Returns
    -------
    cdf : float or iterable
          Value of the quantile. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.
    """
    # Unzip hyperparameters
    mu, k = hyperparameters
    # Evaluate pdf
    I0 = iv(0, k)
    S = 0.
    ii = 1
    f = 1e30
    while f > 1e-5:
        Iii = iv(ii, k)
        S += Iii * np.cos(ii * (samples - mu))
        f = Iii / I0
    return ((S * 2. / I0) + 1.) / (2. * np.pi)


def fit_from_samples(samples):
    """
    Fits the data to a von Mises distribution.

    Parameters
    ----------
    samples : iterable
        One-dimensional dataset.

    Returns
    -------
    mu : float
         average
    kappa : float
            precision-like parameter
    loglikelihood : float
                    natural logarithm of the likelihood function
    """
    # Evaluate some useful variables
    N = len(samples)
    T = 2. * np.pi * np.array(samples)
    Z = np.exp(1j * T)
    EZ = np.average(Z)
    R2 = np.average(np.sin(T)) ** 2 + np.average(np.cos(T)) ** 2
    Re = np.sqrt(N / (N - 1.) * (R2 - 1. / N))
    # Estimation of mu
    mu = np.log(EZ / Re).imag % (2. * np.pi)
    # Estimation of k
    k = 2.
    f = iv(1, k) / iv(0, k) - Re
    should_loop = np.abs(f) > 1e-6
    while should_loop:
        km = k - 1e-4
        kp = k + 1e-4
        fm = iv(1, km) / iv(0, km)
        fp = iv(1, kp) / iv(0, kp)
        df = (fp - fm) / 2e-4
        dk = f / df
        while k - dk < 0:
            dk /= 2
        k -= dk
        if k > 700:
            k = 700.
            break
        f = iv(1, k) / iv(0, k) - Re
        should_loop = np.abs(dk) > 1e-3 and np.abs(f) > 1e-6
    # Calculate likelihood
    log_likelihood = - np.log(2. * np.pi * iv(0, k))
    for t in T:
        log_likelihood += k * np.cos(t - mu)
    log_likelihood /= N
    # Return results
    return mu, k, log_likelihood
