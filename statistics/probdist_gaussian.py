# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from scipy.stats import norm


def get_pdf(samples, hyperparameters):
    """
    Evaluates the values of the Gaussian pdf for a given set of points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the pdf is to be evaluated

    hyperparameters : iterable
                      Iterable with mu and s2.

    Returns
    -------
    pdf : float or iterable
          Value of the pdf. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.
    """
    mu, s2 = hyperparameters
    return norm.pdf(samples, loc=mu, scale=np.sqrt(s2))


def get_cdf_quantile(samples, hyperparameters):
    """
    Evaluates the quantiles of the Gaussian distribution for a given set of
    points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the cdf is to be evaluated

    hyperparameters : iterable
                      Iterable with mu and s2.

    Returns
    -------
    cdf : float or iterable
          Value of the quantile. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.
    """
    mu, s2 = hyperparameters
    return norm.cdf(samples, loc=mu, scale=np.sqrt(s2))


def get_cdf_point(quantiles, hyperparameters):
    """
    Evaluates the point at which the quantile takes the given value for the
    Gaussian distribution.

    Parameters
    ----------
    quantiles : iterable
                One-dimensional NumPy array with values or float for those quantile
                 whose point must be calculated.

    hyperparameters : iterable
                      Iterable with mu and s2.

    Returns
    -------
    X : float or iterable
        Value of the point. If X is a float, return value is a float.
        Otherwise the return value is a NumPy array.
    """
    mu, s2 = hyperparameters
    return norm.ppf(quantiles, loc=mu, scale=np.sqrt(s2))


def fit_from_samples(samples):
    """
    Fits the data to a normal (Gaussian) distribution.

    Parameters
    ----------
    samples : iterable
              One-dimensional dataset.

    Returns
    -------
    mu : float
         average
    s2 : float
         variance
    loglikelihood : float
                    natural logarithm of the likelihood function
    """
    # Evaluate some useful variables
    N = len(samples)
    EX = np.average(samples)
    EX2 = np.average([x ** 2 for x in samples])
    # Fit using maximum likelihood approximation.
    mu = EX
    s2 = EX2 - EX ** 2
    # Calculate likelihood
    log_likelihood = - 0.5 * N * np.log(2. * np.pi * s2)
    for x in samples:
        log_likelihood -= 0.5 * (x - mu) ** 2 / s2
    log_likelihood /= N
    # Return results
    return mu, s2, log_likelihood


def fit_from_statistics(mean, stdev):
    """
    Fits the statistics to a normal (Gaussian) distribution.

    Parameters
    ----------
    mean : iterable
           Average values

    stdev : iterable
            Standard deviation

    Returns
    -------
    mu : iterable or float
         average
    s2 : iterable or float
         variance

    Notes
    -----
    The length of `mean` and `stdev` must be equal.
    """
    if len(mean) != len(stdev):
        raise ValueError("Inputs must be of the same size.")
    # "Fitting"
    mu = mean
    s2 = np.array([s ** 2 for s in stdev])
    # Return results
    return mu, s2


def jacobian(MEAN, STDEV, MU, SG2):
    """
    Jacobian used for the transformation of variables from Gaussian to
    (mean, standard deviation).

    Parameters
    ----------
    MEAN : NumPy Array
           Mean values

    STDEV : NumPy Array
            Standard deviation

    MU : NumPy Array
         Value of the average, first parameter of the Gaussian distribution

    SG2 : NumPy Array
          Value of the variance, second parameter of the Gaussian distribution

    Returns
    -------
    out : NumPy Array
          Value of the determinant of the jacobian
    """
    return 2. * STDEV
