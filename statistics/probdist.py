# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import probdist_gaussian as gauss
import probdist_lognormal as lognormal
import probdist_weibull as weibull
import probdist_beta as beta
import probdist_vonmises as vonmises
from itertools import product


# Required normalization for each distribution
normalization_type = {"Gaussian": None,
                      "Log-normal": None,
                      "Weibull": None,
                      "Beta": "Compact"}


class ProbDist(object):

    def __init__(self, model, normalization_type=None, var_range=None):
        """
        ProbDist class manages all the coded methods related with probability
        distributions and allows an easy integration with normalizing methods.

        Parameters
        ----------
        model : string, optional
                Type of probability model to be used. It can take the values
                "Gaussian", "Log-normal", "Weibull", "Beta", or "von Mises".

        normalization_type : string, optional
                             Type of method use to normalize the inputs. It can
                             take the values None, "Compact", or "Standard".

                             If None (default) no normalization is performed

        var_range : 2-tuple of floats
                    minimum and maximum values that the random variable can
                    take.
        """
        # Selection of the set of methods to use
        if model == "Gaussian":
            self.problib = gauss
        elif model == "Log-normal":
            self.problib = lognormal
        elif model == "Weibull":
            self.problib = weibull
        elif model == "Beta":
            self.problib = beta
        elif model == "von Mises":
            self.problib = vonmises

        # Check that range has been provided
        if (normalization_type == "Compact" and var_range is None):
            raise ValueError("Variable <var_range> must be provided if " +
                             "normalization_type is Compact")
        # Check that normalization type is correct
        if normalization_type not in [None, "Compact", "Standard"]:
            raise ValueError("Unrecognized value of <normalization_type>")
        # For certain models some operations are compulsory
        must_have_range = ["Beta", "von Mises"]
        if model in must_have_range:
            if var_range is None:
                raise ValueError("Variable <var_range> must be provided")
            if normalization_type != "Compact":
                raise ValueError("<normalization_type> must be Compact")

        # Assign boundaries to the function
        if var_range is None:
            self.max = None
            self.min = None
        elif isinstance(var_range, (list, tuple)):
            self.min, self.max = var_range
            if self.min >= self.max:
                raise ValueError("""Minimum value of the range is greater than
                 the maximum value provided.""")
        else:
            raise TypeError("""Variable var_range is not one of the
             expected types""")
        # Assign value to the normalization flag
        self.normalization_type = normalization_type
        # Store normalization and denormalization methods
        if normalization_type is None:
            self.normalize_data = self._nothing_to_do
            self.denormalize_data = self._nothing_to_do
        elif normalization_type == "Compact":
            self.normalize_data = self._compact_normalization
            self.denormalize_data = self._compact_denormalization
        elif normalization_type == "Standard":
            self.normalize_data = self._standard_normalization
            self.denormalize_data = self._standard_denormalization

    def _nothing_to_do(self, X, **kwargs):
        """
        Method to do nothing.

        Parameters
        ----------
        X : object
            Something

        Returns
        -------
        X : object
            The inputted object, unchanged.
        """
        return X

    def _compact_normalization(self, X, **kwargs):
        """
        Method to compact some variables, normalizing them.

        Parameters
        ----------
        X : iterable
             Values to normalize.

        Returns
        -------
        XI : iterable
             Compacted iterable.
        """
        return (np.array(X) - self.min) / (self.max - self.min)

    def _compact_denormalization(self, XI):
        """
        Method to de-compact.

        Parameters
        ----------
        XI : iterable
             Values to de-compact.

        Returns
        -------
        X : iterable
            De-compacted iterable.
        """
        return (self.max - self.min) * XI + self.min

    def _standard_normalization(self, X, can_calculate_params=False):
        """
        Method to compact some variables, normalizing them.

        Parameters
        ----------
        X : iterable
             Values to normalize.

        Returns
        -------
        XI : iterable
             Compacted iterable.
        """
        if ((not hasattr(self, "mean") or not hasattr(self, "stDev")) and
                can_calculate_params):
            self.mean = np.average(X)
            self.stDev = np.std(X)
        else:
            raise ValueError("<mean> and <stdev> must be provided.")
        return (X - self.mean) / self.stDev

    def _standard_denormalization(self, XI):
        """
        Method to undo the standarization.

        Parameters
        ----------
        XI : iterable
             Values to de-standarize.

        Returns
        -------
        X : iterable
            De-standarize iterable.
        """
        return self.stDev * XI + self.mean

    def get_pdf(self, hyperparameters, samples, product_of_variables=False):
        """
        Evaluates the values of the Gaussian pdf for a given set of points.

        Parameters
        ----------
        hyperparameters : iterable
                          Iterable with mu and s2.

        samples : iterable
                  One-dimensional NumPy array with values or float where
                   the pdf is to be evaluated

        product_of_variables : Boolean.
                               If True, M1 must be equal to M2 and the
                                probability, p(Y[j, 0]|X[j,:]) is calculated for
                                each row; thus, the result is an array of size
                                `Mx1` where `M=M1=M2`. If False For each value of
                                the response and each point in the features space
                                a probability is calculated. In this case an array
                                of size `M1xM2` is obtained; it has the values
                                p(Y[j,0]|[X[k, :]).

        Returns
        -------
        pdf : float or iterable
              Value of the pdf. If X is a float, return value is a float.
              Otherwise the return value is a NumPy array.
        """
        factor = {None: 1.,
                  "Compact": (self.max - self.min
                              if not (self.max is None and self.min is None)
                              else None),
                  "Standard": self.stDev if hasattr(self, "stDev") else None}
        XI = self.normalize_data(samples)
        results = []
        if not product_of_variables:
            for hps, x in product(hyperparameters, XI):
                results.append(self.problib.get_pdf(x, hps))
        else:
            for hps in hyperparameters:
                results.append(self.problib.get_pdf(XI, hps))
        return np.array(results) / factor[self.normalization_type]

    def get_cdf_quantile(self, samples, hyperparameters):
        """
        Evaluates the quantiles of the Gaussian distribution for a given set of
        points.

        Parameters
        ----------
        samples : iterable
                  One-dimensional NumPy array with values or float where
                   the cdf is to be evaluated

        hyperparameters : iterable
                          Iterable with mu and s2.

        Returns
        -------
        cdf : float or iterable
              Value of the quantile. If X is a float, return value is a float.
              Otherwise the return value is a NumPy array.
        """
        XI = self.normalize_data(samples)
        return self.problib.get_cdf_quantile(XI, hyperparameters)

    def get_cdf_point(self, Q, hyperparameters):
        """
        Evaluates the point at which the quantile takes the given value for the
        Gaussian distribution.

        Parameters
        ----------
        Q : iterable
            One-dimensional NumPy array with values or float for those quantile
            whose point must be calculated.

        hyperparameters : iterable
                          Iterable with mu and s2.

        Returns
        -------
        X : float or iterable
            Value of the point. If X is a float, return value is a float.
            Otherwise the return value is a NumPy array.
        """
        XI = self.problib.getCdfpoint(Q, hyperparameters)
        return self.denormalize_data(XI)

    def fit_from_samples(self, X):
        """
        Fits the data to a normal (Gaussian) distribution.

        Parameters
        ----------
        X : iterable
            One-dimensional dataset.

        Returns
        -------
        mu : float
             average
        s2 : float
             variance
        loglikelihood : float
                        natural logarithm of the likelihood function
        """
        XI = self.normalize_data(X, can_calculate_params=True)
        return self.problib.fit_from_samples(XI)

    def fit_from_statistics(self, mean, stdev):
        """
        Fits the statistics to a normal (Gaussian) distribution.

        Parameters
        ----------
        mean : iterable or float
               average values

        stdev : iterable or float
                standard deviations

        Returns
        -------
        mu : iterable or float
             average
        s2 : iterable or float
             variance

        Notes
        -----
        The length of `mean` and `stdev` must be equal.
        """
        factor = {None: (0., 1.),
                  "Compact": ((self.min, self.max - self.min)
                              if not (self.max is None and self.min is None)
                              else None),
                  "Standard": (self.mu, self.stDev) if
                  (hasattr(self, "stDev") and hasattr(self, "mu")) else None}
        n, m = factor[self.normalization_type]
        mean = (np.array(mean) - n) / m
        stdev = np.array(stdev) / m
        return self.problib.fit_from_statistics(mean, stdev)

    def jacobian(self, mean, stdev, param1, param2):
        """
        Determinant of the Jacobian used for the transformation of variables
        from this distribution to (mean, standard deviation).

        Parameters
        ----------
        mean : NumPy Array
               Mean values

        stdev : NumPy Array
                Standard deviation

        param1 : NumPy Array
                 Value of the first parameter of the distribution

        param2 : NumPy Array
                  Value of the second parameter of the distribution

        Returns
        -------
        out : NumPy Array
              Value of the determinant of the jacobian
        """
        return self.problib.jacobian(mean, stdev, param1, param2)
