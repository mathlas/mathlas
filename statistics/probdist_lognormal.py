# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from scipy.stats import norm


def get_pdf(samples, hyperparameters):
    """
    Evaluates the values of the Log-normal pdf for a given set of points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the pdf is to be evaluated

    hyperparameters : iterable
                      Iterable with mu and s2.

    Returns
    -------
    pdf : float or iterable
          Value of the pdf. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.
    """
    # Unzip hyperparameters
    mu, s2 = hyperparameters
    # Evaluate pdf
    e = -0.5 * (np.log(samples + 1e-300) - mu) ** 2 / s2
    f = np.sqrt(2. * np.pi * s2) * samples
    if isinstance(e, float):
        if e < -1000:
            return 0.
        else:
            return np.exp(e) / f
    else:
        return np.exp(e) / f


def get_cdf_quantile(samples, hyperparameters):
    """
    Evaluates the quantiles of the GauLog-normalssian distribution for a given
    set of points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the cdf is to be evaluated

    hyperparameters : iterable
                      Iterable with mu and s2.

    Returns
    -------
    cdf : float or iterable
          Value of the quantile. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.
    """
    mu, s2 = hyperparameters
    return norm.cdf(np.log(samples), loc=mu, scale=np.sqrt(s2))


def get_cdf_point(quantiles, hyperparameters):
    """
    Evaluates the point at which the quantile takes the given value for the
    Log-normal distribution.

    Parameters
    ----------
    quantiles : iterable
                One-dimensional NumPy array with values or float for those quantile
                whose point must be calculated.

    hyperparameters : iterable
                      Iterable with mu and s2.

    Returns
    -------
    X : float or iterable
        Value of the point. If X is a float, return value is a float.
        Otherwise the return value is a NumPy array.
    """
    mu, s2 = hyperparameters
    return np.exp(norm.ppf(quantiles, loc=mu, scale=np.sqrt(s2)))


def fit_from_samples(samples):
    """
    Fits the data to a log-normal distribution.

    Parameters
    ----------
    samples : iterable
              One-dimensional dataset.

    Returns
    -------
    mu : float
         average in the log-space
    s2 : float
         variance in the log-space
    loglikelihood : float
                    natural logarithm of the likelihood function
    """
    # Evaluate some useful variables
    N = len(samples)
    EX = np.average(np.log(samples))
    EX2 = np.average(np.log(samples) ** 2)
    # Fit using maximum likelihood approximation.
    mu = EX
    s2 = EX2 - EX ** 2
    # Calculate likelihood
    log_likelihood = 0.
    for x in samples:
        log_likelihood -= 0.5 * np.log(2. * np.pi * s2 * x ** 2)
        log_likelihood -= 0.5 * (np.log(x) - mu) ** 2 / s2
    log_likelihood /= N
    # Return results
    return mu, s2, log_likelihood


def fit_from_statistics(mean, stdev):
    """
    Fits the statistics to a log-normal distribution.

    Parameters
    ----------
    mean : iterable
           Average values

    stdev : iterable
            Standard deviation

    Returns
    -------
    mu : iterable or float
         average in the log-space
    s2 : iterable or float
         variance in the log-space

    Notes
    -----
    The length of `mean` and `stdev` must be equal.
    """
    if len(mean) != len(stdev):
        raise ValueError("Inputs must be of the same size.")
    # Fitting from mean values and variances
    mean = [m + 1e-16 for m in mean]
    s2 = np.array([np.log(1. + (v / m) ** 2.) for m, v in
                   zip(mean, stdev)])
    mu = np.array([np.log(m + 1e-300) - 0.5 * sigma2 for m, sigma2 in
                   zip(mean, s2)])

    # Return results
    return mu, s2


def jacobian(MEAN, STDEV, MU, SG2):
    """
    Jacobian used for the transformation of variables from Log-normal to
    (mean, standard deviation).

    Parameters
    ----------
    MEAN : NumPy Array
           Mean values

    STDEV : NumPy Array
            Standard deviation

    MU : NumPy Array
         Value of the average of the log-data, first parameter of the
         Log-normal distribution

    SG2 : NumPy Array
          Value of the variance of the log-data, second parameter of the
          Log-normal distribution

    Returns
    -------
    out : NumPy Array
          Value of the determinant of the jacobian
    """
    return (2. * STDEV) / MEAN / (MEAN ** 2 + STDEV ** 2)
