# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import pandas as pd


def confusion_matrix(data, reduced=False, as_dataframe=True):
    """
    Create a confusion matrix for the input data prediction, which needs not be normalized

    Parameters
    ----------
    data: numpy array or pandas DataFrame
          Square matrix containing the data to be analyzed, with actual classes of the data
          in rows and predictions in columns.
    reduced : boolean, optional
              Whether a reduced confusion matrix (containing only precision, recall and F1 columns)
              should be returned.

              Defaults to False (returning the complete matrix)
    as_dataframe : boolean, optional
                   Whether data should be returned as a labeled DataFrame (which is convenient) or
                   a numpy array (which is faster).
                   The same values are returned in either case.

                   If the input is a pandas DataFrame and `as_dataframe` is `True`, the labels
                   will be copied in the returned DataFrame.

                   Defaults to True (returning a labeled DataFrame).

    Returns
    -------
    confusion_matrix : numpy array or pandas DataFrame (depending on the value of `as_dataframe`)
    global_f1 : float
                Harmonic mean of the F1 scores
    """
    # Input sanity checks, and store the numpy array version of the data
    if isinstance(data, pd.DataFrame):
        raw_data = data.values
    elif isinstance(data, np.ndarray):
        raw_data = data
    else:
        raise TypeError('The given input is not a numpy array nor a pandas DataFrame')

    if data.shape[0] != data.shape[1]:
        raise ValueError('The input must be a square matrix')

    # Create and fill the confusion matrix
    matrix = np.empty((data.shape[0], data.shape[0] + 7))
    matrix[:, :data.shape[0]] = raw_data
    # Compute confusion matrix values
    for i in range(data.shape[0]):
        # True positives
        matrix[i, data.shape[0]] = raw_data[i, i]
        # False positives
        matrix[i, data.shape[0] + 1] = raw_data[:, i].sum() - raw_data[i, i]
        # False negatives
        matrix[i, data.shape[0] + 3] = raw_data[i, :].sum() - raw_data[i, i]
        # True negatives
        matrix[i, data.shape[0] + 2] = (raw_data.sum() -
                                        matrix[i, data.shape[0]] -
                                        matrix[i, data.shape[0] + 1] -
                                        matrix[i, data.shape[0] + 3])
        # Precision
        matrix[i, data.shape[0] + 4] = matrix[i, data.shape[0]] / (matrix[i, data.shape[0]] +
                                                                   matrix[i, data.shape[0] + 1] +
                                                                   1e-16)
        # Recall
        matrix[i, data.shape[0] + 5] = matrix[i, data.shape[0]] / (matrix[i, data.shape[0]] +
                                                                   matrix[i, data.shape[0] + 3] +
                                                                   + 1e-16)
        # F1 score
        matrix[i, data.shape[0] + 6] = (2 * matrix[i, data.shape[0]] /
                                        (2 * matrix[i, data.shape[0]] +
                                         matrix[i, data.shape[0] + 1] +
                                         matrix[i, data.shape[0] + 3] +
                                         1e-16))

    # Compute the global f1 score
    global_f1 = data.shape[0] / (np.reciprocal(matrix[:, data.shape[0] + 6]).sum() + 1.e-100)

    # Handle the case where the user wants a Dataframe and/or reduced output
    if as_dataframe:
        # Copy/create columns and indices
        if isinstance(data, pd.DataFrame):
            indices = data.index
            columns = list(data.columns)
        elif isinstance(data, np.ndarray):
            indices = list(range(data.shape[0]))
            columns = list(range(data.shape[0]))
        else:
            raise RuntimeError

        # Add additional columns
        extra_columns = ['True positives', 'False positives', 'True negatives',
                         'False negatives', 'Precision', 'Recall', 'F-1']
        matrix = pd.DataFrame(matrix, columns=(columns+extra_columns), index=indices)
        if reduced:
            matrix = matrix[columns + ['Precision', 'Recall', 'F1 score']]
    else:
        if reduced:
            matrix = matrix[:, list(range(data.shape[0])) +
                               [data.shape[0] + 4, data.shape[0] + 5, data.shape[0] + 6]]

    return matrix, global_f1

if __name__ == '__main__':
    data = pd.DataFrame([[5, 3, 0], [2, 3, 1], [0, 2, 11]],
                        index=('Cat', 'Dog', 'Rabbit'),
                        columns=('Predicted cat', 'Predicted dog', 'Predicted rabbit'))
    print(confusion_matrix(data).to_string())
