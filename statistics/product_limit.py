# Copyright 2017 Mathlas Consulting S.L.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import pandas as pd
import collections
import sys


def pl(data, sampling='All', time=None, return_iterations_data=False):
    """
    Computes the survival function based on data using the product limit algorithm

    Parameters
    ----------
    data : Pandas DataFrame
           Experimental survival data. Each individual is stored in a row. The DataFrame must
           contain three columns, namely:

            * `"entry"` float which stands for the date in which the individual is introduced in the
              study (if larger than zero, this means the data is left-truncated)
            * `"exit"` float which stands for the date in which the individual is removed from the
              study (it can be because the individual dies or because the individual is extracted in
              the study and is not longer taken into account, that is, right-censored)
            * `"censored"` boolean which is True if the data is censored and False if the data
              related to a death at date `exit`

    sampling : string, optional
               Defines the sampling mode:

               * If sampling = 'All' (default) every entry and exit time is considered
               * If sampling = 'Deaths' only death times are considered (not implemented yet)
               * If sampling = 'Censored' only censored times are considered (not implemented yet)
               * If sampling = 'Userdefined' time sampling is provided by the user via `time`
                 argument (not implemented yet)

    time : 1D NumPy array, optional
           Defines the time values used to compute the product limit survival function. If sampling
           different from 'All' it must be provided by the user.

    return_iterations_data : boolean, optional
                             If True return auxiliary data used to compute the product limit,
                             namely, :math:`N0(t_j)` which stand for the number of individuals
                             controlled during the interval :math:`[t_j, t_{j+1}]` and
                             :math:`N1(t_j)` which stand for the number of individuals which were
                             controlled during the interval :math:`[t_{j-1}, t_{j}]` and survive at
                             :math:`t_j`.


    Returns
    -------
    pl : Pandas DataFrame
         Survival function computed using the product limit. Each row stand for a point of the
         survival function. The DataFrame contains two columns or more columns depending on
         return_iterations_data value, namely:

         * `"t"` (float) time values.
         * `"pl"` (float) product limit survival function values corresponding to the time values.
         * `"N0"` (integer) only if return_iterations_data == True. Number of individuals
           controlled during the interval :math:`[t_j, t_{j+1}]`.
         * `"N1"` (integer) only if return_iterations_data == True. Number of individuals which
           were controlled during the interval :math:`[t_{j-1}, t_{j}]` and survive at :math:`t_j`

    References
    ----------
    [1] Kaplan, E. L. & Meier, P., Nonparametric Estimation from Incomplete Observations Journal
    of the American Statistical Association, American Statistical Association, 1958, 53, 457-481
    """

    # Check data
    if not isinstance(data, pd.DataFrame):
        raise ValueError("Input argument data must be a Pandas DataFrame")

    if not data.columns.equals(pd.Index(["entry", "exit", "censored"])):
        raise ValueError("Data wrong format. Columns names do not match the required ones")

    for num, column in zip(data.dtypes[0:2], data.columns[0:2]):
        if not num == float:
            # Try to convert to float
            try:
                data[column] = data[column].astype(float)
            except:
                raise ValueError("data {} column must be float and it was unable to"
                                 " convert it.\n".format(column))
            sys.stderr.write("Warning: data {} column converted to float.\n".format(column))

    if not data.dtypes[2] == bool:
        raise ValueError("data 'censored' column must be boolean")

    # Check sampling
    if not sampling == 'All':
        raise ValueError("Sampling option not implemented yet")

    # Remove rows whose entry and exit time are equal (do not carry any information for product
    # limit computation)
    data = data[data["entry"] != data["exit"]]

    # Define array t (sorted in ascending order)
    t = np.unique(np.concatenate((data["exit"].values, data["entry"].values)))

    # Added t=0 if not in t
    if t[0] != 0.0:
        t = np.insert(t, 0, 0.0)

    # Define array of number of entries (number of new individuals at t)
    entries = np.zeros(t.shape[0], dtype=int)
    entry_values = collections.Counter(data["entry"].values)
    for index in range(t.shape[0]):
        entries[index] = int(entry_values[t[index]])

    # Define array of losses (censored individuals at t)
    lost = np.zeros(t.shape[0], dtype=int)
    for index in range(t.shape[0]):
        lost[index] = int(len(data[data["censored"].values][data["exit"] == t[index]]))

    # Define array of deaths (number of deaths at t)
    death = np.zeros(t.shape[0], dtype=int)
    for index in range(t.shape[0]):
        death[index] = int(len(data[np.logical_not(data["censored"].values)][data["exit"]
                                                                             == t[index]]))

    # Define N1 and N0 arrays
    # N0 : number of individuals which were controlled in the current interval
    # N1 : number of individuals which while controlled in the previous interval survive at t
    N1 = np.zeros(t.shape[0], dtype=int)
    N0 = np.zeros(t.shape[0], dtype=int)
    N0[0] = entries[0]
    for index in range(1, t.shape[0]):
        N1[index] = N0[index - 1] - death[index]
        N0[index] = entries[index] + N1[index] - lost[index]

    # Compute product limit
    pl = np.ones(t.shape[0], dtype=float)
    for index in range(1, t.shape[0]):
        pl[index] = pl[index - 1] * N1[index] / N0[index - 1]

    result = np.zeros((t.shape[0], 2), dtype=float)
    result[:, 0] = t
    result[:, 1] = pl
    result = pd.DataFrame(result, columns=["t", "pl"])

    if return_iterations_data:
        result['N0'] = pd.Series(N0, index=result.index)
        result['N1'] = pd.Series(N1, index=result.index)

    return result

if __name__ == "__main__":
    pass


