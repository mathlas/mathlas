# Copyright 2017 Joseba Echevarria García
# DO NOT DISTRIBUTE THIS FILE

import sys


class HereBeDragonsException(RuntimeError):
    def __init__(self, message):
        # Dragon created by running `cowsay -f dragon "Mathlas"`
        l = len(message) + 2
        dragon_bubble = '         {}\n        < {} >\n         {}'.format('_' * l, message, '-' * l)

        dragon = dragon_bubble + r''' 
                       \                    / \  //\
                        \    |\___/|      /   \//  \\
                             /0  0  \__  /    //  | \ \    
                            /     /  \/_/    //   |  \  \  
                            @_^_@'/   \/_   //    |   \   \ 
                            //_^_/     \/_ //     |    \    \
                         ( //) |        \///      |     \     \
                       ( / /) _|_ /   )  //       |      \     _\
                     ( // /) '/,_ _ _/  ( ; -.    |    _ _\.-~        .-~~~^-.
                   (( / / )) ,-{        _      `-.|.-~-.           .~         `.
                  (( // / ))  '/\      /                 ~-. _ .-~      .-~^-.  \
                  (( /// ))      `.   {            }                   /      \  \
                   (( / ))     .----~-.\        \-'                 .~         \  `. \^-.
                              ///.----..>        \             _ -~             `.  ^-`  ^-_
                                ///-._ _ _ _ _ _ _}^ - - - - ~                     ~-- ,.-~
                                                                                   /.-~'''
        sys.stderr.write('{}\n'.format(dragon))
        sys.stderr.flush()
        super().__init__(message)
