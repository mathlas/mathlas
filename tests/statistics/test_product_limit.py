# Copyright 2017 Mathlas Consulting S.L.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
import numpy as np
import pandas as pd
import pandas.util.testing as pdt
from mathlas.statistics.product_limit import pl


class TestProductLimit(unittest.TestCase):

    guilbaud_data = [[0., 6., False],
                     [0., 6., False],
                     [0., 6., False],
                     [0., 6., True],
                     [0., 7., False],
                     [0., 9., True],
                     [0., 10., True],
                     [0., 10., False],
                     [0., 11., True],
                     [0., 13., False],
                     [0., 16., False],
                     [0., 17., True],
                     [0., 19., True],
                     [0., 20., True],
                     [0., 22., False],
                     [0., 23., False],
                     [0., 25., True],
                     [0., 32., True],
                     [0., 32., True],
                     [0., 34., True],
                     [0., 35., True]]

    guilbaud_result = [[0., 1., 21, 0],
                       [6., 0.857142857, 17, 18],
                       [7., 0.806722689, 16, 16],
                       [9., 0.806722689, 15, 16],
                       [10., 0.752941176, 13, 14],
                       [11., 0.752941176, 12, 13],
                       [13., 0.690196078, 11, 11],
                       [16., 0.62745098, 10, 10],
                       [17., 0.62745098, 9, 10],
                       [19., 0.62745098, 8, 9],
                       [20., 0.62745098, 7, 8],
                       [22., 0.537815126, 6, 6],
                       [23., 0.448179272, 5, 5],
                       [25., 0.448179272, 4, 5],
                       [32., 0.448179272, 2, 4],
                       [34., 0.448179272, 1, 2],
                       [35., 0.448179272, 0, 1]]
    guilbaud_result = np.array(guilbaud_result)

    guilbaud_truncated_data = [[0., 6., False],
                               [0., 6., False],
                               [0., 6., False],
                               [6., 6., True],
                               [3., 7., False],
                               [3., 9., True],
                               [0., 10., True],
                               [6., 10., False],
                               [3., 11., True],
                               [3., 13., False],
                               [6., 16., False],
                               [6., 17., True],
                               [0., 19., True],
                               [0., 20., True],
                               [3., 22., False],
                               [3., 23., False],
                               [6., 25., True],
                               [6., 32., True],
                               [0., 32., True],
                               [3., 34., True],
                               [6., 35., True]]

    guilbaud_truncated_result = [[0., 1.000000000, 7, 0],
                                 [3., 1.000000000, 14, 7],
                                 [6., 0.785714286, 17, 11],
                                 [7., 0.739495798, 16, 16],
                                 [9., 0.739495798, 15, 16],
                                 [10., 0.690196078, 13, 14],
                                 [11., 0.690196078, 12, 13],
                                 [13., 0.632679739, 11, 11],
                                 [16., 0.575163399, 10, 10],
                                 [17., 0.575163399, 9, 10],
                                 [19., 0.575163399, 8, 9],
                                 [20., 0.575163399, 7, 8],
                                 [22., 0.492997199, 6, 6],
                                 [23., 0.410830999, 5, 5],
                                 [25., 0.410830999, 4, 5],
                                 [32., 0.410830999, 2, 4],
                                 [34., 0.410830999, 1, 2],
                                 [35., 0.410830999, 0, 1]]
    guilbaud_truncated_result = np.array(guilbaud_truncated_result)

    def testProductLimitAllGuilbaud(self):
        """
        Test the product limit algorithm with numerical example in Guilbaud's paper [1] using
        sampling option "All"


        References
        ----------
        [1] Guilbaud, O., Exact Kolmogorov-type tests for left-truncated and/or right-censored data
        Journal of the American Statistical Association, 1988, 83, 213-221.
        """

        data = pd.DataFrame(self.guilbaud_data, columns=["entry", "exit", "censored"])
        result = pd.DataFrame(self.guilbaud_result[:, 0:2], columns=["t", "pl"], dtype=float)

        surv = pl(data, sampling='All')
        pdt.assert_frame_equal(surv, result)

    def testProductLimitAllGuilbaudReturnTrue(self):
        """
        Test the product limit algorithm with numerical example in Guilbaud's paper [1] using
        sampling option "All" and return_iterations_data True


        References
        ----------
        [1] Guilbaud, O., Exact Kolmogorov-type tests for left-truncated and/or right-censored data
        Journal of the American Statistical Association, 1988, 83, 213-221.
        """

        data = pd.DataFrame(self.guilbaud_data, columns=["entry", "exit", "censored"])
        result = pd.DataFrame(self.guilbaud_result[:, 0:2], columns=["t", "pl"], dtype=float)
        result['N0'] = pd.Series(self.guilbaud_result[:, 2], index=result.index).astype(int)
        result['N1'] = pd.Series(self.guilbaud_result[:, 3], index=result.index).astype(int)

        surv = pl(data, sampling='All', return_iterations_data=True)
        pdt.assert_frame_equal(surv, result)

    def testProductLimitAllGuilbaudTruncated(self):
        """
        Test the product limit algorithm with a numerical example derived from Guilbaud's paper [1]
        including truncated data using sampling option "All"


        References
        ----------
        [1] Guilbaud, O., Exact Kolmogorov-type tests for left-truncated and/or right-censored data
        Journal of the American Statistical Association, 1988, 83, 213-221.
        """

        data = pd.DataFrame(self.guilbaud_truncated_data, columns=["entry", "exit", "censored"])
        result = pd.DataFrame(self.guilbaud_truncated_result[:, 0:2], columns=["t", "pl"],
                              dtype=float)

        surv = pl(data, sampling='All')
        pdt.assert_frame_equal(surv, result)

    def testProductLimitAllGuilbaudTruncatedReturnTrue(self):
        """
        Test the product limit algorithm with a numerical example derived from Guilbaud's paper [1]
        including truncated data using sampling option "All" and return_iterations_data True


        References
        ----------
        [1] Guilbaud, O., Exact Kolmogorov-type tests for left-truncated and/or right-censored data
        Journal of the American Statistical Association, 1988, 83, 213-221.
        """

        data = pd.DataFrame(self.guilbaud_truncated_data, columns=["entry", "exit", "censored"])
        result = pd.DataFrame(self.guilbaud_truncated_result[:, 0:2], columns=["t", "pl"],
                              dtype=float)
        result['N0'] = pd.Series(self.guilbaud_truncated_result[:, 2],
                                 index=result.index).astype(int)
        result['N1'] = pd.Series(self.guilbaud_truncated_result[:, 3],
                                 index=result.index).astype(int)

        surv = pl(data, sampling='All', return_iterations_data=True)
        pdt.assert_frame_equal(surv, result)

if __name__ == "__main__":
    unittest.main()
