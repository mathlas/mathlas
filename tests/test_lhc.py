# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
from mathlas.doe.lhc import LHC


class TestLHC(unittest.TestCase):

    def testLHCWithFixedPoints(self):
        """
        Create a Latin Hypercubes distribution in 4D with 3 fixed points
        """
        fixed_points = [[0.5, 0.5, 0.37, 0.15],
                        [0.6, 0.3, 0.92, 0.],
                        [0.1, 0.8, 0.501, 0.6]]
        Nf = len(fixed_points)
        N = 20
        D = 4
        points = LHC(N, D, fixed_points, nIters=100)
        self.assertEqual(points.shape[0], N+Nf)
        self.assertEqual(points.shape[1], D)
        # Test the uniqueness of each of the coordinates
        for col in range(D):
            coords = list(points[:, col])
            uniqueCoords = list(set(coords))
            self.assertEqual(sorted(coords), sorted(uniqueCoords))

    def testLHC(self):
        """
        Create a Latin Hypercubes distribution in 16D with 100 points
        """
        N = 25
        D = 10
        points = LHC(N, D, nIters=100)
        self.assertEqual(points.shape[0], N)
        self.assertEqual(points.shape[1], D)
        for col in range(D):
            coords = list(points[:, col])
            # Test that the output coordinates are correctly normalized
            self.assertEqual(max(coords), 1.0)
            self.assertEqual(min(coords), 0.0)
            # Test for the uniqueness of each of the coordinates
            uniqueCoords = list(set(coords))
            self.assertEqual(sorted(coords), sorted(uniqueCoords))

if __name__ == "__main__":
    unittest.main()
