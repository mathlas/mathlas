# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
import numpy as np
from mathlas.analytical.branin import BraninFunction2D
from mathlas.optimization.optimizers import hooke_n_jeeves


def f(x):
    return BraninFunction2D(x[0], x[1])


def g(x):
    if (-5. <= x[0] <= 10.) and (0. <= x[1] <= 15.):
        return BraninFunction2D(x[0], x[1])
    else:
        return 0.0


def h(x):
    return -f(x)


class TestHookeNJeeves(unittest.TestCase):

    def testHNJMinBoundariesInitialpoint(self):
        """
        Minimize Branin funtion given boundaries and a unique initial point
        """
        # x0 given as a one dimensional array
        x0 = np.array([2.5, 0.0])
        bounds = np.array([[-5., 0.], [10., 15.]])
        x, fopt = hooke_n_jeeves(f, x0, optimization='min', bounds=bounds, fitness=0.1)
        solution = np.array([3.0625, 2.25])
        np.testing.assert_array_equal(x, solution)
        fsolution = np.array([0.43555979])
        np.testing.assert_array_almost_equal(fopt, fsolution)
        # x0 given as a two dimensional array
        x0 = np.reshape(x0, newshape=(1, 2))
        x, fopt = hooke_n_jeeves(f, x0, optimization='min', bounds=bounds, fitness=0.1)
        solution = np.reshape(solution, newshape=(1, 2))
        np.testing.assert_array_equal(x, solution)
        np.testing.assert_array_almost_equal(fopt, fsolution)

    def testHNJMinBoundariesInitialpoints(self):
        """
        Minimze Branin funtion given boundaries and two initial points
        """
        x0 = np.array([[2.5, 0.0], [-3.0, 14.0]])
        bounds = np.array([[-5., 0.], [10., 15.]])
        x, fopt = hooke_n_jeeves(f, x0, optimization='min', bounds=bounds, fitness=0.1)
        solution = np.array([[3.0625, 2.25], [-3., 11.9375]])
        fsolution = np.array([0.43555979, 0.493980569])
        np.testing.assert_array_equal(x, solution)
        np.testing.assert_array_almost_equal(fopt, fsolution)

    def testHNJMinWithoutBoundaries(self):
        """
        Minimize function with and without boundaries.

        The function is equal to Branin Function inside the domain [-5, 10] X [0, 15] and equal
        to 0 outside.
        """
        x0 = np.array([2.5, 0.0])
        bounds = np.array([[-5., 0.], [10., 15.]])

        # Minimization with boundaries
        x, fopt = hooke_n_jeeves(g, x0, optimization='min', bounds=bounds, fitness=0.1)
        solution = np.array([3.0625, 2.25])
        np.testing.assert_array_equal(x, solution)

        # Minimization without boundaries
        x, fopt = hooke_n_jeeves(g, x0, optimization='min', fitness=0.1)
        solution = np.array([2.5, -0.1])
        fsolution = np.array([0.0])
        np.testing.assert_array_equal(x, solution)
        np.testing.assert_array_almost_equal(fopt, fsolution)

    def testHNJMaxBoundariesInitialpoints(self):
        """
        Minimze Branin funtion given boundaries and two initial points
        """
        x0 = np.array([[2.5, 0.0], [-3.0, 14.0]])
        bounds = np.array([[-5., 0.], [10., 15.]])
        x, fopt = hooke_n_jeeves(h, x0, bounds=bounds, fitness=0.1)
        solution = np.array([[3.0625, 2.25], [-3., 11.9375]])
        fsolution = np.array([-0.43555979, -0.493980569])
        np.testing.assert_array_equal(x, solution)

    def testHNJMinBoundariesInitialpointSpanlist(self):
        """
        Minimize Branin function given boundaries and different span values for each coordinate
        """
        x0 = np.array([2.5, 0.0])
        bounds = np.array([[-5., 0.], [5., 10.]])
        span = np.array([0.1, 0.2])
        x, fopt = hooke_n_jeeves(f, x0, optimization='min', bounds=bounds, fitness=0.1, span=span)
        solution = np.array([3.125, 2.25])
        fsolution = np.array([0.400651])
        np.testing.assert_array_equal(x, solution)
        np.testing.assert_array_almost_equal(fopt, fsolution)

    def testHNJMinBoundariesInitialpointFitness(self):
        """
        Same as `testHNJMinBoundariesInitialpoint` defining a higher fitness
        """
        # x0 given as a one dimensional array
        x0 = np.array([2.5, 0.0])
        bounds = np.array([[-5., 0.], [10., 15.]])
        x, fopt = hooke_n_jeeves(f, x0, optimization='min', bounds=bounds, fitness=0.2)
        solution = np.array([3.25, 2.25])
        np.testing.assert_array_equal(x, solution)
        fsolution = np.array([0.4576217])
        np.testing.assert_array_almost_equal(fopt, fsolution)

    def testHNJMinBoundariesInitialpointsRemoveduplicatesfalse(self):
        """
        Minimze Branin funtion given boundaries and two initial points
        """
        x0 = np.array([[-3.00123, 14.012], [2.5, 0.0], [-3.0, 14.0]])
        bounds = np.array([[-5., 0.], [10., 15.]])
        x, fopt = hooke_n_jeeves(f, x0, optimization='min', bounds=bounds, fitness=0.1,
                                 remove_duplicates=False)
        solution = np.array([[-3.00123, 11.9495], [3.0625, 2.25], [-3., 11.9375]])
        fsolution = np.array([0.492407, 0.43555979, 0.493980569])
        np.testing.assert_array_equal(x, solution)
        np.testing.assert_array_almost_equal(fopt, fsolution)


    def testHNJMinBoundariesInitialpointsRemoveduplicatestrue(self):
        """
        Minimze Branin funtion given boundaries and two initial points
        """
        x0 = np.array([[-3.00123, 14.012], [2.5, 0.0], [-3.0, 14.0]])
        bounds = np.array([[-5., 0.], [10., 15.]])
        x, fopt = hooke_n_jeeves(f, x0, optimization='min', bounds=bounds, fitness=0.1,
                                 remove_duplicates=True)
        solution = np.array([[3.0625, 2.25], [-3.00123, 11.9495]])
        fsolution = np.array([0.43555979, 0.492407])
        np.testing.assert_array_equal(x, solution)
        np.testing.assert_array_almost_equal(fopt, fsolution)

if __name__ == "__main__":
    unittest.main()