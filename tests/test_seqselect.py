# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
import pandas as pd
import numpy as np
from mathlas.doe.seq_select import seq_select


class TestSeq_select(unittest.TestCase):

    def testSeqSelect3points(self):
        """
        Sequentially selects 3 points from the dataframe
        """
        values = np.array([[1., 1.], [-1., 2.], [-2., 2.], [0., 3.],
                           [4., 2.], [3., 0.5], [-2., 0.], [3., -2.],
                           [0., -3.], [-3., -3.]])
        indexes = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'P9', 'P10']
        columns = ['X1', 'X2']
        setofpoints = pd.DataFrame(values, index=indexes, columns=columns)
        solution = setofpoints.loc[['P3', 'P1', 'P10', 'P8', 'P5', 'P9']]
        selected_points = seq_select(setofpoints, 6, ['P3', 'P1'])
        check = (solution == selected_points).all().all()
        self.assertTrue(check)

if __name__ == "__main__":
    unittest.main()
