# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Define some color and font styles for use in ANSI terminals
Some codes takes from:
https://en.wikipedia.org/wiki/ANSI_escape_code#Colors

NOTE: Not all colors work with all terminals.
      PyCharm has an odd default palette and it does not support
      strikethrough or italics text.
"""

# Formatting
END = '\033[0m'
BOLD = '\033[1m'
ITALICS = '\033[3m'
UNDERLINE = '\033[4m'
INVERSE = '\033[7m'
STRIKETHROUGH = '\033[9m'
ENDBOLD = '\033[22m'
ENDITALICS = '\033[23m'
ENDUNDERLINE = '\033[24m'
ENDINVERSE = '\033[27m'
ENDSTRIKETHROUGH = '\033[29m'
# Colors
BLACK = '\033[30m'
RED = '\033[31m'
LIGHT_RED = '\033[91m'
GREEN = '\033[32m'
LIGHT_GREEN = '\033[92m'
YELLOW = '\033[33m'
LIGHT_YELLOW = '\033[93m'
BLUE = '\033[34m'
LIGHT_BLUE = '\033[94m'
PURPLE = '\033[35m'
LIGHT_PURPLE = '\033[95m'
CYAN = '\033[36m'
LIGHT_CYAN = '\033[96m'
WHITE = '\033[37m'
DEFAULT = '\033[39m'
# Background color
BBLACK = '\033[40m'
BRED = '\033[41m'
BGREEN = '\033[42m'
BYELLOW = '\033[43m'
BBLUE = '\033[44m'
BPURPLE = '\033[45m'
BCYAN = '\033[46m'
BWHITE = '\033[47m'
BDEFAULT = '\033[49m'


if __name__ == '__main__':
    import sys

    formats = {'Bold': BOLD, 'Italics': ITALICS,
               'Underline': UNDERLINE,
               'Strikethrough': STRIKETHROUGH}
    backgrounds = {'Black background': BBLACK, 'Red background': BRED,
                   'Green background': BGREEN, 'Yellow background': BYELLOW,
                   'Blue background': BBLUE, 'Purple background': BPURPLE,
                   'Cyan background': BCYAN, 'White background': BWHITE,
                   'Default background': BDEFAULT}
    colors = {'Black': BLACK,
              'Red': RED, 'Light red': LIGHT_RED,
              'Green': GREEN, 'Light Green': LIGHT_GREEN,
              'Yellow': YELLOW, 'Light yellow': LIGHT_YELLOW,
              'Blue': BLUE, 'Light blue': LIGHT_BLUE,
              'Purple': PURPLE, 'Light purple': LIGHT_PURPLE,
              'Cyan': CYAN, 'Light cyan': LIGHT_CYAN,
              'White': WHITE, 'Default': DEFAULT}

    for f in sorted(formats.keys()):
        for b in sorted(backgrounds.keys()):
            for c in sorted(colors.keys()):
                sys.stdout.write(backgrounds[b] + formats[f] + colors[c] +
                                 '{} - {} - {}'.format(f, b, c) + END +
                                 '\t{} - {} - {}\n'.format(f, b, c))

