# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This module implements some methods for displaying information
to the user with graphical modal dialogs.

They rely on Qt for the notifications but in case Qt can not be found,
the notifications will be printed to sys.stdout/sys.stderr, with colours.
"""

# Try to import PyQt5/PyQt4
try:
    from PyQt5.QtWidgets import QApplication, QMessageBox
    use_qt = True
except ImportError:
    try:
        from PyQt4.QtGui import QApplication, QMessageBox
        use_qt = True
    except ImportError:
        import sys
        from mathlas.misc.ansi_color import RED, YELLOW, BOLD, END
        use_qt = False


def critical(title, msg, create_qapp=True):
    """
    This method tries to display a graphical critical message box
    
    In case that PyQt is not installed, the message will be printed to
    sys.stderr.
    
    This function will not exit your program; you're responsible for that.
    
    This function will, by default, create a new (dummy) QApplication object
    if you have already created one, you can set create_qapp to False and it
    won't be created.
    
    Parameters
    ----------
    title : string
            The title of the message
    msg : string
          The message to display
    create_qapp : boolean, optional
                  Whether a new QApplication should be created.
                  Set it to False in case you already have a 
                  running Qt application.
    """
    if use_qt:
        if create_qapp:
            app = QApplication([])
        QMessageBox.critical(None, title, msg)
        if create_qapp:
            del app
    else:
        sys.stderr.write(RED + BOLD +
                         'ERROR: {}\n'.format(title) + END +
                         '\t{}\n'.format(msg))


def warning(title, msg, create_qapp=True):
    """
    This method tries to display a graphical warning message box

    In case that PyQt is not installed, the message will be printed to
    sys.stderr.

    This function will not exit your program; you're responsible for that.

    This function will, by default, create a new (dummy) QApplication object
    if you have already created one, you can set create_qapp to False and it
    won't be created.

    Parameters
    ----------
    title : string
            The title of the message
    msg : string
          The message to display
    create_qapp : boolean, optional
                  Whether a new QApplication should be created.
                  Set it to False in case you already have a 
                  running Qt application.
    """
    if use_qt:
        if create_qapp:
            app = QApplication([])
        QMessageBox.warning(None, title, msg)
        if create_qapp:
            del app
    else:
        sys.stdout.write(YELLOW + BOLD +
                         'WARNING: {}\n'.format(title) + END +
                         '\t{}\n'.format(msg))


def information(title, msg, create_qapp=True):
    """
    This method tries to display a graphical information message box

    In case that PyQt is not installed, the message will be printed to
    sys.stderr.

    This function will not exit your program; you're responsible for that.

    This function will, by default, create a new (dummy) QApplication object
    if you have already created one, you can set create_qapp to False and it
    won't be created.

    Parameters
    ----------
    title : string
            The title of the message
    msg : string
          The message to display
    create_qapp : boolean, optional
                  Whether a new QApplication should be created.
                  Set it to False in case you already have a 
                  running Qt application.
    """
    if use_qt:
        if create_qapp:
            app = QApplication([])
        QMessageBox.information(None, title, msg)
        if create_qapp:
            del app
    else:
        sys.stderr.write(BOLD + 'NOTICE: {}\n'.format(title) + END +
                         '\t{}\n'.format(msg))

if __name__ == '__main__':
    critical('Critical error title',
             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non purus vitae elit commodo '
             'mollis et eget urna. Nulla egestas consequat ullamcorper. Vivamus eu commodo metus. Suspendisse '
             'congue lacus sit amet nisl laoreet, a aliquam magna pharetra. Nam vitae rhoncus arcu, at commodo '
             'nisi. Fusce at malesuada ipsum. Vestibulum scelerisque nisi nunc, et pellentesque neque interdum '
             'dictum. Ut pulvinar urna orci, id varius dolor vehicula vel.')
    warning('Warning title',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non purus vitae elit commodo '
            'mollis et eget urna. Nulla egestas consequat ullamcorper. Vivamus eu commodo metus. Suspendisse '
            'congue lacus sit amet nisl laoreet, a aliquam magna pharetra. Nam vitae rhoncus arcu, at commodo '
            'nisi. Fusce at malesuada ipsum. Vestibulum scelerisque nisi nunc, et pellentesque neque interdum '
            'dictum. Ut pulvinar urna orci, id varius dolor vehicula vel.')
    information('Information title',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non purus vitae elit commodo '
                'mollis et eget urna. Nulla egestas consequat ullamcorper. Vivamus eu commodo metus. Suspendisse '
                'congue lacus sit amet nisl laoreet, a aliquam magna pharetra. Nam vitae rhoncus arcu, at commodo '
                'nisi. Fusce at malesuada ipsum. Vestibulum scelerisque nisi nunc, et pellentesque neque interdum '
                'dictum. Ut pulvinar urna orci, id varius dolor vehicula vel.')
