# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys


class ProgressBar:
    """Create a command line progress bar of the given total width"""
    def __init__(self, width=80, total_steps=None, label=""):
        """
        Create a command line progress bar

        Parameters
        ----------
        width: total width of the progress bar
        total_steps: If given, the total number of steps the
                     bar can have.
        """
        # Given width is the total width that will be used
        self.width = width-10
        self.progress = 0
        self.percent = 0
        self.total_steps = total_steps
        self.label = label
        self.current_step = 0
        sys.stdout.write('\r{} |{}| ({}%)'.format(self.label, ' '*self.width, 0))
        sys.stdout.flush()

    def set_percent(self, percent):
        """Update the progress bar value to the terminal to the
           given percent value"""

        old_progress = self.progress
        self.progress = min(int(self.width*percent/100.), self.width)
        if self.progress != old_progress or self.percent != percent:
            self.percent = percent
            sys.stdout.write('\r{} |{}{}| ({}%)'.format(self.label,
                                                        '='*self.progress,
                                                        ' '*(self.width - self.progress),
                                                        int(percent)))
            sys.stdout.flush()

    def step(self):
        """
        Advance one step.
        `total_steps` must be supplied when creating the progress bar
        for this to work.
        """
        if self.total_steps is None:
            raise RuntimeError('This ProgressBar does not have a valid value for total_steps')

        self.current_step += 1
        self.set_percent(100. * self.current_step / self.total_steps)

    def stop(self):
        """Print a newline and flush stdout, probably not needed..."""
        self.progress = 0
        self.percent = 0
        sys.stdout.write('\n')
        sys.stdout.flush()

    def delete(self):
        """
        Remove the progres bar by writing blanks all over it and
        resetting the cursor to the first column of the terminal
        """
        self.progress = 0
        self.percent = 0
        sys.stdout.write('\r{}\r'.format(' '*(self.width+len(self.label)+11)))
        sys.stdout.flush()

if __name__ == '__main__':
    import time
    p = ProgressBar(80)
    for i in range(100):
        p.set_percent(i + 1)
        time.sleep(0.01)

    p.stop()