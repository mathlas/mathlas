# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
from time import time


def measure_time(func):
    """
    Simply returns the time a function has been running. It has been designed to be used as a
     decorator.

    Parameters
    ----------
    func : function
           whatever

    Returns
    -------
    out : outputs of the function

    """
    def my_decorator(*args, **kwargs):
        t = time()
        ret = func(*args, **kwargs)
        t -= time()
        sys.stdout.write('{0} function took {1:.3f} seconds\n'.format(func.__name__, -t))
        return ret

    return my_decorator
